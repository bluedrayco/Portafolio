#include "biocheckLib.c"

int createFile(char *fileName, void *data, unsigned int typeDataLength, unsigned int dataLength);

int createDirectory(char *dir);

int directoryExists(char* path );

int saveFMD(char *path,char *filedir,unsigned char* pEnrollmentFmd,unsigned int nEnrollmentFmdSize);

void signal_handler(int nSignal);

DPFPDD_DEV selectAndOpenFP(void);

int captureFP(DPFPDD_DEV hReader, unsigned char** ppFt, unsigned int* pFtSize);

int enrollment(DPFPDD_DEV hReader,char *path,char *user);

int getNumeroUsuarios(char *path);

char ** getUsersName(char *path);

unsigned int getSizeFMD(char *path,char * user_name);

unsigned int* getSizeFMDS(char *path, char** users_name,int numUsuarios);

unsigned char* getFMD(char *path,char *user_name,unsigned int FMDLength);

unsigned char** getFMDS(char* path,char** users_name,unsigned int* VFMDSize,int numUsers);

FMDS_STRUCTURE extractUsers(char *path);

char* identifyFP(DPFPDD_DEV hReader,unsigned char* FMD_tmp,unsigned int Size_tmp,FMDS_STRUCTURE usuarios);
