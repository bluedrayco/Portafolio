#include <dirent.h>
/////////////mkdir
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
/////////////
#include <dpfj.h>
#include <dpfj_compression.h>
#include <dpfj_quality.h>
#include <dpfpdd.h>

#define CONFIG_FILE_NAME "config.tec"
#define CONTENT_FILE_NAME "content.tec"

typedef struct configFP{
	int num_bytes;
}CONFIG_FP;

typedef struct FMDsStructure{
	unsigned int *VFMDSize;
	unsigned char **VFMDS;
	char **usersName;
	int numUsers;
}FMDS_STRUCTURE;

////////////////////////tratamiento de almacenamiento////////////////////////////

//Creates a file to save fingers and configurations
int createFile(char *fileName, void *data, unsigned int typeDataLength, unsigned int dataLength){
	FILE *fichero;
	fichero = fopen( fileName, "w+b" );
	
	if( !fichero ){
		//Error (NO ABIERTO)
		return 0;
	}
	fwrite(data, typeDataLength, dataLength, fichero );
	if( fclose(fichero) ){
		//Error: fichero NO CERRADO
		return 0;
	}
	return 1;	
}

//creates a directory to save the data fingers
int createDirectory(char *dir){
	return mkdir(dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)==0?1:0;
}


//check if the directory exists
int directoryExists(char* path ){
	struct stat st;
	if(stat(path,&st) == 0){
    		if((st.st_mode & S_IFDIR) != 0){
        		return 1;
		}
	}
	return 0;
}

//save the FMD from a finger to a file 
int saveFMD(char *path,char *filedir,unsigned char* pEnrollmentFmd,unsigned int nEnrollmentFmdSize){
	CONFIG_FP *config_file=malloc(sizeof(CONFIG_FP));
	config_file->num_bytes=nEnrollmentFmdSize;
	char *pathConfigFile=(char *)malloc((sizeof(char)*strlen(path))+(sizeof(char)*strlen(filedir))+(sizeof(char)*15));
	char *pathFile=(char *)malloc((sizeof(char)*strlen(path))+(sizeof(char)*strlen(filedir))+(sizeof(char)*15));
	if(NULL == path){
		//error con malloc()\n"
		return 0;
	}
	
	strcpy(pathFile,path);
	strcat(pathFile,"/");
	strcat(pathFile,filedir);
	if(!directoryExists(pathFile)){
		if(!createDirectory(pathFile)){
			return 0;
		}
	}
	strcat(pathFile,"/");
	strcpy(pathConfigFile,pathFile);
	strcat(pathConfigFile,CONFIG_FILE_NAME);
	strcat(pathFile,CONTENT_FILE_NAME);
	if(!createFile(pathConfigFile,config_file,sizeof(CONFIG_FP),1)){
		free(pathFile);
		free(pathConfigFile);
		return 0;
	}
	
	if(!createFile(pathFile,pEnrollmentFmd,sizeof(unsigned char),nEnrollmentFmdSize)){
		free(pathFile);
		free(pathConfigFile);
		return 0;
	}	
	
	free(pathFile);
	free(pathConfigFile);
	return 1;
}
////////////////////////tratamiento de almacenamiento////////////////////////////
///////////////////////////propios del dispositivo///////////////////////////////

//device handler
DPFPDD_DEV g_hReader = NULL;
//flag for cancel the current operation of the device
int        g_bCancel = 0;

//check the device's state
void signal_handler(int nSignal) {
	if(SIGINT == nSignal){
		g_bCancel = 1;
		//cancel capture
		if(NULL != g_hReader) dpfpdd_cancel(g_hReader);
	}
}

//select the first connected device to use it
DPFPDD_DEV selectAndOpenFP(void){
	DPFPDD_DEV hReader = NULL;
	int bStop  = 0;
	int result = 0;
	while(!bStop){
		//enumerate the readers
		unsigned int nReaderCnt = 1;
		DPFPDD_DEV_INFO* pReaderInfo = (DPFPDD_DEV_INFO*)malloc(sizeof(DPFPDD_DEV_INFO) * nReaderCnt);
		while(NULL != pReaderInfo){
			unsigned int i = 0;
			for(i = 0; i < nReaderCnt; i++){
				pReaderInfo[i].size = sizeof(DPFPDD_DEV_INFO);
			}
			
			unsigned int nNewReaderCnt = nReaderCnt;
			result = dpfpdd_query_devices(&nNewReaderCnt, pReaderInfo);
			
			//quit if error
			if(DPFPDD_SUCCESS != result && DPFPDD_E_MORE_DATA != result){
				free(pReaderInfo);
				pReaderInfo = NULL;
				nReaderCnt = 0;
				break;
			}
			
			//allocate memory if needed and do over
			if(DPFPDD_E_MORE_DATA == result){
				DPFPDD_DEV_INFO* pri = (DPFPDD_DEV_INFO*)realloc(pReaderInfo, sizeof(DPFPDD_DEV_INFO) * nNewReaderCnt);
				if(NULL == pri){
					break;
				}
				pReaderInfo = pri;
				nReaderCnt = nNewReaderCnt;
				continue;
			}
			
			//success
			nReaderCnt = nNewReaderCnt;
			break;
		}
		if(nReaderCnt > 0){
		
			//open reader
			result = dpfpdd_open(pReaderInfo[0].name, &hReader);
			if(DPFPDD_SUCCESS == result){
				//read capabilities
				unsigned int nCapsSize = sizeof(DPFPDD_DEV_CAPS);
				while(1){
					DPFPDD_DEV_CAPS* pCaps = (DPFPDD_DEV_CAPS*)malloc(nCapsSize);
					if(NULL == pCaps){
						break;
					}
					pCaps->size = nCapsSize;
					result = dpfpdd_get_device_capabilities(hReader, pCaps);
				
					if(DPFPDD_SUCCESS != result && DPFPDD_E_MORE_DATA != result){
						free(pCaps);
						break;
					}
					if(DPFPDD_E_MORE_DATA == result){
						nCapsSize = pCaps->size;
						free(pCaps);
						continue;
					}
					//capabilities acquired, print them out
					free(pCaps);
					break;
				}
			}
			bStop = 1;
		}
		
		//release memory
		if(NULL != pReaderInfo) free(pReaderInfo);
		pReaderInfo = NULL;
		nReaderCnt = 0;
	}
	return hReader;
}

//function that initialize the device for read a finger
int captureFP(DPFPDD_DEV hReader, unsigned char** ppFt, unsigned int* pFtSize){
	int result = 0;
	*ppFt = NULL;
	*pFtSize = 0;
	//prepare capture parameters and result
	DPFPDD_CAPTURE_PARAM cparam = {0};
	cparam.size = sizeof(cparam);
	cparam.image_fmt = DPFPDD_IMG_FMT_ISOIEC19794;
	cparam.image_proc = DPFPDD_IMG_PROC_NONE;
	cparam.image_res = 500;
	DPFPDD_CAPTURE_RESULT cresult = {0};
	cresult.size = sizeof(cresult);
	cresult.info.size = sizeof(cresult.info);
	//get size of the image
	unsigned int nImageSize = 0;
	result = dpfpdd_capture(hReader, &cparam, 0, &cresult, &nImageSize, NULL);
	if(DPFPDD_E_MORE_DATA != result){
		//dpfpdd_capture() %d", result
		return result;
	}
	unsigned char* pImage = (unsigned char*)malloc(nImageSize);
	if(NULL == pImage){
		//malloc() %d", ENOMEM
		return ENOMEM;
	}
	//set signal handler
	g_hReader = hReader;
	struct sigaction new_action, old_action;
	new_action.sa_handler = &signal_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction(SIGINT, &new_action, &old_action);
	//unblock SIGINT (Ctrl-C)
	sigset_t new_sigmask, old_sigmask;
	sigemptyset(&new_sigmask);
	sigaddset(&new_sigmask, SIGINT);
	pthread_sigmask(SIG_UNBLOCK, &new_sigmask, &old_sigmask);
	while(1){
		//wait until ready
		int is_ready = 0;
		while(1){
			DPFPDD_DEV_STATUS ds;
			ds.size = sizeof(DPFPDD_DEV_STATUS);
			result = dpfpdd_get_device_status(hReader, &ds);
			if(DPFPDD_SUCCESS != result){
				//dpfpdd_get_device_status() %d", result
				break;
			}
			
			if(DPFPDD_STATUS_FAILURE == ds.status){
				//Lectura fallida: %d", DPFPDD_STATUS_FAILURE
				break;
			}
			if(DPFPDD_STATUS_READY == ds.status || DPFPDD_STATUS_NEED_CALIBRATION == ds.status){
				is_ready = 1;
				break;
			}
		}
		if(!is_ready) break;
		//capture fingerprint
		result = dpfpdd_capture(hReader, &cparam, -1, &cresult, &nImageSize, pImage);
		if(DPFPDD_SUCCESS != result){
			//dpfpdd_capture() %d", result
		}
		else{
			if(cresult.success){
				//captured
				//get max size for the feature template
				unsigned int nFeaturesSize = MAX_FMD_SIZE;
				unsigned char* pFeatures = (unsigned char*)malloc(nFeaturesSize);
				if(NULL == pFeatures){
					//malloc() %d", ENOMEM
					result = ENOMEM;
				}
				else{
					//create template
										result = dpfj_create_fmd_from_fid(DPFJ_FID_ISO_19794_4_2005,pImage,nImageSize,DPFJ_FMD_ANSI_378_2004,pFeatures,&nFeaturesSize);
					if(DPFJ_SUCCESS == result){
						*ppFt = pFeatures;
						*pFtSize = nFeaturesSize;
					}
					else{
						//dpfj_create_fmd_from_fid() %d", result
						free(pFeatures);
					}
				}
			}
			else if(DPFPDD_QUALITY_CANCELED == cresult.quality){
				//capture canceled
				result = EINTR;
			}
			else{
				//bad capture
				printf("    bad capture, quality feedback: 0x%x\n", cresult.quality);
				unsigned int i = 0;
				for(i = 1; i < 0x80000000; i <<= 1){
					switch(cresult.quality & i){
					case 0: break;
					case DPFPDD_QUALITY_TIMED_OUT:            printf("    tiempo de espera expirado \n"); break;
					case DPFPDD_QUALITY_CANCELED:             printf("    la captura fue cancelada \n"); break;
					case DPFPDD_QUALITY_NO_FINGER:            printf("    no hay un dedo detectado en el lector \n"); break;
					case DPFPDD_QUALITY_FAKE_FINGER:          printf("    dedo falso detectado \n"); break;
					case DPFPDD_QUALITY_FINGER_TOO_LEFT:      printf("    el dedo esta muy a la izquierda del lector \n"); break;
					case DPFPDD_QUALITY_FINGER_TOO_RIGHT:     printf("    el dedo esta muy a la derecha del lector \n"); break;
					case DPFPDD_QUALITY_FINGER_TOO_HIGH:      printf("    el dedo esta muy arriba del lector \n"); break;
					case DPFPDD_QUALITY_FINGER_TOO_LOW:       printf("    el dedo esta muy debajo del lector \n"); break;
					case DPFPDD_QUALITY_FINGER_OFF_CENTER:    printf("    el dedo no esta centrado en el lector \n"); break;
					case DPFPDD_QUALITY_SCAN_SKEWED:          printf("    el escaneo esta demasiado movido \n"); break;
					case DPFPDD_QUALITY_SCAN_TOO_SHORT:       printf("    el escaneo es muy corto \n"); break;
					case DPFPDD_QUALITY_SCAN_TOO_LONG:        printf("    el escaneo es muy largo \n"); break;
					case DPFPDD_QUALITY_SCAN_TOO_SLOW:        printf("    la velocidad en la que se coloco el dedo es muy lenta \n"); break;
					case DPFPDD_QUALITY_SCAN_TOO_FAST:        printf("    la velocidad en la que se coloco el dedo es muy rapida \n"); break;
					case DPFPDD_QUALITY_SCAN_WRONG_DIRECTION: printf("    la direccion en la que se coloco la huella es erronea \n"); break;
					case DPFPDD_QUALITY_READER_DIRTY:         printf("    el lector necesita ser limpiado, imagen muy sucia \n"); break;
					default: printf("    desconocida mascara de calidad de : 0x%x \n", i); break;
					}
				}
				continue;
			}
		}
		break;
	}
	
	//restore signal mask
	pthread_sigmask(SIG_SETMASK, &old_sigmask, NULL);
	
	//restore signal handler
	sigaction (SIGINT, &old_action, NULL);
	g_hReader = NULL;
	
	if(NULL != pImage) free(pImage);
	return result;
}

//enroll an user to extract their FMDs
int enrollment(DPFPDD_DEV hReader,char *path,char *user){
	int bStop = 0;
	while(!bStop){
		int capture_cnt = 0;
		unsigned char* pFmd = NULL;
		unsigned int nFmdSize = 0;
		//start the enrollment
		int result = dpfj_start_enrollment(DPFJ_FMD_ANSI_378_2004);
		if(DPFJ_SUCCESS != result){
			//dpfj_start_enrollment() %d", result
			return 0;
		}
		//capture fingers, create templates
		int bDone = 0;
		int bFirst = 1;
		while(!bDone){
			capture_cnt++;
			//capture finger, create template
			if(bFirst){
				bFirst = 0;
				if(0 != captureFP(hReader, &pFmd, &nFmdSize)){
					bStop = 1;
					break;
				}
			}
			else{
				if(0 != captureFP(hReader, &pFmd, &nFmdSize)){
					bStop = 1;
					break;
				}
			}
			//add template to enrollment
			result = dpfj_add_to_enrollment(DPFJ_FMD_ANSI_378_2004, pFmd, nFmdSize, 0);
			//template is not needed anymore
			free(pFmd);
			pFmd = NULL;
			if(DPFJ_E_MORE_DATA == result){
				//need to add another template
				continue;
			}
			else if(DPFJ_SUCCESS == result){
				//enrollment is ready
				bDone = 1;
				break;
			}
			else{
				//dpfj_add_to_enrollment() %d", result
				break;
			}
		}
		
		//determine size (optional, MAX_FMR_SIZE can be used)
		unsigned char* pEnrollmentFmd = NULL;
		unsigned int nEnrollmentFmdSize = 0;
		if(bDone){
			result = dpfj_create_enrollment_fmd(NULL, &nEnrollmentFmdSize);
			if(DPFJ_E_MORE_DATA == result){
				pEnrollmentFmd = (unsigned char*)malloc(nEnrollmentFmdSize);
				if(NULL == pEnrollmentFmd){
					//problema al reservar memoria con malloc
					return 0;
				}
				else{
					result = dpfj_create_enrollment_fmd(pEnrollmentFmd, &nEnrollmentFmdSize);
				}
			}
			if(DPFJ_SUCCESS != result){
				//dpfj_create_enrollment_fmd() %d", result
				nEnrollmentFmdSize = 0;
			}
			if(NULL != pEnrollmentFmd && 0 != nEnrollmentFmdSize){
				//se creo el enrolamiento
				//se almacena en un archivo la nueva fmd de un nuevo usuario
				saveFMD(path,user,pEnrollmentFmd,nEnrollmentFmdSize);
				//release memory
				free(pEnrollmentFmd);
				goto salir;
			}
		}
		salir:
		//finish the enrollment
		result = dpfj_finish_enrollment();
		if(DPFJ_SUCCESS != result){
			//dpfj_finish_enrollment() %d", result
		}
		return 1;
	}
	return 1;
}
///////////////////////////propios del dispositivo///////////////////////////////

//get the number of users enrolled in the system
int getNumeroUsuarios(char *path){
	int x=0;
	struct dirent *dp;
	DIR *fd;
	if ((fd = opendir(path)) == NULL) {
		return 0;
	}
	while ((dp = readdir(fd)) != NULL) {
		if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue; 
		if(dp->d_type==DT_DIR){
			x++;
			
		}
	}
	closedir(fd);
	return x;
}

//get the name of each enrolled users
char ** getUsersName(char *path){
	int x=0;
	struct dirent *dp;
	DIR *fd;
	char** users_name=(char **)malloc(sizeof(char *)*getNumeroUsuarios(path));
	if ((fd = opendir(path)) == NULL) {
		return NULL;
	}
	while ((dp = readdir(fd)) != NULL) {
		if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue; 
		if(dp->d_type==DT_DIR){
			users_name[x]=(char *)malloc(sizeof(char)*(strlen(dp->d_name)+1));
			strcpy(users_name[x],dp->d_name);
			x++;
			
		}
	}
	closedir(fd);
	return users_name;
}

//get the FMD size asked
unsigned int getSizeFMD(char *path,char * user_name){
	CONFIG_FP configuracion;
	char *configPath = NULL;
	FILE *fichero;
	configPath = (char *)malloc(sizeof(char)*(strlen(path)+strlen("/")+strlen(user_name)+strlen("/")+strlen(CONFIG_FILE_NAME)+1));
	if(errno == ENOMEM){
		//no se pudo pedir memoria....
		return 0;
	}
	strcpy(configPath,path);
	strcat(configPath,"/");
	strcat(configPath,user_name);
	strcat(configPath,"/");
	strcat(configPath,CONFIG_FILE_NAME);
	fichero = fopen(configPath, "rb" );
	if( !fichero ){
		//Error (NO ABIERTO)
		return 	0;
	}
	fread(&configuracion, sizeof(CONFIG_FP), 1, fichero );
	if( fclose(fichero) ){
		//Error: fichero NO CERRADO
		return 0;
	}
	free(configPath);	
	return (unsigned int)configuracion.num_bytes;
}

//get the size of each FMDs saved previously
unsigned int* getSizeFMDS(char *path, char** users_name,int numUsuarios){
	int x=0;
	unsigned int* sizeFMD = NULL;
	sizeFMD = malloc(sizeof(unsigned int)*numUsuarios);
	for(x=0;x<numUsuarios;x++){	
		sizeFMD[x] = getSizeFMD(path,users_name[x]); 
		if(sizeFMD[x]==0){
			//hubo un problema retornamos NULL
			return NULL;
		}
	}
	return sizeFMD;
}

//get the FMD for the user asked specifying the path
unsigned char* getFMD(char *path,char *user_name,unsigned int FMDLength){
	char *filePath = NULL;
	unsigned char *FMD= NULL;
	FILE *fichero;
	FMD      = (unsigned char *)malloc(sizeof(unsigned char)*FMDLength);
	filePath = (char *)malloc(sizeof(char)*(strlen(path)+strlen("/")+strlen(user_name)+strlen("/")+strlen(CONTENT_FILE_NAME)+1));	
	
	if(errno == ENOMEM){
		//no se pudo pedir memoria....
		return NULL;
	}
	strcpy(filePath,path);
	strcat(filePath,"/");
	strcat(filePath,user_name);
	strcat(filePath,"/");
	strcat(filePath,CONTENT_FILE_NAME);
	fichero = fopen(filePath, "rb" );
	if( !fichero ){
		//hubo un problema al abrir el fichero
		return 	NULL;
	}
	fread(FMD, sizeof(unsigned char)*FMDLength, 1, fichero );
	if( fclose(fichero) ){
		//hubo un problema al cerrar el fichero
		return NULL;
	}
	free(filePath);		
	return FMD;
}

//get the FMDs saved previously
unsigned char** getFMDS(char* path,char** users_name,unsigned int* VFMDSize,int numUsers){
	int x=0;
	unsigned char **VFMDS = (unsigned char **)malloc(sizeof(char *)*numUsers);
	for(x=0;x<numUsers;x++){	
		VFMDS[x] = getFMD(path,users_name[x],VFMDSize[x]);
		if(VFMDS[x]==NULL){
			//hubo un problema retornamos NULL
			return NULL;
		}
		
	}	
	return VFMDS;
}

//extract users and their FMDs specifying the path where the fingers were saved
FMDS_STRUCTURE extractUsers(char *path){
	FMDS_STRUCTURE FMDS;
	FMDS.numUsers      = getNumeroUsuarios(path);
	FMDS.usersName     = getUsersName(path);
	FMDS.VFMDSize      = getSizeFMDS(path,FMDS.usersName,FMDS.numUsers);
	FMDS.VFMDS         = getFMDS(path,FMDS.usersName,FMDS.VFMDSize,FMDS.numUsers);
	return FMDS;
}

//obtains all the FMDs saved and compare the finger captured with the finger's pool and it tries to identify it
char* identifyFP(DPFPDD_DEV hReader,unsigned char* FMD_tmp,unsigned int Size_tmp,FMDS_STRUCTURE usuarios){
int resultLed = dpfpdd_led_config(hReader, DPFPDD_LED_ACCEPT | DPFPDD_LED_REJECT, DPFPDD_LED_CLIENT, NULL);
	if(DPFPDD_SUCCESS != resultLed && DPFPDD_E_NOT_IMPLEMENTED != resultLed){
		//dpfpdd_led_config() %d", resultLed
	}
	int bStop = 0;
	while(!bStop){
		if(!bStop){
			//run identification
			//target false positive identification rate: 0.00001
			//for a discussion of  how to evaluate dissimilarity scores, as well as the statistical validity of the dissimilarity score and error rates, consult the Developer Guide
			unsigned int falsepositive_rate = DPFJ_PROBABILITY_ONE / 100000; 
			unsigned int nCandidateCnt = usuarios.numUsers;
			DPFJ_CANDIDATE vCandidates[usuarios.numUsers];
int resultIdentify = dpfj_identify(DPFJ_FMD_ANSI_378_2004, FMD_tmp, Size_tmp, 0,DPFJ_FMD_ANSI_378_2004, usuarios.numUsers, usuarios.VFMDS, usuarios.VFMDSize, falsepositive_rate, &nCandidateCnt, vCandidates);
			if(DPFJ_SUCCESS == resultIdentify){
				if(0 != nCandidateCnt){
					//optional: to get false match rate run compare for the top candidate
					unsigned int falsematch_rate = 0;
					resultIdentify = dpfj_compare(DPFJ_FMD_ANSI_378_2004,  FMD_tmp, Size_tmp, 0, DPFJ_FMD_ANSI_378_2004, usuarios.VFMDS[vCandidates[0].fmd_idx],usuarios.VFMDSize[vCandidates[0].view_idx], 0, &falsematch_rate);
					//turn green LED on for 1 sec
					dpfpdd_led_ctrl(hReader, DPFPDD_LED_ACCEPT, DPFPDD_LED_CMD_ON);
					sleep(1);
					dpfpdd_led_ctrl(hReader, DPFPDD_LED_ACCEPT, DPFPDD_LED_CMD_OFF);
					//el dedo fue identificado
					return usuarios.usersName[vCandidates[0].fmd_idx];
					bStop=1;
				}
				else{
					//turn red LED on for 1 sec
					dpfpdd_led_ctrl(hReader, DPFPDD_LED_REJECT, DPFPDD_LED_CMD_ON);
					sleep(1);
					dpfpdd_led_ctrl(hReader, DPFPDD_LED_REJECT, DPFPDD_LED_CMD_OFF);
					//print out the results no fue identificado
					return NULL;
					bStop=1;
				}
			}
			else printf("dpfj_identify() %d", resultIdentify);
		}
		//release memory
	}
	return NULL;
}

