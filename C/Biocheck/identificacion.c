#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <locale.h>
#include <time.h>
#include "biocheckLib.h"

//program to identify and compare each FMD with the current finger captured.
int main(int argc, char **argv){
	if(argc<2){
		printf("Falta especificar path...\n");
		return 1;
	}
	int return_value = 0;
	DPFPDD_DEV hReader = NULL; //handle of the selected reader
	//block all signals, SIGINT will be unblocked and handled in CaptureFinger()
	sigset_t sigmask;
	sigfillset(&sigmask);
	pthread_sigmask(SIG_BLOCK, &sigmask, NULL);
	setlocale(LC_ALL, "");
	int resultado_inicializacion = 0;
	int resultado_desconexion    = 0;
	
	//inicializamos el dispositivo
	resultado_inicializacion = dpfpdd_init();
	if(resultado_inicializacion != DPFPDD_SUCCESS){
		//no pudimos conectarnos con el fingerprint
		return_value = 1;
		return return_value;	
	}
	hReader = selectAndOpenFP();
	unsigned char* FMD_tmp    = NULL;
	unsigned int   Size_tmp   = NULL;
	if(captureFP(hReader,&FMD_tmp,&Size_tmp)==0){
		//extraccion del pool de huellas guardadas previamente
		FMDS_STRUCTURE usuarios = extractUsers(argv[1]);
		char *usuarioIdentificado = identifyFP(hReader,FMD_tmp,Size_tmp,usuarios);
		if(usuarioIdentificado==NULL){
		    time_t tiempo = time(0);
    		struct tm *tlocal = localtime(&tiempo);
    		char output[128];
    		strftime(output,128,"%H:%M:%S",tlocal);
    		printf("%s\n",output);			
			printf("Not_found\n");
			return_value = 1;
		}else{
		    time_t tiempo = time(0);
    		struct tm *tlocal = localtime(&tiempo);
    		char output[128];
    		strftime(output,128,"%H:%M:%S",tlocal);
    		printf("%s\n",output);				
			printf("%s\n",usuarioIdentificado);
		}
	}
	int result=0;
	if(NULL != hReader){
		//cerramos el dispositivo
		result = dpfpdd_close(hReader);
		if(DPFPDD_SUCCESS != result){ 
			//"dpfpdd_close() %d\n", result
		}
		hReader = NULL;
	}
	resultado_desconexion = dpfpdd_exit();
	if(resultado_desconexion == DPFPDD_SUCCESS){
		//pudimos desconectarnos del fingerprint
	}
	else{
		//no pudimos desconectarnos del fingerprint	
	}
	return return_value;
	
}
