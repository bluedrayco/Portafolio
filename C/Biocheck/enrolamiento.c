#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <locale.h>
#include <time.h>
#include"biocheckLib.h"

//program to enroll a user finger in the system
int main(int argc, char **argv){
	if(argc<2){
		printf("faltan parametros en la llamada al programa.\n");
		return 1;	
	}
	DPFPDD_DEV hReader = NULL; //handle of the selected reader
	//block all signals, SIGINT will be unblocked and handled in CaptureFinger()
	sigset_t sigmask;
	sigfillset(&sigmask);
	pthread_sigmask(SIG_BLOCK, &sigmask, NULL);
	setlocale(LC_ALL, "");
	int resultado_inicializacion = 0;
	int resultado_desconexion    = 0;
	//inicializamos el dispositivo
	resultado_inicializacion = dpfpdd_init();
	if(resultado_inicializacion != DPFPDD_SUCCESS){
		//no pudimos conectarnos con el fingerprint...
		goto no_desconectar;	
	}
///////////////////////////
	hReader = selectAndOpenFP();	
////////////////////////////
	//obtencion de caracteristicas del dedo
	enrollment(hReader,argv[1],argv[2]);
		time_t tiempo = time(0);
		struct tm *tlocal = localtime(&tiempo);
		char output[128];
		strftime(output,128,"%H:%M:%S",tlocal);
		printf("%s\n",output);			
		printf("%s\n",argv[2]);
	//close reader
	int result=0;
	if(NULL != hReader){
		//cerramos el dispositivo...
		result = dpfpdd_close(hReader);
		if(DPFPDD_SUCCESS != result){ 
			//dpfpdd_close() %d", result
		}
		hReader = NULL;
	}
	resultado_desconexion = dpfpdd_exit();
	if(resultado_desconexion != DPFPDD_SUCCESS){
		//no pudimos desconectarnos del fingerprint...	
	}
	no_desconectar:
	return 0;
	
}