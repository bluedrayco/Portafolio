## Biocheck 

This project creates a way to identify students in a high school.<br/>
This code snippet perform two diferent actions:<br/>

**1.-** Enroll a finger (extract its feactures and convert them in to a FMD) and save in a file system.<br/>
**2.-** Identify a captured finger and compare it with the all FMDs previously captured in the system.<br/>

There are the next files, their respective description is:<br/>

**biocheckLib.c:** It contains all the functions used in the enrolamiento.c and identificacion.c files.<br/>
**biocheckLib.h:** It only contains the signatures of each functions and describe them in **biocheckLib.c**.<br/>
**enrolamiento.c:** Generates a enroll process for a student's finger.<br/>
**identificacion.c:** Capture a finger and compare this finger with the FMD's pool and return it in to the response of this comparation<br/>
