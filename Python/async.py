#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Task to run async process

@author Roberto Monroy <roberto.monroy@enova.mx>
"""

import os
import sys
import uuid
import json
import socket
import requests
import subprocess
from time import strftime
from celery import Celery
from celery.task.control import revoke


#notas:
#se ha detectado que aumenta el numero de queues en el servidor que esta enviando las sincronizaciones, en este caso se ha notado que la anoalia es en central
#probablemente no esta funcionando adecuadamente el parámetro retry, esto porque no se ha encontrado en que momento se obtiene el nuevo valor incrementado, siempre se ve que el parametro retry se inicializa a uno

#configuracion de celery y creación de instancia
app = Celery('async')
env = 'dev' + os.environ['CELERY_ASYNC_ENV'] if 'CELERY_ASYNC_ENV' in os.environ else ''
ce_dir = os.environ['MAKO_CONTROL_ESCOLAR_HOME'] if 'MAKO_CONTROL_ESCOLAR_HOME' in os.environ else '/var/www/control-escolar'#cambie la ruta default no es mako es control-escolar

#se carga la configuracion proveniente de config.py
app.config_from_object('config' + env)

#clase de excepciones
class CESyncError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

    def string(self):
        return str(self.value)

#task principal que ejecuta el paquete de sincronizacion

#Command: "./app/console enova:sync:procesar --env=prod -d '/tmp/json-sync-8f891eb1-7e86-407b-886a-aa1ae6073322-Mon-21-Sep-2015-18-03-49' -r 'http://10.0.2.13:666/sync/api/action/status/3/'"
#Valid response! [<PUT> http://10.0.2.13:666/sync/api/action/status/3/Recibido
#change status url: [http://10.0.2.13:666/sync/api/action/status/3/Sincronizado]

#parametros usados en este task para los status: Recibido,Error,Sincronizado

@app.task(serializer='json', default_retry_delay=30, max_retries=None)############3se modificó el numero maximo de retries en None, esto afecta en que siempre seguira reintentando la sincronizacion
def finishSync(params, **kwargs):
    try:
        retry = 1##################
        args = ''
        command = ''
        cmd = ''

        print 'Starting finishSync...'
#si encuentra en el paquete json data ejecuta la sincronizacion
        if 'data' in params:
            #####################se crea el archivo para su ejecución posterior
            datetime = strftime('%a-%d-%b-%Y-%H-%M-%S')
            json_filename = '/tmp/json-sync-' + str(uuid.uuid4()) + '-' + datetime
            json_file = open(json_filename, 'w')

            json_file.write(json.dumps(params['data']))
            json_file.close()

            args = '-d \'' + json_filename + '\''
            #####################termino de la creacion del archivo para su sincronizacion

#si encuentra una url de cambio de estatus la obtiene
        if 'status_url' in params:
            #si el parametro estatus no contiene diagonal en su ultimo caracter extrae el status_url
            if params['status_url'][-1:] != '/':
                params['status_url'] = params['status_url'][:(params['status_url'].rfind('/') + 1)]
            #se arman los argumentos para el estatus del comando a ejecutar para la sincronización
            args = args + ' -r \'' + params['status_url'] + '\''
#se llamará al comando enova:sync:procesar el cual recibe dos parámetros, el archivo que se ejecutará y la url de donde debe enviar la informacion del estatus de la sincronizacion
        cmd = './app/console enova:sync:procesar --env=prod ' + args
#se cambia el estatus de la sincronizacion a recibida
        sync_change_status(params['status_url'] + 'Recibido', cmd)
#se imprime el comando a ejecutar
        print 'Command: "' + cmd + '"'
#nos cambiamos a /var/www/control-escolar
        os.chdir(ce_dir)
#se ejecuta en la linea de comandos la instruccion de
        command = subprocess.check_output(cmd, shell=True)
#impresion del comando ejecutado de control escolar
        print '==== cmd result ====',\
              command,\
              '====================', "\n"
#si no se regreso el texto 'Sincronizacion completada exitosamente.' quiere decir que no se ejecuto adecuadamente la sincronizacion
        if 'Sincronizacion completada exitosamente.' not in command:
            #si hubo un error se actualiza el estatus del paquete en error
            sync_change_status(params['status_url'] + 'Error', command)
            raise Exception('El comando no termino exitosamente.')
#si se termino adecuadamenteel procesamiento del paquete se cambia el estatus del paquete a sincronizado
        sync_change_status(params['status_url'] + 'Sincronizado', command)

        print 'Finishing sync'
        revoke(finishSync.request.id, terminate=True)#linea agregada, posiblemente este elimina la task que se esta ejecutando en ese momento
        subprocess.call('/var/www/celery/stopTask.sh ce CE controlescolar '+str(finishSync.request.id)+'  &', shell=True)
#excepcion si falla la ejecucion del comando, el comando de enova:sync:procesar envia return 1 para error y return 0 para exito
#aparte genera una excepcion de tipo CalledProcessError
    except subprocess.CalledProcessError as error:
        print 'ERROR!======== ' + str(error.returncode) + ' =========', error.output, '=================='
        #se cambia el estatus de la sincronizacion a Error
        sync_change_status(params['status_url'] + 'Error', error.output)
        return True
        subprocess.call('/var/www/celery/stopTask.sh ce CE controlescolar '+str(finishSync.request.id)+'  &', shell=True)

    except CESyncError as error:#excepcion si falla la modificacion del estatus del paquete de sincronizacion
        print 'CESyncError!!', error

        msg = 'Async task cannot be retried due to\n=========\n' + str(error)
        #envio de un email critico al equipo de desarrollo si hubo un problema en la modificación del status
        send_email('[CRITICAL ERROR] Async task cannot be retried!', msg)
        #termina esta task en caso de que tenga un error fatal (probablemente esta es la forma de eliminar una task)
        revoke(finishSync.request.id, terminate=True)
        subprocess.call('/var/www/celery/stopTask.sh ce CE controlescolar '+str(finishSync.request.id)+'  &', shell=True)
    except Exception as e:
        #imprime la excepcion del sistema
        handle_exception(e)
        #genera un reintento de sincronizacion  enviando de nuevo el comando
        handle_retry('finishSync', exception=e, retry=retry, cmd=cmd, cmdoutput=command)
        subprocess.call('/var/www/celery/stopTask.sh ce CE controlescolar '+str(finishSync.request.id)+'  &', shell=True)
#modificar estatus de la sincronizacion
def sync_change_status(status_url, msg='', **kwargs):
    try:
        retry = 1

        print 'change status url: [' + status_url + ']'

        headers = {
            'content-type': 'application/json'
        }
        #se obtiene el metodo put para envío de la sincronizacion
        call = getattr(requests, 'put')
        #armado del servicio de tipo put que se enviará
        json_payload = json.dumps({'msg': msg}, separators=(',', ':'))
        #se hace la llamada al servicio rest y se obtiene la respuesta de este
        rsp = call(status_url, data=json_payload, headers=headers)
        #se valida que el paquete de respuesta este correctamente bien formado, si no esta bien formado se obtendrá un mensaje de error
        response = validate_response(rsp)
        #se evalua si el json obtenido no fue correcto se envia una excepcion de tipo CESyncError
        if response is not True:
            if type(response) is str or type(response) is unicode:
                raise Exception(response)
            raise CESyncError('Unknown error!')
        #se retorna un booleano en true
        return True

    except CESyncError as error:

        print 'CESyncError!!', error

        msg = 'Async task cannot be retried due to\n=========\n' + str(error)
        send_email('[CRITICAL ERROR] Async task cannot be retried!', msg)
        #se termina la task que se ejecuto en ese momento
        revoke(sync_change_status.request.id, terminate=True)

    except Exception as e:
        #si ocurre un problema al consumir el servicio rest se reintenta el status
        handle_exception(e)
        handle_retry('sync_change_status', exception=e, retry=retry, response=rsp)

# You can add more async process here!

#esta funcion se encarga de imprimir la excepcion que se obtuvo
def handle_exception(e):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

    print 'EXCEPTION!!!', e, exc_type, fname, exc_tb.tb_lineno

#funcion que reintenta una task, la cual puede ser un cambio de estatus (sync_change_status->servicio rest) o el comando de symfony2 (finishSync->./app/console enova:sync:procesar)
def handle_retry(function, **kwargs):
    #se obtienen las veces que se ha reintentado la task
    retry = kwargs['retry'] + 1 if 'retry' in kwargs else 1
    #se obtiene la excepcion por la cual se reintento la sincronizacion
    exception = kwargs['exception'] if 'exception' in kwargs else 'Unknown reason.'
    #si es llamada a un servicio rest ahi se obtendrá la invocación al servicio rest
    response = kwargs['response'] if 'response' in kwargs else None
    #si es un comando el que se quiere reintentar se obtiene la sentencia de la ejecucion del comando
    cmd = kwargs['cmd'] if 'cmd' in kwargs else None
    #si es un comando tambien se obtiene el mensaje de respuesta del comando ejecutado
    cmdoutput = kwargs['cmdoutput'] if 'cmdoutput' in kwargs else None

    info_extra = ''
    #si es un reintento de una sincronizacion (comando symfony2) el que se reintentará se almacena que comando fue y la salida de este
    if cmd is None and cmdoutput is None:
        info_extra = '==============================================\n\n' +\
                     'Command executed: ' + cmd + '\n\n' +\
                     '==============================================\n\n' +\
                     'Command output:\n' + cmdoutput + '\n\n'
    #si es un reintento de cambio de status (servicio rest http://10.0.2.13:666/sync/api/action/status/...) se almacena el metodo de envio del la ultima llamada al rest,
    #la url de la petición y el codigo de respuesta
    elif response is not None:
        info_extra = '==============================================\n\n' +\
                     'Last response: [<' + response.request.method + '> ' + response.url + ' (' + str(response.status_code) + ')]\n\n' +\
                     '==============================================\n\n' +\
                     response.text + '\n\n'
    #solo si el reintento llego a los 10,500,1000,5000 y 10000 reintentos se envia un email al administrador indicando el problema en cuestion (ver nota numero 2)
    if retry in [10, 500, 1000, 5000, 10000]:
        msg = 'The sync task (' + function + ') is on its # ' + str(retry) + ' retry. ' +\
              'And so far... it cannot be completed, because:\n\n' +\
              '==============================================\n\n' +\
              str(exception) + '\n\n' +\
              info_extra +\
              '++++++++++++++++++++++++++++++++++++++++++++++\n\n' +\
              'Rabbit/celery/system information:\n' +\
              '  * Hostname: "' + socket.gethostname() + '"\n' +\
              '  * IP: "' + [(s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]

        send_email('Celery "' + function + '"" is idle...', msg)
    #si es un reintento de tipo finishSync (comando de symfony) se reintenta este.
    if function == 'finishSync':
        #se reintenta finishSync, el tiempo de retry viene dado de forma exponencial, reintentará la tarea a los 2,4,8,16,32,64... exponente base 2 segundos
        finishSync.retry(exc=exception, kwargs={'retry': retry}, countdown=(2 ** finishSync.request.retries))
    elif function == 'sync_change_status':
        #se reintenta sync_change_status, el tiempo de retry viene dado de forma exponencial, reintentará la tarea a los 2,4,8,16,32,64... exponente base 2 segundos
        sync_change_status.retry(exc=exception, kwargs={'retry': retry}, countdown=(2 ** sync_change_status.request.retries))

#funcion que verifica que el paquete este adecuadamente formado y no sea este estatus diferente a 200 (exito)
def validate_response(response, **kwargs):
    response_data = '[<' + response.request.method + '> ' + response.url + ' (' + str(response.status_code) + ')]'
    if response.status_code is not 200:
        print 'Non 200 response code ' + response_data
        if 'text' in dir(response):
            json_response = json.loads(response.text)[0]
            if type(json_response) is not dict and ('message' not in json_response):
                print 'Not valid response structure ' + response_data, json_response
                return 'Bad response structure.'
            else:
                return 'Bad response: "' + json_response['message'] + '"'
        else:
            return 'Bad response structure.'
    else:
        json_response = json.loads(response.text)

    if type(json_response) is not dict and ('status' not in json_response or 'code' not in json_response):
        print 'Not valid response structure ' + response_data, response
        if type(response) is list and len(response) is not 0 and 'message' in response[0]:
            print 'Not enought data in response ' + response_data
            return 'Bad request "' + response[0]['message'] + '"'
        return 'Bad response structure.'

    if json_response['status'] is not True or json_response['code'] is not 200:
        print 'Bad status and code ' + response_data

        if 'message' in json_response:
            return 'Bad response: "' + (json_response['message'] if json_response['message'] is not None else 'null') + '" {' + response.text + '}'
        else:
            return 'Bad response AND structure: "' + response.text + '"'

    print 'Valid response! ' + response_data

    return True


def send_email(subject, message_body, **kwargs):
    import os
    import smtplib

    TO = ['control.escolar.desarrollo@enova.mx'] if 'to' not in kwargs else kwargs['to']
    FROM = 'celery_bruno@dev.bruno' if 'from' not in kwargs else kwargs['from']
    gmail_user = os.getenv('GMAIL_USER')
    gmail_pwd = os.getenv('GMAIL_PASSWORD')

    if gmail_user is None:
        raise Exception('Necesitas definir GMAIL_USER como variable de ambiente!')

    if gmail_pwd is None:
        raise Exception('Necesitas definir GMAIL_PASSWORD como variable de ambiente!')

    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), subject, message_body)

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
    except Exception as e:
        handle_exception(e)
