## Agent-PCWatcher

In the "Control Escolar" project it was required to check the state (session and ip) of each computers in the center. In order to solve that, I created an Agent in Java Multiplatform using 
polymorphism to detect the IP and session of each computer, the program was installed as a daemon in ubuntu for work in background.

The list below describe each file and their functionality:

**pom.xml:** This file contains the description of every dependencie for maven that I needed to configure for compile the project in an unique jar.<br/>
**src/main/java/config.properties:** This is the config file for the application, it contains three parameters: url (the rest service url where we need to send the data), retry (delay time for each request to the server), threads (number of threads to create for resolve each response from the server).<br/>
**src/main/java/com/enova/agent/Main.java**: It's the main file, obtains the configuration properties, get the current operating system and load dynamically depending the O.S (com.enova.so.Windows, com.enova.so.Linux and com.enova.so.Mac), after that obtains user's session and the hots's IP, send the information to the rest service specified in the config.properties file, receiving the response and find a valid command from the server and execute in a thread.<br/>
**src/main/java/com/enova/command/Command.java:** It contains the methods to execute a valid command from the server that we received from the JSON response.<br/>
**src/main/java/com/enova/rest/RestClient.java:** Class for send the username, ip, mac_address and O.S to the server for register the activity from the computer.<br/>
**src/main/java/com/enova/so/IOperatingSystem.java:** Interface which is used to implement the methods: getIp, getUserSession and getMacAddress for each Operating System.<br/>
**src/main/java/com/enova/so/Linux.java:** Implementation of the IOperatingSystem interface for Linux Operating System like Ubuntu or Gento.<br/>
**src/main/java/com/enova/so/Windows.java:** Implementation of the same interface for Windows operating systems.<br/>