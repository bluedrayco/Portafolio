/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enova.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 *
 * @author rm
 */
public class RestClient {

    private String url = null;

    public RestClient(String url) {
        this.url = url;
    }

    public ClientResponse enviar(String username, String ip, String macAddress, String sistemaOperativo, String fecha, String hora) throws Exception {
        try {
            Client client = Client.create();

            WebResource webResource = client
                    .resource(this.url + "?username=" + username + "&ip=" + ip + "&mac_address=" + macAddress + "&so=" + sistemaOperativo + "&hora=" + hora + "&fecha=" + fecha);
            ClientResponse response = webResource.accept("application/json")
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Codigo HTTP de error: "
                        + response.getStatus());
            }
            return response;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
