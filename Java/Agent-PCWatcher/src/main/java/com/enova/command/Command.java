package com.enova.command;

import com.enova.agent.Propiedades;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import javax.ws.rs.core.MultivaluedMap;
import org.json.simple.JSONObject;

/**
 * Clase encargada de ejecutar alguna tarea que se tenga asignada como respuesta a la peticion rest
 * @version 1.0
 * @author roberto.monroy
 */
public class Command implements Runnable {

    /**
     * Constantes de los metodos HTTP permitidos para la respuesta
     */
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";

    /**
     * Constantes de los comandos internos disponibles para el agente
     */
    public static final String SALUDO = "SALUDO";
    public static final String CHECK_PARAMETERS = "CHECK_PARAMETERS";
    public static final String CHANGE_PARAMETERS = "CHANGE_PARAMETERS";

    private JSONObject command = null;
    Map<String, String> dataResponse = null;
    private Client client = null;
    private WebResource webResource = null;
    private MultivaluedMap params = null;
    private ClientResponse response = null;
    private String operatingSystem = null;
    private String ip = null;
    private String id =null;


    /**
     * constructor que recibe el objeto comando, la ip y el sistema operativo en el cual se esta ejecutando el Agente
     * @param command objeto de tipo json que contiene los parametros del comando a ejecutar
     * @param ip ip del cliente
     * @param sistemaOperativo sistema operativo en el cual se esta ejecutando el Agente
     */
    public Command(JSONObject command, String ip, String sistemaOperativo) {
        this.command = command;
        this.client = Client.create();
        this.operatingSystem = sistemaOperativo;
        this.ip = ip;
        this.dataResponse= new HashMap<>();
    }

    /**
     * Procesa el comando a ejecutarse ya sea interno o externo
     * @param command comando a ejecutar con diversos parametros
     * @return se obtiene un true en caso que el comando haya sigo ejecutado correctamente false en caso contrario
     */
    public boolean dispatcher(JSONObject command) {
        try {
            String comando = command.get("command") == null ? null : command.get("command").toString();
            String data = command.get("data") == null ? null : command.get("data").toString();
            this.id = command.get("id") == null ? null : command.get("id").toString();
            String urlResponse = command.get("url_response") == null ? null : command.get("url_response").toString();
            String method = command.get("method") == null ? null : command.get("method").toString();
            String contentAccept = command.get("content_accept") == null ? null : command.get("content_accept").toString();
            switch (comando.toUpperCase()) {
                case Command.SALUDO:
                    System.out.println("////////////////Hola mundo desde el hilo...//////////////////////");
                    break;

                case Command.CHECK_PARAMETERS:
                    Propiedades p = new Propiedades();
                    Properties propiedades = p.getProperties();
                    Enumeration e = propiedades.propertyNames();
                    while (e.hasMoreElements()) {
                        String key = (String) e.nextElement();
                        this.dataResponse.put(key, propiedades.getProperty(key));
                    }

                    break;
                case Command.CHANGE_PARAMETERS:

                    break;
                default:
            }
            if (urlResponse != null) {
                this.response = this.send(urlResponse, method, contentAccept, dataResponse);
                System.out.println("---------->" + this.response.getEntity(String.class));
                if (this.response.getStatus() == 200) {
                    System.out.println("----------->recepcion exitosa.");
                    return true;
                } else {
                    System.out.println("----------->recepcion no exitosa.");
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * Metodo para la ejecucion del hilo el cual procesara el comando recibido por el servicio rest consumido
     */
    @Override
    public void run() {
        System.out.println("entre al hilo...");
        this.dispatcher(this.command);
    }

    /**
     * 
     * @param url
     * @param method
     * @param typeFormat
     * @param data
     * @return
     * @throws Exception
     */
    public ClientResponse send(String url, String method, String typeFormat, Map<String, String> data) throws Exception {
        data.put("so", this.operatingSystem);
        data.put("ip", this.id);
        switch (method.toUpperCase()) {
            case Command.GET:
                this.webResource = this.client.resource(url);
                if (data != null) {
                    System.out.println("-------->entre a data...");
                    this.params = this.MapToMultivaluedMapImpl(data);
                    this.webResource.type(typeFormat);
                    response = this.webResource.queryParams(this.params).get(ClientResponse.class);
                } else {
                    response = this.webResource.get(ClientResponse.class);
                }
                break;
            case Command.POST:
                break;
            case Command.PUT:
                break;
            case Command.DELETE:
                break;
            default:
                throw new Exception("metodo de respuesta erroneo, solo se permiten: GET,POST,PUT y DELETE");
        }
        return response;
    }

    public MultivaluedMap MapToMultivaluedMapImpl(Map<String, String> data) {
        params = new MultivaluedMapImpl();
        for (Entry<String, String> element : data.entrySet()) {
            System.out.println("\t" + element.getKey() + "\t" + element.getValue());
            params.add(element.getKey(), element.getValue());
        }
        return params;
    }
}
