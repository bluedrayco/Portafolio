package com.enova.agent;

import com.enova.command.Command;
import com.enova.rest.RestClient;
import com.enova.so.IOperatingSystem;
import com.sun.jersey.api.client.ClientResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Clase Base para la ejecucion del Agente
 * @version 1.0
 * @author roberto.monroy
 */
public class Main {

    //constantes de los diferentes sistemas operativos soportados por el Agente
    public static final String Windows = "Windows";
    public static final String Mac = "Mac";
    public static final String Linux = "Linux";

    //propiedad para la obtencion de las propiedades provista por el archivo de configuracion
    public static Propiedades p = null;

    //propiedad para envio de servicio rest
    public static ClientResponse response = null;

    /**
     * Metodo principal en el cual se ejecuta el consumo de metodos y envio a servicios rest
     * @param args Argumentos provistos por linea de comandos
     */
    public static void main(String[] args) throws InterruptedException {
        Properties propiedades = new Properties();
        IOperatingSystem op = null;
        String clase = "";
        String username = "";
        String ip = "";
        String macAddress = "";

        String sistemaOperativo = Main.getOperativeSystem();
        switch (sistemaOperativo) {
            case Windows:
                clase = Windows;
                break;
            case Linux:
                clase = Linux;
                break;
            case Mac:
                clase = Mac;
                break;
        }
        ClassLoader classLoader = Main.class.getClassLoader();
        while (true) {
            try {
                p = new Propiedades();
                propiedades = p.getProperties();
                op = (IOperatingSystem) classLoader.loadClass("com.enova.so." + clase).newInstance();

                ip = op.getIp();
                ip = ip.equals("") ? "no_ip_found" : ip;
                username = op.getUserSession();
                macAddress = op.getMacAddress();
                System.out.println(username);
                System.out.println(ip);
                System.out.println(propiedades.getProperty("url"));
                System.out.println(propiedades.getProperty("retry"));
                response = Main.sendDataToRest(propiedades.getProperty("url"), ip, macAddress, sistemaOperativo, username);
                String output = response.getEntity(String.class);
                System.out.println(output);
                JSONParser parser = new JSONParser();
                JSONObject obj = (JSONObject) parser.parse(output);
                System.out.println("Username:" + obj.get("username"));
                System.out.println("Sistema Operativo:" + obj.get("so"));
                System.out.println("Ip:" + obj.get("ip"));
                System.out.println("Mac Address:" + obj.get("mac_address"));
                System.out.println("Command: " + obj.get("command"));
                String comando = obj.get("command") == null ? null : obj.get("command").toString();
                if (response.getStatus() == 200) {
                    System.out.println("Envio exitoso...");
                } else {
                    System.out.println("Error en la conexion...");
                }
                if (comando != null) {
                    if (comando.length() > 0) {
                        System.out.println("entre....");
                        Thread hilo = new Thread(new Command((JSONObject) parser.parse(comando), ip, sistemaOperativo));
                        hilo.start();
                    }
                }

                Thread.sleep(Integer.valueOf(propiedades.getProperty("retry")));

            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                Thread.sleep(Integer.valueOf(propiedades.getProperty("retry")));
            }
        }
    }

    /**
     * Metodo para obtencion del sistema operativo en el que se esta ejecutando el Agente
     * @return String sistema operativo
     */
    public static String getOperativeSystem() {
        String os = System.getProperty("os.name");
        //String os= "Windows 2000";
        if (os.contains(" ")) {
            int index = os.indexOf(" ");
            os = os.substring(0, index);
        }
        return os;
    }

    /**
     * Metodo para envio de datos provenientes del Agente consumiendo un servicio Rest
     * @param url url a la cual se enviara la informacion
     * @param ip cadena de ips separadas por comas, las cuales se encontraron en la maquina donde esta hospedado el Agente
     * @param macAddress cadena de mac address separadas por comas que se encontraron en la maquina
     * @param sistemaOperativo sistema operativo en el cual se esta ejecutando el Agente
     * @param username cadena de usuarios los cuales se encuentran en sesion en ese momento en la maquina.
     * @return ClientResponse Objeto de tipo ClientResponse el cual contiene la informacion del status y el json de retorno
     * @throws Exception si se encuentra un error al momento de enviar el servicio rest se activara dicha excepcion.
     */
    public static ClientResponse sendDataToRest(String url, String ip,String macAddress, String sistemaOperativo, String username) throws Exception {
        RestClient rc = new RestClient(url);
        Date date = new Date();
        SimpleDateFormat formatFecha = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatHora = new SimpleDateFormat("hh:mm:ss");
        String hora = formatHora.format(date);
        String fecha = formatFecha.format(date);
        try {
            ClientResponse response = rc.enviar(username, ip,macAddress, sistemaOperativo, fecha, hora);
            return response;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }
}
