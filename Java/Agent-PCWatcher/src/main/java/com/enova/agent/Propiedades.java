package com.enova.agent;

import java.io.IOException;
import java.util.Properties;

/**
 * Clase encargada de la manipulacion del archivo de propiedades de la aplicacion
 * @version 1.0
 * @author roberto.monroy
 */
public class Propiedades {

    /**
     * Obtencion de las propiedades del archivo de configuracion
     * @return Objeto de tipo properties el cual contiene un arreglo asociativo con las propiedades extraidas
     * @throws IOException Ocurre si no se puede acceder al archivo de configuracion
     */
    public  Properties getProperties() throws IOException {
        try {
            //se crea una instancia a la clase Properties
            Properties propiedades = new Properties();
            //se leen el archivo .properties

            propiedades.load(getClass().getResourceAsStream("/config.properties"));
            //si el archivo de propiedades NO esta vacio retornan las propiedes leidas
            if (!propiedades.isEmpty()) {
                return propiedades;
            } else {//sino  retornara NULL
                return null;
            }
        } catch (IOException ex) {
            throw new IOException(ex);
        }
    }
}
