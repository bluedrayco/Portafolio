/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.enova.so;

/**
 * 
 * @author rm
 */
public interface IOperatingSystem {
    public String getIp();
    public String getUserSession();
    public String getMacAddress();
}
