/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enova.so;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Aplicacion de obtencion de ip y usuario logueado en la maquina en la que se
 * ejecuta la aplicacion para Sistemas Windows
 *
 * @author Roberto Leroy Monroy Ruiz
 */
public class Windows implements IOperatingSystem {

    @Override
    public String getIp() {
        String salida = "";
        try {
            Runtime rt = Runtime.getRuntime();
            String[] cmd = {
                "/bin/sh",
                "-c",
                "ip route get 8.8.8.8|cut -d' ' -f8"
            };
            Process proc = rt.exec(cmd);

            InputStreamReader ost = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(ost);
            String line = "";
            while ((line = br.readLine()) != null) {
                salida += line + "";
            }
            int exitVal = proc.waitFor();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            return salida;
        }
    }

    @Override
    public String getUserSession() {
        String salida = "";
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("echo %username%");

            InputStreamReader ost = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(ost);
            String line = "";
            while ((line = br.readLine()) != null) {
                salida += line + "\n";
            }
            int exitVal = proc.waitFor();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            return salida.substring(0, salida.length() - 1);
        }
    }

    @Override
    public String getMacAddress() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

