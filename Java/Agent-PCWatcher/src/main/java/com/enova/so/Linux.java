/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enova.so;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Aplicacion de obtencion de ip y usuario logueado en la maquina en la que se
 * ejecuta la aplicacion para Sistemas Linux
 *
 * @author Roberto Leroy Monroy Ruiz
 */
public class Linux implements IOperatingSystem {

    @Override
    public String getIp() {
        String salida = "";
        try {
            Runtime rt = Runtime.getRuntime();
            String[] cmd = {
                "/bin/sh",
                "-c",
                "ip addr |grep -e 'inet ' |cut -d' ' -f6|cut -d'/' -f1|grep -v '127.0.0.1'"
            };
            Process proc = rt.exec(cmd);

            InputStreamReader ost = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(ost);
            String line = "";
            while ((line = br.readLine()) != null) {
                salida += line + ",";
            }
            int exitVal = proc.waitFor();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            return salida.substring(0, salida.length() - 1);
        }
    }

    @Override
    public String getUserSession() {
        String salida = "";
        try {
            Runtime rt = Runtime.getRuntime();
            String[] cmd = {
                "/bin/sh",
                "-c",
                "who |grep ':0' |cut -d' ' -f1|sort|uniq"
            };
            Process proc = rt.exec(cmd);

            InputStreamReader ost = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(ost);
            String line = "";
            while ((line = br.readLine()) != null) {
                salida += line + ",";
            }
            int exitVal = proc.waitFor();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            return salida.substring(0, salida.length() - 1);
        }
    }

    @Override
    public String getMacAddress() {
        String salida = "";
        try {
            Runtime rt = Runtime.getRuntime();
            String[] cmd = {
                "/bin/sh",
                "-c",
                "ifconfig -a|grep 'HW'|sed 's/ /|/g'|tr -s '|'|cut -d'|' -f5"
            };
            Process proc = rt.exec(cmd);

            InputStreamReader ost = new InputStreamReader(proc.getInputStream());
            BufferedReader br = new BufferedReader(ost);
            String line = "";
            while ((line = br.readLine()) != null) {
                salida += line + ",";
            }
            int exitVal = proc.waitFor();
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            return salida.substring(0, salida.length() - 1);
        }
    }

}
