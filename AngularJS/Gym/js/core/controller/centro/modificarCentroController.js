principal.controller('modificarCentroController', function ($scope, $routeParams, $location, commonService, sesionDataFactory, centros,centroService,modalService) {
    $scope.pagina = {
        titulo: "Modificación de un Centro"
    };
    $scope.centros = centros.data.data;

    $scope.centroInput = {
        nombre: "",
        clave: "",
        direccion: "",
        telefono: "",
        telefono2: "",
        codigo_postal: "",
        latitud: "",
        longitud: "",
        hora_apertura:"",
        hora_cierre:"",
        activo: true
    };



    $scope.init = function () {
        if ($scope.centros[0].id == 0 || $routeParams.id < 0) {
            document.location.href = $location.absUrl().replace($location.$$url, '');
        } else {
            $scope.centroInput = obtenerCentro($scope.centros, $routeParams.id);
            $scope.centroInput.hora_apertura = new Date($scope.centroInput.hora_apertura);
            $scope.centroInput.hora_cierre = new Date($scope.centroInput.hora_cierre);
            horaAperturaCentro=$scope.centroInput.hora_apertura;
            horaCierreCentro=$scope.centroInput.hora_cierre;
        }
    }

    $scope.almacenarCentro = function (centro) {
        centro.hora_apertura=$('#datetimepicker1').data('DateTimePicker').date().format('YYYY-MM-DD HH:mm:ss');
        centro.hora_cierre=$('#datetimepicker2').data('DateTimePicker').date().format('YYYY-MM-DD HH:mm:ss');
        centroService.update(centro.id, centro, function (result) {
            alert("Se modificó correctamente el centro...");
            $scope.centroInput = commonService.limpiarObjeto(centro);
            $scope.centroInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
            $('#datetimepicker2').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema tratar de modificar el centro...");
        });
    }
    $scope.limpiarCentro = function (centro) {
        $scope.centroInput = commonService.limpiarObjeto(centro);
        $scope.centroInput.activo = true;
    }

    obtenerCentro = function (centros, id) {
        for (i = 0; i < centros.length; i++) {
            if (centros[i].id == id) {
                return centros[i];
            }
        }
    }
});