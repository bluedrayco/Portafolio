principal.controller('altaCentroController', function ($scope, commonService,sesionDataFactory,centroService) {
    $scope.pagina={
        titulo:"Alta de un Centro"
    };
    
    $scope.centroInput = {
        nombre: "",
        clave: "",
        direccion: "",
        telefono: "",
        telefono2: "",
        codigo_postal: "",
        latitud: "",
        longitud: "",
        hora_apertura:"",
        hora_cierre:"",
        activo: true
    };
    
    
    
    $scope.init = function () {
        horaAperturaCentro=new Date();
        horaCierreCentro =new Date();
    }
    
    $scope.almacenarCentro = function (centro) {
        centro.hora_apertura=$('#datetimepicker1').data('DateTimePicker').date().format('YYYY-MM-DD HH:mm:ss');
        centro.hora_cierre=$('#datetimepicker2').data('DateTimePicker').date().format('YYYY-MM-DD HH:mm:ss');
        centroService.create(centro, function (result) {
            alert("Se almacenó correctamente el centro...");
            $scope.centroInput = commonService.limpiarObjeto(centro);
            $scope.centroInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
            $('#datetimepicker2').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema al almacenar el centro...");
        });
    }
    $scope.limpiarCentro = function (centro) {
        $scope.centroInput = commonService.limpiarObjeto(centro);
        $scope.centroInput.activo = true;
    }
});