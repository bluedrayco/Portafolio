principal.controller('listaCentroController', function (
        $scope, 
        $location,
        commonService, 
        centrosDataFactory, 
        centros,
        modalService,
        centroService) {
    $scope.pagina = {
        titulo: "Listado de Centros"
    };
    $scope.centros = centros.data.data;

    $scope.columnas = {
        nombre:'',
        clave:'',
        direccion:'',
        telefono:'',
        telefono2:'',
        codigo_postal:'',
        latitud:'',
        longitud:'',
        hora_apertura:'',
        hora_cierre:''
    };
    
    $scope.init = function () {
        obtenerCentros();
    }

    $scope.cambiarVisibilidadColumna = function (columna) {
        $scope.columnas[columna] = $scope.columnas[columna].length == 0 ? "hidden-phone" : "";
    }

    $scope.visibilidadColumna = function (columna) {
        return $scope.columnas[columna].length == 0 ? true : false;
    }

    $scope.modificarDatosCentro = function (id) {
        centrosDataFactory.centros=$scope.centros;
        document.location.href=$location.absUrl().replace($location.$$url,'/centro/modificar/'+id);
    }
    $scope.eliminarDatosCentro=function(id){
        $scope.idEliminar = id;
        $scope.modal      = modalService.construct({
                tituloContent:'Eliminar un Centro',
                bodyContent:'¿Estás seguro que quieres eliminar este Centro del sistema?',
                colorModal:modalService.typeColors.guinda,
                botonAceptar:true,
                mensajeBotonCancelar:'Cancelar',
                mensajeBotonAceptar:'Eliminar',
                botonCancelar:true,
                callbackBotonAceptar:eliminarCentro
        });
            modalService.mostrar();         
    }
    
    var eliminarCentro = function(){
        centroService.delete($scope.idEliminar,function(response){
            modalService.close();
            obtenerCentros();
        },function(errorResponse){
            $scope.modal = modalService.construct({tituloContent:'Error al eliminar el Centro',bodyContent:errorResponse.data.message,colorModal:modalService.typeColors.guinda,botonAceptar:false,mensajeBotonCancelar:'Aceptar'});
            modalService.mostrar();
        });        
    }
    
    var obtenerCentros=function(){
        centroService.list("centro.activo=true",'centro.nombre|asc', function (result) {
            $scope.centros = result.data;
        }, function (errorResult) {
            alert(errorResult.message);
        });
    }
});