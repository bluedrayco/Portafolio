principal.controller('listaProfesorController', function (
        $scope, 
        $location,
        commonService, 
        sesionDataFactory,
        profesoresDataFactory, 
        centros,
        modalService,
        profesorService) {
    $scope.pagina = {
        titulo: "Listado de Profesores"
    };
    $scope.centros = centros.data.data;

    $scope.columnas = {
        nombre: '',
        apellido_paterno: '',
        apellido_materno: '',
        curp: 'hidden-phone',
        email: 'hidden-phone',
        edad:'hidden-phone',
        centro_id: sesionDataFactory.usuario.multicentro ==false?'hidden-phone':'',
        telefono_particular: 'hidden-phone',
        telefono_celular: 'hidden-phone',
        fecha_nacimiento: 'hidden-phone',
        direccion: 'hidden-phone'
    };
    
    $scope.init = function () {
        obtenerProfesores();
    }

    $scope.cambiarVisibilidadColumna = function (columna) {
        $scope.columnas[columna] = $scope.columnas[columna].length == 0 ? "hidden-phone" : "";
    }

    $scope.visibilidadColumna = function (columna) {
        return $scope.columnas[columna].length == 0 ? true : false;
    }
    $scope.obtenerCentroById = function (centro_id) {
        for (i = 0; i < $scope.centros.length; i++) {
            if ($scope.centros[i].id == centro_id)
                return $scope.centros[i].nombre;
        }
    }

    $scope.modificarDatosProfesor = function (id) {
        profesoresDataFactory.profesores=$scope.profesores;
        document.location.href=$location.absUrl().replace($location.$$url,'/profesor/modificar/'+id);
    }
    $scope.eliminarDatosProfesor=function(id){
        $scope.idEliminar = id;
        $scope.modal      = modalService.construct({
                tituloContent:'Eliminar un Profesor',
                bodyContent:'¿Estás seguro que quieres eliminar este Profesor del sistema?',
                colorModal:modalService.typeColors.guinda,
                botonAceptar:true,
                mensajeBotonCancelar:'Cancelar',
                mensajeBotonAceptar:'Eliminar',
                botonCancelar:true,
                callbackBotonAceptar:eliminarProfesor
        });
            modalService.mostrar();         
    }
    
    var eliminarProfesor = function(){
        profesorService.delete($scope.idEliminar,function(response){
            modalService.close();
            obtenerProfesores();
        },function(errorResponse){
            $scope.modal = modalService.construct({tituloContent:'Error al eliminar al Profesor',bodyContent:errorResponse.data.message,colorModal:modalService.typeColors.guinda,botonAceptar:false,mensajeBotonCancelar:'Aceptar'});
            modalService.mostrar();
        });        
    }
    
    var obtenerProfesores=function(){
        var condicionalCentro = sesionDataFactory.usuario.multicentro == false ? " and profesor.centro_id=" + sesionDataFactory.usuario.centro_id : "";
        profesorService.list("profesor.activo=true" + condicionalCentro,'centro.nombre|asc', function (result) {
            $scope.profesores = result.data;
        }, function (errorResult) {
            alert(errorResult.message);
        });
    }
    $scope.obtenerEdadProfesor = function(fecha_nac){
        var fecha_nacimiento = new Date(fecha_nac);
        var hoy = new Date();
        return parseInt((hoy -fecha_nacimiento)/365/24/60/60/1000);
    }
});