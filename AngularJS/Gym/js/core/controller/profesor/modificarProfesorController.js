principal.controller('modificarProfesorController', function ($scope, $routeParams, $location, commonService, sesionDataFactory, profesoresDataFactory, centros,profesorService,modalService) {
    $scope.pagina = {
        titulo: "Modificación de un Profesor"
    };
    $scope.centros = centros.data.data;

    $scope.profesorInput = {
        nombre: "",
        apellido_paterno: "",
        apellido_materno: "",
        curp: "",
        telefono_celular: "",
        telefono_particular: "",
        email: "",
        direccion: "",
        fecha_nacimiento: "",
        centro_id: sesionDataFactory.usuario.centro_id,
        activo: true
    };



    $scope.init = function () {
        if (profesoresDataFactory.profesores[0].id == 0 || $routeParams.id < 0) {
            document.location.href = $location.absUrl().replace($location.$$url, '');
        } else {
            $scope.profesorInput = obtenerProfesor(profesoresDataFactory.profesores, $routeParams.id);
            $scope.profesorInput.fecha_nacimiento = new Date($scope.profesorInput.fecha_nacimiento);
            fechaNacimientoProfesor=$scope.profesorInput.fecha_nacimiento;
        }
    }

    $scope.almacenarProfesor = function (profesor) {
        profesor.fecha_nacimiento=$('#datetimepicker1').data('DateTimePicker').date().format("YYYY-MM-DD");
        profesorService.update(profesor.id, profesor, function (result) {
            alert("Se modificó correctamente al profesor...");
            $scope.profesorInput = commonService.limpiarObjeto(profesor);
            $scope.profesorInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema tratar de modificar al profesor...");
        });
    }
    $scope.limpiarProfesor = function (profesor) {
        $scope.profesorInput = commonService.limpiarObjeto(profesor);
        $scope.profesorInput.activo = true;
    }

    $scope.mostrarCampoCentro = function () {
        return sesionDataFactory.usuario.multicentro == 1 ? true : false;
    }

    obtenerProfesor = function (profesores, id) {
        for (i = 0; i < profesores.length; i++) {
            if (profesores[i].id == id) {
                return profesores[i];
            }
        }
    }
});