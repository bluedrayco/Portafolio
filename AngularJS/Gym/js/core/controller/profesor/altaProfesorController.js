principal.controller('altaProfesorController', function ($scope, commonService, sesionDataFactory, centros, profesorService) {
    $scope.pagina = {
        titulo: "Alta de un profesor"
    };
    $scope.centros = centros.data.data;

    $scope.profesorInput = {
        nombre: "",
        apellido_paterno: "",
        apellido_materno: "",
        curp: "",
        telefono_celular: "",
        telefono_particular: "",
        email: "",
        direccion: "",
        fecha_nacimiento: "",
        centro_id: sesionDataFactory.usuario.centro_id,
        activo: true
    };



    $scope.init = function () {
        fechaNacimientoProfesor = new Date();
    }

    $scope.almacenarProfesor = function (profesor) {
        profesor.fecha_nacimiento = $('#datetimepicker1').data('DateTimePicker').date().format("YYYY-MM-DD");
        profesorService.create(profesor, function (result) {
            alert("Se almacenó correctamente el profesor...");
            $scope.profesorInput = commonService.limpiarObjeto(profesor);
            $scope.profesorInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema al almacenar al profesor...");
        });
    }
    $scope.limpiarProfesor = function (profesor) {
        $scope.profesorInput = commonService.limpiarObjeto(profesor);
        $scope.profesorInput.activo = true;
    }

    $scope.mostrarCampoCentro = function () {
        return sesionDataFactory.usuario.multicentro == 1 ? true : false;
    }
});