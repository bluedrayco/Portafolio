principal.controller('altaAlumnoController', function ($scope, commonService,sesionDataFactory,centros,alumnoService) {
    $scope.pagina={
        titulo:"Alta de un alumno"
    };
    $scope.centros=centros.data.data;
    
    $scope.alumnoInput = {
        nombre: "",
        apellido_paterno: "",
        apellido_materno: "",
        email: "",
        direccion: "",
        telefono_celular: "",
        telefono_particular: "",
        fecha_nacimiento: "",
        centro_id:sesionDataFactory.usuario.centro_id,
        activo: true
    };
    
    
    
    $scope.init = function () {
        fechaNacimientoAlumno=new Date();
    }
    
    $scope.almacenarAlumno = function (alumno) {
        alumno.fecha_nacimiento=$('#datetimepicker1').data('DateTimePicker').date().format("YYYY-MM-DD");
        alumnoService.create(alumno, function (result) {
            alert("Se almacenó correctamente al alumno...");
            $scope.alumnoInput = commonService.limpiarObjeto(alumno);
            $scope.alumnoInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema al almacenar al alumno...");
        });
    }
    $scope.limpiarAlumno = function (alumno) {
        $scope.alumnoInput = commonService.limpiarObjeto(alumno);
        $scope.alumnoInput.activo = true;
    }
    
    $scope.mostrarCampoCentro = function(){
        return sesionDataFactory.usuario.multicentro==1?true:false;
    }
});