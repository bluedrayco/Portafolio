principal.controller('listaAlumnoController', function (
        $scope, 
        $location,
        commonService, 
        sesionDataFactory,
        alumnosDataFactory, 
        centros,
        modalService,
        alumnoService) {
    $scope.pagina = {
        titulo: "Listado de Alumnos"
    };
    $scope.centros = centros.data.data;

    $scope.columnas = {
        nombre: '',
        apellido_paterno: '',
        apellido_materno: '',
        edad: 'hidden-phone',
        email: 'hidden-phone',
        centro_id: sesionDataFactory.usuario.multicentro ==false?'hidden-phone':'',
        telefono_particular: 'hidden-phone',
        telefono_celular: 'hidden-phone',
        fecha_nacimiento: 'hidden-phone',
        direccion: 'hidden-phone',
    };
    
    $scope.init = function () {
        obtenerAlumnos();
    }

    $scope.cambiarVisibilidadColumna = function (columna) {
        $scope.columnas[columna] = $scope.columnas[columna].length == 0 ? "hidden-phone" : "";
    }

    $scope.visibilidadColumna = function (columna) {
        return $scope.columnas[columna].length == 0 ? true : false;
    }
    $scope.obtenerCentroById = function (centro_id) {
        for (i = 0; i < $scope.centros.length; i++) {
            if ($scope.centros[i].id == centro_id)
                return $scope.centros[i].nombre;
        }
    }

    $scope.modificarDatosAlumno = function (id) {
        alumnosDataFactory.alumnos=$scope.alumnos;
        document.location.href=$location.absUrl().replace($location.$$url,'/alumno/modificar/'+id);
    }
    $scope.eliminarDatosAlumno=function(id){
        $scope.idEliminar = id;
        $scope.modal      = modalService.construct({
                tituloContent:'Eliminar un Alumno',
                bodyContent:'¿Estás seguro que quieres eliminar este Alumno del sistema?',
                colorModal:modalService.typeColors.guinda,
                botonAceptar:true,
                mensajeBotonCancelar:'Cancelar',
                mensajeBotonAceptar:'Eliminar',
                botonCancelar:true,
                callbackBotonAceptar:eliminarAlumno
        });
            modalService.mostrar();         
    }
    
    var eliminarAlumno = function(){
        alumnoService.delete($scope.idEliminar,function(response){
            modalService.close();
            obtenerAlumnos();
        },function(errorResponse){
            $scope.modal = modalService.construct({tituloContent:'Error al eliminar al Alumno',bodyContent:errorResponse.data.message,colorModal:modalService.typeColors.guinda,botonAceptar:false,mensajeBotonCancelar:'Aceptar'});
            modalService.mostrar();
        });        
    }
    
    var obtenerAlumnos=function(){
        var condicionalCentro = sesionDataFactory.usuario.multicentro == false ? " and alumno.centro_id=" + sesionDataFactory.usuario.centro_id : "";
        alumnoService.list("alumno.activo=true" + condicionalCentro,'centro.nombre|asc', function (result) {
            $scope.alumnos = result.data;
        }, function (errorResult) {
            alert(errorResult.message);
        });
    }
    $scope.obtenerEdadAlumno = function(fecha_nac){
        var fecha_nacimiento = new Date(fecha_nac);
        var hoy = new Date();
        return parseInt((hoy -fecha_nacimiento)/365/24/60/60/1000);
    }
});