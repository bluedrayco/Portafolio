principal.controller('modificarAlumnoController', function ($scope, $routeParams, $location, commonService, sesionDataFactory, alumnosDataFactory, centros,alumnoService,modalService) {
    $scope.pagina = {
        titulo: "Modificación de un Alumno"
    };
    $scope.centros = centros.data.data;

    $scope.alumnoInput = {
        nombre: "",
        apellido_paterno: "",
        apellido_materno: "",
        email: "",
        direccion: "",
        telefono_celular: "",
        telefono_particular: "",
        fecha_nacimiento: "",
        centro_id: 0,
        activo: true
    };



    $scope.init = function () {
        if (alumnosDataFactory.alumnos[0].centro_id == 0 || $routeParams.id < 0) {
            document.location.href = $location.absUrl().replace($location.$$url, '');
        } else {
            $scope.alumnoInput = obtenerAlumno(alumnosDataFactory.alumnos, $routeParams.id);
            $scope.alumnoInput.fecha_nacimiento = new Date($scope.alumnoInput.fecha_nacimiento);
            fechaNacimientoAlumno=$scope.alumnoInput.fecha_nacimiento;
        }
    }

    $scope.almacenarAlumno = function (alumno) {
        alumno.fecha_nacimiento=$('#datetimepicker1').data('DateTimePicker').date().format("YYYY-MM-DD");
        alumnoService.update(alumno.id, alumno, function (result) {
            alert("Se modificó correctamente el alumno...");
            $scope.alumnoInput = commonService.limpiarObjeto(alumno);
            $scope.alumnoInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema tratar de modificar al alumno...");
        });
    }
    $scope.limpiarAlumno = function (alumno) {
        $scope.alumnoInput = commonService.limpiarObjeto(alumno);
        $scope.alumnoInput.activo = true;
    }

    $scope.mostrarCampoCentro = function () {
        return sesionDataFactory.usuario.multicentro == 1 ? true : false;
    }

    obtenerAlumno = function (alumnos, id) {
        for (i = 0; i < alumnos.length; i++) {
            if (alumnos[i].id == id) {
                return alumnos[i];
            }
        }
    }
});