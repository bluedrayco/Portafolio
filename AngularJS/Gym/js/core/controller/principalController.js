principal.controller('principalController', function ($scope, $location, $cookieStore, $interval, genericNewRestFactory, centrosDataFactory,sesionDataFactory) {

    $scope.init = function () {
        if (validacionSesionActiva()) {
            $scope.usuario = $cookieStore.get('usuario');
            $scope.permiso = $cookieStore.get('permiso');
            $scope.credencial = $cookieStore.get('credencial');
            almacenarDatosUsuario($scope.usuario,$scope.permiso,$scope.credencial);
            obtenerCatalogos();
            $interval(messengerPolling, 5000);
            messengerPolling();
        } else {
            document.location.href = $location.absUrl().replace('usuario.html', 'index.html');
        }
    }
    $scope.logout = function () {
        $cookieStore.remove("usuario");
        $cookieStore.remove("permiso");
        $cookieStore.remove("credencial");
        document.location.href = $location.absUrl().replace('usuario.html', 'index.html');
    }

    validacionSesionActiva = function () {
        if ($cookieStore.get('usuario') == undefined || $cookieStore.get('permiso') == undefined || $cookieStore.get('credencial') == undefined) {
            return false;
        } else {
            return true;
        }
    }
    messengerPolling = function () {
        genericNewRestFactory.get({resource: 'comentario', type: 'generic', filter: "usuario_id=" + $scope.usuario.id + " and activo=true"}, function (result) {
            $scope.mensajes = result.data;
        }
        );
    }
    $scope.mostrarMenuAlumnos = function () {
        if (sesionDataFactory.permiso.listar_alumnos
                || sesionDataFactory.permiso.crear_alumno
                ) {
            return true;
        } else {
            return false;
        }
    }

    $scope.mostrarMenuCentros = function () {
        if (sesionDataFactory.permiso.listar_centros
                || sesionDataFactory.permiso.crear_centro
                ) {
            return true;
        } else {
            return false;
        }
    }   
    $scope.mostrarMenuProfesores = function () {
        if (sesionDataFactory.permiso.listar_profesores
                || sesionDataFactory.permiso.crear_profesor
                ) {
            return true;
        } else {
            return false;
        }
    }  
    $scope.mostrarMenuUsuarios = function () {
        if (sesionDataFactory.permiso.listar_usuarios
                || sesionDataFactory.permiso.crear_usuario
                ) {
            return true;
        } else {
            return false;
        }
    }  
    
    obtenerCentros = function () {
        genericNewRestFactory.get({resource: 'centro', type: 'generic', filter: "activo=true"}, function (result) {
            if (result.data.length != 0) {
                centrosDataFactory.centros = result.data;
            } else {
                alert("No se encontró el centro en la base de datos...");
            }
        }, function (errorResult) {
            alert(errorResult.message);
        });
    }

    obtenerCatalogos = function (usuario) {
        obtenerCentros();
    }
    
    almacenarDatosUsuario=function (usuario,permiso,credencial){
        sesionDataFactory.usuario=usuario;
        sesionDataFactory.permiso=permiso;
        sesionDataFactory.credencial=credencial;
    }
});
