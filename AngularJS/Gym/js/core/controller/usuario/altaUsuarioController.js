principal.controller('altaUsuarioController', function ($scope, commonService, sesionDataFactory, centros,usuarioService) {
    $scope.pagina = {
        titulo: "Alta de un usuario"
    };
    $scope.centros = centros.data.data;

    $scope.usuarioInput = {
        nombre: "",
        apellido_paterno: "",
        apellido_materno: "",
        multicentro: false,
        permiso_id: 0,
        centro_id: sesionDataFactory.usuario.centro_id,
        activo: true
    };

    $scope.credencialInput={
        username:"",
        pwd:"",
        activo:true,
        access_key:"",
        usuario_id:0,
        profesor_id:null,
        alumno_id:null
    };

    $scope.init = function () {
    }

    $scope.almacenarProfesor = function (profesor) {
        profesor.fecha_nacimiento = $('#datetimepicker1').data('DateTimePicker').date().format("YYYY-MM-DD");
        profesorService.create(profesor, function (result) {
            alert("Se almacenó correctamente el profesor...");
            $scope.profesorInput = commonService.limpiarObjeto(profesor);
            $scope.profesorInput.activo = true;
            $('#datetimepicker1').data('DateTimePicker').date(new Date());
        }, function (errorResult) {
            alert("Hubo un problema al almacenar al profesor...");
        });
    }
    $scope.limpiarProfesor = function (profesor) {
        $scope.profesorInput = commonService.limpiarObjeto(profesor);
        $scope.profesorInput.activo = true;
    }

    $scope.mostrarCampoCentro = function () {
        return sesionDataFactory.usuario.multicentro == 1 ? true : false;
    }
});