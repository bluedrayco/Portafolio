principal.controller('loginController', function (modalService, $scope, genericNewRestFactory, commonService, $location, $cookieStore) {
    $scope.usuarioInput = {};
    $scope.registroUsuario = {};

    $scope.init = function () {
        $scope.usuarioInput = {};
        if ($location.absUrl().indexOf("index.html") == -1) {
            document.location.href=$location.absUrl().replace($location.absUrl(), $location.absUrl()+"index.html");
        }
    };

    $scope.loginAcceso = function () {
        obtenerDatosUsuario($scope.usuarioInput.usuario, $scope.usuarioInput.password);
    }

    obtenerDatosUsuario = function (usuario, pass) {
        genericNewRestFactory.get({resource: 'credencial', type: 'generic', filter: "username='" + usuario + "' and pwd='" + pass + "' and activo=true"}, function (result) {
            if (result.data.length != 0) {
                $scope.credencial = result.data[0];
                if ($scope.credencial.usuario_id != null) {
                    obtenerUsuario($scope.credencial.usuario_id);
                }
            } else {
                mostrarModalError('Mensaje inicio de Sesión', 'El usuario o contraseña no es válido...');
            }
        }, function (errorResult) {
            alert(errorResult.message);
        });

    }
    obtenerUsuario = function (usuario_id) {
        genericNewRestFactory.get({resource: 'usuario', type: 'generic', filter: "id=" + usuario_id}, function (result) {
            if (result.data.length != 0) {
                $scope.usuario = result.data[0];
                obtenerPermiso($scope.usuario.permiso_id);
            } else {
                mostrarModalError("Problema con usuario", 'Hubo un error al obtener el usuario ya que no existe en la base de datos...');
            }
        }, function (errorResult) {
            mostrarModalError("Problema con usuario", 'Hubo un error al obtener el usuario ya que no existe en la base de datos...');
        });
    }

    obtenerPermiso = function (permiso_id) {
        genericNewRestFactory.get({resource: 'permiso', type: 'generic', filter: "id=" + permiso_id}, function (result) {
            if (result.data.length != 0) {
                $scope.permiso = result.data[0];
                $cookieStore.put('usuario', $scope.usuario);
                $cookieStore.put('permiso', $scope.permiso);
                $cookieStore.put('credencial', $scope.credencial);
                $scope.usuarioInput = commonService.limpiarObjeto($scope.usuarioInput);
                if ($location.absUrl().indexOf("index.html") != -1) {
                    document.location.href = $location.absUrl().replace('index.html', 'usuario.html');
                } else {
                    document.location.href = $location.absUrl() + "index.html";
                }
            } else {
                mostrarModalError("Problema con permisos", 'Hubo un error al obtener los permisos ya que este usuario no los posee en la base de datos...');
            }
        }, function (errorResult) {
            mostrarModalError("Problema con permisos", 'Hubo un error al obtener los permisos ya que este usuario no los posee en la base de datos...');
        });
    }
    mostrarModalError = function (title, content) {
        $scope.modal = modalService.construct({tituloContent: title, bodyContent: content, colorModal: modalService.typeColors.guinda, botonAceptar: false, mensajeBotonCancelar: 'Aceptar'});
        modalService.mostrar();
        $scope.usuarioInput = commonService.limpiarObjeto($scope.usuarioInput);
    }

    $scope.crearUsuario = function (registroUsuario) {
        $scope.registroUsuario.rol_id = rolEstudiante.id;
        genericNewRestFactory.create({resource: 'usuario', type: 'generic'}, $scope.registroUsuario, function (result) {
            $('#registroSistema').modal('hide');
            $scope.modal = modalService.construct({tituloContent: 'Nuevo aspirante a beca creado', bodyContent: 'Has creado tu cuenta de forma exitosa, ingresa al sistema para solicitar una beca o transferencia a otra beca.', colorModal: modalService.typeColors.guinda, botonAceptar: false, mensajeBotonCancelar: 'Aceptar'});
            modalService.mostrar();
            $scope.registroUsuario = {};
        }, function (errorResult) {
            $scope.modal = modalService.construct({tituloContent: 'Error al crear un aspirante a beca', bodyContent: 'Ocurrió un error al tratar de crear un aspirante a beca.', colorModal: modalService.typeColors.guinda, botonAceptar: false, mensajeBotonCancelar: 'Aceptar'});
            modalService.mostrar();
        });
    }
});


