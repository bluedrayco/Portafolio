principal.factory('centroRestFactory',function($resource){
        return $resource(rootSystem+'/rest.php/api/v1/entity/centro/:id',{},{
            delete:{
                method      : 'DELETE',
                transformResponse : function(data, headers){
                    return angular.fromJson(data);
                }
            },
            create:{
                method      : 'POST',
                transformResponse : function(data, headers){
                    return angular.fromJson(data);
                }
            },
            update:{
                method      : 'PUT',
                transformResponse : function(data, headers){
                    return angular.fromJson(data);
                }
            },
        });
});