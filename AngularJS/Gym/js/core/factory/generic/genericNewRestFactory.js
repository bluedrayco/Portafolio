principal.factory('genericNewRestFactory',function($resource,$location){
        return $resource(rootSystem+'/rest.php/api/v1/:type/:resource/:id',{},{
            delete:{
                method      : 'DELETE',
                transformResponse : function(data, headers){
                    return angular.fromJson(data);
                }
            },
            create:{
                method      : 'POST',
                transformResponse : function(data, headers){
                    return angular.fromJson(data);
                }
            },
            update:{
                method      : 'PUT',
                transformResponse : function(data, headers){
                    return angular.fromJson(data);
                }
            },
        });
});