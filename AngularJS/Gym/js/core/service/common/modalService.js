principal.service('modalService',function(){
    this.typeColors={
        error:'#FF0000',
        warning:'#0034FF',
        guinda:'#8E2522',
        green:'#39DE41',
        white:'#FFFFFF'
    };
    this.modal={
        modalId:"customModal",
        modalHeaderId:'modalHeader',
        botonCandelarId:'botonCancelar',
        botonAceptarId:'botonAceptar',
        colorModal:this.typeColors.guinda,
        colorBotonCancelar:this.typeColors.white,
        colorBotonAceptar:this.typeColors.green,
        titulo:true,
        header:true,
        body:true,
        footer:true,
        botonClose:true,
        botonCancelar:true,
        botonAceptar:true,
        tituloContent:"",
        bodyContent:"",
        mensajeBotonCancelar:"Cancelar",
        mensajeBotonAceptar:"Aceptar",
        callbackBotonCancelar:function(){},
        callbackBotonAceptar:function(){},
        callbackIniciarModal:function(){},
        callbackSalirModal:function(){}
    };
    this.construct = function(params){
        if(params==undefined){
            var params={};
        }
        var modal={};
        $.extend(this.modal,params);
        $("#"+this.modal.modalHeaderId).css({'background':this.modal.colorModal});
        $("#"+this.modal.botonCandelarId).css({'background':this.modal.colorBotonCancelar});
        $("#"+this.modal.botonAceptarId).css({'background':this.modal.colorBotonAceptar});
        $("#"+this.modal.modalId).on('show.bs.modal',this.modal.callbackIniciarModal());
        $("#"+this.modal.modalId).on('hide.bs.modal',this.modal.callbackSalirModal());
        
        $("#"+this.modal.botonAceptarId).css({'background':this.modal.colorBotonAceptar});        
        //$('.'+$('#'+this.modal.modalHeaderId).attr('class').replace(' ng-hide','')).css({'background':this.modal.color});
        return this.modal;
    }
    this.mostrar = function (){
        $("#"+this.modal.modalId).modal('show');
    }
    this.close  =function(){
        $("#"+this.modal.modalId).modal('hide');
    }
});