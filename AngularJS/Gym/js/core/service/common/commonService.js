principal.service('commonService', function () {
    this.limpiarObjeto = function (objeto) {
        for (i in objeto) {
            switch (typeof (objeto[i])) {
                case 'object':
                    if (this.is_array(objeto[i])) {
                        objeto[i] = [];
                    } else {
                        if (this.is_date(objeto[i])) {
                            objeto[i] = new Date();
                        } else {
                            objeto[i] = {};
                        }
                    }
                    break;

                case 'number':
                    objeto[i] = 0;
                    break;

                case 'string':
                    objeto[i] = '';
                    break;

                case 'boolean':
                    objeto[i] = false;
                    break;
            }
        }
        return objeto;
    }
    this.is_array = function (objeto) {
        return (typeof (objeto) == 'object' && (objeto instanceof Array));
    }

    this.transformBoolean = function (objeto) {
        for (i in objeto) {
            if (i != 'id' && i != 'activo') {
                switch (objeto[i]) {
                    case '0':
                        objeto[i] = false;
                        break;

                    case '1':
                        objeto[i] = true;
                        break;
                }
            }
        }
        return objeto;
    }

    this.is_date = function (objeto) {
        return objeto instanceof Date ? true : false;
    }
});