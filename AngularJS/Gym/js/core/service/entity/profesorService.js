principal.service('profesorService',['genericNewRestFactory','profesorRestFactory',function(genericNewRestFactory,profesorRestFactory){
    this.list = function(filter,order,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'profesor',type:'generic',filter:filter,order:order},responseCallback,errorResponseCallback);
    };
    
    this.delete=function(id,responseCallback,errorResponseCallback){
        profesorRestFactory.delete({id:id},responseCallback,errorResponseCallback);
    }
    
    this.getById = function(id,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'profesor',type:'generic',filter:'id='+id},responseCallback,errorResponseCallback);
    };
    
    this.create = function(profesor,responseCallback,errorResponseCallback){
         genericNewRestFactory.create({resource: 'profesor', type: 'generic'},profesor, responseCallback,errorResponseCallback);
    }

    this.update = function(profesorId,profesor,responseCallback,errorResponseCallback){
        genericNewRestFactory.update({resource: 'profesor', type: 'generic',id:profesorId}, profesor,responseCallback,errorResponseCallback);
    }

}]);