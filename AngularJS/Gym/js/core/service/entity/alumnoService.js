principal.service('alumnoService',['genericNewRestFactory','alumnoRestFactory',function(genericNewRestFactory,alumnoRestFactory){
    this.list = function(filter,order,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'alumno',type:'generic',filter:filter,order:order},responseCallback,errorResponseCallback);
    };
    
    this.delete=function(id,responseCallback,errorResponseCallback){
        alumnoRestFactory.delete({id:id},responseCallback,errorResponseCallback);
    }
    
    this.getById = function(id,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'alumno',type:'generic',filter:'id='+id},responseCallback,errorResponseCallback);
    };
    
    this.create = function(alumno,responseCallback,errorResponseCallback){
         genericNewRestFactory.create({resource: 'alumno', type: 'generic'},alumno, responseCallback,errorResponseCallback);
    }

    this.update = function(alumnoId,alumno,responseCallback,errorResponseCallback){
        genericNewRestFactory.update({resource: 'alumno', type: 'generic',id:alumnoId}, alumno,responseCallback,errorResponseCallback);
    }

}]);