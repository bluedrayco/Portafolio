principal.service('rolService',['genericNewRestFactory','rolRestFactory',function(genericNewRestFactory,rolRestFactory){
    this.list = function(order,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'alumno',type:'generic',filter:'alumno.activo=true',order:order},responseCallback,errorResponseCallback);
    };
    
    this.delete=function(id,responseCallback,errorResponseCallback){
        rolRestFactory.delete({id:id},responseCallback,errorResponseCallback);
    }
    
    this.getById = function(id,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'rol',type:'generic',filter:'rol.id='+id},responseCallback,errorResponseCallback);
    };
}]);