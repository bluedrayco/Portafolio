principal.service('usuarioService',['genericNewRestFactory','usuarioRestFactory',function(genericNewRestFactory,usuarioRestFactory){
    this.list = function(filter,order,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'usuario',type:'generic',filter:filter,order:order},responseCallback,errorResponseCallback);
    };
    
    this.delete=function(id,responseCallback,errorResponseCallback){
        usuarioRestFactory.delete({id:id},responseCallback,errorResponseCallback);
    }
    
    this.getById = function(id,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'usuario',type:'generic',filter:'usuario.id='+id},responseCallback,errorResponseCallback);
    };
    
    this.create = function(usuario,responseCallback,errorResponseCallback){
         genericNewRestFactory.create({resource: 'usuario', type: 'generic'},usuario, responseCallback,errorResponseCallback);
    }

    this.update = function(usuarioId,usuario,responseCallback,errorResponseCallback){
        genericNewRestFactory.update({resource: 'usuario', type: 'generic',id:usuarioId}, usuario,responseCallback,errorResponseCallback);
    }

}]);