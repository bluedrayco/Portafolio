principal.service('centroService',['genericNewRestFactory','centroRestFactory',function(genericNewRestFactory,centroRestFactory){
    this.list = function(filter,order,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'centro',type:'generic',filter:filter,order:order},responseCallback,errorResponseCallback);
    };
    
    this.delete=function(id,responseCallback,errorResponseCallback){
        centroRestFactory.delete({id:id},responseCallback,errorResponseCallback);
    }
    
    this.getById = function(id,responseCallback,errorResponseCallback){
        genericNewRestFactory.get({resource:'centro',type:'generic',filter:'id='+id},responseCallback,errorResponseCallback);
    };

    this.create = function(centro,responseCallback,errorResponseCallback){
         genericNewRestFactory.create({resource: 'centro', type: 'generic'},centro, responseCallback,errorResponseCallback);
    }

    this.update = function(centroId,centro,responseCallback,errorResponseCallback){
        genericNewRestFactory.update({resource: 'centro', type: 'generic',id:centroId}, centro,responseCallback,errorResponseCallback);
    }
}]);