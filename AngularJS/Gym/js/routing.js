principal.config(function ($routeProvider) {
    $routeProvider
            .when('/',
                    {
                        templateUrl: "frontend/views/core/partials/home.html",
                        controller: "homeController"
                    }
            )
            .when('/alumno/alta',
                    {
                        templateUrl: "frontend/views/core/partials/alumno/alta_alumno.html",
                        controller: "altaAlumnoController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/alumno/lista',
                    {
                        templateUrl: "frontend/views/core/partials/alumno/lista_alumno.html",
                        controller: "listaAlumnoController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/alumno/modificar/:id',
                    {
                        templateUrl: "frontend/views/core/partials/alumno/modificar_alumno.html",
                        controller: "modificarAlumnoController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/centro/alta',
                    {
                        templateUrl: "frontend/views/core/partials/centro/alta_centro.html",
                        controller: "altaCentroController",
                    }
            )
            .when('/centro/lista',
                    {
                        templateUrl: "frontend/views/core/partials/centro/lista_centro.html",
                        controller: "listaCentroController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/centro/modificar/:id',
                    {
                        templateUrl: "frontend/views/core/partials/centro/modificar_centro.html",
                        controller: "modificarCentroController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/profesor/alta',
                    {
                        templateUrl: "frontend/views/core/partials/profesor/alta_profesor.html",
                        controller: "altaProfesorController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/profesor/lista',
                    {
                        templateUrl: "frontend/views/core/partials/profesor/lista_profesor.html",
                        controller: "listaProfesorController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/profesor/modificar/:id',
                    {
                        templateUrl: "frontend/views/core/partials/profesor/modificar_profesor.html",
                        controller: "modificarProfesorController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/usuario/alta',
                    {
                        templateUrl: "frontend/views/core/partials/usuario/alta_usuario.html",
                        controller: "altaUsuarioController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/usuario/lista',
                    {
                        templateUrl: "frontend/views/core/partials/usuario/lista_usuario.html",
                        controller: "listaUsuarioController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .when('/usuario/modificar/:id',
                    {
                        templateUrl: "frontend/views/core/partials/usuario/modificar_usuario.html",
                        controller: "modificarUsuarioController",
                        resolve: {
                            centros: function (catalogsServices) {
                                return catalogsServices.getCentros();
                            }

                        }
                    }
            )
            .otherwise({
                redirectTo: "/"
            });
});
