principal.factory('catalogsServices', ['$http', function($http) {
  var sdo = {
    getCentros: function() {
      var promise = $http({ 
        method: 'GET', 
        url: rootSystem+'/rest.php/api/v1/generic/centro?filter=centro.activo=true' 
      });
      promise.success(function(data, status, headers, conf) {
        return data.data;
      });
      return promise;
    }
  }
  return sdo;
}]);