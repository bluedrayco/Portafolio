## Gym 
<br/><br/>
Gym Program, This code was generated to control some issues, such as: assistance, manage courses, clients, professors, centers, etc.<br/>
It uses the MVC pattron using AngularJS 1.x with Bootstrap 3 and PHP7.0 as backend, the interface between the two parts was created using 
RESTFul.<br/>

**index.html:** This is the main file, it calls all the Javascript files and other dependencies such as Bootstrap or AngularJS.<br/>
**usuario.html:** This is the dashboard for gym's employees.<br/>
**views/core/forms/*:** Contains the forms that are displayed to the users for create and edit resources.<br/>
**views/core/partials/*:** Contains the general structure which is called by routing in each route, these views call the init method to initialize each controller.<br/>
**modals/*:** Contains all the modals in the application such as custom modals, modals to recover password, new user in the system, access login, etc.<br/>
**structure/*:** Contains the top baners for showing the pending tasks or notifications.<br/>
**js/principal.js:** It creates the angular application and enable the dependencies such as ng-route.<br/>
**js/routing.js:** It contains all the routes for the application with their own controllers.<br/>
**js/init/catalogsServices.js:** Contains the factory that obtains active centers.<br/>
**js/core/controller/*:** Contains controllers for the routes or banners, they use factories and services.<br/>
**js/core/factory/*:** Contains the url to the rests and config each http method to use, also it contains the genericNewRestFactory, it's a special factory for comsumes the RestFul generic rest in backend.<br/>
**js/core/service/*:** Contains two directories, the common contains services that they not enter to the database like modals and methods to manipulate dates and the entity directory contains the factories to consume each Rest services for the backend, a service could use one or more of factories.<br/>
