<?php

namespace Modules\V1\Lib;

class Tracker {

    const VARIABLE_PARSING = "Parseo de variables";
    const SELECT_PRINTER_TYPE = "Seleccion de Impresora";
    const TEMPLATE_RENDERER = "Generacion de template";
    const FILE_GENERATION = "Generacion de archivo";
    const PRINTER_FILE = "Impresion de archivo";

    private $tracking = null;

    public function __construct() {
        $this->tracking = '';
    }

    public function setState($state) {
        if ($this->tracking != "") {
            $this->tracking.= " , " . $this->tracking;
        }
    }

    public function getState() {
        return $this->tracking;
    }

}
