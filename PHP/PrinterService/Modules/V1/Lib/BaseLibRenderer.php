<?php

namespace Modules\V1\Lib;

use \Slim\Views\PhpRenderer;

class BaseLibRenderer extends PhpRenderer {

    public function __construct($c) {
        $this->container = $c;
        parent::__construct($this->container['renderer']->getTemplatePath());
    }

    public function render($template, array $data = []) {
        $functions = array('functions' => $this);
        $data = array_merge($data, $functions);
        $output = $this->fetch($template, $data);
        return $output;
    }

}