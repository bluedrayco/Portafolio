<?php

namespace Modules\V1\Lib;

use \Syllable as Syllable;


class Template extends BaseLibRenderer {

    public function __construct($c) {
        $this->newLine  = "\n";
        parent::__construct($c);
    }
    /**
     * Generate String than will use to send to print.
     * @dataString array from strings than wants to print
     * @dataConfig array from configurations. This must have 'length' and 'align' keywords with a respective array.
     * @spaceCharacter string define the char than replace on spaces.
     * @return string organized ready for send to print.
     * @return false if something was wrong
    */
    public function printArrayString($dataString, $dataConfig, $spaceCharacter = " "){



        if($this->validateArraysToPrint($dataString,$dataConfig) == false){     // Validate the structure from data and config array.
            return false;
        }


        $vtRows         = array();
        $result         = "";
        $countConfig    = count($dataConfig["length"]);                         // Length of columns than we going to print.
        $maxRows        = 0;                                                    // max legth of syllables arryas
        $maxLines       = count($dataString);

        foreach ($dataString as $lineIndex => $vtLines) {

            $vtRows     = array();

            if(count($vtLines) != $countConfig){                               // The number of string it's <> number of length and aling
                    // Trace log
                    return false;
            }

            foreach ($vtLines as $key => $string) {

                $vtRows[]   = $this->getStringAsSplit($string, $dataConfig["length"][$key]);
                $maxRows    = (count($vtRows[count($vtRows)-1]) > $maxRows)?(count($vtRows[count($vtRows)-1])):$maxRows;
            }

            $stringLength       = 0;
            $stringOrientation  = "left";
            $stringValue        = "";

            // We going to generate a full line with a spaces
            for($n = 0; $n < $maxRows; $n++){

                for ($i=0; $i < $countConfig; $i++) {

                    $stringLength         = $dataConfig["length"][$i];
                    $stringOrientation    = $dataConfig["align"][$i];
                    $stringValue          = "";

                    if(array_key_exists($n, $vtRows[$i])){
                        $stringValue      = $vtRows[$i][$n];
                    }
                    //Adjust the string
                    $result              .= $this->fillString($stringValue, $stringLength, $stringOrientation, $spaceCharacter);
                }
                $result                  .= $this->newLine;
            }
        }
        return $result;
    }



    private function fillString($string, $length, $orientation, $key=" "){
        //error_log("{$string}--{$length}");
        $result = "";
        switch ($orientation) {
            case 'left':
                 $result = $this->mb_str_pad($string, $length, $key);
                break;

            case 'right':
                 $result = $this->mb_str_pad($string, $length, $key, STR_PAD_LEFT);
                break;

            case 'center':
                 $result = $this->mb_str_pad($string, $length, $key, STR_PAD_BOTH);
                break;

            default:
                $result = $this->mb_str_pad($string, $length, $key);
                break;
        }

        return $result;
    }


    private function mb_str_pad( $input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT){
        $diff = strlen( $input ) - mb_strlen( $input );
        return str_pad( $input, $pad_length + $diff, $pad_string, $pad_type );
    }




    private function validateArraysToPrint($dataString, $dataConfig){

        // Validate the data variables structure.
        if(!array_key_exists("length", $dataConfig)){                           // IF length key it's missing on dataConfig array.
            //Trace log here
            return false;
        }

        if(!array_key_exists("align", $dataConfig)){                            // IF align key it's missing on dataConfig array.
            //Trace log here
            return false;
        }

        if(count($dataConfig["align"]) != count($dataConfig["length"])){        // IF number of config it's <> to number of lengths
            //Trace log here
            return false;
        }

        return true;

    }


    /**
      * Separate a string in array of string where each of the strings of array have a specific length.
      * @string  String than wan to separate.
      * @$length Length of each string than we wan to separate.
      * return Array of strings
    */
    public function getStringAsSplit($string, $length){


        $result         = array();
        $stringLine     = "";
        $counter        = 0;
        $syllables      = $this->getStringAsSyllable($string);                  //Get the syllables of string


        foreach ($syllables as $key => $value) {                                // generate a string <= $length

            if( (strlen($value) + $counter) < $length){

                $counter         += strlen($value);
                $stringLine      .= $value;

            }
            else{

                $counter          = strlen(trim($value));
                $stringLine      .= (($value[0]==" ")?"": "-");
                $result[]         = $stringLine;                                // Add the stringLine on result array.
                $stringLine       = trim($value);

            }
        }

        if($counter >0){
            $result[]             = $stringLine;                                // Add the rest of stringLine on result array.
        }

        return $result;
    }



    /**
    * Convert a string in a Syllable's array
    * @string String than we wan to separate.
    * return Array of syllables strings.
    */
    private function getStringAsSyllable($string){

        //Prepare the Syllable Library
        Syllable::setCacheDir(realpath('cache'));
        Syllable::setLanguageDir(realpath('languages'));

        $syllable             = new Syllable();
        $vtSyllables          = array();

        $syllable->setHyphen('-');
        $syllable->setLanguage('es');

        $tmpSyllables         = $syllable->splitText($string);                  // We have the syllables array

        foreach ($tmpSyllables as $key => $value) {                             // We going to find a space words to separate it.

            $syllable         = str_replace(" ", "@^~| "    , $value);          // We replace the space for a seed to split
            $tmp              = explode("@^~|"            , $syllable);
            $vtSyllables      = array_merge($vtSyllables    , $tmp);

        }

        return $vtSyllables;

    }

    public function printCharacter($character, $length){
        return $this->fillString($character, $length, "left", $character) . "\n";
    }
}