<?php

namespace Modules\V1\Controller;

use Modules\BaseRestController;
use Modules\V1\Lib\Template as LTemplate;
use Modules\V1\Lib\Tracker as LTracker;

final class Partner extends BaseRestController {

    public function postPrint() {
        $trackerLibrary = new LTracker();
        try {
            $data = $this->params['body'];
            $data['printer_config'] = $this->container->get('printers')[$this->params['body']['printer']]['partner'];
            $data['printer_config']['columns']=$this->container->get('printers')[$this->params['body']['printer']]['columns'];
            $data['config']=$this->container->get('parameters')['Configuration'];
//            print_r($data);
//            exit();
            $templateLibrary = new LTemplate($this->container);
            $dataFormatted = $templateLibrary->render("Partner.phtml", $data);

            file_put_contents("partner.txt", $dataFormatted);
        } catch (\Exception $ex) {
            $this->container->logger->error("Exception: ".$ex->getMessage()." Process: ".$trackerLibrary->getState());
        }
        $this->container->logger->info("Process: ".$trackerLibrary->getState());
        return $this->returnResponse(array('message' => 'ok'), self::HTTP_OK);
    }

}
