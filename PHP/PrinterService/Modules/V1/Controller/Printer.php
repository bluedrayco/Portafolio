<?php
namespace Modules\V1\Controller;
use Modules\BaseRestController;

final class Printer extends BaseRestController{

    public function getHolaMundo(){
        $this->container->logger->info("entre get...");
        $response = $this->returnResponse($this->container->get('printers'),  self::HTTP_OK);
        return $response;
    }

}