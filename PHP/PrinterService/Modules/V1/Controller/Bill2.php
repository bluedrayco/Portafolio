<?php

namespace Modules\V1\Controller;

use Modules\BaseRestController;
use Modules\V1\Lib\Template as LTemplate;
use Modules\V1\Lib\Tracker as LTracker;

final class Bill extends BaseRestController {

    public function postPrint() {
        $trackerLibrary = new LTracker();
        try {
            $data = $this->params['body'];
            $data['config'] = $this->container->get('printers')[$this->params['body']['printer']];


            $billLibrary = new LTemplate($this->container);
            $dataFormatted = $billLibrary->render("Bill2.phtml", $data);
            file_put_contents("bill2.txt", $dataFormatted);

        } catch (\Exception $ex) {
            $this->container->logger->error("Exception: ".$ex->getMessage()." Process: ".$trackerLibrary->getState());
        }
        $this->container->logger->info("Process: ".$trackerLibrary->getState());
        return $this->returnResponse(array('message' => $dataFormatted), self::HTTP_OK);
    }

}
