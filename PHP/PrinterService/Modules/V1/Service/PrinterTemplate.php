<?php
namespace Modules\V1\Service;

final class PrinterTemplate extends \Slim\Views\PhpRenderer{

    public function __construct($c) {
        $this->container = $c;
        parent::__construct();
    }

    public function imprimirGuiones($numVeces){
        echo "\n";
        for($i=0;$i<$numVeces;$i++){
            echo "-";
        }
        echo "\n";
    }
    public function render($template, array $data = [])
    {
        $funciones =array('funciones' =>$this);
        $data = array_merge($data,$funciones);
        $output = $this->fetch($template, $data);
        return $output;
    }

}
