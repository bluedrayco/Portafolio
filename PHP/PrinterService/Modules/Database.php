<?php
namespace Modules;

class Database{

    public static function getStringConnection($params){
        $parametros = $params;
        $usuario  = $parametros['Database']['username'];
        $password = $parametros['Database']['password'];
        $conexion = '';

        switch($parametros['Database']['driver']){
            case 'pgsql':
                $conexion = 'pgsql:dbname='.$parametros['Database']['database'].';host='.$parametros['Database']['host'];
                break;

            case 'sqlite':
                $conexion = 'sqlite:'.$parametros['Database']['database'];
                $usuario  = null;
                $password = null;
                break;

            case 'sqlite_memory':
                $conexion = 'sqlite::memory';
                $usuario  = null;
                $password = null;
                break;

            case 'mysql':
                $conexion = 'mysql:host='.$parametros['Database']['host'].';dbname='.$parametros['Database']['database'].($parametros['Database']['charset']!=''?(';charset='.$parametros['Database']['charset']):'').($parametros['Database']['port']!=''?';port='.$parametros['Database']['port']:'');
                break;

            case 'firebird':
                $conexion = 'firebird:dbname='.$parametros['Database']['host'].':'.$parametros['Database']['database'];
                break;

            case 'informix':
                $conexion = 'informix:'.$parametros['Database']['host'].'='.$parametros['Database']['database'];
                break;

            case 'oracle':
                $conexion = 'OCI:dbname='.$parametros['Database']['database'].($parametros['Database']['charset']!=''?(';charset='.$parametros['Database']['charset']):'');
                break;

            case 'odbc':
                $conexion = 'odbc:Driver={SQL Native Client};Server='.$parametros['Database']['host'].';Database='.$parametros['Database']['database'].'; Uid='.$parametros['Database']['username'].';Pwd='.$parametros['Database']['password'].';';
                $usuario  = null;
                $password = null;
                break;

            case 'dblib':
                $conexion = 'dblib:host='.$parametros['Database']['host'].':'.$parametros['Database']['port'].';dbname='.$parametros['Database']['database'];
                break;

            case 'db2':
                $conexion = 'ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE='.$parametros['Database']['database'].'; HOSTNAME='.$parametros['Database']['host'].($parametros['Database']['port']!=''?';PORT='.$parametros['Database']['port']:'').';PROTOCOL=TCPIP;';
                break;

            default:
                throw new \Exception('Base de Datos no conocida...');
        }

        return $conexion;
    }
}