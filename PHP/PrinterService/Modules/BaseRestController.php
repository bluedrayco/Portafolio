<?php

namespace Modules;

use Slim\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RedBeanPHP\Facade as R;

class BaseRestController implements CRUDInterface {

    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NO_CONTENT = 204;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;

    protected $container;
    protected $request;
    protected $response;
    protected $args;
    protected $params;

    public function __construct(Container $c, Request $request, Response $response, $args) {
        $viewsPath = str_replace('Modules', "", substr(get_called_class(), 0, strrpos(get_called_class(), "\\")));
        $viewsPath = str_replace("\\", "/", __DIR__ . substr($viewsPath, 0, strrpos($viewsPath, "\\")) . "/View/");
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
        $this->params = $this->getParams();
        $this->container = $c;
        $this->container['renderer'] = new \Slim\Views\PhpRenderer($viewsPath);
    }

    private function setContentHeader($tipoHeader = "application/json") {
        return $this->response->withHeader('Content-type', !$tipoHeader ? "application/json" : $tipoHeader);
    }

    public function returnResponse($respuesta, $code = null, $tipoHeader = null) {
        $this->response = $this->setContentHeader($tipoHeader);
        $this->response->getBody()->write(!$tipoHeader ? json_encode($respuesta) : $respuesta);
        $this->response = $this->response->withStatus(!$code ? $respuesta['code'] : $code);
        return $this->response;
    }

    public function getParams($urlParamsRequired = array()) {
        $params = array("body" => $this->request->getParsedBody(), "query" => $this->request->getQueryParams(), 'url' => $this->args);
        foreach ($urlParamsRequired as $clave) {
            if (!array_key_exists($clave, $params['url'])) {
                $params['url'][$clave] = null;
            }
        }
        return $params;
    }

    public function checkParameters($params, $compare) {
        foreach ($compare as $comp) {
            if (!array_key_exists($comp, $params)) {
                return false;
            }
        }
        return true;
    }

    public function create($data) {
        try {
            $bean = R::dispense($table);
            if (!$bean) {
                throw new \Exception("No se pudo crear la entidad {$table}");
            }
            $bean->activo = array_key_exists('activo', $params) ? $params['activo'] : true;
            $bean->fecha_alta = new \DateTime();
            $bean->fecha_modificacion = new \DateTime();
            $columnas = R::getColumns($table);
            if (array_key_exists('usuario_alta_id', $columnas)) {
                $bean->usuario_alta = $usuario;
            }
            if (array_key_exists('usuario_modificacion_id', $columnas)) {
                $bean->usuario_modificacion = $usuario;
            }
            $bean->import($params);
            $id = R::store($bean);
        } catch (\Exception $ex) {
            $this->logger->error($this->extractMessageException($ex));
            return $this->buildRespuesta("Error al guardar la entidad {$table} en la base de datos...", null, 404);
        }
        return $this->buildRespuesta("post", R::exportAll($bean));
    }

    private function getEntityConfiguration($table) {
        $configuration = array();
        $configuration = array_key_exists($this->args['version'] . ".configuration.entity", $this->container->keys()) ? $this->container->get($this->args['version'] . ".configuration.entity") : array();
        $configuration = $this->container->get($this->args['version'] . ".configuration.entity");
        return array_key_exists($table, $configuration) ? $configuration[$table] : array();
    }

    public function delete($id) {
        $table = $this->getResource();
        $configuration = $this->getEntityConfiguration($table);
        try {
            $bean = R::load($table, $id);
            if ($bean->id == 0) {
                throw new \Exception("Not found {$table} with the id: {$id}");
            }
            if (!array_key_exists('logical_delete', $configuration)) {
                R::trash($bean);
            } else {
                $data=array();
                if (array_key_exists("update_date", $configuration)) {
                    $data[$configuration['update_date']] = new \DateTime();
                }
                if (array_key_exists("user_update", $configuration)) {
                    $data[$configuration['user_update']] = $this->user;
                }

                $data[$configuration['logical_delete']] = false;
                $bean->import($data);
                R::store($bean);
            }
        } catch (\Exception $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        } catch (Security $ex) {
            $this->container->logger->error($ex->getMessage());
            return $this->returnResponse(array('message' => $ex->getMessage()), self::HTTP_NOT_FOUND);
        }
        return $this->returnResponse(null, self::HTTP_NO_CONTENT);
    }

    public function getAll() {
        $resource = $this->getResource();
        return $this->listar($resource, $this->params['query']);
    }

    public function getById($id) {
        $resource = $this->getResource();
        return $this->listar($resource, $this->params['query'], $id);
    }

    public function update($id, $data) {
        try {
            $bean = R::findOne($table, "id = ?", [$id]);
            if (!$bean) {
                throw new \Exception("No se pudo encontrar la entidad {$table} con el id {$id}");
            }
            $bean->fecha_modificacion = new \DateTime();
            $columnas = R::getColumns($table);
            if (array_key_exists('usuario_modificacion_id', $columnas)) {
                $bean->usuario_modificacion = $usuario;
            }
            $bean->import($params);
            R::store($bean);
        } catch (\Exception $ex) {
            $this->logger->error($this->extractMessageException($ex));
            return $this->buildRespuesta($ex->getMessage(), null, 404);
        }
        return $this->buildRespuesta("put", null);
    }

    private function getResource() {
        return $this->args['controller'];
    }

    private function listar($table, $params, $id = null) {
        try {
            $order = "";
            $datoOrder = "";
            if (array_key_exists('order', $params)) {
                $order = " order by ";
                $datos = explode("|", $params['order']);
                if (count($datos) < 2) {
                    throw new \Exception("Falta un parametro en el campo order...");
                }
                $order = $order . $datos[0] . " " . $datos[1];
                $separacionPunto = explode(".", $datos[0]);
                $datoOrder = $separacionPunto[0];
            }
            $columnsTable = R::getColumns($table);
            $filter = array_key_exists('filter', $params) ? $params['filter'] : "";
            $relaciones = $this->obtenerNombreRelaciones($columnsTable, $filter, $datoOrder);
            $join = array_key_exists('filter', $params) ? $this->obtenerJoins($table,$columnsTable, $params['filter'], $datoOrder) : '';
            $tablas = $relaciones == ' ' ? $table : "{$table} {$table}" . ($relaciones == '' ? '' : ",{$relaciones}");
            $fields = array_key_exists('fields', $params) ? $params['fields'] : $this->obtenerCamposTabla($table, $columnsTable);
            $limit = array_key_exists('limit', $params) ? "limit " . $params['limit'] : "";
            $offset = array_key_exists('offset', $params) ? " offset " . $params['offset'] : '';
            $hidratate = array_key_exists('hidratate', $params) ? true : false;
            $order = "";
            if (array_key_exists('order', $params)) {
                $order = " order by ";
                $datos = explode("|", $params['order']);
                if (count($datos) < 2) {
                    throw new \Exception("Falta un parametro en el campo order...");
                }
                $order = $order . $datos[0] . " " . $datos[1];
            }

            $where = $join != '' || $filter != "" ? ' where ' : "";
            $where.=$join == '' ? '' : $join;
            if ($filter) {
                $filter = $join != '' ? ' and ' . $filter : $filter;
            }
            $query = "Select {$fields} from {$tablas} {$where} {$filter}  {$order} {$limit}";
//            echo $query;
//            exit();
            if ($id == null) {
                $records = R::getAll($query);
            } else {
                $records = R::findOne($table, "id = ?", [$id]);
            }
        } catch (\Exception $e) {
            $this->container->logger->error($e->getMessage());
            return $this->returnResponse(array("message" => $e->getMessage()), self::HTTP_NOT_FOUND);
        }
        if ($hidratate) {
            $records = $this->obtenerObjetosRelacionados($records);
        }
        $records = array_key_exists('offset', $params) ? array_slice($records, $params['offset'] - 1) : $records;
        return $this->returnResponse(array_key_exists('count', $params) ? array('count' => count($records)) : $records, $records == NULL ? self::HTTP_NO_CONTENT : self::HTTP_OK);
    }

    private function obtenerNombreRelaciones($columns, $filter, $order) {
        $relaciones = "";
        foreach ($columns as $llave => $valor) {
            if (strpos($llave, "_id") && ($llave != "usuario_alta_id" && $llave != "usuario_modificacion_id") && ($this->existeEnCadena($llave, $filter) || $this->existeEnCadena($order, $llave))) {
                $llave = str_replace("_id", "", $llave);
                $relaciones.= "{$llave} {$llave},";
            }
        }

        return substr($relaciones, 0, strlen($relaciones) - 1);
    }

    private function obtenerCamposTabla($table, $columns) {
        $tmp = "";
        foreach ($columns as $llave => $valor) {
            $tmp.="{$table}.{$llave},";
        }
        return substr($tmp, 0, strlen($tmp) - 1);
    }

    private function obtenerJoins($table,$columns, $filter, $order) {
        $relaciones = "";
        $x = 0;
        foreach ($columns as $llave => $valor) {
            if (($llave != "usuario_alta_id" && $llave != "usuario_modificacion_id") && ($this->existeEnCadena($llave, $filter) || $this->existeEnCadena($order, $llave))) {
                if ($x == 0 && strpos($llave, "_id")) {
                    $relaciones.="(";
                }
                if ($x != 0) {
                    $relaciones.=" and ";
                }
                if (strpos($llave, "_id")) {
                    $tablaRelacionada = str_replace("_id", "", $llave);
                    $relaciones.="{$table}.{$llave}={$tablaRelacionada}.id";
                    $x++;
                }
            }
        }
        if ($x != 0) {
            $relaciones.=")";
        }
        return $relaciones;
    }

    private function existeEnCadena($llave, $cadena) {
        $llave = $llave == '' ? 'NonExist' : $llave;
        return strpos($cadena, $llave) !== false;
    }

    private function createRegister($tabla, $bean) {
        $columnas = R::getColumns($tabla);
        $query = "insert into {$tabla}(";
        $values = "(";
        $x = 0;
        foreach ($columnas as $columna => $value) {
            if (array_key_exists($columna, $bean)) {
                $query.=$columna;
                switch ($value) {
                    case'integer':
                    case'bigint':
                    case'numeric':
                        if ($bean[$columna] === 0) {
                            $values.=0;
                        } elseif ($bean[$columna] === null) {
                            $values.="null";
                        } else {
                            $values.=$bean[$columna];
                        }

                        break;
                    case'boolean':
                        $values.=$bean[$columna] == false ? "false" : "true";
                        breaK;
                    default:
                        if (strpos($value, "timestamp") !== false) {
                            $values.="null";
                        } else {
                            $texto = $bean[$columna];
                            $texto = str_replace("'", "''", $texto);
                            $values.="'{$texto}'";
                        }

                        break;
                }
                $query.=",";
                $values.=",";
            }
        }
        $query = substr($query, 0, strlen($query) - 1);
        $values = substr($values, 0, strlen($values) - 1);
        $query.=")";
        $values.=")";
        $query.=" values " . $values;
        R::exec($query);
    }

}
