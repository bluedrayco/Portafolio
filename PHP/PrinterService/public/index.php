<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->add('Modules', __DIR__.'/../Modules');
require __DIR__ . '/../vendor/autoload.php';
require_once __DIR__.'/../vendor/socloz/spyc/Spyc.php';
$parameters         = \Spyc::YAMLLoad(__DIR__.'/../configuration/application.yml');
$printersParameters = \Spyc::YAMLLoad(__DIR__.'/../configuration/printers.yml');
use RedBeanPHP\Facade as R;
use Modules\Database;

if(array_key_exists("Database", $parameters)){
    R::setup(Database::getStringConnection($parameters), $parameters['Database']['username'], $parameters['Database']['password'], true);
}
session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
