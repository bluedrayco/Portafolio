<?php

// Routes

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view

    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->any('/api/{version}/{controller}[/{resource}]', function($request, $response, $args) use ($container) {

    $class = "\\Modules\\" . ucwords($args['version']) . "\\Controller\\" . ucwords($args['controller']);
    if (!class_exists($class)) {
        switch ($request->getMethod()) {
            case 'GET':
                if (array_key_exists("resource", $args)) {
                    if (is_numeric($args['resource'])) {
                        $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                        return $generic->getById($args['resource']);
                    }
                } else {
                    $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                    return $generic->getAll();
                }
                break;

            case 'DELETE':
                if (array_key_exists("resource", $args)) {
                    if (is_numeric($args['resource'])) {
                        $generic = new \Modules\BaseRestController($this, $request, $response, $args);
                        return $generic->delete($args['resource']);
                    }
                }
                break;
        }

        return notFound($request, $response);
    }
    $object = new $class($this, $request, $response, $args);

    if (!array_key_exists("resource", $args) && $request->getMethod() == 'GET') {
        return $object->getAll();
    }

    if (is_numeric($args['resource']) && $request->getMethod() == 'DELETE') {
        return $object->delete($args['resource']);
    }

    if (is_numeric($args['resource']) && $request->getMethod() == 'GET') {
        return $object->getById($args['resource']);
    }

    $method = strtolower($request->getMethod()) . $args['resource'];
    if (!method_exists($object, $method) && !is_numeric($args['resource'])) {
        return methodNotAllowed($request, $response);
    }


    return $object->$method();
});
