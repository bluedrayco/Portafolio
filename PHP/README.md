## PHP

There are two projects:

**Control Escolar:** Is a Project for management centers (teachers, courses, students), it uses the synchronization system created in python (RabbitMQ). Te project was made separately in Bundles, each Bundle contains Business layer, Access to databases layer, Rest services, Controllers, Services and Unit Tests.<br\>
**PrinterService:** Our ancient project Mako is an older manage centers system, this system can print in thermal printers, we decided to separate this funcionality in a microservice named PrinterService, it was made in SlimPHP, it uses ReadBeanPHP as ORM and uses the paradigm Convention insted of Configuration.<br\>

For example, if you need to create the next url: "/api/v1/student/send" using the Http method POST you only need to create a class extending the class BaseRestController named Student, inside it you need create a method named postSend and that's all, without configuration you created a rest service. 