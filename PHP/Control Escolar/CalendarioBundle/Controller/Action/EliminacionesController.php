<?php
namespace ControlEscolar\CalendarioBundle\Controller\Action;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use ControlEscolar\CalendarioBundle\Business\Entity\Ocupacion as BOcupacion;

use Core\CoreBundle\Controller\BaseController;

class EliminacionesController extends BaseController {

    /**
     * Rest que elimina los cursos que cumplan con las caracteristicas.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminacion de cursos.",
     *  section="CALENDARIZACION Eliminaciones - (ControlEscolarCalendarioBundle)",
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/ocupaciones")
     */
    public function deleteEliminacionOcupacionesAction() {
        $params = $this->parseParametros('all');

        if (!isset($params['usuario_id'])) {
            $params['usuario_id'] = $this->getUsuarioId();
        }

        $ocupacionBusiness = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta         = $ocupacionBusiness->eliminarOcupaciones($params);

        return $this->view($respuesta, $respuesta['code']);
    }

}
