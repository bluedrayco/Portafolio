<?php

namespace ControlEscolar\CalendarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Core\SyncBundle\Command\ObtenAulasYFacilitadoresCommand        AS CAulaFacilitador;
use Core\SyncBundle\Business\Action\ObtenAulasYFacilitadoresSync   AS BAulasYFacilitadores;



// Uso de input y output para la ejecución del comando.
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;


class DefaultController extends Controller
{
    public function __construct() {
        $fechaActual = new \DateTime();
        $this->fechaActual['fecha']          = $fechaActual->format("Y-m-d");
        $this->fechaActual['fechaHora']      = $fechaActual->format("Y/m/d H:i:s");
        $this->fechaActual['timezoneOffset'] = date_default_timezone_get();

    }
    public function indexAction()
    {
        return $this->render('ControlEscolarCalendarioBundle::index.html.twig');
    }
    public function pruebaCalendarioAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:ofertaeducativa:calendario.html.twig');
    }

    public function ofertaEducativaAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:ofertaeducativa:ofertaEducativa.html.twig'
                ,array(
                        'fechaActual' => $this->fechaActual,
                        'tipoProgramacionCE'       => 'normal'
                        ));
    }

    /*
     * Metódo para llamar a la URL de programación extemporanea
     */
    public function ofertaEducativaExtemporaneaAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:ofertaeducativa:ofertaEducativa.html.twig'
                ,array(
                        'fechaActual' => $this->fechaActual,
                        'tipoProgramacionCE'       => 'extemporanea'
                        ));
    }

    /*
      * Controlador para pantalla de calendarizacion
      * @Description: Se creo esta pantalla para que control escolar edite los
      *                parametros globalmente antes de bajar a los centros.
      *                En centro se tiene acceso pero solo lectura
      * @Author: Jaime Hernández Cruz
      */
    public function parametrosAction()
    {
       $ambiente = $this->container->getParameter("ambiente");
       return $this->render('ControlEscolarCalendarioBundle:parametrizacion:parametros.html.twig',array("ambiente"=> $ambiente));
     }

     /*
      * Controlador para pantalla de calendarizacion
      * @Description: Se creo esta pantalla para poder cambiar los parametros desde central
      * @Author: Jaime Hernández Cruz
      */
     public function parametrosDeCentrosAction()
    {
         $ambiente = $this->container->getParameter("ambiente");
         return $this->render('ControlEscolarCalendarioBundle:parametrizacion:parametrosCentro.html.twig',array("ambiente"=> $ambiente));
    }

    public function calendarizacionEnCentroAction()
    {
        try{
            //Aqui ponemos la generación del comando
            $AulasYFacilitadores  = new BAulasYFacilitadores($this->getDoctrine()->getManager());
            $AulasYFacilitadores->obtenerAulasYFacilitadores();
        }
        catch (\Exception $e) {
            $GLOBALS['kernel']->getContainer()->get('logger')->error("No logramos conectarnos con mako local 1.4");

        }
        $ambiente = $this->container->getParameter("ambiente");
        return $this->render('ControlEscolarCalendarioBundle:calendarizacion:calendarizacionEnCentro.html.twig'
                ,array(
                    'fechaActual'=>$this->fechaActual,
                    "ambiente"=> $ambiente
                ));
    }

    public function calendarizacionEnCentralAction()
    {
        $ambiente = $this->container->getParameter("ambiente");
        return $this->render('ControlEscolarCalendarioBundle:calendarizacion:calendarizacionEnCentral.html.twig'
                ,array(
                    'fechaActual'=>$this->fechaActual,
                    "ambiente"=> $ambiente
                    ));
    }
    public function movimientosAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:movimientos:movimientos.html.twig');
    }
    public function resumenAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:resumen:resumen.html.twig');
    }
    public function reporteFacilitadoresAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:reporteFacilitadores:reporteFacilitador.html.twig'
                ,array(
                    'fechaActual'=>$this->fechaActual,
                 ));
    }
    public function listadoOfertaEducativaAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:listadoOfertas:indexOfertaEducativa.html.twig');
    }

    public function reporteFacilitadoresHrsByCursoAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:reportes:facilitadoreshoras-curso.html.twig');
    }

    public function reporteFacilitadoresHrsAsignadasAction()
    {
        return $this->render('ControlEscolarCalendarioBundle:reportes:facilitadoreshorasasignadas.html.twig');
    }

    public function listadoDiaFeriadoAction()
    {

        $ambiente = $this->container->getParameter("ambiente");
        return $this->render('ControlEscolarCalendarioBundle:diaFeriado:indexDiasFeriados.html.twig', array("ambiente"=> $ambiente));
    }


}
