<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\TipoEscenario as BTipoEscenario;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class TipoEscenarioController extends BaseController {

    /**
     * Rest que Obtiene todos los TiposEscenarios de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los TipoEscenario",
     *  section="CALENDARIZACION TipoEscenario - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\TipoEscenario"
     * )
     * @return array
     * @Rest\View()
     * @Get("/tiposescenario")
     */
    public function getTiposEscenarioAction() {
        $parametros    = $this->getRequest()->query->all();
        $tipoEscenarioBusiness = new BTipoEscenario($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $tipoEscenarioBusiness->listar();
        } else {
            $respuesta = $tipoEscenarioBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }
    
}