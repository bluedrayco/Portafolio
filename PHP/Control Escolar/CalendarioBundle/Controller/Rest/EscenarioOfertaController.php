<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\EscenarioOferta as BEscenarioOferta;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class EscenarioOfertaController extends BaseController {

    /**
     * Rest que Obtiene el EscenarioOferta para un escenario de todos|grupo|centro|agrupacion
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtiene del EscenarioOferta para un escenario de todos|grupo|centro|agrupacion",
     *  section="CALENDARIZACION EscenarioOferta - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="oferta_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la oferta"
     *      },
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del grupo|centro, este parámetro puede ser omitido en la url"
     *      },
     *      {
     *          "name"="tipo",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="especifica el tipo de Escenario que se quiere buscar, los posibles valores son: (todos|grupo|centro|agrupacion), en el caso que se elija todos o agrupacion no es necesario que se incluya el parámetro id"
     *      }
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\EscenarioOferta"
     * )
     * @return array
     * @Rest\View()
     * @Get("/escenariooferta/{tipo}/{oferta_id}/{id}" , defaults={"id"="1"})
     */
    public function getEscenarioOfertaByOfertaIdCentroIdAction($tipo, $oferta_id , $id) {
        $parametros    = $this->getRequest()->query->all();
        
        $parametros['oferta_id'] = $oferta_id;
        $parametros['id'] = $id;
        $parametros['tipo']= $tipo;
        $parametros['usuario_id']= $this->getUsuarioId();
        
        $escenarioOfertaBusiness = new BEscenarioOferta($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $escenarioOfertaBusiness->obtenerEscenarioOfertaByOfertaIdCentroId($parametros);

        return $this->view($respuesta, $respuesta['code']);
    }
}
