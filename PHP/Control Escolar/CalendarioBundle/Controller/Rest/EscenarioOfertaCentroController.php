<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\EscenarioOfertaCentro as BEscenarioOfertaCentro;

/**
 * Servicios Rest correspondientes al tratamiento de los registros de la tabla EscenarioOfertaCentro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */

class EscenarioOfertaCentroController extends BaseController {

    /**
     * Rest que Obtiene todos los Escenarios Ofertas Centros de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Escenarios Ofertas Centros",
     *  section="CALENDARIZACION EscenarioOfertaCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="escenario_oferta_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del escenario_oferta"
     *      }
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\EscenarioOfertaCentro"
     * )
     * @return array
     * @Rest\View()
     * @Get("/escenarioofertacentrowithcentrosgrupos/{escenario_oferta_id}")
     */
    public function getEscenarioOfertaCentroWithCentrosGruposAction($escenario_oferta_id) {
        $parametros    = $this->getRequest()->query->all();
        $escenarioOfertaCentroBusiness = new BEscenarioOfertaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $escenarioOfertaCentroBusiness->getCentrosyGruposByEscenarioOfertaId($escenario_oferta_id);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que almacena una relacion de un EscenarioOferta con la relacion de un Centro|Grupo en la Base de Datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Almacenamiento de un EscenarioOferta con la relacion de un Centro|Grupo en la Base de Datos",
     *  section="CALENDARIZACION EscenarioOfertaCentro - (ControlEscolarCalendarioBundle)",
     *  parameters={
     *      {"name"="escenario_oferta_id", "dataType"="integer", "required"=true, "description"="EscenarioOferta que se relacionará con el (Centro|Grupo)"},
     *      {"name"="centro_id", "dataType"="integer", "required"=false, "description"="centro que se relacionará con el EscenarioOferta"},
     *      {"name"="grupo_id", "dataType"="integer", "required"=false, "description"="grupo que se relacionará con el EscenarioOferta"}
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\EscenarioOfertaCentro"
     * )
     * @return array
     * @Rest\View()
     * @Post("/escenarioofertacentro")
     */
    public function postEscenarioOfertaCentroByCentroAction() {
        if(!$this->checkRequestParameters(array(
            'escenario_oferta_id'))) {
            return $this->buildErrorView('Faltan especificar datos.');
        }

        $this->parametros['usuario_id'] = $this->getUsuarioId();
        $escenarioOfertaCentroBusiness  = new BEscenarioOfertaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $escenarioOfertaCentroBusiness->crearByCentroId($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }
    
    /**
     * Rest que elimina en la base de datos eliminacion de una relación entre un EscenarioOfertaCentro con un Centro
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminación de la relación de un EscenarioOfertaCentro con un Centro en la Base de Datos",
     *  section="CALENDARIZACION EscenarioOfertaCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="escenario_oferta_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del escenario_oferta que esta vinculado al centro"
     *      },
     *      {
     *          "name"="centro_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del centro que esta vinculado al escenario_oferta"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/escenarioofertacentrobycentro/{escenario_oferta_id}/{centro_id}")
     */
    public function deleteEscenarioOfertaCentroWithCentroIdAction($escenario_oferta_id,$centro_id) {
        $escenarioOfertaCentroBusiness = new BEscenarioOfertaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $escenarioOfertaCentroBusiness->eliminarByCentroId($escenario_oferta_id,$centro_id);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que elimina en la base de datos eliminacion de una relación entre un EscenarioOfertaCentro con un Centro
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminación de la relación de un EscenarioOfertaCentro con un Grupo en la Base de Datos",
     *  section="CALENDARIZACION EscenarioOfertaCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="escenario_oferta_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del escenario_oferta que esta vinculado al centro"
     *      },
     *      {
     *          "name"="grupo_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del grupo que esta vinculado al escenario_oferta"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/escenarioofertacentrobygrupo/{escenario_oferta_id}/{grupo_id}")
     */
    public function deleteEscenarioOfertaCentroWithGrupoIdAction($escenario_oferta_id,$grupo_id) {
        $escenarioOfertaCentroBusiness = new BEscenarioOfertaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $escenarioOfertaCentroBusiness->eliminarByGrupoId($escenario_oferta_id,$grupo_id);

        return $this->view($respuesta, $respuesta['code']);
    }
}
