<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\Parametro as BParametro;


/**
 * @author Isaed Martinez Borrego <isaed.martinez@enova.mx>
 */
class ParametroController extends BaseController {

    /**
     * Rest que Obtiene todos los parametros de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Parametros",
     *  section="CALENDARIZACION Parametro - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\Parametro"
     * )
     * @return array
     * @Rest\View()
     * @Get("/parametros")
     */
    public function getParametrosAction() {

        $this->parseParametros();

        $parametrosBusiness = new BParametro($this->getDoctrine()->getManager());

            $respuesta = $parametrosBusiness->listar($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que almacena un Parametro en la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Registro de Parametro en la base de datos",
     *  section="CALENDARIZACION Parametro - (ControlEscolarCalendarioBundle)",
     *  parameters={
     *      {"name" = "nombre",         "dataType"="string",  "required"=true,   "description"="nombre del parametro"                                                    },
     *      {"name" = "etiqueta",       "dataType"="string",  "required"=true,   "description"="etiqueta del parametro"                                                                                                              },
     *      {"name" = "valor",          "dataType"="string",  "required"=true,   "description"="valor del parametro"                                                                                                    },
     *      {"name" = "puede_publicar", "dataType"="boolean"  , "required"=true,  "description"="identificador si el parametro puede ser publicado"                   },
     *      {"name" = "activo",         "dataType"="boolean"  , "required"=true,  "description"="identificador si el parametro puede ser publicado"                   }
     *
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\Parametro"
     * )
     * @return array
     * @Rest\View()
     * @Post("/parametros")
     */
    public function postParametrosAction() {

        if(!$this->checkRequestParameters(array('nombre','etiqueta','valor','puede_publicar', 'activo'))) {
            return $this->buildErrorView('Faltan especificar datos.');
        }
        $this->parametros['usuario_id'] = $this->getUsuarioId();
        $parametrosBusiness = new BParametro($this->getDoctrine()->getManager());
        $respuesta          = $parametrosBusiness->crear($this->parametros);

        return $this->view($respuesta, $respuesta['code']);

    }


    /**
     * Rest que elimina en la base de datos un parametro
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminacion de un Parametro",
     *  section="CALENDARIZACION Parametro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="parametro_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del parametro a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/parametros/{parametro_id}")
     */
    public function deleteParametroAction($parametro_id){
        $parametroBusiness = new BParametro($this->getDoctrine()->getManager());
        $respuesta          = $parametroBusiness->eliminar($parametro_id);
        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que modifica en la base de datos un Parametro
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Modificación de un parametro en la Base de Datos",
     *  section="CALENDARIZACION Parametro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="parametro_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del parametro a modificar"
     *      }
     *  },
     *  parameters={
     *      {"name" = "nombre",         "dataType"="string",  "required"=true,   "description"="nombre del parametro"                                                    },
     *      {"name" = "etiqueta",       "dataType"="string",  "required"=true,   "description"="etiqueta del parametro"                                                                                                              },
     *      {"name" = "tipo",           "dataType"="integer", "required"=true,   "description"="tipo del parametro"                                                                                                 },
     *      {"name" = "valor",          "dataType"="string",  "required"=true,   "description"="valor del parametro"                                                                                                    },
     *      {"name" = "puede_publicar", "dataType"="boolean"  , "required"=true,  "description"="identificador si el parametro puede ser publicado"                   },
     *      {"name" = "activo",         "dataType"="boolean"  , "required"=true,  "description"="identificador si el parametro puede ser publicado"                   },
     *      },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/parametros/{id}")
     */
    public function putParametroAction($id){
        $this->parseParametros();
        $this->parametros['usuario_id']  = $this->getUsuarioId();
        $this->parametros['parametro_id']= $id;

        $parametroBusiness = new BParametro($this->getDoctrine()->getManager());

        $respuesta = $parametroBusiness ->actualizar($this->parametros);
        return $this->view($respuesta, $respuesta['code']);
    }

     /**
     *
     * @return array
     * @Rest\View()
     * @Get("/parametros/{id}")
     */
    public function getParametroAction($id) {
        $parametroBusiness = new BParametro($this->getDoctrine()->getManager());
        $respuesta       = $parametroBusiness->obtenerParametro($id);

        return $this->view($respuesta, $respuesta['code']);
    }
    /**
     * Rest para la sincronización de los parametros a los centros.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Envio de sincronizacion de los parametros de calendarización a los centros",
     *  section="CALENDARIZACION Parametros - (ControlEscolarCalendarioBundle)",
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/enviaparametrosacentros")
     */
    public function putEnvioParametrosCentrosAction(){
        //podría recibir key destinos con los/el destino_id separados por comas, sino envía a todos los destinos activos
        $this->parseParametros();
        $this->parametros = $this->getRequest()->query->all();
        $usuario = $this->getUsuarioId();

        $params = array_merge($this->parametros,array('usuario_id' => $usuario));

        $parametroBusiness = new BParametro($this->getDoctrine()->getManager(),$this);

        $result = $parametroBusiness->sincronizar($params);
        return $this->view($result,$result['code']);

    }
}
