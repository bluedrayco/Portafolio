<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\CentroOfertaPublica as BCentroOfertaPublica;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class CentroOfertaPublicaController extends BaseController {

    /**
     * Rest que Publica un calendario para los centros
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Publicación de un calendario a los centros",
     *  section="CALENDARIZACION CentroOfertaPublica - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="escenario_oferta_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del escenario_oferta a publicar a los centros"
     *      }
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\CentroOfertaPublica"
     * )
     * @return array
     * @Rest\View()
     * @Get("/publicacioncalendario/{escenario_oferta_id}")
     */
    public function getPublicacionCalendarioAction($escenario_oferta_id) {

        $parametros    = $this->getRequest()->query->all();
        $parametros['escenario_oferta_id'] = $escenario_oferta_id;
        $parametros['usuario_id'] =$this->getUsuarioId();
        $centroOfertaPublicaBusiness = new BCentroOfertaPublica($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta = $centroOfertaPublicaBusiness->publicarCalendario($parametros);

        return $this->view($respuesta, $respuesta['code']);
    }
}
