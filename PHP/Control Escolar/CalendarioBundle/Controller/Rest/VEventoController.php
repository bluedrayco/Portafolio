<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\VEvento as BVEvento;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class VEventoController extends BaseController {

    /**
     * Rest que Obtiene todos los Eventos de una vista de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Eventos",
     *  section="CALENDARIZACION VEvento - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\VEvento"
     * )
     * @return array
     * @Rest\View()
     * @Get("/veventos")
     */
    public function getVistaEventosAction() {
        $parametros    = $this->getRequest()->query->all();
        $veventoBusiness = new BVEvento($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $veventoBusiness->listar();
        } else {
            $respuesta = $veventoBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }
}
