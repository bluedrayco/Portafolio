<?php

namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;


use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaActividadCentro as BOfertaActividadCentro;


/**
 * Controlador para administrar las operaciones sobre REST para CRUD de Actividades Academicas y
 * No Academicas asignadas a una Oferta Educativa determinada.
 * @author silvio.bravo@enova.mx
 */
class OfertaActividadCentroController extends BaseController {

    /**
     *
     *
     * Rest que Obtiene todas las Actividades (Academicas y NO) a partir de una Oferta Educativa y un Escenario Especifico.
     *
     * @ApiDoc(
     *  resource=true,
     *  description ="Obtencion de todas las Actividades (Academicas y NO) a partir de una Oferta Educativa y un Escenario Especifico",
     *  section     ="CALENDARIZACION OfertaActividad - (ControlEscolarCalendarioBundle)",
     *  parameters  ={
     *      {"name"="oferta_educativa_id"   , "dataType"="integer", "required"=true, "description"="Id de la oferta Educativa"},
     *      {"name"="escenario_id"          , "dataType"="integer", "required"=true, "description"="Id del escenario recibe el valor de todos cuando se requiere el calendario completo"},
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\EventoActividadOferta"
     * )
     * @return array
     * @Rest\View()
     * @Get("/ofertasactividadcentro/{oferta_educativa_id}/{escenario_id}/{fecha_inicio}/{fecha_final}")
     *
     *
    **/

    public function getActividadesCentroOfertaAction($oferta_educativa_id, $escenario_id, $fecha_inicio, $fecha_final){

        $BOfertaActividad   = new BOfertaActividad($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta          = $BOfertaActividad->obtenDatos($oferta_educativa_id, $escenario_id, $fecha_inicio, $fecha_final);
        return $this->view($respuesta, $respuesta["code"]);

    }

    /**
     * Rest que modifica en la base de datos una OfertaActividadCentro
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Modificación de una OfertaActividadCentro en la Base de Datos",
     *  section="CALENDARIZACION OfertaActividadCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la OfertaActividadCentro a modificar"
     *      }
     *  },
     *  parameters={
     *      {"name" = "fecha_inicio",           "dataType"="date",    "required"=false,  "description"="Nueva fecha a partir de la cual se programa todos los eventos de esta actividad"                                              },
     *      {"name" = "escenario_oferta_id",    "dataType"="integer", "required"=false,  "description"="Nuevo EscenarioOferta vinculado"                                                                                              },
     *      {"name" = "actividad_id",           "dataType"="integer", "required"=false,  "description"="Nuevo Id de Actividad NO academica"                                                                                           },
     *      {"name" = "actividad_academica_id", "dataType"="integer", "required"=false,  "description"="Nuevo Id de Actividad academica"                                                                                              },
     *      {"name" = "horario",                "dataType"="array"  , "required"=false,  "description"="Array que representa el nuevo horario, dicho array debe contener los keys: dia_semana:0-6, hora_inicio, hora_fin"             },
     *      {"name" = "obligatorio",            "dataType"="boolean", "required"=false,  "description"="true|false que define si debe ser obligatorio o no"                                                                           },
     *      {"name" = "esquema_id",             "dataType"="integer", "required"=false,  "description"="Id del Esquema que se aplicará a la programación de los eventos, siempre y cuando sea una actividad academica"                },
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/ofertaactividadcentro/{id}")
     */
    public function putOfertaActividadCentroAction($id){
        $this->parseParametros();

        $this->parametros["oferta_actividad_id"] = $id;
        $this->parametros['usuario_id'] = $this->getUsuarioId();
        if(!array_key_exists("fecha_final", $this->parametros)){
            $this->parametros["fecha_final"]  = null;
        }
        $ofertaActividadBusiness   = new BOfertaActividadCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ofertaActividadBusiness ->actualizar($this->parametros);
        return $this->view($respuesta, $respuesta['code']);
    }
}

?>