<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\Ocupacion as BOcupacion;

class OcupacionController extends BaseController {

    /**
     * Rest que almacena una Actividad (academica o no) en una oferta educativa
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Registro de Actividades (academicas o no) para una oferta academica",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  parameters={
     *      {"name" = "fecha_inicio",           "dataType"="date",    "required"=true,  "description"="Fecha a partir de la cual se programa todos los eventos de esta actividad"                                                    },
     *      {"name" = "oferta_id",    "dataType"="integer", "required"=true,  "description"="Oferta del centro"                                                                                                              },
     *      {"name" = "actividad_id",           "dataType"="integer", "required"=true,  "description"="Id de Actividad NO academica"                                                                                                 },
     *      {"name" = "actividad_academica_id", "dataType"="integer", "required"=true,  "description"="Id de Actividad academica"                                                                                                    },
     *      {"name" = "horario",                "dataType"="array"  , "required"=true,  "description"="Array que representa el horario, dicho array debe contener los keys: dia_semana:0-6, hora_inicio, hora_fin"                   },
     *      {"name" = "obligatorio",            "dataType"="boolean", "required"=true,  "description"="true|false que define si debe ser obligatorio o no"                                                                           },
     *      {"name" = "esquema_id",             "dataType"="integer", "required"=false, "description"="Id del Esquema que se aplicará a la programación de los eventos, siempre y cuando sea una actividad academica"                },
     *      {"name" = "facilitador_id",         "dataType"="integer", "required"=false, "description"="Id facilitador que serà el encargado del aula"                },
     *      {"name" = "aula_id",                "dataType"="integer", "required"=false, "description"="Id del aula"                },
     *      {"name" = "fecha_final",            "dataType"="integer", "required"=false, "description"="fecha final que abarcara la actividad"                },
     *      {"name" = "oferta_actividad_id",    "dataType"="integer", "required"=false, "description"="identificador de la oferta actividad ligada a esa ocupacion"                },
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaActividad"
     * )
     * @return array
     * @Rest\View()
     * @Post("/eventosocupacion")
     */
    public function postEventosOcupacionAction() {
        if (!$this->checkRequestParameters(array('oferta_id','facilitador_id','aula_id','horario'))) {
            return $this->buildErrorView('Faltan especificar datos.');
        }

        if (!array_key_exists('actividad_id', $this->parametros) and
            !array_key_exists('actividad_academica_id', $this->parametros) and
            !array_key_exists('fecha_inicio', $this->parametros)) {
            return $this->buildErrorView('Faltan especificar datos.');
        }

        if (!array_key_exists("actividad_id", $this->parametros)) {
            $this->parametros["actividad_id"] = null;
        }

        if (!array_key_exists("actividad_academica_id", $this->parametros)) {
            $this->parametros["actividad_academica_id"] = null;
        }

        if (!array_key_exists("fecha_final", $this->parametros)) {
            $this->parametros["fecha_final"] = null;
        }

        if (!array_key_exists("obligatorio", $this->parametros)) {
            $this->parametros["obligatorio"] = false;
        }

        if (!array_key_exists("esquema_id", $this->parametros)) {
            $this->parametros["esquema_id"] = null;
        }

        if (!array_key_exists("oferta_actividad_id", $this->parametros)) {
            $this->parametros["oferta_actividad_id"] = null;
        }

        $this->parametros['usuario_id'] = $this->getUsuarioId();

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $this->parametros['contenedor'] = $this->container;
        $respuesta          = $ocupacionBusiness->crearEventosCargaMasiva($this->parametros);

        return $this->view($respuesta, $respuesta['code']);

    }

    /**
     * Rest que Obtiene el EscenarioOferta para un escenario de todos|grupo|centro|agrupacion
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtiene Los eventos de una oferta educativa, un evento ya calendarizado o incluso todos",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="oferta_educativa_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="oferta educativa del centro"
     *      },
     *      {
     *          "name"="tipo",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="especifica el tipo de busqueda, los tipos validos son; oferta|centro|todos"
     *      },
     *      {
     *          "name"="fecha_inicio",
     *          "dataType"="datetime",
     *          "requirement"="yyyy-mm-dd",
     *          "description"="Fecha de inicio desde la que se quiere obtener la lista de eventos"
     *      },
     *      {
     *          "name"="fecha_fin",
     *          "dataType"="datetime",
     *          "requirement"="yyyy-mm-dd",
     *          "description"="Fecha de fin hasta cuando se quieren obtener la lista de eventos"
     *      },
     *      {
     *          "name"="aula_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="identificador del aula que quiere obtener los eventos"
     *      },
     *      {
     *          "name"="facilitador_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="identificador del aula que quiere obtener el facilitador"
     *      }
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\EventoActividadOfertaCentro"
     * )
     * @return array
     * @Rest\View()
     * @Get("/obtenereventoscentro/{oferta_educativa_id}/{tipo}/{fecha_inicio}/{fecha_fin}/{aula_id}/{facilitador_id}" , defaults={"aula_id"="0","facilitador_id"="0"})
     */

    public function getEventosDesdeCentroAction(
        $oferta_educativa_id,
        $tipo,
        $fecha_inicio,
        $fecha_fin,
        $aula_id,
        $facilitador_id
    ) {
        if ($tipo!='todos' and $tipo!='oferta' and $tipo!='centro') {
            return $this->buildErrorView(
                'No es un tipo de busqueda de eventos correcta, solo pueden ser de tipo: (centro|todos|oferta).'
            );
        }

        $ocupacionBusiness = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->obtencionEventosCentroOTodos(
            $tipo,
            $oferta_educativa_id,
            $tipo,
            $fecha_inicio,
            $fecha_fin,
            $aula_id,
            $facilitador_id
        );

        return $this->view($respuesta, $respuesta['code']);
    }


    /**
     * Rest que elimina en la base de datos una Actividad con sus eventos en Centro
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminacion de Oferta Actividad",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="ocupacion_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la ocupacion a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/ocupacion/{ocupacion_id}")
     */
    public function deleteOcupacionAction($ocupacion_id) {
        $datos=$this->obtenerDatosRequest();
        if ($this->esCentral($datos['ip_request'],$datos['headers'])) {
            $central = true;
        } else {
            $central = false;
        }
        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta           = $ocupacionBusiness->eliminarOcupacion($ocupacion_id, $this->getUsuarioId(),null,$central);
        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que modifica en la base de datos un Evento Calendarizado
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Modificación de un evento calendarizado en la Base de Datos",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la Ocupacion a modificar"
     *      }
     *  },
     *  parameters={
     *      {"name" = "fecha_inicio",           "dataType"="date",    "required"=false,  "description"="nueva Fecha a partir de la cual se programa todos los eventos de esta actividad"                                           },
     *      {"name" = "oferta_id",              "dataType"="integer", "required"=true,  "description"="nueva Oferta del centro"                                                                                                    },
     *      {"name" = "actividad_id",           "dataType"="integer", "required"=false,  "description"="nuevo Id de Actividad NO academica"                                                                                        },
     *      {"name" = "actividad_academica_id", "dataType"="integer", "required"=false,  "description"="nuevo Id de Actividad academica"                                                                                           },
     *      {"name" = "horario",                "dataType"="array"  , "required"=false,  "description"="nuevo Array que representa el horario, dicho array debe contener los keys: dia_semana:0-6, hora_inicio, hora_fin"          },
     *      {"name" = "obligatorio",            "dataType"="boolean", "required"=false,  "description"="nuevo estado true|false que define si debe ser obligatorio o no"                                                           },
     *      {"name" = "esquema_id",             "dataType"="integer", "required"=false, "description"="nuevo Id del Esquema que se aplicará a la programación de los eventos, siempre y cuando sea una actividad academica"        },
     *      {"name" = "facilitador_id",         "dataType"="integer", "required"=false, "description"="nuevo Id facilitador que serà el encargado del aula"                                                                        },
     *      {"name" = "aula_id",                "dataType"="integer", "required"=false, "description"="nuevo Id del aula"                                                                                                          },
     *      {"name" = "fecha_final",            "dataType"="integer", "required"=false, "description"="nueva fecha final que abarcara la actividad"                                                                                },
     *      {"name" = "oferta_actividad_id",    "dataType"="integer", "required"=false, "description"="nuevo identificador de la oferta actividad ligada a esa ocupacion"                                                          },
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/eventosocupacion/{id}")
     */
    public function putEventosOcupacionAction($id) {
        $this->parseParametros();
        $datos=$this->obtenerDatosRequest();

        if (!array_key_exists("actividad_id", $this->parametros)) {
            $this->parametros["actividad_id"] = null;
        }
        if (!array_key_exists("actividad_academica_id", $this->parametros)) {
            $this->parametros["actividad_academica_id"]  = null;
        }
        if (!array_key_exists("fecha_final", $this->parametros)) {
            $this->parametros["fecha_final"] = null;
        }
        if (!array_key_exists("obligatorio", $this->parametros)) {
            $this->parametros["obligatorio"] = false;
        }
        if (!array_key_exists("esquema_id", $this->parametros)) {
            $this->parametros["esquema_id"] = null;
        }
        if (!array_key_exists("oferta_actividad_id", $this->parametros)) {
            $this->parametros["oferta_actividad_id"] = null;
        }
        if (!array_key_exists("numero_inscritos", $this->parametros)) {
            $this->parametros["numero_inscritos"] = null;
        }
        if (!array_key_exists("numero_interesados", $this->parametros)) {
            $this->parametros["numero_interesados"] = null;
        }

        if ($this->esCentral($datos['ip_request'],$datos['headers'])) {
            $this->parametros['central'] = true;
        } else {
            $this->parametros['central'] = false;
        }

        $this->parametros['usuario_id'] = $this->getUsuarioId();
        $this->parametros['ocupacion_id']= $id;
        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $this->parametros['contenedor'] = $this->container;



        $respuesta = $ocupacionBusiness ->modificarCargaMasiva($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que obtiene un listado de inscritos e interesados desde mako 1.4 a una ocupacion
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Listado desde mako 1.4 para obtener un listado de inscritos e interesados a una ocupacion",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la Ocupacion obtener sus inscritos e interesados"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/listadoinscritoseinteresados/{id}")
     */
    public function getListadoInscritosInteresadosAction($id) {

        $this->parametros['ocupacion_id']= $id;

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->obtenerListadoInscritosEInteresados($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que valida si una ocupacion puede eliminarse o no
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Validación si una ocupacion puede eliminarse o no",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la Ocupacion a analizar si puede ser eliminada"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/validacioneliminacionocupacion/{id}")
     */
    public function getValidacionEliminacionOcupacionAction($id) {

        $this->parametros['ocupacion_id']= $id;

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->validarEliminacionOcupacion($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que valida si una ocupacion puede cancelarse o no
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Validación si una ocupacion puede cancelarse o no",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la Ocupacion a analizar si puede ser cancelada"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/validacioncancelacionocupacion/{id}")
     */
    public function getValidacionCancelacionOcupacionAction($id) {

        $this->parametros['ocupacion_id']= $id;

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->validarCancelacionOcupacion($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que modifica en la base de datos un Evento Calendarizado
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Cancelacion de una ocupacion calendarizado en la Base de Datos",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la Ocupacion a cancelar"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/cancelacionocupacion/{id}")
     */
    public function putCancelacionOcupacionAction($id) {
        $this->parseParametros();
        $datos=$this->obtenerDatosRequest();
        if ($this->esCentral($datos['ip_request'],$datos['headers'])) {
            $this->parametros['central'] = true;
        } else {
            $this->parametros['central'] = false;
        }

        $this->parametros['usuario_id'] = $this->getUsuarioId();
        $this->parametros['ocupacion_id']= $id;
        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->cancelacionOcupacion($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que secciona una ocupacion calendarizada
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Seccionado de una ocupacion calendarizada en la Base de Datos",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la OcupacionReserva a seccionar"
     *      }
     *  },
     *  parameters={
     *      {"name" = "fecha_desplazamiento",           "dataType"="date",    "required"=true,  "description"="fecha a la cual se desplazara todo el bloque de eventos"                                           }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     }
     * )
     * @return array
     * @Rest\View()
     * @Put("/seccionadoocupacion/{id}")
     */
    public function putSeccionadoOcupacionAction($id) {
        $this->parseParametros();
        $datos=$this->obtenerDatosRequest();

        if ($this->esCentral($datos['ip_request'],$datos['headers'])) {
            $this->parametros['central'] = true;
        } else {
            $this->parametros['central'] = false;
        }

        //Se revisa que contenga el facilitaor_id, de caso contrario,
        //se setea como false
        if (array_key_exists("facilitador_id", $this->parametros)){
            $facilitadorId = $this->parametros['facilitador_id'];
        }else{
            $facilitadorId = false;
        }

        //Seteando el usuario que modifica
        $this->parametros['usuario_id'] = $this->getUsuarioId();
        //Seteando el Id de la ocupación reserva
        $this->parametros['ocupacion_reserva_id']= $id;

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->seccionarOcupacion($this->parametros['ocupacion_reserva_id'],$this->parametros['fecha_desplazamiento'],$this->parametros['usuario_id'],$this->parametros['central'],$facilitadorId);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que obtiene todas las actividades calendarizadas
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Validación si una ocupacion puede eliminarse o no",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/reportegrupos")
     */
    public function getReporteGruposAction() {

        //$this->parametros['ocupacion_id']= $id;
        $parametros = $this->getRequest()->query->all();
        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->reportegrupos($parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que obtiene el resumen de grupos asignados a facilitadores
     *
     * @ApiDoc(
     *  resource=true,
     *  description="resumen de grupos asignados a facilitadores",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/reportegruposbyfacilitadores")
     */
    public function getReporteGruposByFacilitadoresAction() {
        $parametros = $this->getRequest()->query->all();

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager());

        $respuesta = $ocupacionBusiness->reporteGruposByFacilitadores($parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que obtiene el detalle de grupos asignados a un facilitador
     *
     * @ApiDoc(
     *  resource=true,
     *  description="detalle de grupos asignados a un facilitador",
     *  section="CALENDARIZACION Ocupacion - (ControlEscolarCalendarioBundle)",
    *  requirements={
     *      {
     *          "name"="facilitador_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Id de facilitador seleccionado"
     *      },
     *      {
     *          "name"="tipo_grupo",
     *          "dataType"="string",
     *          "requirement"="\s+",
     *          "description"="especifica el tipo de grupo, los tipos validos son; finalizados|actuales|proximos"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/reportegruposbyfacilitador/{facilitador_id}/{tipo_grupo}")
     */
    public function getReporteGruposByFacilitadorAction(
        $facilitador_id,
        $tipo_grupo
    )
    {
        $parametros = $this->getRequest()->query->all();
        //$this->parametros['ocupacion_id']= $id;

        $ocupacionBusiness   = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ocupacionBusiness->reporteGruposByFacilitador(
            $facilitador_id,
            $tipo_grupo,
            $parametros
        );

        return $this->view($respuesta, $respuesta['code']);
    }


}
