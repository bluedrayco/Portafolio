<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;
use ControlEscolar\CalendarioBundle\Business\Entity\Ocupacion AS BOcupacion;


use Core\CoreBundle\Controller\BaseController;

class pruebaController extends BaseController{
    /**
     * @return array
     * @Rest\View()
     * @Post("/prueba")
     */
    public function postPruebaRestAction() {
        //print_r(__CLASS__);
        $atributos = $this->getDoctrine()->getManager()->getClassMetadata('ControlEscolar\CalendarioBundle\Entity\Movimiento')->getFieldNames();
        $identificador = $this->getDoctrine()->getManager()->getClassMetadata('ControlEscolar\CalendarioBundle\Entity\Movimiento')->getIdentifier();
        $relaciones = $this->getDoctrine()->getManager()->getClassMetadata('ControlEscolar\CalendarioBundle\Entity\Movimiento')->getAssociationNames();
        $ejemplo = $this->getDoctrine()->getManager()->getClassMetadata('ControlEscolar\CalendarioBundle\Entity\Movimiento')->getAssociationTargetClass('TipoMovimiento');
        print_r ($atributos);
        print_r($relaciones);
        print_r($identificador);
        print_r($ejemplo);
        exit();
        $this->parseParametros('post');

        $this->parametros['usuario_id'] = $this->getUsuarioId();

        $movimientoBusiness = new \ControlEscolar\CalendarioBundle\Business\Entity\Movimiento($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $movimientoBusiness->crearMovimiento('"\Core\CoreBundle\Entity\Usuario"',5,'cancelacion_ocupacion','[{"campo":"nombre","valor_anterior":"roberto","valor_nuevo":"silvio"}]',$this->getUsuarioId());

        return $this->view($respuesta, $respuesta['code']);
    }
    
    /**
     * @return array
     * @Rest\View()
     * @Get("/prueba22")
     */
    public function getPrueba22Action() {

        $movimientoBusiness = new \ControlEscolar\CalendarioBundle\Business\Entity\Movimiento($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $movimientoBusiness->obtenerMovimientos('\Core\CoreBundle\Entity\ActividadAcademicaTemario22',5, 'cancelacion_ocupacion20');

        return $this->view($respuesta, 200);
    }    
    
        /**
     * @return array
     * @Rest\View()
     * @Get("/pruebacompararobjetos")
     */
    public function getPruebaCompareObjectsAction() {
        $objetoAnterior = $this->getDoctrine()->getManager()->getRepository('ControlEscolarCalendarioBundle:Ocupacion')->find(array('ocupacion_id'=>197));
        $objetoNuevo    = $this->getDoctrine()->getManager()->getRepository('ControlEscolarCalendarioBundle:Ocupacion')->find(array('ocupacion_id'=>199));
        
        //return $this->view(array('objetoAnterior'=>$objetoAnterior,'objetoNuevo'=>$objetoNuevo), 200);
        
        $transBusiness = new \Core\CoreBundle\Lib\EnovaTransformsToArray();

        $respuesta = $transBusiness->obtenerModificacionesToArray($objetoAnterior, $objetoNuevo);

        return $this->view($respuesta, 200);
    }    



    /**
     * @return array
     * @Rest\View()
     * @Get("/prueba2")
     */
    public function getPruebaRest23Action() {
        $params = array('usuario_id' => $this->getUsuarioId());
        $BOcupacion = new BOcupacion($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta  = $BOcupacion->eliminaOcupacionEngine($params);
        //$respuesta = array("code"=>200, "data"=>array(), "message"=>"");
        return $this->view($respuesta, $respuesta['code']);
    }
}
