<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaEducativaCentro as BOfertaEducativaCentro;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OfertaEducativaCentroController extends BaseController {


    /**
     * Rest que Obtiene todos las OfertasEducativasCentros de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todas las OfertasEducativasCentros",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro"
     * )
     * @return array
     * @Rest\View()
     * @Get("/ofertaseducativascentros")
     */
    public function getOfertasEducativasCentrosAction() {
        $parametros    = $this->getRequest()->query->all();
        $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $ofertaEducativaCentroBusiness->listar();
        } else {
            $respuesta = $ofertaEducativaCentroBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que Obtiene Estadisticas de la OfertaEducativaCentro de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de Estadisticas de la OfertasEducativaCentro",
     *  section="CALENDARIZACION OfertaEducativaCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="oferta_educativa_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="oferta educativa del centro"
     *      },
     *      {
     *          "name"="aula_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="identificador del aula que quiere obtener los eventos"
     *      },
     *      {
     *          "name"="facilitador_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="identificador del aula que quiere obtener el facilitador"
     *      }
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\EventoActividadOfertaCentro"
     * )
     * @return array
     * @Rest\View()
     * @Get("/estadisticasofertaeducativacentro/{oferta_educativa_id}/{aula_id}/{facilitador_id}" , defaults={"aula_id"="0","facilitador_id"="0"})
     */

    public function getEstadisticasOfertaEducativaCentroAction($oferta_educativa_id,$aula_id,$facilitador_id){
        $parametros    = $this->getRequest()->query->all();

        $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ofertaEducativaCentroBusiness->obtenerEstadisticas($oferta_educativa_id,$aula_id,$facilitador_id);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que publica una calendarizacion de centro con notificacion de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Rest que publica una calendarizacion de centro con notificacion",
     *  section="CALENDARIZACION OfertaEducativaCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="oferta_educativa_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="oferta educativa del centro a publicar la calendarizacion"
     *      },
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro"
     * )
     * @return array
     * @Rest\View()
     * @Get("/publicacioncalendarizacioncentro/{oferta_educativa_id}")
     */
    public function getPublicacionCalendarizacionCentroAction($oferta_educativa_id){
        $parametros    = $this->getRequest()->query->all();

        $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));


        $respuesta = $ofertaEducativaCentroBusiness->publicacionCalendarizacion($oferta_educativa_id,$this->getUsuarioId());

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que devuelve a partir de una oferta educativa en centro el número de horas disponibles que tienen los failitadores para esa oferta
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Rest que devuelve a partir de una oferta educativa en centro el número de horas disponibles que tienen los failitadores para esa oferta",
     *  section="CALENDARIZACION OfertaEducativaCentro - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="oferta_educativa_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la oferta educativa en la que se estimarán los tiempos disponibles de los facilitadores"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/horasasignadasfacilitadores/{oferta_educativa_id}")
     */
    public function getHorasFacilitadoresAction($oferta_educativa_id) {

        $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta = $ofertaEducativaCentroBusiness->obtenerHorasAsignadasFacilitadores($oferta_educativa_id);

        return $this->view($respuesta, $respuesta['code']);
    }
}