<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\OcupacionReserva as BOcupacionReserva;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OcupacionReservaController extends BaseController {

    /**
     * Rest que Obtiene todos los eventos de OcupacionReserva de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Eventos de un centro en base a OcupacionReserva",
     *  section="CALENDARIZACION OcupacionReserva - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OcupacionReserva"
     * )
     * @return array
     * @Rest\View()
     * @Get("/eventoscentro")
     */
    public function getEventosCentroAction() {
        $parametros    = $this->getRequest()->query->all();
        $ocupacionesReservaBusiness = new BOcupacionReserva($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

            $respuesta = $ocupacionesReservaBusiness->listar($parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que modifica en la base de datos un Evento Calendarizado
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Modificación de un evento calendarizado en la Base de Datos",
     *  section="CALENDARIZACION OcupacionReserva - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del Evento a modificar"
     *      }
     *  },
     *  parameters={
     *      {"name" = "hora_inicio",      "dataType"="time",    "required"=false,  "description"="nueva hora inicio del evento"                                              },
     *      {"name" = "facilitador_id",    "dataType"="integer",    "required"=false,  "description"="identificador del facilitador que cambiara"                                              },
     *      {"name" = "aula_id",           "dataType"="integer",    "required"=false,  "description"="identificador de la nueva aula"                                              },
     *      {"name" = "hora_fin",           "dataType"="time",    "required"=false,  "description"="nueva hora final del evento"                                              },
     *      {"name" = "fecha_evento",           "dataType"="date",    "required"=false,  "description"="nueva fecha del evento"                                              },
     *      {"name" = "numero_asistentes",           "dataType"="integer",    "required"=false,  "description"="nuevo numero de asistentes del evento"                                              },
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/eventocentro/{id}")
     */
    public function putEventoCentroAction($id){
        $this->parseParametros();
        $datos=$this->obtenerDatosRequest();
        if ($this->esCentral($datos['ip_request'],$datos['headers'])) {
            $this->parametros['central']=true;
        }else{
            $this->parametros['central']=false;
        }
        $this->parametros['usuario_id'] = $this->getUsuarioId();
        $this->parametros['ocupacion_reserva_id']=$id;
        $ocupacionReservaBusiness   = new BOcupacionReserva($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta = $ocupacionReservaBusiness ->actualizar($this->parametros);
        return $this->view($respuesta, $respuesta['code']);
    }

}
