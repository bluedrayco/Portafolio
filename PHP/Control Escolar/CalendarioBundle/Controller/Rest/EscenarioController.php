<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\Escenario as BEscenario;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class EscenarioController extends BaseController {

    /**
     * Rest que Obtiene todos los Escenarios de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Escenarios",
     *  section="CALENDARIZACION Escenario - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\Escenario"
     * )
     * @return array
     * @Rest\View()
     * @Get("/escenarios")
     */
    public function getEscenariosAction() {
        $parametros    = $this->getRequest()->query->all();
        $escenarioBusiness = new BEscenario($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $escenarioBusiness->listar();
        } else {
            $respuesta = $escenarioBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }
}
