<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\FacilitadorHorasAsignadas as BFacilitadorHorasAsignadas;


class FacilitadorHorasAsignadasController extends BaseController {

   
     /**
     * Rest que las horas asignadas por facilitador
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtiene las horas asignadas por facilitador",
     *  section="CALENDARIZACION Reportes - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="centro_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Centro ID"
     *      },
     *      {
     *          "name"="facilitador_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Facilitador ID"
     *      },
     *      {
     *          "name"="id_facilitador_mako",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="ID Facilitador MAKO"
     *      },
     *
     *      {
     *          "name"="fecha_inicio",
     *          "dataType"="datetime",
     *          "requirement"="yyyy-mm-dd",
     *          "description"="Fecha de inicio del rango de búsqueda"
     *      },
     *      {
     *          "name"="fecha_fin",
     *          "dataType"="datetime",
     *          "requirement"="yyyy-mm-dd",
     *          "description"="Fecha de fin del rango de búsqueda"
     *      },
     *      {
     *          "name"="tipo_rpt",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Tipo de reporte a generar"
     *      },
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaEducativa"
     * )
     * @return array
     * @Rest\View()
     * @Get("/horasasignadas/{centro_id}/{facilitador_id}/{id_facilitador_mako}/{fecha_inicio}/{fecha_fin}/{tipo_rpt}" , defaults={"centro_id"="0","facilitador_id"="0", "id_facilitador_mako"=0, "tipo_rpt"="1"})
     */
    public function getHorasAsignadasAction(
        $centro_id,
        $facilitador_id,
        $id_facilitador_mako,
        $fecha_inicio,
        $fecha_fin,
        $tipo_rpt = 1
    ) {
        $FacilitadorHorasAsignadasBusiness = new BFacilitadorHorasAsignadas($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $FacilitadorHorasAsignadasBusiness->obtenerReporteFacilitadorHorasAsignadas(
            $centro_id,
            $facilitador_id,
            $id_facilitador_mako,
            $fecha_inicio,
            $fecha_fin,
            $tipo_rpt
        );
        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que obtiene las equivalencias con centros en MAKO
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtención de Id's de centros de Mako",
     *  section="CALENDARIZACION Reportes - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaEducativa"
     * )
     * @return array
     * @Rest\View()
     * @Get("/centrosmako")
     */
    public function getIdMakoCentrosAction() {

        $FacilitadorHorasAsignadasBusiness = new BFacilitadorHorasAsignadas($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $FacilitadorHorasAsignadasBusiness->obtenerIdMakoCentro();
        return $this->view($respuesta, $respuesta['code']);
    }


}
