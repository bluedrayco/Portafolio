<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaEducativa as BOfertaEducativa;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OfertaEducativaController extends BaseController {


    /**
     * Rest que Obtiene todos las OfertasEducativas de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todas las OfertasEducativas",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaEducativa"
     * )
     * @return array
     * @Rest\View()
     * @Get("/ofertaseducativas")
     */
    public function getOfertasEducativasAction() {
        $parametros    = $this->getRequest()->query->all();
        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $ofertaEducativaBusiness->listar();
        } else {
            $respuesta = $ofertaEducativaBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que Obtiene la ultima fecha de la última oferta educativa de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todas las OfertasEducativas",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="\DateTime"
     * )
     * @return array
     * @Rest\View()
     * @Get("/ultimafechavigente")
     */
    public function getObtenerUltimaFechaVigenteAction(){
        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
            $respuesta = $ofertaEducativaBusiness->obtenerFechaUltimaOfertaEducativa();

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que Obtiene la última OfertaEducativa que este en Estatus 1, Sincronizado en False y activo en True
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de la Oferta Educativa con los cambios requeridos",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *  statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaEducativa"
     * )
     * @return array
     * @Rest\View()
     * @Get("/ofertaeducativaactiva")
     */
    public function getOfertaEducativaActivaAction() {
        $parametros    = $this->getRequest()->query->all();
        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
            $respuesta = $ofertaEducativaBusiness->obtenerOfertaEducativaActiva();

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que almacena una OfertaEducativa en la Base de Datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Almacenamiento de una OfertaEducativa en la Base de Datos",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *  parameters={
     *      {"name"="fecha_inicio", "dataType"="date", "required"=true, "description"="fecha de inicio de la oferta educativa"},
     *      {"name"="nombre", "dataType"="string", "required"=true, "description"="nombre de la oferta educativa"},
     *      {"name"="fecha_final", "dataType"="date", "required"=true, "description"="fecha final de la oferta educativa"}
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\OfertaEducativa"
     * )
     * @return array
     * @Rest\View()
     * @Post("/ofertaeducativa")
     */
    public function postOfertaEducativaAction() {
        if(!$this->checkRequestParameters(array(
            'fecha_inicio',
            'nombre',
            'fecha_final'
            ))) {
            return $this->buildErrorView('Faltan especificar datos.');
        }

        $this->parametros['usuario_id'] = $this->getUsuarioId();

        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ofertaEducativaBusiness->crear($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que modifica en la base de datos una OfertaEducativa
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Modificación de una OfertaEducativa en la Base de Datos",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la OfertaEducativa a modificar"
     *      }
     *  },
     *  parameters={
     *      {"name"="oferta_educativa_estatus_id", "dataType"="integer", "required"=false, "description"="nueva oferta educativa estatus de la oferta educativa"},
     *      {"name"="periodo_id", "dataType"="integer", "required"=false, "description"="nuevo periodo de la oferta educativa"},
     *      {"name"="fecha_inicio", "dataType"="date", "required"=false, "description"="nueva fecha de inicio de la oferta educativa"},
     *      {"name"="nombre", "dataType"="string", "required"=false, "description"="nuevo nombre de la oferta educativa"},
     *      {"name"="sincronizado", "dataType"="bool", "required"=false, "description"="nuevo estatus de sincronizado de la oferta educativa"},
     *      {"name"="fecha_final", "dataType"="date", "required"=true, "description"="fecha final de la oferta educativa"}
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/ofertaeducativa/{id}")
     */
    public function putOfertaEducativaAction($id, Request $request) {
        $this->parseParametros();

        $this->parametros["oferta_educativa_id"] = $id;
        $this->parametros['usuario_id'] = $this->getUsuarioId();

        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ofertaEducativaBusiness ->actualizar($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que elimina en la base de datos una OfertaEducativa
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminación de una OfertaEducativa en la Base de Datos",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador de la OfertaEducativa a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/ofertaeducativa/{id}")
     */
    public function deleteOfertaEducativaAction($id) {
        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ofertaEducativaBusiness->eliminar($id);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que publica a los centros la oferta educativa vigente
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Publicacion de una oferta educativa vigente a cada uno de los centros",
     *  section="CALENDARIZACION OfertaEducativa - (ControlEscolarCalendarioBundle)",
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Get("/publicarofertaeducativa/{oferta_educativa_id}")
     */
    public function getPublicacionOfertaEducativaAction($oferta_educativa_id) {
        $this->parseParametros();

        $this->parametros['usuario_id'] = $this->getUsuarioId();
        //Asignando el id de la oferta educativa en caso de que no sea la activa
        $this->parametros['oferta_educativa_id'] = $oferta_educativa_id;

        $ofertaEducativaBusiness = new BOfertaEducativa($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $ofertaEducativaBusiness ->publicar($this->parametros);

        return $this->view($respuesta, $respuesta['code']);
    }
}