<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\Aula as BAula;
use Core\SyncBundle\Business\Action\EntitiesSync as BEntitiesSync;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class AulaController extends BaseController {

    /**
     * Rest que Obtiene todas las Aulas de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todas las Aulas",
     *  section="CALENDARIZACION Aula - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\Aula"
     * )
     * @return array
     * @Rest\View()
     * @Get("/aulas")
     */
    public function getAulasAction() {
        $parametros = $this->getRequest()->query->all();

        $aulaBusiness = new BAula($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        if (count($parametros) == 0) {
            $respuesta = $aulaBusiness->listar();
        } else {
            $respuesta = $aulaBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest para probar generacion de paquetes para envio de datos a los centros
     *
     * @ApiDoc(
     *  resource=true,
     *  description="generacion de paquetes para envio de datos a los centros",
     *  section="CALENDARIZACION PruebaPaquetes - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="paquete"
     * )
     * @return array
     * @Rest\View()
     * @Get("/pruebapaquetes")
     */
    public function getPruebaPaquetesAction() {
        $entitiesSyncBusiness = new BEntitiesSync($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        //$respuesta = $entitiesSyncBusiness->syncEntidadesFromEsquemaActividadPeriodoActividadAcademica();
        $respuesta = $entitiesSyncBusiness->generateInitialPackageToCentro();
        return $this->view($respuesta, $respuesta['code']);
    }
}
