<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\Facilitador as BFacilitador;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class FacilitadorController extends BaseController {

    /**
     * Rest que Obtiene todos los Facilitadores de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Facilitadores",
     *  section="CALENDARIZACION Facilitador - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\Facilitador"
     * )
     * @return array
     * @Rest\View()
     * @Get("/facilitadores")
     */
    public function getFacilitadoresAction() {
        $parametros    = $this->getRequest()->query->all();
        $facilitadorBusiness = new BFacilitador($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $facilitadorBusiness->listar();
        } else {
            $respuesta = $facilitadorBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }


    /**
     * Rest que Obtiene todos los Facilitadores activos e inactivos de la bd
     * que tienen grupos para el reporte de facilitadores
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los Facilitadores Activos",
     *  section="CALENDARIZACION Facilitador - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\Facilitador"
     * )
     * @return array
     * @Rest\View()
     * @Get("/facilitadoresreporte")
     */
    public function getFacilitadoresReporteAction() {
        $parametros    = $this->getRequest()->query->all();
        $facilitadorBusiness = new BFacilitador($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $facilitadorBusiness->listarFacilitadoresReporte($parametros);

        return $this->view($respuesta, $respuesta['code']);
}
}
