<?php
namespace ControlEscolar\CalendarioBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations as Rest;

use Core\CoreBundle\Controller\BaseController;
use ControlEscolar\CalendarioBundle\Business\Entity\DiaFeriado as BDiaFeriado;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class DiaFeriadoController extends BaseController {

    /**
     * Rest que Obtiene todos los DiasFeriados de la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los DiasFeriados",
     *  section="CALENDARIZACION DiaFeriado - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\DiaFeriado"
     * )
     * @return array
     * @Rest\View()
     * @Get("/diasferiados")
     */
    public function getDiasFeriadosAction() {
        $parametros    = $this->getRequest()->query->all();
        $diasFeriadosBusiness = new BDiaFeriado($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        if(count($parametros) == 0) {
            $respuesta = $diasFeriadosBusiness->listar();
        } else {
            $respuesta = $diasFeriadosBusiness->listar($parametros);
        }

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que Obtiene todos los DiasFeriados de la base de datos con formato compatible con la aplicación
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los DiasFeriados formateados",
     *  section="CALENDARIZACION DiaFeriado - (ControlEscolarCalendarioBundle)",
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\DiaFeriado"
     * )
     * @return array
     * @Rest\View()
     * @Get("/diasferiadoswithformato")
     */
    public function getDiasFeriadosFormateadosAction() {
        $parametros    = $this->getRequest()->query->all();
        $diasFeriadosBusiness = new BDiaFeriado($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        
        $respuesta = $diasFeriadosBusiness->obtenerDiasFeriadosFormateado();

        return $this->view($respuesta, $respuesta['code']);
    }

     /**
     * Rest que Obtiene todos los DiasFeriados de un año de la base de datos 
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Obtencion de todos los DiasFeriados formateados",
     *  section="CALENDARIZACION DiaFeriado - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="fecha_inicio",
     *          "dataType"="date",
     *          "requirement"="\d+",
     *          "description"="Fecha inicio de la consulta"
     *      },
     *      {
     *          "name"="fecha_fin",
     *          "dataType"="date",
     *          "requirement"="\d+",
     *          "description"="Fecha final de la consulta"
     *      }
     *  },
     *     statusCodes={
     *         200={
     *           "Exito",
     *           "No se encontraron datos en la base de datos"
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\DiaFeriado"
     * )
     * @return array
     * @Rest\View()
     * @Get("/diasferiadosporreglondefecha/{fecha_inicio}/{fecha_fin}")
     */
    public function getDiasFeriadosPorReglonDeFechaAction($fecha_inicio, $fecha_fin) {
        $parametros    = $this->getRequest()->query->all();
        $diasFeriadosBusiness = new BDiaFeriado($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        
        $respuesta = $diasFeriadosBusiness->obtenerDiasFeriadosPorReglonDeFecha($fecha_inicio, $fecha_fin);

        return $this->view($respuesta, $respuesta['code']);
    }

    /**
     * Rest que almacena un DiaFeriado en la base de datos
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Registro de DiaFeriado en la base de datos",
     *  section="CALENDARIZACION DiaFeriado - (ControlEscolarCalendarioBundle)",
     *  parameters={
     *      {"name" = "nombre",           "dataType"="string",    "required"=true,  "description"="nombre del dia feriado"                                                    },
     *      {"name" = "descripcion",      "dataType"="string", "required"=true,  "description"="descripcion del dia feriado"                                                                                                              },
     *      {"name" = "fecha",            "dataType"="DateTime", "required"=true,  "description"="fecha del dia feriado"                                                                                                 },
     *      {"name" = "puede_publicar", "dataType"="boolean", "required"=true,  "description"="identificador si el dia feriado puede publicarse o no"                                                                                                    },
     *      {"name" = "corporativo",                "dataType"="boolean"  , "required"=true,  "description"="identificador si el dia feriado proviene de corporativo o del centro"                   }     
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * output="ControlEscolar\CalendarioBundle\Entity\DiaFeriado"
     * )
     * @return array
     * @Rest\View()
     * @Post("/diaferiado")
     */
    public function postDiaFeriadoAction() {

        if(!$this->checkRequestParameters(array('nombre','descripcion','fecha','puede_publicar','corporativo'))) {
            return $this->buildErrorView('Faltan especificar datos.');
        }
        $this->parametros['usuario_id'] = $this->getUsuarioId();
        $diasFeriadosBusiness = new BDiaFeriado($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta          = $diasFeriadosBusiness->crear($this->parametros);
        
        return $this->view($respuesta, $respuesta['code']);

    }

    
    /**
     * Rest que elimina en la base de datos un dia Feriado
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Eliminacion de un DiaFeriado",
     *  section="CALENDARIZACION DiaFeriado - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="dia_feriado_id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del dia Feriado a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Delete("/diaferiado/{dia_feriado_id}")
     */
    public function deleteDiaFeriadoAction($dia_feriado_id){
        $this->parseParametros();
        $this->parametros['usuario_id']  = $this->getUsuarioId();
        $this->parametros['dia_feriado_id']= $dia_feriado_id;

        $diaFeriadoBusiness = new BDiaFeriado($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));
        $respuesta          = $diaFeriadoBusiness->eliminar($this->parametros);
        return $this->view($respuesta, $respuesta['code']);
    }
    
    /**
     * Rest que modifica en la base de datos un DiaFeriado
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Modificación de un dia feriado en la Base de Datos",
     *  section="CALENDARIZACION DiaFeriado - (ControlEscolarCalendarioBundle)",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="Identificador del dia feriado a modificar"
     *      }
     *  },
     *  parameters={
     *      {"name" = "nombre",           "dataType"="string",    "required"=true,  "description"="nombre del dia feriado"                                                    },
     *      {"name" = "descripcion",      "dataType"="string", "required"=true,  "description"="descripcion del dia feriado"                                                                                                              },
     *      {"name" = "fecha",            "dataType"="DateTime", "required"=true,  "description"="fecha del dia feriado"                                                                                                 },
     *      {"name" = "puede_publicar", "dataType"="boolean", "required"=true,  "description"="identificador si el dia feriado puede publicarse o no"                                                                                                    },
     *      {"name" = "corporativo",                "dataType"="boolean"  , "required"=true,  "description"="identificador si el dia feriado proviene de corporativo o del centro"                   }     
     *  },
     *  statusCodes={
     *         200={
     *           "Exito",
     *         },
     *         404={
     *           "Sin sesión",
     *           "Error en la base de datos"
     *         }
     *     },
     * )
     * @return array
     * @Rest\View()
     * @Put("/diaferiado/{id}")
     */
    public function putDiaFeriadoAction($id){
        $this->parseParametros();
        $this->parametros['usuario_id']  = $this->getUsuarioId();
        $this->parametros['dia_feriado_id']= $id;

        $diaFeriadoBusiness = new BDiaFeriado($this->getDoctrine()->getManager(),$this->getDoctrine()->getManager('mako14'));

        $respuesta = $diaFeriadoBusiness ->actualizar($this->parametros);
        return $this->view($respuesta, $respuesta['code']);        
    } 
}
