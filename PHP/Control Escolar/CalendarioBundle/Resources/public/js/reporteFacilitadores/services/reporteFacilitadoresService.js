
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('reporteFacilitadoresService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/reportegrupos', { },{
        get:       {
                    method: 'GET',
                    isArray:true,
                    transformResponse : function(data, headerrs){

                        result                              = angular.fromJson(data).data;

                        return result;
                    }
                },
        getResumen:       {
                    method: 'GET',
                    url         : "/calendarizacion/api/entity/calendario/reportegrupos/:ocupacion_id",
                    isArray:false,
                    transformResponse : function(data, headerrs){

                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return result.parametro;
                    }
                },
        getReporteFacilitadores:       {
                    method: 'GET',
                    url     : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/reportegruposbyfacilitadores",
                    isArray:true,
                    transformResponse : function(data, headerrs){
                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return result.reporte;
                    }
                },
        countReporteFacilitadores:       {
                    method: 'GET',
                    url     : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/reportegruposbyfacilitadores",
                    isArray:true,
                    transformResponse : function(data, headerrs){
                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return [result.reporte];
                    }
                },
        getReporteFacilitador:       {
                    method: 'GET',
                    url     : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/reportegruposbyfacilitador/:facilitador_id/:tipo_grupo",
                    isArray:true,
                    transformResponse : function(data, headerrs){

                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        
                        return result.reporte;
                    }
                },
        countReporteFacilitador:       {
                    method: 'GET',
                    url     : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/reportegruposbyfacilitador/:facilitador_id/:tipo_grupo",
                    isArray:true,
                    transformResponse : function(data, headerrs){

                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return [result.reporte];
                    }
                }

        }
    );

});