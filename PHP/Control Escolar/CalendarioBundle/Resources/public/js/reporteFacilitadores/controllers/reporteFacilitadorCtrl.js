app.requires.push('ngCsv');
/**
* @author samuel.abad@enova.mx
* @description Controlador para obtener reporte de facilitador en centros
* @param {angular} $scope
* @param {angular} $compile
* @param {angular} paginadorFactory	-- Factoria para el paginado de smart-table
* @param {angular} facilitadorFuncionesFactory.facilitador  --Factoria para los datos de el facilitador
* @param {angular} facilitadorFuncionesFactory  --Factoria para el puente de funciones entre FacilitadorresCtrl y FacilitadorCtrl
* @param {api} uiCalendarConfig
* @param {factory} centroFactory       -- Factoria que contiene las funciones para la comunicacion con otros controladores
* @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
* @returns {undefined}
**/

function reporteFacilitadorCtrl(
	$scope,
	$filter,
	paginadorFactory,
	facilitadorFuncionesFactory,
	centroFactory,
	centroService,
	reporteFacilitadoresService,
	equivalenciaService
	){

	// 	tableState = {pagination:{start:0,number:15,numberOfPages:2}};
	facilitadorFuncionesFactory;
	paginadorFactory;
	var NUMEROSXPAGINA = 10;
    $scope.collectionfiltered = [];

	/**
	*@description Función que gestiona la carga de objetos al inicio
	*
	**/
	$scope.init = function(){

		facilitadorFuncionesFactory.contarPaginasFacilitador = $scope.contarPaginasFacilitador;
		facilitadorFuncionesFactory.pipeReporteFacilitador = $scope.pipeReporteFacilitador;
		facilitadorFuncionesFactory.getReporteFacilitador = $scope.getReporteFacilitador;
	}
	/*
     * @description Función que obtiene reporte facilitador CSV
     */
    $scope.getReporteFacilitador = function(){
    	if(facilitadorFuncionesFactory.facilitador.id != null){
	        reporteFacilitadoresService.restResource. getReporteFacilitador(
	            {
	            	hostCentro: limpiarUri(centroFactory.centroActual.ip),
	             	facilitador_id: facilitadorFuncionesFactory.facilitador.id,
	             	tipo_grupo: facilitadorFuncionesFactory.facilitador.grupo
	            },
	            function(data){
	                if(data.length == 0){
	                    toastr.error("No se encontraron datos facilitador");
	                }else{
                            $scope.collectionfiltered = data;
	                }

	            },function(){
	                toastr.error("Ocurrio un error al traer la información");
	                $scope.gruposDeFacilitador  = [];
	            }
	        );
		}
    }
    /**
    *@description Función que obtiene los items actividades/curso del facilitador
    * de acuerdo al paginado con smart-table
    **/
    $scope.paginaReporteFacilitador = function(tableState){

    	reporteFacilitadoresService.restResource. getReporteFacilitador(
        	{
        		hostCentro: limpiarUri(centroFactory.centroActual.ip),
            	facilitador_id: facilitadorFuncionesFactory.facilitador.id,
            	tipo_grupo: facilitadorFuncionesFactory.facilitador.grupo,
            	'limit': NUMEROSXPAGINA,
            	'offset':tableState.pagination.start || 0
            },
            function(data){
            	if(data.length == 0){
                	toastr.error("No se encontraron datos facilitador");
                }else{

                    $scope.nombre = facilitadorFuncionesFactory.facilitador.nombre.replace(/ /g,'');
                    $scope.nombreReporte = "facilitador_"+facilitadorFuncionesFactory.facilitador.nombre.replace(/ /g,'')+".csv";
                    $scope.gruposDeFacilitador = data;

                }
            },function(){
                toastr.error("No se encontraron resultados para el facilitador");
                    $scope.gruposDeFacilitador  = [];

            }
        );
    }
    /**
    *@description Función que obtiene los items actividades/curso del facilitador
    * de acuerdo al paginado con smart-table y ordenados de acuerdo al campo (st-sort)
    **/
    $scope.orderReporteFacilitador = function(tableState){
    	tableState.sort.busca = tableState.sort.predicate.split('.');

    	reporteFacilitadoresService.restResource. getReporteFacilitador(
        	{
        		hostCentro: limpiarUri(centroFactory.centroActual.ip),
            	facilitador_id: facilitadorFuncionesFactory.facilitador.id,
            	tipo_grupo: facilitadorFuncionesFactory.facilitador.grupo,
            	'limit': NUMEROSXPAGINA,
            	'offset':tableState.pagination.start || 0,
            	'order': tableState.sort.busca[1],
            	'reverse': tableState.sort.reverse
            },
            function(data){
            	if(data.length == 0){
                	toastr.error("No se encontraron datos facilitador");
                }else{
                    $scope.nombre = facilitadorFuncionesFactory.facilitador.nombre.replace(/ /g,'');
                    $scope.nombreReporte = "facilitador_"+facilitadorFuncionesFactory.facilitador.nombre.replace(/ /g,'')+".csv";
                    $scope.gruposDeFacilitador = data;

                }
            },function(){
                toastr.error("No se encontraron resultados para el facilitador");
                    $scope.gruposDeFacilitador  = [];

            }
        );
    }
    /**
    *@description Función que gestiona el paginado de actividades/curso
    * del facilitador de acuerdo a la directiva st-pipe (smart-table)
    **/
    $scope.pipeReporteFacilitador = function(tableState){
    	if(facilitadorFuncionesFactory.facilitador.id != null){


                if(tableState.sort.predicate == null){
                   $scope.paginaReporteFacilitador(tableState);
                }else{
                    $scope.orderReporteFacilitador(tableState);
                }

    	}else{
    		paginadorFactory = tableState;
    	}
    }
    /**
    *@description Función que obtiene el total de páginas del reporte de
    * facilitador para el paginado con smart-table
    **/
    $scope.contarPaginasFacilitador = function(){

    	if(facilitadorFuncionesFactory.facilitador.id!=null){
    		reporteFacilitadoresService.restResource. countReporteFacilitador(
	            {
	            	hostCentro: limpiarUri(centroFactory.centroActual.ip),
	             	facilitador_id: facilitadorFuncionesFactory.facilitador.id,
	             	tipo_grupo: facilitadorFuncionesFactory.facilitador.grupo,
	             	'count':""
	            },
	            function(data){
	                if(data.length == 0){
	                    //toastr.error("No se cuenta con registros para el Facilitador ");
	                }else{

	                    var residuo = data[0] % NUMEROSXPAGINA;
	                    if(residuo <1){
	                        paginadorFactory.pagination.numberOfPages = data[0] / NUMEROSXPAGINA;
	                    }else{
	                        paginadorFactory.pagination.numberOfPages = ((data[0] - residuo) / NUMEROSXPAGINA) + 1;
	                    }
	                    paginadorFactory.pagination.start = 0;
                        paginadorFactory.search.predicateObject={'$':""};
                        $scope.collectionfiltered = [];
                        $scope.gruposDeFacilitador = [];
                        $scope.pipeReporteFacilitador(paginadorFactory);

	                }

	            },function(){
                            //toastr.error("Ocurrio un error, datos para facilitador inexistentes");
	                    $scope.gruposDeFacilitador  = [];
	            }
        	);
    	}

    }
    /**
    *@description Función que gestiona la busqueda de un curso
    * de acuerdo al nombre o fecha
    ***/
    $scope.buscarCurso = function($event){
    	var keyCode = $event.which || $event.keyCode;
      	if(keyCode === 13){
            if(angular.equals($scope.busquedaCurso,"")){
                $scope.pipeReporteFacilitador(paginadorFactory);
            }else {
                var coincidencia;
                //buscar por fecha
                if(/^\d{2}(\/|-)\d{2}(\/|-)\d{4}$/.test($scope.busquedaCurso) |
                /^\d{4}(\/|-)\d{2}(\/|-)\d{2}$/.test($scope.busquedaCurso)){
                    coincidencia = {'where': $scope.busquedaCurso};
                //buscar por texto
                }else{
                    coincidencia = {'like':$scope.busquedaCurso};
                }
                $scope.buscarFiltro(coincidencia);
            }
      }

    }
    /**
    *@description Función que busca un registro de acuerdo a una entrada de texto
    * en el reporte de facilitador
    ***/
    $scope.buscarFiltro = function(coincidencia){

        if(coincidencia.like){
            reporteFacilitadoresService.restResource. getReporteFacilitador(
                {hostCentro: limpiarUri(centroFactory.centroActual.ip),
                 facilitador_id: facilitadorFuncionesFactory.facilitador.id,
                 tipo_grupo: facilitadorFuncionesFactory.facilitador.grupo,
                 'like':coincidencia.like
                },
                function(data){
                    if(data.length == 0){
                        toastr.error("No se encontraron datos facilitador");
                    }else{
                        $scope.gruposDeFacilitador   = data;

                    }

                },function(){
                    toastr.error("No se encontraron resultados para el facilitador");
                         $scope.gruposDeFacilitador  = "";

                }
            );
        }
        else if(coincidencia.where){
            reporteFacilitadoresService.restResource. getReporteFacilitador(
                {hostCentro: limpiarUri(centroFactory.centroActual.ip),
                 facilitador_id: facilitadorFuncionesFactory.facilitador.id,
                 tipo_grupo: facilitadorFuncionesFactory.facilitador.grupo,
                 'where':coincidencia.where
                },
                function(data){
                    if(data.length == 0){
                        toastr.error("No se encontraron datos facilitador");
                    }else{
                        $scope.gruposDeFacilitador   = data;

                    }

                },function(){
                    toastr.error("No se encontraron resultados para el facilitador");
                         $scope.gruposDeFacilitador  = "";

                }
            );
        }
    }

          /*
     * Funcion que me regresa la cabecera del detalle CSV para exportar
     * @returns array cabecera
     */
    $scope.getHeaderCSVDetalle = function () {
        	if($scope.collectionfiltered.length > 0){
        		return ['Centro', 'Curso', 'Clave Grupo', 'Fecha Inicio', 'Fecha Fin'];
        	}
    }

    $scope.getDatosExportarDetalle = function () {
        //$scope.collectionfiltered = [];
        $scope.getReporteFacilitador();

        if($scope.collectionfiltered.length > 0){
            var reporte = [];
            var src = $scope.collectionfiltered;

            for(var i = 0; i < src.length ; i++){
                reporte.push({
                    nombre_centro:src[i].nombre_centro,
                    curso_nombre:src[i].curso_nombre,
                    clave_grupo:src[i].clave_grupo,
                    fecha_inicio:src[i].fecha_inicio,
                    fecha_fin:src[i].fecha_fin
                });
            }
         return reporte;
        }

    }

    $scope.exportandoACSV = function(){

   		if($scope.collectionfiltered.length > 0){
   			toastr.success('Exportando actividades a CSV.');
   			$scope.collectionfiltered = [];
   		}else{
                $scope.getReporteFacilitador();
            }
    }


}
