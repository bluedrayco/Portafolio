
// Agregando el modulo de ngCsv para exportar a CSV utilizado en los reportes
app.requires.push('ngCsv');


/*
 * @author Isaed Martínez
 * @description Controlador principal para obtener reporte de grupos en centros
 *
 * @param {angular} $scope
 * @param {angular} $compile
 * @param {api} uiCalendarConfig
 * @param {factory} centroFactory       -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function reporteFacilitadoresCtrl(
        $scope,
        $filter,
        paginadorFactory,
        facilitadorFuncionesFactory,
        centroFactory,
        centroService,
        reporteFacilitadoresService,
        equivalenciaService
        ) {

    var tableStateH;
    paginadorFactory;
    $scope.id;
    $scope.nombre;
    $scope.grupo;
    $scope.collectionfiltered = [];
    facilitadorFuncionesFactory;
    var ELEMENTOSXPAGINA = 10;

    var paginas;


    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };

    $scope.exportDataDetalle = function () {
        var blob = new Blob([document.getElementById('exportable2').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };

    /*
     * Agregando las funciones para que cargue el inicio
     **/
    $scope.init = function(fechaSistema){

        centroFactory.fechaSistema = fechaSistema;
        $scope.calculaMeses();

        centroService.restResource.get(
            {},
            function(data){
                if(data.length == 0){
                    toastr.error("No se encontraron centros");
                }else{

                    $scope.centrosRemotos  =  data;


                }
            },function(){
                toastr.error("No se encontraron centros");
            }
        );

    }

    /*
     * se calculan los meses a mostrar en el listado del reporte
     */

    $scope.calculaMeses = function () {

        $scope.mesActual  = $filter('date')(convertirFechaYMD(centroFactory.fechaSistema), "MMMM");
        $scope.hace6Meses = convertirFechaYMD(centroFactory.fechaSistema);
        $scope.hace6Meses.setMonth($scope.hace6Meses.getMonth() -6 );
        $scope.hace6Meses = $filter('date')($scope.hace6Meses, "MMMM");
        $scope.hace2Meses = convertirFechaYMD(centroFactory.fechaSistema);
        $scope.hace2Meses.setMonth($scope.hace2Meses.getMonth() -2 );
        $scope.hace2Meses = $filter('date')($scope.hace2Meses, "MMMM");
        $scope.anoActual  = $filter('date')(convertirFechaYMD(centroFactory.fechaSistema), "yyyy");

    }

    /*
     * evaluando el cambio de fuentes
     */
    $scope.eligiendoCentroFuente = function(){

        if($scope.centro != null){
           var noSeEcontro = "No se encontro el centro";
           //equivalenciaService.restResource.getUrlCentro.url;
            equivalenciaService.restResource.getUrlCentro(
                {centro_id : $scope.centro.centro_id},
                function(data){
                    if(data.length == 0){
                        toastr.success(noSeEcontro);
                    }else{
                        centroFactory.centroActual  =  data[0]

                        $scope.facilitadores =  "";
                        $scope.contarPaginas();
                        $scope.calculaMeses();
                        $scope.collectionfiltered = [];
                        //$scope.getReporteFacilitadores();


                    }
                },function(){
                    toastr.success(noSeEcontro);
                }
            );
        }
    }

    /*
     * @description Función que obtiene la información del reporte de grupos
     */
    $scope.getReporteGrupos = function(){

         resumenService.restResource.getReporteGrupos(
            {hostCentro: limpiarUri(centroFactory.centroActual.ip)},
            function(data){
                if(data.length == 0){

                    toastr.success("No se encontraron grupos");

                }else{

                    $scope.ofertasEducativas  =  data.reporte;
                    $scope.filtroOpen         = true;
                }
            },function(){
                toastr.success("No se encontraron grupos en el centro");
                $scope.ofertasEducativas = "";
                $scope.filtroOpen        = false;
            }
        );

    }

     /*
     * @description Función que obtiene reporte facilitadores totales para exportar CSV
     */
    $scope.getReporteFacilitadores = function(){



         reporteFacilitadoresService.restResource.getReporteFacilitadores(
            {hostCentro: limpiarUri(centroFactory.centroActual.ip)},
            function(data){

                if(data.length == 0){
                    //toastr.success("No se encontraron facilitadores");
                }else{
                    $scope.collectionfiltered = data;

                }

            },function(){

                //toastr.success("No se encontraron grupos en el centro");

            }
        );

    }
    /**
    * @description Función que obtiene parte del reporte de facilitadores
    * de acuerdo a la paginación (numero de página)
    **/
    $scope.paginaReporteFacilitadores = function(tableState){

        reporteFacilitadoresService.restResource.getReporteFacilitadores(
            {
                hostCentro: limpiarUri(centroFactory.centroActual.ip),
                'limit': ELEMENTOSXPAGINA,
                'offset':tableState.pagination.start || 0},
            function(data){

                if(data.length == 0){
                    toastr.success("No se encontraron facilitadores");
                }else{

                    $scope.facilitadores  =  data;

                }

            },function(){

                toastr.success("No se encontraron grupos en el centro");
                $scope.ofertasEducativas  ="";
                $scope.filtroOpen         = false;
            }
        );
    }
    /**
    * @description Función que obtiene parte del reporte de facilitadores
    * de acuerdo a la paginación (numero de página) en orden a un campo
    **/
    $scope.orderReporteFacilitadores = function(tableState){
        tableState.sort.busca = tableState.sort.predicate.split('.');

        reporteFacilitadoresService.restResource.getReporteFacilitadores(
                {
                    hostCentro: limpiarUri(centroFactory.centroActual.ip),
                    'limit': ELEMENTOSXPAGINA,
                    'offset':tableState.pagination.start || 0,
                    'order': tableState.sort.busca[0],
                    'reverse': tableState.sort.reverse
                },
                function(data){

                    if(data.length == 0){
                        toastr.success("No se encontraron facilitadores");
                    }else{

                        $scope.facilitadores  =  data;


                    }

                },function(){

                    toastr.success("No se encontraron grupos en el centro");
                    $scope.ofertasEducativas  ="";
                    $scope.filtroOpen         = false;
                }
            );
    }
    /**
    *@descrition Función que gestiona la directiva st-pipe para
    * el paginado de smart-table
    **/
    $scope.pipeReporteFacilitadores = function(tableState){
        if($scope.centro != null){
            if(tableState.sort.predicate == null){
                $scope.paginaReporteFacilitadores(tableState);
            }else{
                $scope.orderReporteFacilitadores(tableState);
            }
        }else{
            tableStateH = tableState;
        }
    }
    /**
    *@description Función que obtiene el numero de paginas para el
    * reporte de Facilitadores de acuerdo al centro consultado
    **/
    $scope.contarPaginas = function(){

        reporteFacilitadoresService.restResource.countReporteFacilitadores(
                {hostCentro: limpiarUri(centroFactory.centroActual.ip),
                    'count':""
                },
                function(data){

                    if(data.length == 0){

                        toastr.success("No se encontraron facilitadores");
                    }else{

                        var residuo = data[0] % ELEMENTOSXPAGINA;
                        if(residuo <1){
                          tableStateH.pagination.numberOfPages = data[0] / ELEMENTOSXPAGINA;

                        }else{
                          tableStateH.pagination.numberOfPages = ((data[0] - residuo) / ELEMENTOSXPAGINA) + 1;

                        }
                        tableStateH.pagination.start = 0;
                        $scope.facilitadores = [];
                        $scope.pipeReporteFacilitadores(tableStateH);

                    }

                },function(){

                    toastr.success("No se encontraron grupos en el centro");
                    $scope.ofertasEducativas  ="";
                    $scope.filtroOpen         = false;
                }
            );
    }

    /**
    *@description Función que gestiona la selección de un facilitador
    * y despliega el modal para su reporte de actividades de curso
    **/
    $scope.seleccionaFacilitador= function(id,grupo,nombre){

        facilitadorFuncionesFactory.facilitador.id = id;
        facilitadorFuncionesFactory.facilitador.grupo = grupo;
        facilitadorFuncionesFactory.facilitador.nombre = nombre;

        facilitadorFuncionesFactory.contarPaginasFacilitador(paginadorFactory);
        $("#idModalDetalle2Facilitador").modal("show");
        facilitadorFuncionesFactory.getReporteFacilitador();

    }

     /*
     * Funcion que me regresa la cabecera del CSV para exportar
     * @returns array cabecera
     */
    $scope.getHeaderCSV = function () {
        if($scope.collectionfiltered.length > 0){
            return ['Nombre', 'Este mes', 'Los próximos meses de este año', 'Los últimos dos meses', 'Los últimos 6 meses', 'Este año', 'Total'];
        }else{
            $scope.getReporteFacilitadores();
        }
    };


    $scope.getDatosExportar = function () {
            if($scope.collectionfiltered.length > 0){
               var reporte = [];
               //var src = $scope.facilitadores;
               var src = $scope.collectionfiltered;

               for(var i = 0; i < src.length ; i++){
                  reporte.push({
                    nombre_facilitador:src[i].nombre_facilitador,
                    tot_hace_2_meses:src[i].tot_hace_2_meses,
                    tot_mes_actual:src[i].tot_mes_actual,
                    tot_ult_6_meses:src[i].tot_ult_6_meses,
                    proximos_meses:src[i].proximos_meses,
                    tot_este_anio:src[i].tot_este_anio,
                    totales:src[i].totales

                  });
               }

                return reporte;
            }else {
                return false;
            }
    }



    /*
     * Funcion que muestra el mensaje de exportando
     *
     */
    $scope.exportandoACSV = function () {
        if($scope.collectionfiltered.length > 0){
            $scope.getDatosExportar();
            toastr.success('Exportando actividades a CSV.');
        }else{
            return false;
        }
    }

    $scope.readFilteredCollection = function(stCtrl) {
       $scope.collectionfiltered = stCtrl.getFilteredCollection();
    }
    /**
    * @description Función que gestiona la busqueda un facilitador por
    * nombre
    ***/
    $scope.buscarFacilitador = function($event){
        var keyCode = $event.which || $event.keyCode;
        if(keyCode === 13){
            reporteFacilitadoresService.restResource.getReporteFacilitadores(
                {hostCentro: limpiarUri(centroFactory.centroActual.ip),
                    'like':$scope.busquedaF
                    },
                function(data){

                    if(data.length == 0){
                        toastr.success("No se encontraron facilitadores");
                    }else{

                        $scope.facilitadores  =  data;

                        //$scope.filtroOpen     = true;

                    }

                },function(){

                    toastr.success("No se encontraron grupos en el centro");
                    $scope.ofertasEducativas  ="";
                    $scope.filtroOpen         = false;
                }
            );
        }
    }


    /**
     * Función que convierte fechas de formato YYYY-mm-dd
     * a date (para no tener prblemas en firefox)
     * @param {fecha} es un string con esta estrucura YYY-mm-dd
     * @return {Date} regresa una variable de tipo date
     */
    function convertirFechaYMD(fecha){
        var tmp                     = fecha.split("-");
        return new Date(tmp[0], parseInt(tmp[1]) -1, tmp[2],0,0,0);

    }


}
/* EOF */