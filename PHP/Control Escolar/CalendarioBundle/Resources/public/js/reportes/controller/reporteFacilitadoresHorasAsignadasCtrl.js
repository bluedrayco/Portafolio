
// Agregando el modulo de ngCsv para exportar a CSV utilizado en los reportes
app.requires.push('ngCsv');


/*
 * @author Jaime Hernandez Cruz y Luis Gonzalez
 * @description Controlador para gwestionar el reporte de facilitadores horas asignadas
 *
 * @param {angular} $scope
 * @param {angular} $compile
 * @param {api} uiCalendarConfig
 * @param {factory} centroFactory       -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {factory} reporteFacilitadoresService  -- Servicio para obtener los datos para generar los reportes
 * @param {factory} reporteFacilitadoresService  -- Servicio para obtener las equivalencias de centros y facilitadores
 * @returns {undefined}
 */
function reporteFacilitadoresHorasAsignadasCtrl(
        $scope,
        $filter,
        centroFactory,
        centroParametrosService,
        reporteFacilitadoresService
        ) {

    $scope.parametros = {centro:{}, facilitador: {}, fecha_inicio:'2016-01-01', fecha_fin:'2016-12-12', tipo_rpt:1 }

    $scope.ipConsultada  = centroFactory.centroActual.ip;
    $scope.idConsultada  = centroFactory.centroActual.id;
    $scope.lstCentros = [];
    $scope.lstFacilitadores = [];
    $scope.objCentroTodos = {
                    centro_id: 0,
                    equivalencia_id: 0,
                    ip: "",
                    mako_id: 0,
                    nombre: "Todos los centros"
                };
    $scope.objFacilitadorTodos = {
                activo: false,
                apellido_materno: "",
                apellido_paterno: "",
                descripcion: null,
                email: "",
                facilitador_id: 0,
                id_facilitador_mako: 0,
                fecha_alta: "",
                fecha_modificacion: "",
                nombre: "Todos los facilitadores"
            }

    $scope.datos_reporte = [];
    $scope.reportes = [];
    var cabeceras = [
                            {
                                      titulo:'Centro'
                                      ,llave :'nombre_centro'
                                     },
                                     {
                                      titulo:'Nombre facilitador'
                                      ,llave :'nombre_facilitador'
                                     },
                                     {
                                      titulo:'Email'
                                      ,llave :'email'
                                     },
                                     {
                                      titulo:'Actividad'
                                      ,llave :'actividad'
                                     },
                                     {
                                      titulo:'Categoría'
                                      ,llave :'categoria_actividad'
                                     },
                                     {
                                      titulo:'Clave de grupo'
                                      ,llave :'clave_grupo'
                                     },
                                     {
                                      titulo:'Estatus grupo'
                                      ,llave :'estatus_grupo'
                                     },
                                     {
                                      titulo:'Horas totales asignadas'
                                      ,llave :'duracion_hrs'
                                     }
                                     ,{
                                      titulo:'Estatus facilitador'
                                      ,llave :'estatus_facilitador'
                                     }
                                 ];
    var cabecerasHorasCurso = [
                                 {
                                      titulo:'Centro'
                                      ,llave :'nombre_centro'
                                     },
                                     {
                                      titulo:'Nombre facilitador'
                                      ,llave :'nombre_facilitador'
                                     },
                                     {
                                      titulo:'Email'
                                      ,llave :'email'
                                     },
                                     {
                                      titulo:'Horas totales'
                                      ,llave :'duracion_hrs'
                                     }
                                     ,{
                                      titulo:'Estatus facilitador'
                                      ,llave :'estatus_facilitador'
                                     }
                                ];

        $scope.reporteSeleccionado = {};

    /*
     * Agregando las funciones para que cargue el inicio
     **/
    $scope.init = function(){
       //Quitando cabeceras no necesarias para el otro reporte
        $scope.reportes = [{
                        titulo    : 'Reporte de horas por actividad asignadas a facilitadores',
                        nombre_csv: 'Reporte_horas_x_actividad',
                        tipo_rpt  : 1,
                        cabeceras : cabeceras
                        },
                       {
                        titulo    : 'Reporte de horas asignadas a facilitadores',
                        nombre_csv: 'Reporte_horas_asignadas',
                        tipo_rpt  : 2,
                        cabeceras : cabecerasHorasCurso
                        }
                       ];
        //Inicializa el form
        $scope.cancelarLimpiar();
        reporteFacilitadoresService.restResource.getCentros(
            {},
            {},
            function(result) {
                $scope.lstCentros.push($scope.objCentroTodos);
                $scope.lstCentros = $scope.lstCentros.concat(result);
                $scope.centro_actual = $scope.objCentroTodos;
            },
            function (error) {
                toastr.error("No se encontraron centros");
            });
    }

    /**
     * Función para evaluar las fechas del reporte
     * @param {type} newDate
     * @param {type} oldDate
     * @param {type} tipo_fecha
     * @returns {undefined}
     */
    $scope.selectDatePicker = function (newDate, oldDate,tipo_fecha) {
        var fecha = new Date(newDate);
        //Revisa si la fecha fin es menor que la fecha inicio
        if(new Date($scope.fechaFin)<new Date($scope.fechaInicio)){
            toastr.warning("Fecha invalida: fecha fin menor a fecha inicio");

            $scope[tipo_fecha] = (tipo_fecha=='fechaInicio')?oldDate:'';
            return;
        }
       $scope[tipo_fecha]= getSoloFecha(fecha);
   }

    /*
     * Función que obtiene los facilitadores por cada centro que ha sido
     * seleccionado
     * @returns null
     */
   $scope.cargaFacilitadoresPorCentro = function(){
       if($scope.centro_actual.ip != 0){
        var m = loader.modal();
         centroParametrosService.restResource.getFacilitadoresReporte(
             {hostCentro: limpiarUri($scope.centro_actual.ip)},{},
             function(data){
                     m.remove();
                     $scope.lstFacilitadores = [];
                     $scope.lstFacilitadores.push($scope.objFacilitadorTodos);
                     $scope.facilitador_actual = $scope.objFacilitadorTodos;

                     $scope.lstFacilitadores =  $scope.lstFacilitadores.concat(data);

                     toastr.success('Se obtuvieron: '+data.length+' registros');
             },
             function(error){
                 $scope.lstFacilitadores = [];
                 $scope.lstFacilitadores.push($scope.objFacilitadorTodos);
                 // Aqui ocurrio un error al momento de obtener los
                 // facilitadores del centro.
                     toastr.error("No se encontraron facilitadores del centro");
                 m.remove();
             });
         }else{
             $scope.lstFacilitadores = [];
             $scope.lstFacilitadores.push($scope.objFacilitadorTodos);
             $scope.facilitador_actual = $scope.objFacilitadorTodos;
         }
   }

   /*
    * Función que limpia los campos del formulario
    * @returns {undefined}
    */
   $scope.limpiaCampos = function () {
        $scope.centro_actual= null;
        $scope.facilitador_actual = null;
   }

    /**
     * Función que reinicia el formulario a 0's
     * @returns {Boolean}
     */
    $scope.cancelarLimpiar = function(){
        $scope.lstFacilitadores = [];
        $scope.lstFacilitadores.push($scope.objFacilitadorTodos);
        $scope.centro_actual        = $scope.objCentroTodos;
        $scope.facilitador_actual   = $scope.objFacilitadorTodos;
        $scope.fechaInicio          = '';
        $scope.fechaFin             = '';
        $scope.chkTodosCentros      = false;
        $scope.reporteSeleccionado  = $scope.reportes[0];
        $scope.datos_reporte = [];
        $scope.datos_reporte2 = [];
    }

    /*
     * Función que desabile el formulario para que no puedas enviar datos
     * hasta que no se cumpla los campos obligatorios
     * @returns {Boolean}
     */
   $scope.deshabilitaFormulario = function() {
        if(!$scope.fechaInicio) {
            return true;
        }
        if(!$scope.fechaFin){
            return true;
        }
        return false;
   }

   /**
    * @ngdoc function
    * @name getReporteHorasAsignadas
    * @eventOf reporteFacilitadoresHorasAsignadasCtrl
    * @description
    * Función que se conecta a SIE para obtener la información a desplegar
    * en el reporte en control-escolar
    * @param {scope} Variable global de angular que tiene los valores seleccionados en frontend
    **/
    $scope.getReporteHorasAsignadas = function(){
        $scope.params_data = {
                centro_id:              $scope.centro_actual.mako_id,
                facilitador_id:         $scope.facilitador_actual.facilitador_id,
                id_facilitador_mako:    $scope.facilitador_actual.id_facilitador_mako,
                fecha_inicio:           $scope.fechaInicio,
                fecha_fin:              $scope.fechaFin,
                tipo_rpt:               $scope.reporteSeleccionado.tipo_rpt
        }
        var m = loader.modal();
        reporteFacilitadoresService.restResource.getHorasAsignadas($scope.params_data,{},
            function(data){
                $scope.datos_reporte = data;
                $scope.datos_reporte2 = data;
                toastr.success('Se obtuvieron: '+$scope.datos_reporte.length+' registros');
                m.remove();
            },
            function(error){
                toastr.error(error);
                m.remove();
            }
        );
    }

    $scope.destruyeRpt = function ()
    {
        //alert("Destuyendo datos...");
        $scope.datos_reporte = [];
        $scope.datos_reporte2 = [];
    }

    /*
     * Funcion que muestra el mensaje de exportando
     *
     */
    $scope.exportandoACSV = function () {
      if(!$scope.deshabilitaFormulario()){
        $scope.getDatosExportar();
        toastr.success('Exportando actividades a CSV.');
        }else{
            toastr.warning('Csv vacio');
        }
        return false;
    }

    $scope.readFilteredCollection = function(stCtrl) {
       $scope.collectionfiltered = stCtrl.getFilteredCollection();
    }

    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };

    $scope.exportDataDetalle = function () {
        var blob = new Blob([document.getElementById('exportable2').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };

    /**
     * Función que concatena el nombrepara colocar en el archivo csv a exportar
     * @returns {Array|reporteFacilitadoresHorasAsignadasCtrl.$scope.cabeceras}
     */
    $scope.obtenerNombreReporte = function(){
        return $scope.reporteSeleccionado.nombre_csv+'_'+$scope.fechaInicio+'_a_'+$scope.fechaFin+'.csv';
    }

    /*
     * Funcion que me regresa la cabecera del CSV para exportar
     * @returns array cabecera
     */
    $scope.getHeaderCSV = function () {
        $scope.cabeceras=[];
        angular.forEach($scope.reporteSeleccionado.cabeceras, function(cabecera) {
            $scope.cabeceras.push(cabecera.titulo);
        });

        //Encontrando el reporte de acuerdo al attributo
        return $scope.cabeceras;
    };

    $scope.getDatosExportar = function () {
            var reporte = [];
               var src = $scope.collectionfiltered;

               for(var i = 0; i < src.length ; i++){
                    if($scope.reporteSeleccionado.tipo_rpt == 1){
                        reporte.push({
                            nombre_centro:src[i].nombre_centro,
                            nombre_facilitador:src[i].nombre_facilitador,
                            email:src[i].email,
                            actividad:src[i].actividad,
                            categoria_actividad:src[i].categoria_actividad,
                            clave_grupo:src[i].clave_grupo,
                            estatus_grupo:src[i].estatus_grupo,
                            duracion_hrs:src[i].duracion_hrs,
                            estatus_facilitador:src[i].estatus_facilitador
                        });
                    }
                    else
                        reporte.push({
                            nombre_centro:src[i].nombre_centro,
                            nombre_facilitador:src[i].nombre_facilitador,
                            email:src[i].email,
                            duracion_hrs:src[i].duracion_hrs,
                            estatus_facilitador:src[i].estatus_facilitador
                        });
               }

                return reporte;
    }

}
/* EOF */