
/*
 * Servicio que obtiene el reporte de horas asignadas por facilitador
 */
app.service ('reporteFacilitadoresService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/reportes/facilitadorhorasasignadas', { },{
        /*
         * Rest que obtiene las horas asignadas de acuerdo al tipo de reporte dado
         * Si es tipo_rpt == 1 obtiene horas por curso asignadas a facilitadores
         * Si es tipo_rpt == 2 obtiene horas totales por facilitador
         * Si centro_id == 0 va por todos los centros de Mako
         * si facilitador_id == 0 va por todos los facilitadores
         */
        getHorasAsignadas:       {
                    method: 'GET',
                    url     : "/calendarizacion/api/entity/calendario/reporte/facilitador/horasasignadas/:centro_id/:facilitador_id/:id_facilitador_mako/:fecha_inicio/:fecha_fin/:tipo_rpt",
                    isArray:true,
                    transformResponse : function(data, headerrs){

                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return result;
                    }
        },
        /*
        *  Servicio angular que obtiene los centros con equivalencia de Mako
        *  @return array centrosMako array con centros de CE equivalencia de Makp
        */
        getCentros:       {
                    method: 'GET',
                    url     : "/calendarizacion/api/entity/calendario/reporte/facilitador/centrosmako",
                    isArray :true,
                    transformResponse : function(data, headerrs){
                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return result.centrosMako;
                    }
                }

        }
    );

});