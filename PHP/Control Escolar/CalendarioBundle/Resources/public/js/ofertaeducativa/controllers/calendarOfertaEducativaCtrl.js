/*
 * @author Jaime Hernández Cruz
 * @description Controlador principal del calendario de oferta educativa cambiado a programación CE
 *
 * @param {angular} $scope
 * @param {angular} $compile
 * @param {api} uiCalendarConfig
 * @param {service} ofertaActividadService
 * @param {factory} ofertaEducativaFactory    -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {factory} eventoFactory             -- Factoria que contiene las funciones de evento para la comunicacion con otros controladores
 * @param {factory} escenarioFactory          -- Factoria que contiene las funciones y datos de escenario a compartir con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function calendarOfertaEducativaCtrl(
        $scope
        ,$compile
        ,uiCalendarConfig
        ,ofertaActividadService
        ,ofertaEducativaFactory
        ,escenarioFactory
        ,eventoFactory
        ,ofertaEducativaService
        ,centroParametrosService
        ,centroFactory
        ) {

      var date = new Date();
    var d    = date.getDate();
    var m    = date.getMonth();
    var y    = date.getFullYear();
    $scope.events                   = [];
    $scope.eventSources             = [];
    $scope.ofertaEducativa          = {};
    $scope.ofertaActividad          = {};
   // $scope.diasFeriados             = [];
    $scope.ofertaFechaFinal         = null;
    $scope.variableEstatus          = {
                                        primerGetActividadesOfertaEscenario : true
                                      };


    /*
     * Agregando las funciones para que cargue el inicio
     * @param {type} fechaSistema
     * @param {type} fechaHoraSistema
     * @param {type} timezone
     * @param {type} tipoProgramacionCE Es el tipo de programación CE que se ocupa para programar
     *                                  ofertas educativas de forma extemporanea o normal
     * @returns {undefined}
     */
    $scope.init = function(fechaSistema,fechaHoraSistema,timezone,tipoProgramacionCE){

        centroFactory.fechaSistema    = fechaSistema;
        centroFactory.fechaHoraSistema= new Date(fechaHoraSistema);
        $scope.timezoneSistema        = timezone;

        ofertaEducativaFactory.renderActividadesEnCalendario = $scope.renderActividadesEnCalendario;   // Cargando la funcion en la variable por
        ofertaEducativaFactory.irAFechaCalendario           = $scope.irAFechaCalendario;
        ofertaEducativaFactory.ofertaSoloLectura            = $scope.ofertaSoloLectura;
        ofertaEducativaFactory.tipoProgramacionCE           = tipoProgramacionCE;

        ofertaEducativaService.restResource.getOfertaEducativaVigente(
            {},
            function(data){
                $scope.ofertaEducativa   =  data.ofertaeducativa;
                if(!$scope.ofertaEducativa){
                    toastr.warning("No exite programación CE activa");
                    escenarioFactory.cargaEscenarios(null);
                    $scope.cargarCalendario();
                }else{
                    $scope.ofertaEducativa.fecha_inicio          = getSoloFecha(new Date($scope.ofertaEducativa.fecha_inicio));
                    $scope.ofertaEducativa.fecha_fin             = getSoloFecha(new Date($scope.ofertaEducativa.fecha_fin));
                    ofertaEducativaFactory.ofertaEducativa       = $scope.ofertaEducativa;
                    escenarioFactory.cargaEscenarios( function(){$scope.cargarCalendario()})
                }
            }
        );

    }
    /*
    * Función que sirve para cargar todo el calendario con sus parametros y la confirguracion de calendar io
    * @returns {undefined}
    */
    $scope.cargarCalendario = function(){
        ofertaEducativaService.restResource.getDiasFeriados(
                            {},
                            function(data){
                                $scope.diasFeriados =  data;
                                if(!$scope.diasFeriados){
                                    toastr.success("No hay días no laborables");
                                }else{
                                    toastr.success("Días no laborables cargados");
                                }

                                //$scope.configurarCalendario();                                                     //Configuración inicial del calendario
                                //Inicializacion de parametros
                               centroParametrosService.restResource.getParametros(
                                          {hostCentro: limpiarUri(centroFactory.centroActual.ip)}
                                          ,{}, function(data){
                                              if(data.length > 0){
                                                centroFactory.cargaInicialParametros(data);
                                                $scope.configurarCalendario();
                                                ofertaEducativaFactory.evaluaDiasPublicarOfertaCentral();

                                              }
                                          });
                            }
                        );

    }


    /*
     * Función que regresa  solo la fecha en formato de cadena
     * @param {type} valor
     * @returns {String}
     *
     */
    $scope.getSoloFecha = function(valor){
      var mesFinal              =  ((valor.getMonth()  + 1) <10)?("0"+(valor.getMonth()  + 1)):(valor.getMonth()  + 1);
      var diaFinal              =  ( valor.getDate() <10 )?("0" + valor.getDate()):valor.getDate();
      var fin                   =  valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;

      return fin;

    }


    /*
     * event source that calls a function on every view switch
     * */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: []
    };

    /**
    *
    * @ngdoc method
    * @name controlEscolar.calendarOfertaEducativaCtrl#eventEdition
    * @methodOf controlEscolar.calendarOfertaEducativaCtrl
    * @param {date} date es la fecha que se le dio click
    * @param {object} jsEvent contiene los atributos del evento que se le io clic
    * @param {object} view contiene los atributos del calendario
    * @description
    * Función que se ejecuta al realizar click sobre un evento
    */
    $scope.eventEdition = function( date, jsEvent, view){
            if(date.esFeriado != undefined){
                toastr.warning("No se pueden registrar sesiones en un <strong>día feriado</strong>");
            }
            else if(date.externo == true && date.nombreOfertaExterna != 'false' ){
                toastr.warning("La actividad pertenece a la programación CE: <strong>" + date.nombreOfertaExterna + "</strong>");
            }
            else{
              eventoFactory.editarEvento(date);
                $("#idModalEventos").modal("show");
            }


    };

    /* alert on Drop */
     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };

    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };

    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };

    /* add custom event*/
    $scope.addEvent = function(titulo,fechaInicio,fechaFin,clase) {
      $scope.events.push({
        title           : titulo,
        start           : fechaInicio,
        end             : fechaFin,
        className       : clase,
        durationEditable: false,
      });
    };

    /*
     * Evento que se detona al dar clic sobre el dia
     *
     * @param {Object} contiene la fecha y hora respecto al timezone
     * @returns {undefined}
     */
    $scope.agregarEventoXDia = function(date,jsEvent,view) {
        if(view.name == "month")
            date = date.zone(-5);
        if($scope.ofertaSoloLectura()){
            var alerta = "No se pueden registrar sesiones, ";
            var ofertaPublicada = ofertaEducativaFactory.puedePublicarOferta();
            alerta = (ofertaPublicada) ? alerta+"la oferta ya esta publicada." : alerta + "oferta solo de lectura.";
           toastr.warning(alerta);
        }else{
             ofertaEducativaFactory.verificarFechaCreacion(
                date
                ,eventoFactory
                ,$scope.timezoneSistema
                ,centroFactory.fechaHoraSistema);
        }
    }

    /*
     * Evalua si un dia es feriado y regresa si es falso o no para el estilo
     * @param {type} date
     * @returns {Boolean}
     */
    $scope.evaluaDiaFeriado  = function(date){
      var inicio = null;
      var fin    = null;
      var fecha  = new Date(date);
      for(var n =0; n < $scope.diasFeriados.length; n++){
        inicio = new Date($scope.diasFeriados[n].start);
        fin    = new Date($scope.diasFeriados[n].end);

        if(fecha >= inicio && date <= fin){
          return true;
        }
      }
      return false;

    }

    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };

    /* Change View */
    $scope.changeView = function(view,calendar) {
      //uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };

    /* Change View */
    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };

     /* Render Tooltip */
    $scope.eventRender = function( event, element, view ) {

        if(event.esFeriado != undefined){
           return false;
        }
        var iconoActividad = event.es_academica? "fa-graduation-cap":"fa fa-cog";


        element.find('.fc-title').before(
            jQuery('<i class="fa '+iconoActividad+' icono-evento" />').text("")
	      );
        if(event.obligatorio){
           element.find('.fc-title').before(
            jQuery('<i class="fa fa-lock icono-evento" />').text("")
	      );
        }

        if(event.hasOwnProperty("extemporanea")){
          if(event.extemporanea){
            element.find('.fc-title').before(
              jQuery('<i class="fa fa-plus-square icono-evento" />').text(""));
          }
        }

        element.find('.fc-title').after(
            jQuery('<div class="event-nombre-aula"/>').text("")
	      );
        var tipoIcono ="";
        switch(event.tipo_escenario){
          case "todos":
            tipoIcono = "fa-home";
          break;
          case "grupo":
            tipoIcono = "fa-group";
          break;
          case "centro":
            tipoIcono = "fa-male";
          break;
        }
        element.find('.fc-title').before(
            jQuery('<i class="fa '+tipoIcono+' icono-evento" />').text("")
        );

        if(event.hasOwnProperty("externo")){
            if(event.externo == true){
                element.addClass("eventoExterno");
                element.find('.fc-title').before(
                    jQuery('<i class="fa fa-external-link  icono-evento" />&nbsp;').text("")

                );
            }
        }

        if(event.hasOwnProperty("tipo_escenario")){

            if(event.tipo_escenario !=  ofertaEducativaFactory.escenarioOferta._escenario._tipo_escenario.clave
                || ofertaEducativaFactory.checkMostrarTodosLosEventos == true){

                element.addClass("eventoSoloLectura");
            }
        }

        if(event.hasOwnProperty("sincronizado") && event.sincronizado == false)
            element.removeClass("eventoSoloLectura");
        else
            element.addClass("eventoSoloLectura");

        $compile(element)($scope);
    };

    /*AGREGANDO LAS FUNCIONES DE INIT*/
    $scope.date = {}

    /**
     * Obtiene los grupos del plan anual
     */
    $scope.listCentros = function() {
        ofertaEducativaService.restResource.get(
                        {},
                        function(result){
                           $scope.ofertasEducativas = result;
                           $scope.ofertasEducativas2 = [].concat($scope.ofertasEducativas);
                           $scope.permitirCrearOferta = true;
                        },
                        function(){
                          toastr.error(ocurrioError+": traer ofertas educativas.");
                        }
                    );
    }

    /*
     * Obtiene los eventos correspondientes al calendario y los renderea en el mismo
     * @returns {undefined}
     */
    $scope.renderActividadesEnCalendario = function(){
        $scope.calendarioActual = uiCalendarConfig.calendars["ofertaEducativaCalendar"];
        $scope.calendarioActual.fullCalendar('refetchEvents');
    }

    /*
     * Setea ek calendario en una nueva fecha
     * @param {type} nuevaFecha nueva fecha a donde se colocara el calendario
     * @returns {undefined}
     */
    $scope.irAFechaCalendario = function(nuevaFecha){
        $scope.calendarioActual = uiCalendarConfig.calendars["ofertaEducativaCalendar"];
        $scope.calendarioActual.fullCalendar( 'gotoDate', nuevaFecha );

        var view = $scope.calendarioActual.fullCalendar( 'getView' )
        $scope.evaluaVista(view);
    }


    /*
     * Genera la configuración del calendario
     * */
    $scope.configurarCalendario = function(){
        var date = new Date();
        var d    = date.getDate();
        var m    = date.getMonth();
        var y    = date.getFullYear();
        var fechaInicio = (ofertaEducativaFactory.ofertaEducativa.fecha_inicio) ? ofertaEducativaFactory.ofertaEducativa.fecha_inicio : new Date();

        $scope.uiConfig = {                                                                                 // configurando el calendario
          calendar:{

            editable         : true,
            lang              :'es',
            header:{
              left: 'prev,next today',
              center: 'title',
              //right: '',
              right: 'month,agendaWeek,agendaDay'
            },
            defaultView      : 'agendaWeek',
            monthNames       : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort  : ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNames         : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            dayNamesShort    : ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
            buttonText       : {
                                today: 'hoy',
                                month: 'mes',
                                week: 'semana',
                                day: 'día'
                             },
            eventClick       : $scope.eventEdition,
            eventDrop        : $scope.alertOnDrop,
            eventResize      : $scope.alertOnResize,
            eventRender      : $scope.eventRender,
            dayClick         : $scope.agregarEventoXDia,
            //timezone         : 'America/Mexico_City',
            //timezoneParam    : 'America/Mexico_City',
            timezone         : 'local',
            timezoneParam    : 'local',
            weekends         : true,
            agenda           : "",
            //contentHeight    : 1300,
            slotDuration     : '00:15:00',
            //minTime          : centroFactory.horarioCentro.apertura + ':00:00',
            //maxTime          : centroFactory.horarioCentro.cierre    + ':00:00',
            minTime          : centroFactory.horarioCentro.apertura + ':00',
            maxTime          : centroFactory.horarioCentro.cierre + ':00',
            viewRender       : $scope.evaluaVista,
            events           : $scope.getActividadesOfertaEscenario,
            allDaySlot       : false,
            allDayDefault    : false,
            hiddenDays       : [ 0 ],
            defaultDate      : fechaInicio
          }
        };

    }


    /*
     * Obtiene las actividades oferta por escenario dado
     * @returns events {Obtiene los eventos}
     */
    $scope.getActividadesOfertaEscenario = function(fechaInicio, fechaFin, timezone, callback){
        if(ofertaEducativaFactory.ofertaEducativa.hasOwnProperty('oferta_educativa_id')){
            //var m               = loader.modal();
            var fechaInicio     = new Date(fechaInicio);
            var fechaFin        = new Date(fechaFin);
            fechaInicio         = $scope.getSoloFecha(fechaInicio);
            fechaFin            = $scope.getSoloFecha(fechaFin);



            var idEscenario = (ofertaEducativaFactory.checkMostrarTodosLosEventos)
                                    ? 'todos'
                                    : ofertaEducativaFactory.hasOwnProperty('escenarioOferta') && ofertaEducativaFactory.escenarioOferta.hasOwnProperty('_escenario')
                                        ? ofertaEducativaFactory.escenarioOferta._escenario.escenario_id
                                        : 'todos';

            ofertaActividadService.restResource.getActividadesOferta(
                        {
                            "oferta_educativa_id"   : ofertaEducativaFactory.ofertaEducativa.oferta_educativa_id,
                            "escenario_id"          : idEscenario,
                            "fecha_inicio"          : fechaInicio,
                            "fecha_final"           : fechaFin
                        },
                        function(data){
                            var  allEvents          = data.concat($scope.diasFeriados);  //concatenamos los dias feriados.
                            callback(allEvents);
                        },
                        function(data){

                            toastr.error(ocurrioError+": traer sesiones.");
                            m.remove();
                        }
            );
        }
    }

    /**
     * Evaluamos la vista seleccionada, aqui validaremos las flechas de sig y atras para ver si se muestran dependiendo
     * de   las fechas de inicio y fin de la oferta.
     * @param  object view Objeto de tipo fecha.
     * @return void
     */
    $scope.evaluaVista =  function(view){
        ofertaEducativaFactory.evaluaVista(view);
    }
    /*
     * Oferta educativa solo lectura si no esta en la oferta indicada
     */
    $scope.ofertaSoloLectura = function(){
        return (ofertaEducativaFactory.checkMostrarTodosLosEventos) ? true : ofertaEducativaFactory.puedePublicarOferta();
    }


}
/* EOF */