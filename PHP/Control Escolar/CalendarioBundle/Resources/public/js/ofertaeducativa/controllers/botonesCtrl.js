/*
 * @author Jaime Hernández Cruz
 * @description Controlador que lleva las acciones del modal de eventos
 *
 * @param {angular} $scope                 -- Scope de angular
 * @param {factory} ofertaEducativaFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function botonesCtrl(
        $scope
        ,ofertaEducativaFactory
        ,ofertaEducativaService
        ) {



    $scope.init = function(ambiente){
        $scope.ofertaEducativaFactory = ofertaEducativaFactory;
    }

    /*
     * Funcion que es ejectuada al publicar la oferta educativa
     */
    $scope.publicarOfertaEducativa = function(){
        var ofertaEducativaId = (ofertaEducativaFactory.tipoProgramacionCE == "extemporanea")
                                    ? ofertaEducativaFactory.ofertaEducativa.oferta_educativa_id
                                        : 0;


        dialogConfirm("Publicar programación CE", "¿Está seguro de publicar esta programación en los centros?", function() {
            var m               = loader.modal();
            ofertaEducativaService.restResource.publicarOfertaEducativa(
            {oferta_educativa_id : ofertaEducativaId},
            function(data){
                if(data.code == "200"){
                    if(data.data.hasOwnProperty("ofertaeducativa")){
                        ofertaEducativaFactory.ofertaEducativa = data.data.ofertaeducativa;
                        toastr.success("Programación CE publicada");
                    }else{
                        toastr.warning(data.message);
                    }
                }
                m.remove();
                },function(data){
                    if(data.code == "404"){
                        toastr.error(data.message);
                    }
                    m.remove();
                });
        });

    }

    /*
     * Funcion que verifica si es posible publicar la oferta educativa
     * @returns {undefined}
     */
    $scope.puedaPublicar = function(){
        if(ofertaEducativaFactory.ofertaEducativa.hasOwnProperty("oferta_educativa_id")){
            if(ofertaEducativaFactory.ofertaEducativa.sincronizado == true || ofertaEducativaFactory.tipoProgramacionCE == "normal")
                return ofertaEducativaFactory.puedePublicarOferta();
        }
            return true;
    }
}
/* EOF */