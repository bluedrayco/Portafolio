/*
 * @author Jaime Hernández Cruz
 * @description Controlador que maneja los escenarios del calendario
 * @date  07 enero 2014
 * @param {angular} $scope                 -- Scope de angular
 * @param {factory} ofertaEducativaFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {factory} escenarioFactory          -- Factoria que contiene las funciones y datos de escenario a compartir con otros controladores
 * @param {service} grupoService              -- Servicio que gestiona los grupos con el server
 * @param {service} centroService             -- Servicio que gestiona los centros con el server
 * @param {service} centroService             -- Servicio que gestiona el listado de escenarios
 * @param {service} escenarioOfertaService    -- Servicio que gestiona la relación entre escenario y oferta
 * @returns {undefined}
 */
function escenarioCtrl(
        $scope
        ,ofertaEducativaFactory
        ,escenarioFactory
        ,grupoService
        ,centroService
        ,escenarioService
        ,escenarioOfertaService
        ,ofertaEducativaService
        ) {
    $scope.ofertaActividad          = {};
    $scope.tipoEscenarioOferta      = {};
    $scope.escenario_actual         = {};

    $scope.init = function(){
        $scope.tipoEscenarioOferta.clave=='todos';
        escenarioFactory.cargaEscenarios            = $scope.cargaEscenarios;
        ofertaEducativaFactory.tipoEscenarioOferta  = $scope.tipoEscenarioOferta;


        ofertaEducativaService.restResource.get(
                        {},
                        function(result){
                           $scope.ofertasEducativas = result;
                        },
                        function(){
                          toastr.error(ocurrioError+": traer programación CE.");
                        }
                    );

        grupoService.restResource.get({}, function(data){ $scope.grupos =  data; });
        centroService.restResource.get({}, function(data){ $scope.centros =  data; ;});

    }


    /**
     * Obtiene los escenarios de una oferta y los muestra
     **/
    $scope.cargaEscenarios = function(callback){
        escenarioService.restResource.getTiposEscenario({}, function(data){
            $scope.tiposEscenario =  data;
            $scope.seleccionarEscenarioTodos();
            if(callback != null){
                $scope.obtieneEscenario(ofertaEducativaFactory.ofertaEducativa.oferta_educativa_id
                                    ,null
                                    ,callback);
            }
        });

    }

    /*
     * Función que obtiene los dias no laborales
     * @returns {undefined}
     */
    $scope.diasNoLaborablesPorFecha = function(){

        var fechaInicioBusqueda = new Date($scope.ofertaEducativaSeleccionada.fecha_inicio);
        var fechaFinBusqueda = new Date($scope.ofertaEducativaSeleccionada.fecha_fin);

        fechaInicioBusqueda  = fechaInicioBusqueda.getFullYear() + "-01-01";
        fechaFinBusqueda  = fechaFinBusqueda.getFullYear() + "-12-31";

        ofertaEducativaService.restResource.getDiasFeriadosPorFecha(
                            {
                             "fecha_inicio"          : fechaInicioBusqueda,
                             "fecha_fin"            : fechaFinBusqueda
                            },
                            function(data){
                                  if (data.length == 0) {
                                    //Muestra el modal si no estamos en programacion extemporanea
                                      if(ofertaEducativaFactory.tipoProgramacionCE != "extemporanea"){
                                          $('#idModalMensajesDiasLaborables').modal('show');
                                      }
                                  }
                            }
        );
    }

    /*
     * Seleccionamis el escenario todos por default
     * @returns {undefined}
     */
    $scope.seleccionarEscenarioTodos = function(){
        for(var i = 0; i <= $scope.tiposEscenario.length; i++ ){
                if($scope.tiposEscenario[i].clave == "todos"){
                $scope.tipoEscenarioOferta = $scope.tiposEscenario[i];
                break;
                }
            }
    }

    $scope.eligeTipoEscenario = function(){
        if($scope.tipoEscenarioOferta.clave=='todos'){

            if($scope.escenario_actual.hasOwnProperty("id_individual")){
                delete $scope.escenario_actual.id_individual;
            }
            $scope.obtieneEscenario(ofertaEducativaFactory.ofertaEducativa.oferta_educativa_id
                                ,null
                                ,ofertaEducativaFactory.renderActividadesEnCalendario);
        }

    }


    /**
     * muestra escenarios escenario
     **/
    $scope.muestraEscenarios = function(vista){
        return $scope.tipoEscenarioOferta.clave == vista ? true:false;
    }

    /**
     * muestra escenarios escenario
     **/
    $scope.eligeEscenario = function(){
        $scope.obtieneEscenario(ofertaEducativaFactory.ofertaEducativa.oferta_educativa_id
                                ,$scope.escenario_actual.id_individual
                                ,ofertaEducativaFactory.renderActividadesEnCalendario);

    }
    /**
     * Muestra escenarios escenario
     * @param {int} idEscenario puede ser de tipo centro_id o tipo grupo_id
     **/
    $scope.obtieneEscenario = function(idOferta,idEscenario,callback){
        var dataSend            = {}
            dataSend.tipo       = $scope.tipoEscenarioOferta.clave;
            dataSend.oferta_id  = idOferta;
            if($scope.tipoEscenarioOferta.clave != "todos"){
                dataSend.id         = idEscenario;
            }

         escenarioOfertaService.restResource.getEscenarioOferta(
            dataSend,
            function(data){
                if(data == false){
                    toastr.warning("No existe el escenario");
                }else{
                    //seteamos el escenarioOferta

                    ofertaEducativaFactory.escenarioOferta = data;
                    callback();
                    $scope.ofertaEducativaSeleccionada = ofertaEducativaFactory.ofertaEducativa;
                    $scope.diasNoLaborablesPorFecha();
                }


            },function(){
                toastr.error("Error al obtener el escenario");
            });

    }

    /*
     * Mostrar ofertas educativas anteriores por oferta educativa
     */
    $scope.mostrarPorOfertaEducativa = function(){
        $scope.diasNoLaborablesPorFecha();
        ofertaEducativaFactory.ofertaEducativa = $scope.ofertaEducativaSeleccionada;
        ofertaEducativaFactory.irAFechaCalendario(ofertaEducativaFactory.ofertaEducativa.fecha_inicio);
        

        ofertaEducativaFactory.evaluaDiasPublicarOfertaCentral();
        $scope.obtieneEscenario(ofertaEducativaFactory.ofertaEducativa.oferta_educativa_id
                                    ,null
                                    ,ofertaEducativaFactory.renderActividadesEnCalendario);

    }

    /*
     * Funcion que muestra todos los eventos agregados a la oferta educativa
     *
     */
    $scope.mostrarTodosLosEventos = function(){
            ofertaEducativaFactory.checkMostrarTodosLosEventos = $scope.checkMostrarTodosLosEventos;
            ofertaEducativaFactory.renderActividadesEnCalendario();
    }

}
/* EOF */