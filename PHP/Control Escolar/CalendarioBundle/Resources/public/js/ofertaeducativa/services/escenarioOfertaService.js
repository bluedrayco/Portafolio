
/*
 * Servicio que obtiene y gestiona los escenarios oferta 
 */
app.service ('escenarioOfertaService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/escenariooferta/', {},{
        /*
         * Servicio que Obtiene todos los Escenarios de la base de datos
         * respecto a la base de datos
         */
        getEscenarioOferta:{
            method      :  'GET',
            url         : '/calendarizacion/api/entity/calendario/escenariooferta/:tipo/:oferta_id/:id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.escenariooferta;
                }
            }   

    });
});