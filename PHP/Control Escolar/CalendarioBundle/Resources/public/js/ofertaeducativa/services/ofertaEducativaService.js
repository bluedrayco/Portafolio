
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('ofertaEducativaService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/ofertaseducativas?filter=ofertaeducativa.activo=true', {},{
        get:{
                method: 'GET',
                isArray:true,
                url     : "/calendarizacion/api/entity/calendario/ofertaseducativas?filter=ofertaeducativa.activo=true",
                transformResponse : function(data, headerrs){
                    var result  = [];
                    result      = angular.fromJson(data).data;
                    for(var i = 0; i < result.ofertaseducativas.length; i++){
                        result.ofertaseducativas[i].fecha_inicio = getSoloFecha(new Date(result.ofertaseducativas[i].fecha_inicio));
                        result.ofertaseducativas[i].fecha_fin    = getSoloFecha(new Date(result.ofertaseducativas[i].fecha_fin));
                    }
                    return result.ofertaseducativas;
                }
            },
        getOfertaEducativaVigente:{
            method   : 'GET',
            url      : "/calendarizacion/api/entity/calendario/ofertaeducativaactiva",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result;
                }
            },

        add:{
            method      : 'POST',
            url         : "/calendarizacion/api/entity/calendario/ofertaeducativa",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.ofertaeducativa;
                }
            },

        update: {
                    method              : 'PUT',
                    url                 : '/calendarizacion/api/entity/calendario/ofertaeducativa/:oferta_educativa_id',
                    isArray             :true,
                    transformResponse   : function(data, headers){

                        var result     = angular.fromJson(data).data;
                        return result;

                    }
        },


        remove: {
                    method              : 'DELETE',
                    url                 : "/calendarizacion/api/entity/calendario/ofertaeducativa/:oferta_educativa_id",
                    transformResponse   : function(data, headerrs){
                        var result      = [];
                        result          = angular.fromJson(data).data;
                        return result.ofertaeducativa;
                    }

        },

        /*
         * Obtiene la ultima fecha vigente y la coloca en la fecha inicial
         * de la siguiente oferta educativa a crear
         */
        getFechaVigente:{
            method: 'GET',
            url         : "/calendarizacion/api/entity/calendario/ultimafechavigente",
            isArray:true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.ofertaeducativa;
                }
            },
        /*
         * Obtiene los dias feriados
         */
        getDiasFeriados:{
            method      : 'GET',
            url         : "/calendarizacion/api/entity/calendario/diasferiadoswithformato",
            isArray     : true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data.diasferiados;
                for(var n = 0; n < result.length; n++){
                    //alert(result[n].hola);
                    result[n].editable  = false;
                    result[n].start     = result[n].start + " 08:00";
                    result[n].end       = result[n].end   + " 22:00";
                    result[n].className = "categoria-feriado";
                    result[n].esFeriado = true;

                }
                return result;
                }
            },
        /*
         * Obtiene los dias feriados por fecha
         */
        getDiasFeriadosPorFecha:{
            method      : 'GET',
            url         : "/calendarizacion/api/entity/calendario/diasferiadosporreglondefecha/:fecha_inicio/:fecha_fin",
            isArray     : true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data["diasferiados"];
                return result;
                }
            },
        /*
         * Metodo para publicar una oferta educativa o calendarización CE
         */
        publicarOfertaEducativa:{
            method      : 'GET',
            url         : "/calendarizacion/api/entity/calendario/publicarofertaeducativa/:oferta_educativa_id",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            }
    })
});