
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('escenarioService',function($http, $resource){
    this.restResource = $resource('asdas', {},{
        /*
         * Servicio que Obtiene todos los Escenarios de la base de datos
         * respecto a la base de datos
         */
        get:{
            method: 'GET',
            isArray:true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.escenarios;
                }
            },
        /*
         * Servicio que obtiene los 
         */
        getTiposEscenario:{
            method: 'GET',
            url   : '/calendarizacion/api/entity/calendario/tiposescenario?filter=tipoescenario.activo=true',
            isArray:true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.tiposescenario;
                }
            }
    });
});