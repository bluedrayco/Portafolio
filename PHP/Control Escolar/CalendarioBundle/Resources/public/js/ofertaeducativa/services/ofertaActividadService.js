
app.service ('ofertaActividadService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/ofertaseducativas', {},{
        add:{
            method      : 'POST',
            url         : "/calendarizacion/api/entity/calendario/ofertaactividad",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.ofertaactividad;
                }
            },
        edit:{
            method      : 'PUT',
            url         : "/calendarizacion/api/entity/calendario/ofertaactividad/:oferta_actividad_id",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        delete:{
            method      : 'DELETE',
            url         : "/calendarizacion/api/entity/calendario/ofertaactividad/:oferta_actividad_id",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.ofertaactividad;
                }
            },    
        get:{
            method: 'GET',
            isArray:true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.centros;
                }
            },
        getActividadesOferta:{
            method  : 'GET',
            isArray :true,
            url     : "/calendarizacion/api/entity/calendario/ofertasactividad/:oferta_educativa_id/:escenario_id/:fecha_inicio/:fecha_final",
            transformResponse : function(data, headerrs){
                var result  = [];
                if(data.status == false){
                    return false;
                }else{
                    result  = angular.fromJson(data).data;
                    return result.eventos;
                }
                }
            }
    });
        
    
    
});