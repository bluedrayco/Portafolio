// Agregando el modulo de ngCsv para exportar a CSV utilizado en los reportes
app.requires.push('ngCsv');

/*
 * @author Luis González Linares
 * @description Controlador principal para obtener reporte de grupos en centros
 *
 * @param {angular} $scope
 * @param {angular} $compile
 * @param {api} uiCalendarConfig
 * @param {factory} centroFactory       -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function calendarizacionResumenCtrl(
        $scope,
        $q,
        centroFactory,
        centroService,
        resumenService,
        $filter,
        equivalenciaService

        ) {
    $scope.collectionfiltered =[];
    var REGISTROSXPAGINAS = 15;
    $scope.filtro = [];
    $scope.displayed = [];
    var tableStateH = {};
    $scope.ipConsultada = '';
    $scope.centrosRemotos = [];
    $scope.centroSeleccionado = null;
    $scope.busqueda = "";
    $scope.numberOfPages = 0;
    var m = {};
    $scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    }
    /**
    * Funcion que gestiona el páginado de smart-table 
    *
    ***/
    $scope.pipeReporteGrupos = function(tableState){
      if($scope.centroSeleccionado != null){
        if('predicateObject' in tableState.search){
          switch(tableState.search.predicateObject.estatus_curso.distinct){
            case 'Grupo finalizado':
              tableState.search.predicate = "finalizado";
              break;
            case 'Grupo iniciado':tableState.search.predicate
              tableState.search.predicate = "iniciado";
              break;
            case 'Grupo próximo':
              tableState.search.predicate = "proximo";
              break;
            default:
              tableState.search.predicate = "";
              break;
          }
        
          if(tableState.sort.predicate == null ){
            tableState.sort.predicate = "";
            tableState.sort.reverse = false;
          }

          if('$' in tableState.search.predicateObject){
            tableState.search['input'] = tableState.search.predicateObject['$'];
          }else{
            tableState.search['input'] = "";
          }

          if(/^\d{2}(\/|-)\d{2}(\/|-)\d{4}$/.test($scope.busqueda) | 
            /^\d{4}(\/|-)\d{2}(\/|-)\d{2}$/.test($scope.busqueda)){
            $scope.getBusqueda(tableState,'where');
            tableState.pagination.numberOfPages = 0;
          }else{
            if($scope.busqueda.length > 0){
              $scope.getBusqueda(tableState,'like');
              tableState.pagination.numberOfPages = 0;
            }else{
              tableState.pagination.numberOfPages = $scope.numberOfPages;
              $scope.paginaReporteGrupos(tableState);
            }
          }




        }

      }else{
        tableStateH = tableState;
      }
    }
    /**
    * Función que obtiene del paginado de acuerdo a
    * 15 items por pagina y en funcion al ordenamiento de campos
    **/
    $scope.paginaReporteGrupos = function(tableState){

       m = loader.modal();        
      //verificamos que un centro del select este seleccionado 
      if($scope.centroSeleccionado != null){
          
          
          resumenService.restResource.getReporteGrupos(
            {hostCentro: limpiarUri($scope.ipConsultada),
              'offset':tableState.pagination.start || 0,
              'limit': REGISTROSXPAGINAS,
              'search': tableState.search.predicate,
              'order' : tableState.sort.predicate,
              'reverse' : tableState.sort.reverse
              },
            function(data){
              
              if(data.reporte == false){
                toastr.error("No se encontraron grupos");
                $scope.displayed = [];
              }else{
                $scope.displayed = data.reporte;
              }
              m.remove();
            },function(){
              
              toastr.error("Nose encontraron grupos en el centro");
              m.remove();
              $scope.displayed = [];
            }
          );
      }else{
        tableStateH = tableState;
      }

    }
    /**
    * función que setea al tableState.paginatio.numberOfPages 
    * de acuerdo al numero de items encontrados en el servidor
    **/
    $scope.contarPaginas = function(){
      
      if($scope.ipConsultada != null){
        resumenService.restResource.getReporteGruposCount(
          {hostCentro: limpiarUri($scope.ipConsultada),
            'count': "cuenta"
          },
          function(data){
            var residuo = data.data.reporte % REGISTROSXPAGINAS;
            
            if(residuo < 1){
              tableStateH.pagination.numberOfPages = data.data.reporte/REGISTROSXPAGINAS;
            }else{
              tableStateH.pagination.numberOfPages = ((data.data.reporte - residuo) / REGISTROSXPAGINAS) + 1;
            }
            $scope.numberOfPages = tableStateH.pagination.numberOfPages;
            $scope.getReporteGrupos();
            $scope.pipeReporteGrupos(tableStateH);
            
          }
        );
      }
    }

    /*
     * Agregando las funciones para que cargue el inicio
     **/
    $scope.initCentros = function(){
        
        centroService.restResource.get(
            {},
            function(data){
                if(data.length == 0){
                    toastr.error("No se encontraron centros");

                }else{
                    $scope.centrosRemotos =  data;
                }
            },function(){
                toastr.error("No se encontraron centros");
            }
        );

    }
    
    /*
     * evaluando el cambio de fuentes
     */
    $scope.eligiendoCentroFuente = function(){
        m = loader.modal();
        if($scope.centroSeleccionado != null){
           var noSeEcontro = "No se encontro el centro";
           
            equivalenciaService.restResource.getUrlCentro(
                {centro_id : $scope.centroSeleccionado.centro_id},
                function(data){

                    if(data.length == 0){
                        toastr.success(noSeEcontro);
                        $scope.ipConsultada = null;
                    }else{
                        $scope.ipConsultada  =  data[0].ip;
                        
                        $scope.contarPaginas();

                    }
                },function(){
                    toastr.success(noSeEcontro);
                }
            );
        }
    }
    /**
    *
    *
    */

    /*
     * @description Función que obtiene la información del reporte de grupos
     *  totales 
     */
    $scope.getReporteGrupos = function(){

         resumenService.restResource.getReporteGrupos(
            {hostCentro: limpiarUri($scope.ipConsultada)},
            function(data){
                if(data.length == 0){
                    toastr.success("No se encontraron grupos");
                }else{
                    $scope.collection = data.reporte;
                    $scope.collectionfiltered = data.reporte;
                }
            },function(){
                toastr.success("No se encontraron grupos en el centro");
                $scope.collection =[];
            }
        );
    }

          /*
     * Funcion que me regresa la cabecera del detalle CSV para exportar
     * @returns array cabecera
     */
    $scope.getHeaderCSV = function () {
        return ['Centro', 'Curso', 'Tipo Actividad', 'Clave de Grupo', 'Último facilitador asignado', 'Aula', 'Fecha Inicio', 'Fecha fin', 'Estatus', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'] };

    /*
     * Funcion que muestra el mensaje de exportando
     *
     */
    $scope.exportandoACSV = function () {
        
        toastr.success('Exportando actividades a CSV.');
        
    }

    $scope.readFilteredCollection = function(stCtrl) {
    $scope.collectionfiltered = stCtrl.getFilteredCollection();
    }

    /**
    * Función que busca una entrada en el servidor de acuerdo a texto o fecha
    *
    **/
    $scope.getBusqueda = function(tableState,tipo){
      m = loader.modal();
      var parametros = {
          'search'    : tableState.search.predicate,
          'order'     : tableState.sort.predicate,
          'reverse'   : tableState.sort.reverse,
          hostCentro  : limpiarUri($scope.ipConsultada)
      };
      if(tipo == 'like'){
        parametros['like'] = tableState.search.input;
      }else if(tipo == 'where'){
        parametros['where'] = tableState.search.input;
      }

      resumenService.restResource.getReporteGrupos(
              parametros
            ,
            function(data){
                if(data.length == 0){
                    toastr.warning("No se encontro: "+$scope.busqueda);
                }else{
                    $scope.displayed = data.reporte;
                }
                m.remove();
            },function(){
                m.remove();
                toastr.warning("No se encontro: "+$scope.busqueda);
                $scope.displayed =[];
            }
        );
    }


}
/* EOF */
