
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('resumenService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/reportegrupos', { },{
        get:       {
                    method: 'GET',
                    isArray:true,
                    transformResponse : function(data, headerrs){

                        result                              = angular.fromJson(data).data;

                        return result;
                    }
                },
        getResumen:       {
                    method: 'GET',
                    url         : "/calendarizacion/api/entity/calendario/reportegrupos/:ocupacion_id",
                    isArray:false,
                    transformResponse : function(data, headerrs){

                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        return result.parametro;
                    }
                },
        getReporteGrupos:       {
                    method: 'GET',
                    //url         : "/calendarizacion/api/entity/calendario/reportegrupos",
                    url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/reportegrupos',
                    isArray:false,
                    transformResponse : function(data, headerrs){
                        
                        var result                          = [];
                        result                              = angular.fromJson(data).data;
                        for(var i = 0; i < result.reporte.length; i++){
                            result.reporte[i]['dia_Lu'] = " ";
                            result.reporte[i]['dia_Ma'] = " ";
                            result.reporte[i]['dia_Mi'] = " ";
                            result.reporte[i]['dia_Ju'] = " ";
                            result.reporte[i]['dia_Vi'] = " ";
                            result.reporte[i]['dia_Sa'] = " ";
                            arr_diasCurso = result.reporte[i]['dias_de_curso'].split(",");
                            for (var j = 0; j < arr_diasCurso.length; j++) {
                                arr_datos_dia = arr_diasCurso[j].split("=>");
                                switch ( arr_datos_dia[0] ) {
                                    case '1':
                                        result.reporte[i]['dia_Lu']      =  arr_datos_dia[1];
                                        break;
                                    case '2':
                                        result.reporte[i]['dia_Ma']      =  arr_datos_dia[1];
                                        break;
                                    case '3':
                                        result.reporte[i]['dia_Mi']      =  arr_datos_dia[1];
                                        break;
                                    case '4':
                                        result.reporte[i]['dia_Ju']      =  arr_datos_dia[1];
                                        break;
                                    case '5':
                                        result.reporte[i]['dia_Vi']      =  arr_datos_dia[1];
                                        break;
                                    case '6':
                                        result.reporte[i]['dia_Sa']      =  arr_datos_dia[1];
                                        break;
                                }

                            delete result.reporte[i]['dias_de_curso'];

                            }

                        }
                        return result;
                    }
                },
                getReporteGruposCount:       {
                    method: 'GET',
                    //url         : "/calendarizacion/api/entity/calendario/reportegrupos",
                    url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/reportegrupos',
                    isArray:false,
                    transformResponse : function(data, headerrs){
                        
                        var result                          = [];
                        result                              = angular.fromJson(data);
                        return result;
                    }
                }

        }
    );

});