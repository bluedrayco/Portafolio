/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('ofertaEducativaCentroService',function($http, $resource){

    this.restResource = $resource('/', {},{

        get:{
            method  : 'GET',
            isArray : true,
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/ofertaseducativascentros',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                for(var i = 0; i < result.ofertaseducativascentros.length; i++){
                        result.ofertaseducativascentros[i].fecha_inicio = getSoloFecha(new Date(result.ofertaseducativascentros[i].fecha_inicio));
                        result.ofertaseducativascentros[i].fecha_fin    = getSoloFecha(new Date(result.ofertaseducativascentros[i].fecha_fin));
                    }
                return result.ofertaseducativascentros;
                }
            },

        getOfertaEducativaVigente:{
            method   : 'GET',
            url      : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/ofertaseducativascentros?filter=ofertaeducativacentro.activo=true&order=oferta_educativa_centro_id|desc&limit=1",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result;
                }
            },

        /*
         * Obtiene los dias feriados
         */

        getDiasFeriados:{
            method      : 'GET',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/diasferiadoswithformato',
            isArray     : true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data.diasferiados;
                for(var n = 0; n < result.length; n++){
                    //alert(result[n].hola);
                    result[n].editable  = false;
                    result[n].start     = result[n].start + " 08:00";
                    result[n].end       = result[n].end   + " 22:00";
                    result[n].className = "categoria-feriado";
                    result[n].esFeriado = true;

                }
                return result;
                }
            },
        /*
         * Servicio que obtiene la ip de Mako correspondiente al centro dado
         * antes de incluir control escolar
         *
         */
        getIpMako:{
            method: 'GET',
            url     : dinamicUri(":hostCentro")+"/core/api/generic/list/sync/destino?filter=destino.nombre='Mako Local PV'",
            isArray :false,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                //Evaluamos si contiene elementos el array dado
                if(result.data.destino.length > 0){
                return result.data.destino[0];
                }else{
                    return false;
                }
            }
        },
        /*
         * Servicio que obtiene los eventos de mako que estuvieron calendarizados
         * antes de incluir control escolar
         *
         */
        getEventosMako:{
            method: 'GET',
            url     : dinamicUri(":hostMako") + "/restconsultaeventos?fecha=:fecha",
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                for(var n = 0; n < result.length; n++){
                    //alert(result[n].hola);
                    result[n].editable  = false;
                    result[n].className = "categoria-eventoMako";

                }
                return result;
                }
        },


    })
});