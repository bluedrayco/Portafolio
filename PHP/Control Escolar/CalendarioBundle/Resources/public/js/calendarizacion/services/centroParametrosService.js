
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('centroParametrosService',function($http, $resource){
    this.restResource = $resource('otro', {},{
        /*
         * Servicio que Obtiene todos los Escenarios de la base de datos
         * respecto a la base de datos
         */
        getCentroActual:{
            method      : 'GET',
            url         : dinamicUri(":hostCentro")+'/core/api/entity/comun/centroactual',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.centro;
                }
            },
        getEspacios:{
            method  : 'GET',
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/aulas?filter=aula.activo=true',
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.aulas;
                }
            },
         /*
         * Servicio que Obtiene todos los facilitadores del centro de la base de datos
         * respecto a la base de datos
         */
        getFacilitadores:{
            method  : 'GET',
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/facilitadores',
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.facilitadores;
                }
            },

         /*
         * Servicio que Obtiene todos los facilitadores del centro de la base de datos
         * que son activos o que tienen Grupos
         * @return array facilitadores
         */
        getFacilitadoresReporte:{
            method  : 'GET',
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/facilitadoresreporte',
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.facilitadores;
                }
            },
         /*
         * Servicio que Obtiene todos los facilitadores del centro de la base de datos
         * respecto a la base de datos
         */
        horasAsignadasFacilitador:{
            method  : 'GET',
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/horasasignadasfacilitadores/:oferta_educativa_id',
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.asignaciones;
                }
            },
             /*
         * Servicio que Obtiene todos los facilitadores del centro de la base de datos
         * respecto a la base de datos
         */
        getActividadAcademicaCategorias:{
            method  : 'GET',
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/facilitadores?filter=facilitador.activo=true',
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.facilitadores;
                }
            },
        getParametro:{
            method: 'GET',
            url         : dinamicUri(":hostCentro")+"/core/api/generic/list/calendario/parametro?filter=parametro.nombre=':nombreParametro'",
            transformResponse : function(data, headerrs){
                var result  = {};
                result      = angular.fromJson(data);
                return result;
                }
        },
        getParametros:{
            method: 'GET',
            url         : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/parametros",
            isArray :true,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.parametros;
                }
        },
    });
});