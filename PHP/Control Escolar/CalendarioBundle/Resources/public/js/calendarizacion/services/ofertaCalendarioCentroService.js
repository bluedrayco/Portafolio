
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('ofertaCalendarioCentroService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/ofertaseducativas', {},{

        getActividadesOfertaCalendario:{
            method  : 'GET',
            isArray :true,
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/obtenereventoscentro/:oferta_educativa_id/:tipo/:escenario_id/:fecha_inicio/:fecha_final/:aula_id/:facilitador_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                    result  = angular.fromJson(data).data;
                    return result.eventos;
                }
            },
        add:{
            method      : 'POST',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/eventosocupacion',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        editEvent:{
            method      : 'PUT',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/eventocentro/:evento_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        editEventBloque:{
            method      : 'PUT',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/seccionadoocupacion/:evento_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        edit:{
            method      : 'PUT',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/eventosocupacion/:ocupacion_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        delete:{
            method      : 'DELETE',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/ocupacion/:ocupacion_id',
            transformResponse : function(data, headerrs){
                var result  = {};
                result      = angular.fromJson(data);
                return result;
                }
            },
        cancelar:{
            method      : 'PUT',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/cancelacionocupacion/:ocupacion_id',
            transformResponse : function(data, headerrs){
                var result  = {};
                result      = angular.fromJson(data);
                return result;
                }
            },
        puedeCancelar:{
            method      : 'GET',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/validacioncancelacionocupacion/:ocupacion_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        puedeEliminar:{
            method      : 'GET',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/validacioneliminacionocupacion/:ocupacion_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        getEstadistica:{
            method: 'GET',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/estadisticasofertaeducativacentro/:oferta_educativa_id/:aula_id/:facilitador_id',
            transformResponse : function(data, headerrs){
                var result  = {};
                result      = angular.fromJson(data).data.estadistica;
                return result;
                }
        },
        publicarCalendario:{
            method: 'GET',
            url         : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/publicacioncalendarizacioncentro/:oferta_educativa_id',
            transformResponse : function(data, headerrs){
                var result  = {};
                result      = angular.fromJson(data);
                return result;
                }
        }

    })
});