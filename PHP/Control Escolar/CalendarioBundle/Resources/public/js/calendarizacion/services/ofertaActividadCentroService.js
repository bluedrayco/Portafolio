
app.service ('ofertaActividadCentroService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/ofertaseducativas', {},{
        /*
         * Actualiza la oferta actividadcentro
         */
        actualizar:{
            method      : 'PUT',
            url         : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/ofertaactividadcentro/:oferta_actividad_centro_id",
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result;
                }
            },
        /*
         * Servicio que Obtiene todos alumnos inscritos de una ofertaActividad
         * respecto a la base de datos
         */
        getInscritosInteresados:{
            method  : 'GET',
            url     : dinamicUri(":hostCentro")+'/calendarizacion/api/entity/calendario/listadoinscritoseinteresados/:ocupacion_id',
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data).data;
                return result.respuesta;
                }
            },

    });



});