/*
 * @author Jaime Hernández Cruz
 * @description Controlador que maneja los filtros de la calendarizacion
 * @date  21 de enero 2015
 * @param {angular} $scope                                      -- Scope de angular
 * @param {service} ofertaEducativaCentroService                -- Servicio que gestiona los grupos con el server
 * @param {service} espacioCentroService                           -- Servicio que gestiona las espacios del sistema con el server
 * @returns {undefined}
 */
function filtrosCtrl(
        $scope
        ,ofertaEducativaCentroService
        ,centroParametrosService
        ,ofertaEducativaFactory
        ,filtrosFactory
        ,centroFactory
        ,centroService
        ) {

    $scope.espacios         = [];
    $scope.facilitadores    = [];
    $scope.centroFactory    = {}; //Factoria de centro
    $scope.ofertaFactory    = {};
    $scope.centroActual    = {
        espacios         :[],
        facilitadores    :[],
        centro           :{}
    };

    $scope.filtro           = {};
    $scope.init = function(ambiente){
        centroFactory.cargaInicialFiltros = $scope.cargaInicialFiltros;

        $scope.centroFactory = centroFactory;
        if(ambiente == 'local'){
            $scope.cargaInicialFiltros();
        }
    }

    $scope.cargaInicialFiltros = function(){
        ofertaEducativaCentroService.restResource.get({hostCentro: limpiarUri(centroFactory.centroActual.ip)},{}, function(data){             $scope.ofertasEducativas =  data;  });
        centroParametrosService.restResource.getEspacios({hostCentro: limpiarUri(centroFactory.centroActual.ip)},{},     function(data){     $scope.centroActual.espacios =  data;      });
        centroParametrosService.restResource.getFacilitadores({hostCentro: limpiarUri(centroFactory.centroActual.ip)},{}, function(data){     $scope.centroActual.facilitadores =  data;});
        centroParametrosService.restResource.getCentroActual({hostCentro: limpiarUri(centroFactory.centroActual.ip)},{}, function(data){     $scope.centroActual.centro =  data; });
        filtrosFactory.cargaFiltroInicialCalendario = $scope.cargaFiltroInicialCalendario;
    }


    /**
     * Obtiene la carga inicial de los filtros
     **/
    $scope.cargaFiltroInicialCalendario = function(callback){

        $scope.filtro.calendario        =   true;
        $scope.filtro.ofertaEducativa   =   true;
        //Se le da seguimiento a la factoria en el scope de este controlador
        //Con la variable ofertaFactory
        $scope.ofertaFactory   =   ofertaEducativaFactory;
        $scope.cargarEventos();
        callback();

    }



     /**
     * Muestra los eventos respecto al tipo dado
     * @param {int} cambia el estatus del filtro a traer
     **/

    $scope.mostrarEventos = function(){

        if($scope.filtro.calendario == true  && $scope.filtro.ofertaEducativa == true)
            filtrosFactory.filtroInicial = "todos";
        else if($scope.filtro.calendario == true  && $scope.filtro.ofertaEducativa == false)
            filtrosFactory.filtroInicial = "centro";
        else if($scope.filtro.calendario == false && $scope.filtro.ofertaEducativa == true)
            filtrosFactory.filtroInicial = "oferta";
        else
            filtrosFactory.filtroInicial = "";

    }

    /*carga los eventos*/
    $scope.cargarEventos = function(){
        filtrosFactory.aula = {};
        $scope.filtro.aula  = {};
        $scope.mostrarEventos();
        $scope.mostrarPorOfertaEducativa();

    }

    /*
     * filtra por aula
     */
    $scope.filtarPorAula = function(){

        filtrosFactory.aula = ($scope.filtro.aula        == '') ? 'todos' : $scope.filtro.aula;
        ofertaEducativaFactory.renderActividadesEnCalendario();
    }

    /*
     * filtra por facilitador
     */
    $scope.filtarPorFacilitador = function(){
        filtrosFactory.facilitador = ($scope.filtro.facilitador == '') ? 'todos' : $scope.filtro.facilitador;

        ofertaEducativaFactory.renderActividadesEnCalendario();
    }

    /*
     * Filtra por oferta educativa
     */
    $scope.mostrarPorOfertaEducativa = function(ofertaEducativaSeleccionada){


        ofertaEducativaFactory.ofertaEducativa = (typeof(ofertaEducativaSeleccionada)!="undefined")? ofertaEducativaSeleccionada: $scope.ofertaFactory.ofertaEducativa;
        ofertaEducativaFactory.renderActividadesEnCalendario();
        ofertaEducativaFactory.irAFechaCalendario(ofertaEducativaFactory.ofertaEducativa.fecha_inicio);

        ofertaEducativaFactory.evaluaDiasPublicarOfertaCentro();


    }
}
/* EOF */
