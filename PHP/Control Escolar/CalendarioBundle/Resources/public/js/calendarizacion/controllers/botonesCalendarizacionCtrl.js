/*
 * @author Jaime Hernández Cruz
 * @description Controlador que lleva las acciones del modal de eventos
 *
 * @param {angular} $scope                 -- Scope de angular
 * @param {factory} ofertaEducativaFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function botonesCtrl(
        $scope
        ,ofertaEducativaFactory
        ,estadisticaFactory
        ,ofertaCalendarioCentroService
        ,centroFactory
        ) {



    $scope.init = function(){
        $scope.ofertaEducativa = ofertaEducativaFactory;
        $scope.centroFactory = centroFactory;
    }


    /*
     * Funcion que es ejectuada al publicar la programación CE
     */
    $scope.publicarCalendario = function(){
    if($scope.puedePublicarCalendario){
        dialogConfirm("Publicar calendario del centro", "¿Está seguro de publicar el calendario y notificar a Control Escolar?", function() {
            var m               = loader.modal();
            ofertaCalendarioCentroService.restResource.publicarCalendario(
            {
                hostCentro : limpiarUri(centroFactory.centroActual.ip),
                oferta_educativa_id   :   ofertaEducativaFactory.ofertaEducativa.oferta_educativa_centro_id
            },
            function(data){
                if(data.code == "200"){
                    //toastr.success(data.message);
                    toastr.success("Se publicó el calendario y se notificó a CE");
                    ofertaEducativaFactory.ofertaEducativa = data.data.ofertaeducativacentro;
                }
                    m.remove();
                },function(data){
                    if(data.code == "404"){
                        toastr.error(data.message);
                    }
                    m.remove();
                });
            });
        }
    }

    /*
     * Funcion que verifica si es posible publicar la programación CE
     * @returns {undefined}
     */
    $scope.puedePublicarCalendario = function(){

        return !(estadisticaFactory.actividades_obligatorias_faltantes == 0
                    && ofertaEducativaFactory.ofertaEducativa.hasOwnProperty("oferta_educativa_centro_id")
                    && ofertaEducativaFactory.ofertaEducativa.publicado != true);


    }
}
/* EOF */