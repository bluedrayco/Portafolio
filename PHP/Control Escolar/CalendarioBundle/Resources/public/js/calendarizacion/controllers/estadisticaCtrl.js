/*
 * @author Jaime Hernández Cruz
 * @description Controlador que lleva las acciones de estadistivas
 * 
 * @param {angular} $scope                 -- Scope de angular
 * @param {factory} ofertaEducativaFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @returns {undefined}
 */
function estadisticaCtrl(
        $scope
        ,estadisticaFactory
        ) {
    $scope.init = function () {
        estadisticaFactory.cargandoVariables = $scope.cargandoVariables;
        $scope.cargandoVariables();
    }
    $scope.cargandoVariables = function () {
        $scope.actividadesObligatoriasFaltantes = estadisticaFactory.actividades_obligatorias_faltantes;
        $scope.totalActividadesCalendarizadas   = estadisticaFactory.total_actividades_calendarizadas;
        $scope.totalActividadesOfertaEducativa  = estadisticaFactory.total_actividades_oferta_educativa;
    }
}
/* EOF */