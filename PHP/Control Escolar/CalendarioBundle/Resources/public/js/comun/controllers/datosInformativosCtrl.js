/*
 * @author Jaime Hernández Cruz
 * @description Controlador que lleva las acciones de la información de ayuda
 * 
 * @param {angular} $scope                 -- Scope de angular
 * @returns {undefined}
 */
function datosInformativosCtrl(
    $scope
    , actividadAcademicaService
    , actividadNoAcademicaService
    , centroFactory
    ) {

    $scope.informativos = {
    };
    $scope.init = function () {
        actividadNoAcademicaService.restResource.getCategoryList(
            {},
            function (data) {
                centroFactory.datosInformativos.categoriasActividadNoAcademica = data;
                $scope.informativos.actividadNoAcademicaCategorias = data;
            }
        );

        actividadAcademicaService.restResource.getCategoryList(
            {},
            function (data) {
                centroFactory.datosInformativos.categoriasActividadAcademica = data;
                $scope.informativos.actividadAcademicaCategorias = data;
            }
        );
    }
}
/* EOF */