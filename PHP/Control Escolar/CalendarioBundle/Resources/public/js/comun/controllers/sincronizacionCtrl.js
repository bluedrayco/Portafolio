/*
 * @author Héctor González Olmos
 * @description Controlador principal para enviar parametros a centros
 *
 * @param {angular} $scope
 * @param {factory} centroFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {service} parametroService -- Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 * @returns {undefined}
 */
function sincronizacionCtrl(
            $scope,
            parametroService,
            centroFactory
	) {

    $scope.publicarParametro = function() {
        $scope.parametroEdicion = {};
        var m = loader.modal();
        parametroService.restResource.publicarParametros(
            {
                hostCentro   : limpiarUri(centroFactory.centroActual.ip)
            },{},
            function(data){
                if(data){
                    m.remove();
                    $scope.parametroEdicion = data;
                    if( $scope.parametroEdicion.data.error.length == 0 ){
                        toastr.success( "Centros actualizados: " + $scope.parametroEdicion.data.succes.length );
                    }
                    else if( $scope.parametroEdicion.data.error.length > 0 ){
                        toastr.warning( "Centros actualizados: " + $scope.parametroEdicion.data.error.length );
                    }
                }                
            },function() {
                m.remove();
                toastr.error(ocurrioError + ": No existen parámetros para publicar en centros.");
            }
        );
    }
    
}