/*
 * @author Jaime Hernández Cruz
 * @description Controlador que lleva las acciones del modal de eventos
 *
 * @param {angular} $scope                 -- Scope de angular
 * @param {factory} ofertaEducativaFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {factory} eventoFactory          -- Factoria que contiene las funciones de evento para la comunicacion con otros controladores
 * @param {service} actividadesService     -- Servicio que gestiona las actividades academicas con el server
 * @param {service} ofertaActividadService -- Servicio que gestiona las ofertas actividades con el server
 * @param {factory} eventoFactory          -- Factoria que contiene y gestiona las peticiones de rest
 * @returns {undefined}
 */
function eventoCtrl(
        $scope
        ,ofertaEducativaFactory
        ,eventoFactory
        ,actividadAcademicaService
        ,actividadNoAcademicaService
        ,ofertaActividadService // Para OfertaEducativa
        ,ofertaActividadCentroService // Para OfertaEducativa desde centro
        ,$filter
        ,centroParametrosService        // Para Calendarizacion
        ,ofertaCalendarioCentroService  // Para Calendarizacion
         ,centroFactory
          ,httpInterceptor
           ,$http
        ) {
    $scope.headers = {};
    $scope.isCoordinador = false;
    $scope.ofertaActividad          = {};
    $scope.funcionesOfertaEducativa = {};
    $scope.tipoFormEvento           = "nuevo";
    $scope.fuenteActividades        = [];

    $scope.tiposOferta              = [
                                        {nombre: "Actividad académica",     clave: "actividadAcademica"},
                                        {nombre: "Actividad NO académica",  clave: "actividadNoAcademica"}
                                       ];
    // Se quito esta opción de tiposOferta  hasta que sea incluida,{nombre: "Ruta",                    clave: "ruta"}
    
    $scope.tabsEvento              = [
                                        {nombre: "Ruta/Actividad",  clave: "rutaActividad"  },
                                        {nombre: "Fecha de inicio",  clave: "fechaInicio"   },
                                        {nombre: "Esquema",         clave: "esquema"        },
                                        {nombre: "Fecha fin",       clave: "fechaFin"       },
                                        {nombre: "Día/Horario",     clave: "horario"        }
                                       ];
    $scope.tabsDetalle              = [
                                        {nombre: "Inscritos",    clave: "inscritos"},
                                        {nombre: "Preinscritos",  clave: "interesados"  }
                                       ];

   $scope.init = function(tipoEvento,usuario){

       $scope.isUserCoordinador(usuario.rol);
       //con la variable $http se setea el valor del userName para los rest
       $scope.agregarHeaders(usuario);
       centroFactory.cargaInicialEvento = $scope.cargaInicialEvento;
       $scope.cargaInicialEvento(tipoEvento,'inicial');
   }

    $scope.cargaInicialEvento = function(tipoEvento,tipoCarga){
        $scope.tipoEvento = tipoEvento;
        //Cargando las funciones contenidas en la factoria oferta Educativa
        $scope.funcionesOfertaEducativa     = ofertaEducativaFactory;
        $scope.eventoFactory                = eventoFactory;
        eventoFactory.editarEvento          = $scope.editarEvento;
        eventoFactory.nuevoEvento           = $scope.nuevoEvento;
        eventoFactory.editarSoloPorEvento   = $scope.editarSoloPorEvento;

        $scope.actividadesAcademicas     = [];
        $scope.actividadesNoAcademicas   = [];

        $scope.hostActividades = {hostCentro: limpiarUri(centroFactory.centroActual.ip)};
        if($scope.tipoEvento == "calendarizacion"){

            centroParametrosService.restResource.getEspacios({hostCentro: limpiarUri(centroFactory.centroActual.ip)},{}, function(data){       $scope.espacios =  data;});
            centroParametrosService.restResource.getFacilitadores({hostCentro: limpiarUri(centroFactory.centroActual.ip)},{}, function(data){  $scope.facilitadores =  data;});
            if(tipoCarga == 'inicial'){
                //Reorganizando las tabbas
                $scope.tabsEvento= [
                                        {nombre: "Ruta/Actividad",  clave: "rutaActividad"  },
                                        {nombre: "Esquema",         clave: "esquema"        },
                                        {nombre: "Fecha de inicio",  clave: "fechaInicio"             },
                                        {nombre: "Fecha fin",       clave: "fechaFin"       },
                                        {nombre: "Fecha de sesión",  clave: "horarioEvento"           },
                                        {nombre: "Día/Horario",     clave: "horario"        },
                                        {nombre: "Aula",             clave: "aula"                    },
                                        {nombre: "Facilitador",      clave: "facilitador"             },
                                        {nombre: "Fecha desplazamiento",      clave: "fechaDesplazamiento"}
                                    ]
            }

        }
        $scope.eventoFactory.tipoCarga = tipoCarga;

        actividadAcademicaService.restResource.getActividadesAcademicasPublicadasWithEsquemasPorHost($scope.hostActividades,
                        function(result){
                           $scope.actividadesAcademicas = result;
                        },
                        function(){
                          toastr.error("Ocurrio un error al realizar la operación solicitada: traer actividades académicas");
                        }
                    );
        actividadAcademicaService.restResource.getActividadesAcademicasPublicadasWithEsquemasPorHost($scope.hostActividades,
                        function(result){
                           $scope.actividadesAcademicas = result;
                        },
                        function(){
                          toastr.error("Ocurrio un error al realizar la operación solicitada: traer actividades académicas");
                        }
                    );
        actividadNoAcademicaService.restResource.getPorHost(
                        $scope.hostActividades,{},
                        function(result){
                           $scope.actividadesNoAcademicas = result;
                        },
                        function(){
                          toastr.error("Ocurrio un error al realizar la operación solicitada: traer actividades NO académicas");
                        }
                    );
    }

    /**
     * Funcion que valida la edicion de acuerdo al tipo de escenario del evento y el actual
     * @param {type} tipo_escenario
     */
    $scope.validarEdicion = function(escenarioEvento){
        var respuesta = "nuevo";
        if($scope.tipoEvento == "ofertaEducativa"){
            var escenarioActual = ofertaEducativaFactory.escenarioOferta._escenario._tipo_escenario.clave;
            respuesta = (escenarioActual == escenarioEvento)?"editar":"soloLectura";
        }else if($scope.tipoEvento == "calendarizacion"){
            respuesta = "editar";
        }
        return respuesta;
    }

    /**
     * Edita la actividad seleccionada
     */
    $scope.editarEvento = function(evento){
        $scope.reiniciarOfertActividad();                                    //Reiniciamos la oferta actividad a 0's
        $scope.reiniciarModalDetalle(evento);                                    //Activamos el modal Detalle para inscritos e interesados
        if($scope.tipoEvento == "calendarizacion"){
            if(evento.hasOwnProperty("tipo")){
                $scope.ofertaActividad.tipo_evento    = evento.tipo;                //Agregando el tipo de evento en caso de que sea calendarizacion
                if(evento.tipo == 'oferta'){
                    $scope.ofertaActividad.evento_actividad    = "actividad";           //Cargamos el tipo de evento actividad
                    }
                else if(evento.tipo == "centro"){
                    $scope.ofertaActividad.ActividadSoloLectura = evento.basado_oferta;
                    }
                 $scope.ofertaActividad.evento.hora_inicio = evento.hora_inicio;
                 $scope.ofertaActividad.evento.hora_fin    = evento.hora_fin;
                }
            if(evento.hasOwnProperty("evento_id"))
                $scope.ofertaActividad.evento.evento_id             = evento.evento_id;
            if(evento.hasOwnProperty("aula_id_actividad"))
                $scope.ofertaActividad.espacio_id_actividad         = evento.aula_id_actividad
            if(evento.hasOwnProperty("facilitador_id_actividad"))
                $scope.ofertaActividad.facilitador_id_actividad     = evento.facilitador_id_actividad;
            if(evento.hasOwnProperty("aula_id_evento"))
                $scope.ofertaActividad.espacio_id_evento            = evento.aula_id_evento
            if(evento.hasOwnProperty("facilitador_id_evento"))
                $scope.ofertaActividad.facilitador_id_evento        = evento.facilitador_id_evento;
            if(evento.hasOwnProperty("clave_grupo"))
                $scope.ofertaActividad.clave_grupo                  = evento.clave_grupo;
            if(evento.hasOwnProperty("basado_oferta"))
                $scope.ofertaActividad.basado_oferta                = evento.basado_oferta;
            if(evento.hasOwnProperty("puede_publicar"))
                $scope.ofertaActividad.puede_publicar               = evento.basado_oferta;
            if(evento.hasOwnProperty("sincronizado"))
                $scope.ofertaActividad.sincronizado                 = evento.basado_oferta;
        }else{
            if(evento.hasOwnProperty("puede_publicar"))
                $scope.ofertaActividad.puede_publicar               = evento.puede_publicar;
            if(evento.hasOwnProperty("sincronizado"))
                $scope.ofertaActividad.sincronizado                 = evento.sincronizado;
        }

        //Se setea el control de acceso, si no trae se coloca uno por default
        if(evento.hasOwnProperty("control_acceso")){
            $scope.ofertaActividad.control_acceso = evento.control_acceso;
        }
        else{
            $scope.ofertaActividad.control_acceso = {
                                                    obligatorio:[]
                                                    };
        }


        $scope.ofertaActividad.fecha_inicio        = $filter('date')(evento.fecha_inicio, "yyyy-MM-dd");
        $scope.ofertaActividad.fecha_inicio_date   = new Date(evento.fecha_inicio);

        //Setea el dia de inicio que fue el dia seleccionado
        $scope.ofertaActividad.diaSemanaSeleccionado = $scope.ofertaActividad.fecha_inicio_date.getDay();

        $scope.ofertaActividad.fecha_final         = $filter('date')(evento.fecha_final, "yyyy-MM-dd");
        $scope.ofertaActividad.fechaEvento         = evento.start;

        $scope.ofertaActividad.oferta_actividad_id = evento.oferta_actividad_id;

        $scope.ofertaActividad.oferta_actividad_centro_id = evento.oferta_actividad_centro_id;
        $scope.tipoFormEvento = $scope.validarEdicion(evento.tipo_escenario);   // Colocamos el tipo de form evento como editar o solo lectura, de acuerdo al escenario


       //Checamos si la oferta educativa no es la actual mandamos el tipo de evento solo de lectura
        $scope.tipoFormEvento = ofertaEducativaFactory.ofertaSoloLectura() ? "soloLectura" : $scope.tipoFormEvento;

        if(evento.es_academica){
            $scope.ofertaActividad.tipoOferta = "actividadAcademica";
        }else if(evento.hasOwnProperty("id_actividad")){
            $scope.ofertaActividad.tipoOferta  = "actividadNoAcademica";
        }else{
            toastr.error(ocurrioError + ": actividad no encontrada.");
            exit();
        }
        var actividad = false;
        $scope.obtenerFuenteActividades();                                      //Cargamos las actividades correspondientes
        var idActividad;
        for(var i = 0; i < $scope.fuenteActividades.length; i++){
             idActividad= $scope.ofertaActividad.tipoOferta =="actividadAcademica"? $scope.fuenteActividades[i].actividad_academica_id:$scope.fuenteActividades[i].actividad_id;
            if(evento.id_actividad == idActividad){
                actividad = $scope.fuenteActividades[i];
            }
        }
        if(actividad != false){

            $scope.ofertaActividad.actividadSeleccionada = $scope.ofertaActividad.tipoOferta =="actividadAcademica"? actividad.actividad_academica_id:actividad.actividad_id;    //seleccionando en el radio button
            $scope.seleccionarActividad(actividad);                                             //Cargando la actividad en ofertaActividad
            $scope.ofertaActividad.obligatorio           = evento.obligatorio;                  //Cargando el valor de obligatorio

            //Cargando y seleccionando el esquema
            if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){
                $scope.ofertaActividad.esquemaSeleccionado   = evento.esquema_id;
                var esquema = false;

                for(var i = 0; i < $scope.ofertaActividad.actividad._esquemas.length; i++){
                    if(evento.esquema_id == $scope.ofertaActividad.actividad._esquemas[i].actividad_academica_esquema_id){
                        esquema = $scope.ofertaActividad.actividad._esquemas[i];
                    }
                }
                $scope.seleccionarEsquema(esquema);
            }
            //cargando dias seleccionados del horario que contiene
            var horarios = evento._horarios;
            //Recorrre cada dia de los horarios cargados
            angular.forEach(horarios,function(value, key){
                if($scope.ofertaActividad.tipoOferta == "actividadNoAcademica"){
                    var indiceHora = $scope.ofertaActividad.dias[value.dia_semana-1].horasFin.indexOf(value.hora_inicio);
                    $scope.ofertaActividad.dias[value.dia_semana-1].horasFin.splice(0,indiceHora+1);
                }

                //Se cargan los dias si estan contenidos en los horarios del evento
                var indiceDia = value.dia_semana-1;
                $scope.ofertaActividad.dias[indiceDia].seleccionado = true;
                $scope.ofertaActividad.dias[indiceDia].hora_inicio  = value.hora_inicio;
                $scope.ofertaActividad.dias[indiceDia].hora_fin     = value.hora_fin.toString();
                $scope.ofertaActividad.horariosSeleccionados.push($scope.ofertaActividad.dias[indiceDia]);  // Empujando a la ofertaActividad los horarios Seleccionados
            });

        }else{
            toastr.error(ocurrioError + ": actividad no encontrada.");
        }
        $scope.deshabilitarOcultarElementos();
    }

    /**
     * cambia el valor de la tab donde se encuetra actualmente
     * @param {type} date
     * @returns {undefined}
     */
    $scope.onClickTab = function (tab){        $scope.currentTab = tab;  $scope.deshabilitarOcultarElementos('tab');  }
    $scope.onClickTabDetalle = function (tab){        $scope.currentTabDetalle = tab;    }

    /*
     * Setea en 0's los elementos relacionados con el evento
     * en este caso solo se trata con ofertas actividades
     * @returns {undefined}
     */
    $scope.nuevoEvento = function(date){
        $scope.tipoFormEvento                            = "nuevo";              // Colocamos el tipo de form evento como nuevo
        $scope.reiniciarOfertActividad();                                       //Reiniciamos la oferta actividad a 0's
        if($scope.tipoEvento == "calendarizacion"){
            $scope.ofertaActividad.evento_actividad      = "actividad";
        }
        $scope.ofertaActividad.fecha_inicio              = date.format("YYYY-MM-DD");
        $scope.ofertaActividad.diaSemanaSeleccionado     = date.day();
        $scope.ofertaActividad.horaSeleccionada          = date.hour();

        $scope.ofertaActividad.control_acceso             = {
                                                            obligatorio:[]
                                                        };

        $scope.reiniciarModalDetalle(date);
        $scope.deshabilitarOcultarElementos();
    }


    /**
     * Selecciona la actividad y coloca en el evento
     */
    $scope.seleccionarActividad = function(actividad){

        $scope.ofertaActividad.esquema                          = false;
        if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){
            if(actividad._esquemas.length == 0){
                toastr.warning("La actividad académica seleccionada no contiene esquemas");
                delete $scope.ofertaActividad.actividad;
                return;
            }
        }
        $scope.ofertaActividad.actividad                        = actividad;
        $scope.ofertaActividad.obligatorio                      = false;

        // Marcando como seleccionada una actividad
        var indexActividad                                      = $scope.fuenteActividades.indexOf(actividad);
        $scope.fuenteActividades[indexActividad].agregado       = true;

        if($scope.ofertaActividad.tipoOferta == "actividadNoAcademica"){
            //Reiniciamos los dias en caso de cambiar
            $scope.agregarDiaPreseleccionado();
        }
    }


    /**
     * Guardar evento
     */
    $scope.guardarEvento = function(){
        var data = {
                    horario                 : $scope.ofertaActividad.horariosSeleccionados,
                    obligatorio             : $scope.ofertaActividad.obligatorio,
                    fecha_inicio            : $scope.ofertaActividad.fecha_inicio,
                    control_acceso          : $scope.ofertaActividad.control_acceso
          };

        if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){
            data.actividad_academica_id = $scope.ofertaActividad.actividad.actividad_academica_id;          //Si la actividad es académica se envia el id de actividada
            data.esquema_id             = $scope.ofertaActividad.esquema.actividad_academica_esquema_id;    //Si la actividad es académica se envia el esquema
        }else if($scope.ofertaActividad.tipoOferta == "actividadNoAcademica"){
            data.actividad_id = $scope.ofertaActividad.actividad.actividad_id;                              //Si la actividad es NO académica se envia el id de actividadad
            data.fecha_final = $scope.ofertaActividad.fecha_final;                                          //Emvoa Fecha final
            data.esquema_id  = 0;
        }

        if($scope.tipoEvento == "calendarizacion"){
            if($scope.ofertaActividad.evento_actividad == "actividad"){
                data.oferta_id              = ofertaEducativaFactory.ofertaEducativa.oferta_educativa_centro_id;
                data.facilitador_id         = $scope.ofertaActividad.facilitador_id;
                data.aula_id                = $scope.ofertaActividad.espacio_id;
                data.oferta_actividad_id    = $scope.ofertaActividad.oferta_actividad_id;
                if($scope.ofertaActividad.tipo_evento == "oferta"){
                    $scope.calendarizarEvento(data);
                }else if($scope.ofertaActividad.tipo_evento == "centro"){
                    $scope.enviarEdicionCalendario(data);
                }
                else if($scope.tipoFormEvento == "nuevo"){
                    $scope.calendarizarEvento(data);
                }
            }
            else{
                data = {};
                //Se modifica solo un evento
                if(eventoFactory.modificarGrupoEventos == false){
                    //Revisar que se puede modificar el evento
                        data.facilitador_id         = $scope.ofertaActividad.facilitador_id;
                        data.aula_id                = $scope.ofertaActividad.espacio_id;
                        data.fecha_evento           = $scope.ofertaActividad.evento.fecha_evento;
                        data.hora_fin               = $scope.ofertaActividad.evento.hora_fin+":00";
                        data.hora_inicio            = $scope.ofertaActividad.evento.hora_inicio+":00";
                        $scope.editarSoloPorEvento(data,$scope.ofertaActividad.evento.evento_id)
                }else{
                        //Se modifica por seccionado
                        data.fecha_desplazamiento = $scope.ofertaActividad.fecha_desplazamiento;
                        //Se envia el faciltador asignado al grupo de eventos a seccionar
                        data.facilitador_id       = $scope.ofertaActividad.facilitador_id;
                        $scope.editarBloquedeEventos(data,$scope.ofertaActividad.evento.evento_id);
                }

            }
        }
        //
        else if($scope.tipoEvento == "ofertaEducativa"){
            data.escenario_oferta_id     = ofertaEducativaFactory.escenarioOferta.escenario_oferta_id;

            if($scope.tipoFormEvento=="nuevo"){
                $scope.enviarNuevoEvento(data);
            }else if($scope.tipoFormEvento=="editar"){
                    $scope.enviarEdicionEvento(data);
            }

        }
    }


    /*
     * Función que valida si el evento seleccionado es el primer evento
     * de la actividad y  también pregunta si la fecha de incio es obligatoria
     * @returns {undefined}
     */
    $scope.sePuedeModificarElEvento = function(){
        if($scope.ofertaActividad.hasOwnProperty('evento')){
            if($scope.ofertaActividad.evento.hasOwnProperty('fecha_evento')){
                return !($scope.ofertaActividad.evento.fecha_evento == $scope.ofertaActividad.fecha_inicio
                         && $scope.esObligatorio('fecha_inicio'));
             }
         }
         return true
    }

    /**
     * Función para eliminar la actividad con todos sus eventos
     */
    $scope.eliminarActividad = function(){
        dialogConfirm("Eliminar actividad", "¿Está seguro que desea eliminar esta actividad?", function() {
            var m = loader.modal();
            ofertaActividadService.restResource.delete(
                    {"oferta_actividad_id": $scope.ofertaActividad.oferta_actividad_id}
                    ,{},
                    function (result) {
                        if(result){
                            m.remove();
                            toastr.success("Se ha eliminado correctamente la actividad", "Actividad");
                            $scope.reiniciarOfertActividad();
                            $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                        }else{
                            toastr.error(ocurrioError + ": eliminar actividad.");
                        }
                    },
                    function (result) {
                        m.remove();
                        toastr.error(ocurrioError + ": eliminar actividad.");
                    }
            );
            $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
        });
    }

    /**
     * Función para cambiar de fuente de actividades academicas vs no academicas
     */
    $scope.obtenerFuenteActividades = function(){
        if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){
            $scope.categoriasActividad = centroFactory.datosInformativos.categoriasActividadAcademica;
            if($scope.tipoFormEvento == "nuevo"){
                $scope.fuenteActividades = [];
                $scope.ofertaActividad.fecha_final = "";
                for(var y = 0; y < $scope.actividadesAcademicas.length; y++){
                    if($scope.actividadesAcademicas[y].Estatus.nombre == "Publicado"){
                        $scope.fuenteActividades.push($scope.actividadesAcademicas[y]);
                    }
                }
            }else{
                $scope.fuenteActividades = $scope.actividadesAcademicas;
            }
        }else if($scope.ofertaActividad.tipoOferta == "actividadNoAcademica"){
            $scope.categoriasActividad = centroFactory.datosInformativos.categoriasActividadNoAcademica;
            $scope.fuenteActividades = $scope.actividadesNoAcademicas;
            if($scope.tipoFormEvento == "nuevo"){
                $scope.ofertaActividad.fecha_final = $scope.ofertaActividad.fecha_inicio;
            }
        }else{
            $scope.fuenteActividades = [];
        }
        $scope.fuenteActividades2 = $scope.fuenteActividades;
        return $scope.fuenteActividades;
    }

    /**
     * Opción para agregar una nueva oferta educativa
     * COnsumimos el api rest  para agregarlo
     * Metodo de ofertaEducativa
     */
    $scope.enviarNuevoEvento = function(data) {
        var m = loader.modal();
        ofertaActividadService.restResource.add(
                data,
                function () {
                    m.remove();
                    toastr.success("Se ha agregado correctamente la actividad", "Actividad");
                    $scope.ofertaActividad = {}
                    //Carga las activ1idades en la pagina actual
                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                    $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
                },
                function () {
                    m.remove();
                    toastr.error(ocurrioError + ": agregar actividad");
                }
        );
    }

    /**
     * Opción para agregar una nueva oferta educativa
     * COnsumimos el api rest  para agregarlo
     * Metodo de ofertaEducativa
     */
    $scope.enviarEdicionEvento = function(data) {
        var m = loader.modal();
        ofertaActividadService.restResource.edit(
                {"oferta_actividad_id": $scope.ofertaActividad.oferta_actividad_id}
                ,data,
                function () {
                    m.remove();
                    toastr.success("Se ha editado correctamente la actividad", "Actividad");
                    $scope.ofertaActividad = {}
                    //Carga las actividades en la pagina actual
                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                    $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
                },
                function (result) {
                    m.remove();
                    toastr.error(ocurrioError + ": editar actividad, <b>" + result.data.message+"</b>");
                }
        );
    }



    /*
     * Función que evalua diferentes condiciones
     */
    $scope.mostrarElemento = function(tipo,data){
        var respuesta =  false;
        switch(tipo){
            case 'rutaActividad':
                respuesta = true;
            break;
            case 'esquema':
                if(   $scope.ofertaActividad.hasOwnProperty("actividad")
                   && $scope.ofertaActividad.tipoOferta == "actividadAcademica"){
                    if($scope.ofertaActividad.actividad.hasOwnProperty("_esquemas")){
                        respuesta = $scope.ofertaActividad.actividad._esquemas.length > 0;
                    }
                }
            break;
            case 'horario':
                if(    $scope.ofertaActividad.hasOwnProperty("actividad")
                    && $scope.ofertaActividad.tipoOferta == "actividadAcademica"){
                    respuesta = $scope.ofertaActividad.esquema != false;
                }else if($scope.ofertaActividad.fecha_final != ""
                        && $scope.ofertaActividad.hasOwnProperty("actividad")){
                    respuesta = true;
                }
               if($scope.tipoEvento == "calendarizacion"){
                    if($scope.ofertaActividad.evento_actividad == 'evento'){
                        respuesta = false;
                    }
                }
            break
            case 'fechaFin':
                if($scope.ofertaActividad.hasOwnProperty("actividad")
                   && $scope.ofertaActividad.tipoOferta == "actividadNoAcademica"
                   ){
                    if($scope.ofertaActividad.actividad.hasOwnProperty("actividad_id")){
                        respuesta = true;
                    }
                }
            break;
            case 'fechaInicio':
                if($scope.ofertaActividad.evento_actividad != 'evento'){
                    respuesta = true;
                }
            break;
            case 'fechaDesplazamiento':
                if($scope.ofertaActividad.evento_actividad == 'evento' && eventoFactory.modificarGrupoEventos == true){
                    respuesta = true;
                }
            break;
            case 'facilitador':
                respuesta=true;
                break;
            case 'aula':
                respuesta=true;
                break;
            case 'horarioEvento':
                respuesta = $scope.ofertaActividad.evento_actividad == 'evento' && eventoFactory.modificarGrupoEventos != true;
                break;

            case 'interesados':
                respuesta=true;
                break;
            case 'inscritos':
                respuesta=true;
                break;
            case 'existenInteresados':
                if($scope.ofertaActividad.hasOwnProperty('interesados')){
                    respuesta = $scope.ofertaActividad.interesados.length > 0;
                }
                break;
            case 'existenInscritos':
                if($scope.ofertaActividad.hasOwnProperty('inscritos')){
                    respuesta = $scope.ofertaActividad.inscritos.length > 0;
                }
                break;



        }
        return respuesta;
    }

    /*
    * Funci+ón que valida si la actividad ha iniciado
    * */
    $scope.actividadIniciada = function(){
        var respuesta = true;
            if($scope.ofertaActividad.hasOwnProperty("fecha_inicio")){

                var fechaActual             = new Date(centroFactory.fechaHoraSistema.getFullYear(),
                                                                                   centroFactory.fechaHoraSistema.getMonth(),
                                                                                   centroFactory.fechaHoraSistema.getDate(),
                                                                                   centroFactory.fechaHoraSistema.getHours() + parseInt(centroFactory.HorasMinimasActualizarSession.valor),
                                                                                   centroFactory.fechaHoraSistema.getMinutes(),
                                                                                   0,0);
                respuesta = fechaActual > $scope.ofertaActividad.fecha_inicio_date;
            }
        return respuesta;
    }
    /*
    * Función que valida si el evento ya a ha pasado
    * */
    $scope.eventoPasado = function(){
        var respuesta = true;
            if($scope.ofertaActividad.hasOwnProperty("evento")){
                if($scope.ofertaActividad.evento.hasOwnProperty("fecha_evento")){
                    respuesta = centroFactory.fechaHoraSistema > $scope.ofertaActividad.fechaEvento._d;
                }
            }
        return respuesta;
    }


    /*
    *

     * @param {type} dia
     * @param {type} quitarPoner
     * @returns {undefined}     */
    $scope.deshabilitarOcultarElementos = function(tipo){
        if(tipo != 'tab'){
            $scope.ocultaElementoEvento = {}
        }

        //Variable que muestra o quita el listado de actividades academincas o no academicas
        //En su negación mustra la activdiad seleccionada
        $scope.ocultaElementoEvento.fuenteActividades = $scope.tipoFormEvento                          != 'soloLectura'
                                                          && $scope.ofertaActividad.evento_actividad     != 'evento'
                                                          && $scope.ofertaActividad.tipo_evento          != 'oferta'
                                                          && $scope.tipoFormEvento                       =='nuevo'
                                                          && $scope.ofertaActividad.ActividadSoloLectura != true
                                                          && $scope.actividadIniciada() != true
                                                          && $scope.ofertaActividad.puede_publicar != true
                                                          ;



        $scope.ocultaElementoEvento.soloLectura = ($scope.tipoFormEvento == 'soloLectura'
                                                    || (
                                                        $scope.ofertaActividad.evento_actividad == 'evento'
                                                         && (
                                                             $scope.currentTab      != 'aula'
                                                             && $scope.currentTab   != 'facilitador'
                                                             && $scope.currentTab   != 'horarioEvento'
                                                             && $scope.currentTab   != 'fechaDesplazamiento'
                                                             || (
                                                                 //Si el el combo determina que es seccionado
                                                                eventoFactory.modificarGrupoEventos == true
                                                                //La opcion solo de lectura se aplica
                                                                //si no esta en estas pantallas
                                                                && !(
                                                                    $scope.currentTab      == 'facilitador'
                                                                    || $scope.currentTab   == 'fechaDesplazamiento'
                                                                    )
                                                                )
                                                            ||
                                                            $scope.eventoPasado()
                                                            )
                                                        )
                                                     || (
                                                         $scope.ofertaActividad.ActividadSoloLectura    == true
                                                         && $scope.currentTab                           == 'rutaActividad'
                                                         )
                                                    // Si la actividad es pasada
                                                     || (
                                                         $scope.ofertaActividad.evento_actividad == "actividad"
                                                         && $scope.actividadIniciada()
                                                        )
                                                     // Si la actividad es extemporanea para tipo actividad o solo lectura
                                                      ||
                                                        ofertaEducativaFactory.tipoProgramacionCE == "extemporanea"
                                                        && (
                                                            $scope.ofertaActividad.puede_publicar == true
                                                            && (
                                                                $scope.currentTab   != 'rutaActividad'
                                                                || $scope.currentTab   != 'esquema'
                                                                || $scope.currentTab   != 'horario'
                                                                )
                                                            )
                                                        && ($scope.tipoFormEvento != "nuevo")
                                                    );


        $scope.ocultaElementoEvento.esquema = $scope.tipoFormEvento                      == 'soloLectura'
                                            || (($scope.ofertaActividad.hasOwnProperty("evento_actividad"))
                                                    ? $scope.ofertaActividad.evento_actividad == 'evento' : false
                                                )
                                            || $scope.actividadIniciada()
                                            || $scope.ocultaElementoEvento.soloLectura;


        $scope.ocultaElementoEvento.dia_horario     =
        $scope.ocultaElementoEvento.fecha_fin       =
        $scope.ocultaElementoEvento.fecha_inicio    = $scope.ocultaElementoEvento.soloLectura
                                                      || $scope.ofertaActividad.evento_actividad == 'evento'
                                                     ;
        //Se revisa si no se puede colocar la obligatoriedad,
        //Si regresa verdadero entonces puede setear el obligatorio
        //En base a:
        //1- Es creada desde oferta educativa
        //2- El evento esta trabajado en calendarizacion y es modificado desde
        //   actividad y a su vez esta basado en oferta educativa
        $scope.ocultaElementoEvento.set_obligatorio_fecha_inicio = $scope.tipoEvento == 'ofertaEducativa'
                                                                     || (
                                                                         $scope.tipoEvento == 'calendarizacion'
                                                                         && $scope.ofertaActividad.evento_actividad == 'actividad'
                                                                         && (
                                                                            $scope.ofertaActividad.tipo_evento      == 'oferta'
                                                                            || $scope.ofertaActividad.basado_oferta == true
                                                                            )
                                                                         );

        // El elemento se bloquea en estos casos:
        // Es solo de lectura y
        // La modificación del modal es de tipo evento
        // es oferta educativa
        $scope.ocultaElementoEvento.fecha_inicio    = $scope.ocultaElementoEvento.fecha_inicio
                                                      || $scope.tipoEvento == 'ofertaEducativa'
                                                      || (
                                                          $scope.tipoEvento == 'calendarizacion'
                                                          && $scope.esObligatorio('fecha_inicio')
                                                         );


         // LA fecha de desplazamientoaplica se puede modificar si es evento y siguientes
        // La fecha inicio no es igual al evento actual y es fija la fecha de inicio
        // ó no trae la bandera solo de lectura

        $scope.ocultaElementoEvento.fecha_desplazamiento = $scope.ocultaElementoEvento.soloLectura
                                                                   || !$scope.sePuedeModificarElEvento();



        // El evento_dia es posible modificarlo si
        // La fecha inicio no es igual al evento actual
        // ó no trae la bandera solo de lectura
        $scope.ocultaElementoEvento.evento_dia = $scope.ocultaElementoEvento.soloLectura
                                                      || !$scope.sePuedeModificarElEvento();


        $scope.ocultaElementoEvento.evento_horario  = $scope.ocultaElementoEvento.soloLectura;





        $scope.ocultaElementoEvento.actividadObligatorio = $scope.ofertaActividad.basado_oferta == true
                                                           || $scope.ofertaActividad.tipo_evento == "oferta"
                                                           || ($scope.tipoEvento == 'ofertaEducativa' && $scope.tipoFormEvento != 'soloLectura')
                                                           || (
                                                                $scope.eventoFactory.tipoCarga == 'calendarizacionCentral'
                                                                && $scope.ofertaActividad.tipo_evento == "oferta"
                                                               )
                                                           ;

        if(tipo != 'tab'){
            $scope.ocultaElementoEvento.eventoBtn = {};
        }
        $scope.ocultaElementoEvento.eventoBtn.btnAgregarModificar = $scope.tipoFormEvento != 'soloLectura'
                                                                    && $scope.ofertaActividad.evento_actividad  != '';

        $scope.ocultaElementoEvento.eventoBtn.nuevo         = $scope.tipoFormEvento == 'nuevo';

        $scope.ocultaElementoEvento.eventoBtn.calendarizar  = !$scope.ocultaElementoEvento.eventoBtn.nuevo
                                                             && $scope.tipoEvento                     == 'calendarizacion'
                                                             && $scope.ofertaActividad.tipo_evento    != 'centro';

        $scope.ocultaElementoEvento.eventoBtn.modificarDatos = !$scope.ocultaElementoEvento.eventoBtn.nuevo
                                                                && ($scope.tipoEvento                 != 'calendarizacion'
                                                                    ||  $scope.ofertaActividad.tipo_evento== 'centro');



        $scope.ocultaElementoEvento.eventoBtn.eliminarMostrar      =    !$scope.ocultaElementoEvento.eventoBtn.nuevo
                                                                        && $scope.tipoEvento                       == 'calendarizacion'
                                                                        && $scope.ofertaActividad.evento_actividad == 'actividad'
                                                                        && $scope.ofertaActividad.tipo_evento      != 'oferta';

        if($scope.ocultaElementoEvento.eventoBtn.eliminarMostrar && tipo != 'tab'){
            $scope.ocultaElementoEvento.eventoBtn.eliminarDeshabilitar = true;
            $scope.puedeCancelarYOEliminarActividad("eliminar",
                function(respuesta){
                    // Cargar el $scope.ocultaElementoEvento.eventoBtn.eliminarDeshabilitar en cuanto este listo el servicio rest de validacion
                    $scope.ocultaElementoEvento.eventoBtn.eliminarDeshabilitar = !respuesta;
                }
            );
        }


        $scope.ocultaElementoEvento.eventoBtn.cancelarMostrar       =   !$scope.ocultaElementoEvento.eventoBtn.nuevo
                                                                        && $scope.tipoEvento                       == 'calendarizacion'
                                                                        && $scope.ofertaActividad.evento_actividad == 'actividad'
                                                                        && $scope.ofertaActividad.tipo_evento      != 'oferta';
                                                                        //curso iniciado sin inscritos;
        if($scope.ocultaElementoEvento.eventoBtn.cancelarMostrar && tipo != 'tab'){
            $scope.ocultaElementoEvento.eventoBtn.cancelarDeshabilitar  = true;
            $scope.puedeCancelarYOEliminarActividad("cancelar",
                function(respuesta){
                    // Cargar el $scope.ocultaElementoEvento.eventoBtn.cancelarDeshabilitar en cuanto este listo el servicio rest de validacion
                    $scope.ocultaElementoEvento.eventoBtn.cancelarDeshabilitar = !respuesta;
                }
            );
        }


    }


    /*
     * Función que evalua si puede seleccionar el dia en base al esquema elegido
     */
    $scope.seleccionarDia = function(dia,quitarPoner){
        if(quitarPoner){
            $scope.ofertaActividad.horariosSeleccionados.push(dia);
        }else{
            $scope.ofertaActividad.horariosSeleccionados.splice($scope.ofertaActividad.horariosSeleccionados.indexOf(dia),1);
        }
    }

    /*
     * Función que evalua si un evento es valido para poder guardarse
     */
    $scope.eventoValido = function(){
        if($scope.ofertaActividad.hasOwnProperty("horariosSeleccionados") ){
            var horario  = $scope.ofertaActividad.horariosSeleccionados;
            if(horario.length==undefined || horario.length ==0){                                 //Horario Vacio
                return false;
            }
            for(var n = 0; n < horario.length; n++){
            if(horario[n].hora_inicio == undefined || horario[n].hora_fin == undefined){    //No ha llenado completamente un horario
                return false;
            }
            }
            if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){                     // Si es una actividad academica (tiene esquema)
                if($scope.ofertaActividad.esquema.dias_semana != horario.length){
                    return false;
                }
            }
        }
        if($scope.tipoEvento == "calendarizacion"){
            if($scope.ofertaActividad.espacio_id == undefined
                    || $scope.ofertaActividad.facilitador_id == undefined
                    || $scope.ofertaActividad.fecha_inicio == undefined
                    || $scope.ofertaActividad.fecha_inicio == ''){
                    return false;
            }
            if($scope.ofertaActividad.evento_actividad == "evento"){
                //si es de tipo evento la modificacion verifica que no sea mayor el evento
                return eventoFactory.eventoModificable(
                        true,
                        $scope.ofertaActividad.fechaEvento._d,
                        centroFactory.fechaHoraSistema,
                        centroFactory.HorasMinimasActualizarSession.valor
                        );
            }else if($scope.ofertaActividad.evento_actividad == "actividad"){
                return !$scope.actividadIniciada();
            }
        }
        return true;
    }

    /**
     * Cambiar de valor el campo de obluigatorio
     */
    $scope.toogleObligatorio = function(){
        $scope.ofertaActividad.obligatorio = $scope.ofertaActividad.obligatorio === false ? true: false ;

        //Encontramos el index del campo
        //en el array de obligatorio el campo fecha_inicio
        var indexCampo = $scope.ofertaActividad.control_acceso.obligatorio.indexOf('fecha_inicio');
        //Verifica si existe el campo en el array de obligatorio y lo quita
        if(indexCampo > -1){
                $scope.ofertaActividad.control_acceso.obligatorio.splice(indexCampo, 1);
        }
    }




    /**
     * Selecciona la actividad y coloca en el evento
     */
    $scope.seleccionarEsquema = function(esquema){
        $scope.ofertaActividad.esquema                  = esquema;
        $scope.ofertaActividad.dias                     = $scope.dias(esquema.horas_sesion);        //Reiniciamos los dias en caso de cambiar
        $scope.ofertaActividad.horariosSeleccionados    = [];
        if($scope.tipoEvento == "calendarizacion"){
            $scope.ofertaActividad.evento.diahoras          = $scope.ofertaActividad.dias[0];
        }
        $scope.agregarDiaPreseleccionado();
    }

    /*
     * Función que te asigna el dia que fue dado click al crear una nueva actividad
     *
     * @returns {Boolean}     */
    $scope.agregarDiaPreseleccionado = function(){
        //Elimnando cualquier horario seleccionado anteriormente
        $scope.ofertaActividad.horariosSeleccionados    = [];
        if($scope.ofertaActividad.hasOwnProperty('diaSemanaSeleccionado') && $scope.ofertaActividad.hasOwnProperty('horaSeleccionada')){
            //Filtra el dia de la semana con la funcion filter nativa de javascript
                var dia = $scope.ofertaActividad.dias.filter(function(obj){return obj.dia_semana == $scope.ofertaActividad.diaSemanaSeleccionado;});
                if(dia.length > 0){
                    var hora_inicio
                    if($scope.ofertaActividad.horaSeleccionada != 0
                        && $scope.ofertaActividad.horaSeleccionada >= centroFactory.horarioCentro.apertura){
                        hora_inicio = $scope.ofertaActividad.horaSeleccionada;
                        dia[0].hora_inicio = (hora_inicio <10 ? "0"+ hora_inicio : hora_inicio) + ":00";
                    }
                    $scope.ofertaActividad.horariosSeleccionados.push(dia[0]);
                    $scope.ofertaActividad.dias[$scope.ofertaActividad.dias.indexOf(dia[0])].seleccionado = true;
                    $scope.generarHorasFinValidas(0,dia[0]);
                }

            }
    }
    /**
     * Función que evalua si puede seleccionar el dia en base al esquema elegido
     * y tambien en base si el dia de inicio fue bloqueado
     * @param {int} dia Dia a revisar
     */
    $scope.puedeSeleccionarDia = function(dia){
        if($scope.ofertaActividad.hasOwnProperty("actividad")){
                if($scope.ofertaActividad.esquema != false
                   && $scope.ofertaActividad.tipoOferta == "actividadAcademica"){
                    if($scope.diaInicialBloqueado(dia) == true){
                        return false;
                    }
                    else if($scope.ofertaActividad.horariosSeleccionados.length
                            <
                            $scope.ofertaActividad.esquema.dias_semana){
                        return true;
                    }else if($scope.ofertaActividad.horariosSeleccionados.indexOf(dia)>=0){
                       return true
                    }
                }else if($scope.ofertaActividad.tipoOferta == "actividadNoAcademica"){
                    if($scope.diaInicialBloqueado(dia) == true){
                        return false;
                    }else{
                        return true;
                    }
                }
        }
        return false;
    }

    /**
     * Función que evalua si se bloquea el dia inicial para la opción de
     * fecha_inicio obligatorio
     * @param {object} dia Objeto dia obtenido de array de $scope.dias
     * @returns {undefined}
     */
    $scope.diaInicialBloqueado = function(dia){
        //Verifica si el formulario es de tipo nuevo
        // Si es el dia esta preseleccionado
        // Si el dia seleccionado esta activo
        // si la fecha inicio esta obligatoria
        return dia.seleccionado == true
                && ($scope.tipoFormEvento == 'nuevo' || $scope.tipoFormEvento == 'editar')
                && $scope.ofertaActividad.diaSemanaSeleccionado == dia.dia_semana
                && $scope.esObligatorio('fecha_inicio');
    }


   /*
     * Metodo para reiniciar la oferta actividad a 0's
     * @param {type} actividad
     * @returns {undefined}
     */
    $scope.reiniciarOfertActividad = function(){
        $scope.currentTab                               = "rutaActividad";
        $scope.ofertaActividad                          = {};                   //Limpiamos el objeto
        $scope.ofertaActividad.horariosSeleccionados    = [];                   //LimpiarOfertaActividad
        $scope.ofertaActividad.dias                     = $scope.dias(1);        //Reiniciamos los dias checkeados
        $scope.ofertaActividad.obligatorio              = false;                //
        $scope.ofertaActividad.fecha_final              = "";
        if($scope.tipoEvento == "calendarizacion"){
            $scope.ofertaActividad.evento_actividad    = "";
            $scope.ofertaActividad.evento              = {}
            $scope.ofertaActividad.evento.diaHoras     = $scope.ofertaActividad.dias[0];
            centroParametrosService.restResource.horasAsignadasFacilitador(
            {
                "oferta_educativa_id":ofertaEducativaFactory.ofertaEducativa.oferta_educativa_centro_id
                , hostCentro : limpiarUri(centroFactory.centroActual.ip)
            },{}, function(data){  $scope.FacilitadoresConHorasAsignadas =  data; });
        }
    }

    $scope.reiniciaEsquemaHorario = function(){
        $scope.ofertaActividad.horariosSeleccionados    = [];                   //LimpiarOfertaActividad
        $scope.ofertaActividad.dias                     = $scope.dias(1);        //Reiniciamos los dias checkeados
        $scope.obtenerFuenteActividades();
        //$scope.ofertaActividad.categoriaActividad = null;
    }

    /**
    * filtrarPorCategoria
    *
    * Se filtran las Actividades (Academicas y no académicas) de acuerdo a la Categoría seleccionada
    *
    * @param {object} item Actividad a la cual se aplicará el filtro
    * @return {boolean} Si ha seleccionado una Categoría, Retorna TRUE si la actividad a filtrar corresponode a la Categoría seleccionada, En caso contrario FALSE. Si no ha seleccionado ninguna categoría retorna TRUE.
    *
    * @author Bet Gader Porcayo Juárez <bet.porcayo@enova.mx>
    * @since Aug 05 2015
    * @version 1.0
    *
    * @link http://stackoverflow.com/questions/13216115/filtering-by-multiple-specific-model-properties-in-angularjs-in-or-relationship
    *
    */
    $scope.filtrarPorCategoria = function(item){

        if(typeof $scope.ofertaActividad.categoriaActividad === "undefined" || $scope.ofertaActividad.categoriaActividad === null){
            return true;
        }
        if (typeof item._actividad_categoria !== "undefined") {
            if (typeof item._actividad_categoria.nombre !== "undefined") {
                if (item._actividad_categoria.nombre === $scope.ofertaActividad.categoriaActividad.nombre) {
                    return true;
                }
            }
        }
        if (typeof item.ActividadAcademicaCategoria !== "undefined") {
            if (typeof item.ActividadAcademicaCategoria.nombre !== "undefined") {

                if (item.ActividadAcademicaCategoria.nombre === $scope.ofertaActividad.categoriaActividad.nombre) {

                    return true;
                }
            }
        }
        return false;
    };

     /*
     * Metodo para generar horas fin validas en base al esquema
     * @param {dia} dia tipo
     * @returns {undefined}
     */
    $scope.generarHorasFinValidas = function(indexDia,dia){
        if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){
            var hora_inicio = $scope.ofertaActividad.horariosSeleccionados[$scope.ofertaActividad.horariosSeleccionados.indexOf(dia)].hora_inicio;
            hora_inicio = parseInt(hora_inicio);
            var hora_fin    = hora_inicio + $scope.ofertaActividad.esquema.horas_sesion;
                if(centroFactory.horarioCentro.cierre >= hora_fin){
                    hora_fin    = (hora_fin<10?"0"+hora_fin:hora_fin) + ":00";
                    $scope.ofertaActividad.horariosSeleccionados[$scope.ofertaActividad.horariosSeleccionados.indexOf(dia)].hora_fin = hora_fin;
                }

        }
        else{

            var dias                                             = $scope.dias(1);
            var indice                                           = $scope.ofertaActividad.horariosSeleccionados.indexOf(dia);
            var indiceDia                                        = $scope.ofertaActividad.dias.indexOf(dia);
            var indiceHora                                       = dias[indiceDia].horasFin.indexOf(dia.hora_inicio);
            dias[indiceDia].horasFin.splice(0,indiceHora +1);
            $scope.ofertaActividad.dias[indiceDia].horasFin      = dias[indiceDia].horasFin;
            if($scope.ofertaActividad.horariosSeleccionados[indice].hora_fin !=undefined){
                $scope.ofertaActividad.horariosSeleccionados[indice].hora_fin = undefined;
            }
        }
    }

     /*
     * Metodo para generar horas fin validas en base al esquema
     * @param {dia} dia tipo
     * @returns {undefined}
     */
    $scope.generarHorasFinValidasXEvento = function(){
        if($scope.ofertaActividad.tipoOferta == "actividadAcademica"){
            var hora_inicio = $scope.ofertaActividad.evento.hora_inicio;

            hora_inicio = parseInt(hora_inicio);
            var hora_fin    = hora_inicio + $scope.ofertaActividad.esquema.horas_sesion;
                hora_fin    = (hora_fin<10?"0"+hora_fin:hora_fin) + ":00";

            $scope.ofertaActividad.evento.hora_fin = hora_fin;
        }
        else{
            //Se carga un dia de la funcion que se genera a partir de la sesion
            $scope.ofertaActividad.evento.diaHoras              = $scope.dias(1)[0];
            var indiceHora                                       = $scope.ofertaActividad.evento.diaHoras.horasFin.indexOf($scope.ofertaActividad.evento.hora_inicio);
            $scope.ofertaActividad.evento.diaHoras.horasFin.splice(0,indiceHora +1);
        }
    }


    /*
     * Función para generar las horas a seleccionar y cargarlas en el elemento ofertaActividad cuando se reinicie
     * el objeto
     * @returns {undefined}
     */
    $scope.dias = function(horasSesion){
        var dias = [
                       {nombre:"Lunes"     ,dia_semana: 1 ,seleccionado: false , horasInicio:[], horasFin:[]},
                       {nombre:"Martes"    ,dia_semana: 2 ,seleccionado: false , horasInicio:[], horasFin:[]},
                       {nombre:"Miercoles" ,dia_semana: 3 ,seleccionado: false , horasInicio:[], horasFin:[]},
                       {nombre:"Jueves"    ,dia_semana: 4 ,seleccionado: false , horasInicio:[], horasFin:[]},
                       {nombre:"Viernes"   ,dia_semana: 5 ,seleccionado: false , horasInicio:[], horasFin:[]},
                       {nombre:"Sabado"    ,dia_semana: 6 ,seleccionado: false , horasInicio:[], horasFin:[]}
                      ];
        var hora;
        var horaInicioDia = centroFactory.horarioCentro.apertura;
        var horaFinDia    = centroFactory.horarioCentro.cierre - horasSesion;
        for(var i = 0; i < dias.length; i++){
            for(var j = 0; j <= horaFinDia - horaInicioDia; j++){
                //para que inicie desde 8 la creación de horas
                hora = j + horaInicioDia;
                dias[i].horasInicio[j] = (hora<10?"0"+hora:hora) + ":00";
                hora = hora + 1;
                dias[i].horasFin[j]    = (hora<10?"0"+hora:hora) + ":00";
            }
        }
        return dias;
    }


    $scope.selectDatePicker = function (newDate, oldDate,tipo_fecha) {

    var fecha = new Date(newDate);
    var mes   = ((fecha.getMonth() +1) <10)?"0" +(fecha.getMonth() +1): (fecha.getMonth() +1);
    var dia   = ((fecha.getDate()) <10)?"0" +(fecha.getDate()): (fecha.getDate());
    var numDia = fecha.getDay() == 0 ?  -1 : fecha.getDay() - 1;




    var fecha_actual = new Date();
    var fecha_inicio_oferta  = new Date(ofertaEducativaFactory.ofertaEducativa.fecha_inicio);
    var fecha_fin_oferta  = new Date(ofertaEducativaFactory.ofertaEducativa.fecha_fin);


    var fecha_fin_actividad  = $scope.ofertaActividad.fecha_fin;       //fecha_evento,fecha_fin,fecha_inicio::''
    var fecha_inicio_actividad  = $scope.ofertaActividad.fecha_fin;       //fecha_evento,fecha_fin,fecha_inicio::''

    fecha_inicio_actividad = (typeof fecha_inicio_actividad == "undefined")?"":
                                (typeof fecha_inicio_actividad == "object") ? fecha_inicio_actividad :
                                    (typeof fecha_inicio_actividad == "string") ? convertirFechaYMD(fecha_inicio_actividad) : "";

    fecha_fin_actividad = (typeof fecha_fin_actividad == "undefined")?"":
                            (typeof fecha_fin_actividad == "object") ? fecha_fin_actividad :
                                (typeof fecha_fin_actividad == "string") ? convertirFechaYMD(fecha_fin_actividad) : "";

        var fechaInvalida = {
            valor : false,
            motivo : ""
        }
        if(fecha < fecha_actual){
            fechaInvalida.valor = true;
            fechaInvalida.motivo = "fecha menor a la actual";
        }
        if(tipo_fecha == "fecha_desplazamiento" && (numDia == -1 || $scope.ofertaActividad.dias[numDia].seleccionado != true) ){
            fechaInvalida.valor = true;
            fechaInvalida.motivo = "el dia está fuera del esquema";
        }
        else if(fecha_inicio_actividad < fecha_actual && fecha_inicio_actividad != ""){
                fechaInvalida.valor = true;
                fechaInvalida.motivo = "fecha de inicio de la actividad menor a la fecha actual";
        }
        else if(tipo_fecha == "fecha_inicio"
                && (fecha_inicio_oferta > newDate
                    || fecha_fin_oferta < newDate
                    ||  fecha_fin_actividad < fecha_inicio_actividad) )
         {
            fechaInvalida.valor = true;
            fechaInvalida.motivo = "Fecha inicio fuera de rangos";
        }else if(tipo_fecha == "fecha_final"
                && (fecha_inicio_oferta > newDate
                    || newDate < fecha_inicio_actividad) )
         {
            fechaInvalida.valor = true;
            fechaInvalida.motivo = "Fecha fin invalida";
        }
        if(fechaInvalida.valor){
            if((tipo_fecha == "fecha_inicio"
               || tipo_fecha == "fecha_evento")
                    && $scope.ofertaActividad.evento_actividad == "evento"){
                $scope.ofertaActividad.evento[tipo_fecha] = oldDate;
            }else{
                $scope.ofertaActividad[tipo_fecha]  = oldDate;
            }
            toastr.error("Fecha invalida: "+fechaInvalida.motivo);
        }else{
            if($scope.ofertaActividad.evento_actividad == "evento"
                && (tipo_fecha == "fecha_inicio" || tipo_fecha == "fecha_evento")
                ){
                $scope.ofertaActividad.evento[tipo_fecha]  = fecha.getFullYear() + "-" + mes + "-" + dia;
            }
            else{
                $scope.ofertaActividad[tipo_fecha]  = fecha.getFullYear() + "-" + mes + "-" + dia;
            }
        }
    }



    /***************************************************************************
    * METODOS PARA CALENDARIZACION
    ****************************************************************************/



    /*
     * Función para  la actividad con todos sus eventos
     * Para Calendarizacion
     * @returns {undefined}
     */
        $scope.eliminarActividadCentro = function(){
            if($scope.ofertaActividad.obligatorio != true){                     //Valida si es una actividad obligatoria

            dialogConfirm("Eliminar actividad", "¿Está seguro que desea eliminar esta actividad?", function() {
                var m = loader.modal();
                ofertaCalendarioCentroService.restResource.delete(
                                {
                                "ocupacion_id" : $scope.ofertaActividad.oferta_actividad_id
                                , hostCentro : limpiarUri(centroFactory.centroActual.ip)
                                 }
                                ,function () {
                                    m.remove();
                                    toastr.success("Se ha eliminado correctamente la actividad", "Actividad calendarizada");
                                    $scope.ofertaActividad = {}
                                    //Carga las actividades en la pagina actual
                                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                                },
                                function (data) {
                                    m.remove();
                                    if(data.hasOwnProperty("data")){
                                        toastr.error(data.data.message);
                                    }else{
                                        toastr.error(ocurrioError + ": eliminar actividad, contacte a soporte.");
                                    }



                                }
                            );
                $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
            });
            }else{
                toastr.error("No se puede eliminar esta actividad, es obligatoria");
            }

    }


    /*
     * Función para cancelar la actividad con todos sus eventos
     * Para Calendarizacion
     * @returns {undefined}
     */
        $scope.cancelarActividadCentro = function(){

                dialogConfirm("Cancelar actividad", "¿Está seguro que desea cancelar esta actividad?", function() {
                var m = loader.modal();
                 ofertaCalendarioCentroService.restResource.cancelar(
                            {
                                "ocupacion_id" : $scope.ofertaActividad.oferta_actividad_id
                                , hostCentro : limpiarUri(centroFactory.centroActual.ip)
                            },
                            {},
                            function (data) {
                                m.remove();
                                toastr.success("Se ha cancelado correctamente la actividad", "Actividad calendarizada");
                                $scope.ofertaActividad = {};
                                //Carga las actividades en la pagina actual
                                $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                            },
                            function (data) {
                                m.remove();
                                if(data.hasOwnProperty("data")){
                                        toastr.error(data.data.message);
                                    }else{
                                        toastr.error(ocurrioError + ": cancelar actividad, contacte a soporte.");
                                    }
                            });


                    $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
            });
    }

    $scope.puedeCancelarYOEliminarActividad = function(tipo,funcionCallBack){
        var paramRest = {
                        "ocupacion_id" : $scope.ofertaActividad.oferta_actividad_id
                        , hostCentro : limpiarUri(centroFactory.centroActual.ip)
                        };

        if(tipo == "cancelar"){
            ofertaCalendarioCentroService.restResource.puedeCancelar(
               paramRest,
               {},
            function(data) {
                if (data.data.respuesta  <= 0) {
                    funcionCallBack(false);
                } else {
                    funcionCallBack(true);
                }
            },
            function(data) {
                if (data.data.status == true) {
                    console.log(data.data.message);
                } else {
                    toastr.error(ocurrioError + ": revisar validación de cancelación.");
                }
                funcionCallBack(false);
            });
        }else if(tipo == "eliminar"){
            ofertaCalendarioCentroService.restResource.puedeEliminar(
            paramRest,
            {}, function(data) {
                if (data.data.respuesta  <= 0) {
                    funcionCallBack(false);
                } else {
                    funcionCallBack(true);
                }
            },
            function(data) {
                if (data.data.status == true) {
                    console.log(data.data.message);
                } else {
                    toastr.error(ocurrioError + ": revisar validación de eliminación.");
                }
                funcionCallBack(false);
            });
        }
    }

    /**
    *
    * @ngdoc method
    * @name controlEscolar.eventoCtrl#agregarHeader
    * @methodOf controlEscolar.eventoCtrl
    * @param {string} name Nombre del header para agregar
    * @param {string} value Valor del header para agregar
    * @description
    * Función para agregar header rol
    */
    $scope.agregarHeaders = function(usuario){
        for(key in usuario){
            if(key != 'rol' ){
                $http.defaults.headers.post[key] = usuario[key];
                $http.defaults.headers.put[key] = usuario[key];
                $http.defaults.headers.patch[key] = usuario[key];
            }
        }
        if($scope.isCoordinador){
            $http.defaults.headers.post['Rol-Usuario'] = "ROLE_COORDINADOR_SUPERVISOR";
            $http.defaults.headers.put['Rol-Usuario'] = "ROLE_COORDINADOR_SUPERVISOR";
            $http.defaults.headers.patch['Rol-Usuario'] = "ROLE_COORDINADOR_SUPERVISOR";
        }
    }

    /**
    * @ngdoc method
    * @name controlEscolar.eventoCtrl#isUserCoordinador
    * @methodOf controlEscolar.eventoCtrl
    * @description
    * Función para verificar si el usuario actual es un coordinador
    */
    $scope.isUserCoordinador = function(rol){
        var roles = rol.split(',');
        if(roles.length > 1){
            for(r in roles){
                if (angular.equals(roles[r],"ROLE_COORDINADOR_SUPERVISOR")) {
                    $scope.isCoordinador = true;
                };
            }
        }else if(angular.equals(roles[0],"ROLE_COORDINADOR_SUPERVISOR")){
            $scope.isCoordinador = true;
        }
    }
    /*
     * Funcion para editar por put un solo evento, es utilizado cuando se arrastra un evento y al editar
     * Para Calendarizacion
     * @param {type} data  datos a enviar en el rest
     * @param {type} evento_id id de evento a modificar
     * @returns {undefined}
     */
    $scope.editarSoloPorEvento = function(data,evento_id,callback){
        var m = loader.modal();

        ofertaCalendarioCentroService.restResource.editEvent(
                {
                 "evento_id": evento_id
                 ,hostCentro: limpiarUri(centroFactory.centroActual.ip)
                }
                ,data,
                function () {
                    m.remove();
                    toastr.success("Se ha editado correctamente la sesión de la actividad", "Actividad");
                    $scope.ofertaActividad = {}
                    //Carga las actividades en la pagina actual
                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                    $('#idModalEventos').modal("hide");                         //Ocultamos el modal
                },
                function (result) {
                    m.remove();
                    if(typeof(callback) != "undefined"){
                        callback();
                    }
                    toastr.warning(result.data.message);
                });
    }
    /*
     * Funcion para editar por put todos los eventos siguientes a un evento dado
     * Para Calendarizacion
     * @param {type} data  datos a enviar en el rest
     * @param {type} evento_id id de evento a modificar
     * @returns {undefined}
     */
    $scope.editarBloquedeEventos = function(data,evento_id){
        var m = loader.modal();
        ofertaCalendarioCentroService.restResource.editEventBloque(
                {
                 "evento_id": evento_id
                 ,hostCentro: limpiarUri(centroFactory.centroActual.ip)
                }
                ,data,
                function () {
                    m.remove();
                    toastr.success("Se ha editado correctamente las sesiones", "Actividad");
                    $scope.ofertaActividad = {}
                    //Carga las actividades en la pagina actual
                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                    $('#idModalEventos').modal("hide");                         //Ocultamos el modal
                },
                function (result) {
                    m.remove();
                    toastr.warning(result.data.message);
                });

    }
    /**
     * Opción para agregar una nueva oferta educativa
     * Metodo de calendarizacion
     * COnsumimos el api rest  para agregarlo
     */
    $scope.calendarizarEvento = function(data) {

        var m = loader.modal();
        ofertaCalendarioCentroService.restResource.add(
                $scope.hostActividades
                ,data
                ,function () {
                    m.remove();
                    toastr.success("Se ha calendarizado correctamente el evento", "Evento calendarizado");
                    $scope.ofertaActividad = {}
                    //Carga las actividades en la pagina actual
                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                    $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
                },
                function (result) {
                    m.remove();
                    toastr.warning(result.data.message);

                }
        );
    }
        /**
     * Opción para agregar una nueva oferta educativa
     * Metodo de calendarizacion
     * COnsumimos el api rest  para agregarlo
     */
    $scope.enviarEdicionCalendario = function(data) {
        var m = loader.modal();
        ofertaCalendarioCentroService.restResource.edit(
             {
                ocupacion_id    : $scope.ofertaActividad.oferta_actividad_id
                ,hostCentro     : limpiarUri(centroFactory.centroActual.ip)
             }, data,
                function () {
                    m.remove();
                    toastr.success("Se ha calendarizado correctamente el evento", "Evento calendarizado");
                    $scope.ofertaActividad = {}
                    //Carga las actividades en la pagina actual
                    $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                    $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
                },
                function (result) {
                    m.remove();
                    toastr.warning(result.data.message);
                }
        );
    }

    /*
     * Detectando el cambio de tipo de edicion a evento o actividad
     * Uso en Calendarizacion
     * @returns {undefined}
     */
    $scope.tipoEventoActividad = function(){
        if($scope.ofertaActividad.evento_actividad  == "eventoYSiguientes"){
            eventoFactory.modificarGrupoEventos     = true;
            $scope.ofertaActividad.evento_actividad = "evento";
            $scope.currentTab = 'fechaDesplazamiento';
            $scope.ofertaActividad.fecha_desplazamiento = $scope.ofertaActividad.fechaEvento.format("YYYY-MM-DD");
        }else{
            eventoFactory.modificarGrupoEventos = false;
        }
        if($scope.ofertaActividad.evento_actividad == "evento"){
            $scope.ofertaActividad.evento.fecha_evento  = $scope.ofertaActividad.fechaEvento.format("YYYY-MM-DD");
            $scope.ofertaActividad.facilitador_id       = $scope.ofertaActividad.facilitador_id_evento;
            $scope.ofertaActividad.espacio_id           = $scope.ofertaActividad.espacio_id_evento;

            $scope.generarHorasFinValidasXEvento();
        }
        else if($scope.ofertaActividad.evento_actividad == "actividad"){
            $scope.ofertaActividad.facilitador_id   = $scope.ofertaActividad.facilitador_id_actividad;
            $scope.ofertaActividad.espacio_id       = $scope.ofertaActividad.espacio_id_actividad;
        }
        $scope.deshabilitarOcultarElementos();
    }



    /**
     * Funcion que agrega el obligatorio al estar calendarizado
     */
    $scope.colocarActividadObligatoria = function(){
        //La cuestion solo es para edicion de eventos
        if($scope.tipoEvento == "calendarizacion"){
            var colocarQuitar = $scope.ofertaActividad.obligatorio ? "quitar":"colocar";
            //Revisa si se coloca o no la obligatoriedad y si esta como obligatoria la fecha inicio)
            dialogConfirm("Obligatoria", "¿Está seguro que desea "
                                          + colocarQuitar
                                          + " como obligatoria esta actividad?", function() {
            //Se carga el modal para la carga de datos
            var m = loader.modal();
            //Swichea el obligatorio
            $scope.toogleObligatorio();


            var data = {
                        obligatorio : $scope.ofertaActividad.obligatorio,
                        control_acceso : $scope.ofertaActividad.control_acceso
                        };
            ofertaActividadCentroService.restResource.actualizar(
                    {
                    "oferta_actividad_centro_id": $scope.ofertaActividad.oferta_actividad_centro_id
                    ,hostCentro: limpiarUri(centroFactory.centroActual.ip)
                    }
                    ,data,
                    function () {
                        m.remove();
                        var colocarQuitar = $scope.ofertaActividad.obligatorio
                                            ? "colocado"
                                            : "quitado";
                        toastr.success("Se ha "
                                        + colocarQuitar
                                        + " como obligatoria la actividad", "Actividad");
                        $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                        $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
                    },
                    function (result) {
                        m.remove();
                        $scope.toogleObligatorio();
                        toastr.error(ocurrioError + ": editar actividad, <b>" + result.data.message+"</b>");
                    }
            );
        });
     }else{
         $scope.toogleObligatorio();
     }

    }



    /*
     * Inicializa el Modal Detalle
     * @returns {undefined}
     */
    $scope.reiniciarModalDetalle = function(evento){
            $scope.modalActivo          = "evento";
            eventoFactory.reiniciarEventoFactory();
            if($scope.tipoEvento == "calendarizacion"){
                $scope.currentTabDetalle    = "inscritos";
                if(evento.hasOwnProperty("ocupacion_id")){
                    $scope.ofertaActividad.ocupacion_id = evento.ocupacion_id;
                }else{
                    $scope.ofertaActividad.ocupacion_id = false;
                }
            }
    }

    /*
    * Funcion que muestra swichea entre los modales de evento y inscritos
     */
    $scope.cambiarModal = function(){
        $scope.modalActivo = $scope.modalActivo === 'evento' ? 'interesadosInscritos' : 'evento' ;
        if($scope.modalActivo == 'interesadosInscritos'){
            if($scope.ofertaActividad.ocupacion_id){
                $scope.llenarInteresadosInscritos();
            }else{
                toastr.warning("La actividad no está calendarizada");
            }
        }
    }
    /*
    * Funcion que muestra swichea entre los modales de evento y inscritos
     */
    $scope.llenarInteresadosInscritos = function(){
        var m = loader.modal();
        ofertaActividadCentroService.restResource.getInscritosInteresados(
                    {
                    "ocupacion_id": $scope.ofertaActividad.ocupacion_id
                    ,hostCentro: limpiarUri(centroFactory.centroActual.ip)
                    }
                    ,{},
                    function (data) {
                        m.remove();
                        $scope.ofertaActividad.inscritos = data.inscritos;
                        $scope.ofertaActividad.interesados = data.interesados;
                    },
                    function (result) {
                        m.remove();
                        toastr.error(ocurrioError + ": editar actividad, <b>" + result.data.message+"</b>");
                    }
            );
    }
    /*
     * Función que setea y quita el campo dado en el array de obligatoriedad
     * esto de acuerdo a si existe anteriormente o no
     * Si es un evento calendarizado se modificaen base directo
     * @param {type} campo Es el campo que se coloca o quita de obligatorios
     * @returns {Boolean}
     */
    $scope.agregarQuitarControlAcceso = function(campo){
        //Si se setea el evento en calendarización desde central
        //Verificar los permisos de click
        if($scope.tipoEvento == "calendarizacion" && $scope.tipoFormEvento != "nuevo" ){
            var colocarQuitar = $scope.esObligatorio(campo)
                                    ? "quitar"
                                    : "colocar";
            var advertencia   = $scope.esObligatorio(campo)
                                    ? "<br><b>Importante:</b> la actividad dejará de ser obligatoria"
                                    : "<br>"
                                      +"      <b>Implicaciones:</b>"
                                      +" <br>"
                                      +" <li>"
                                      +"     No se podrá modificar la fecha de inicio en el centro."
                                      +"  </li>"
                                      +"  <li>"
                                      +"     Se bloqueará el día correspondiente a la fecha inicio en Día Horario."
                                      +"  </li>"
                                      +"  <li>"
                                      +"     La actividad será obligatoria"
                                      +"  </li>";
            dialogConfirm("Obligatoria", "¿Está seguro que desea " + colocarQuitar
                           + " la fecha inicio fija? <br>"
                           +  advertencia, function() {

             //Mostrando el loader
            var m = loader.modal();
            //Se modifica el valor del acceso
            $scope.toogleControlAccesoObligatorio(campo);

            var data = {
                        control_acceso : $scope.ofertaActividad.control_acceso,
                        obligatorio:$scope.ofertaActividad.obligatorio
                        };
            ofertaActividadCentroService.restResource.actualizar(
                    {
                    "oferta_actividad_centro_id" : $scope.ofertaActividad.oferta_actividad_centro_id
                    ,hostCentro : limpiarUri(centroFactory.centroActual.ip)
                    }
                    ,data,
                    function () {
                        //ocultando el loader
                        m.remove();
                        //de acuerdo a la respuesta se modifica
                        var colocarQuitar = $scope.esObligatorio(campo)
                                                ? "colocado"
                                                : "quitado";
                        toastr.success("Se ha "
                                        + colocarQuitar
                                        + " la fecha fija y obligatoriedad de la actividad"
                                        , "Actividad");
                        $scope.funcionesOfertaEducativa.renderActividadesEnCalendario();
                        $('#idModalEventos').modal("hide");                                     //Ocultamos el modal
                    },
                    function (result) {
                        //ocultando el loader
                        m.remove();
                        $scope.toogleControlAccesoObligatorio('fecha_inicio');
                        toastr.error(ocurrioError + ": al modificar obligatoriedad de la fecha inicio , <b>" + result.data.message+"</b>");
                    }
            );
        });
        }else{
            $scope.toogleControlAccesoObligatorio('fecha_inicio');
        }
    }
    /*
     * Cambiar de valor del control de acceso obligatorio
     * @param {type} campo
     * @returns {Boolean}
     */
    $scope.toogleControlAccesoObligatorio = function(campo){

        //Encontramos el index del campo en el array de obligatorio
        var indexCampo = $scope.ofertaActividad.control_acceso.obligatorio.indexOf(campo);
        //Verifica si existe el campo en el array de obligatorio
        if(indexCampo > -1){
                $scope.ofertaActividad.obligatorio          = false;
                $scope.ofertaActividad.control_acceso.obligatorio.splice(indexCampo, 1);
            }else{
                //Si existe lo elimina del array de obligatorio
                //Si no existe lo agrega al array
                $scope.ofertaActividad.control_acceso.obligatorio.push(campo);
                $scope.ofertaActividad.obligatorio          = true;
            }
        }
    /**
     * Función que verifica si el campo dado de la oferta actividad es obligatorio
     * @param {String} campo Es el campo a checar en el array de obligatorios
     * @returns {boolean} Regresa el boleano que indica si existe o no el campo
     */
    $scope.esObligatorio = function(campo){
        if($scope.ofertaActividad.hasOwnProperty('control_acceso')){
            if($scope.ofertaActividad.control_acceso.hasOwnProperty('obligatorio')){
                return  $scope.ofertaActividad.control_acceso.obligatorio.indexOf(campo) > -1;
            }
        }
        return false;
    }



}
/* EOF */