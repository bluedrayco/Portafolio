/*
 * @author Jaime Hernández Cruz
 * @description Controlador que lleva las acciones del modal de eventos
 * 
 * @param {angular} $scope                 -- Scope de angular
 * @param {factory} ofertaEducativaFactory -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @returns {undefined}
 */
function datetimepickerCtrl(
        $scope
        ,ofertaEducativaFactory
        ) {
    $scope.selectDatePicker = function (newDate, oldDate) {
        var fecha = new Date(newDate);
        var mes   = ((fecha.getMonth() +1) <10)?"0" +(fecha.getMonth() +1): (fecha.getMonth() +1);
        var dia   = ((fecha.getDate()) <10)?"0" +(fecha.getDate()): (fecha.getDate());
        ofertaEducativaFactory.irAFechaCalendario(fecha.getFullYear() + "-" + mes + "-" + dia)
    }
}
/* EOF */