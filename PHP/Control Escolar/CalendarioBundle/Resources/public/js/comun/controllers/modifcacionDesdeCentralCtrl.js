/*
 * @author Jaime Hernández Cruz
 * @description Controlador para poder modificar cualquier cosa desde central,
 * se encarga de cambiar la IP de acuerdo a los centros disponibles
 *
 * @param {angular} $scope
 * @param {angular} $compile
 * @param {api} uiCalendarConfig
 * @param {factory} centroFactory       -- Factoria que contiene las funciones para la comunicacion con otros controladores
 * @param {service} ofertaEducativaService    -- Servicio que gestiona las ofertas educativas con el server
 * @returns {undefined}
 */
function modifcacionDesdeCentralCtrl(
        $scope,
        centroFactory,
        centroService,
        equivalenciaService
        ) {

    /*
     * Agregando las funciones para que cargue el inicio
     **/
    $scope.init = function(){
        centroService.restResource.get(
            {},
            function(data){
                if(data.length == 0){
                    toastr.error("No se encontraron centros");
                }else{
                    $scope.centrosRemotos  =  data;
                }
            },function(){
                toastr.error("No se encontraron centros");
            }
        );
    }

    /*
     * evaluando el cambio de fuentes
     */
    $scope.eligiendoCentroFuente = function(){
        if($scope.centroRemoto != null){
           var noSeEcontro = "No se encontro el centro";
           equivalenciaService.restResource.getUrlCentro.url;
            equivalenciaService.restResource.getUrlCentro(
                {centro_id : $scope.centroRemoto.centro_id},
                function(data){
                    if(data.length == 0){
                        toastr.error(noSeEcontro);
                    }else{

                        centroFactory.centroActual  =  data[0];
                        centroFactory.centroActual.id = $scope.centroRemoto.centro_id;
                        //Esta funcion puede ser sobre cargada de acuetrdo a la pantalla
                        // donde esta en uso
                        centroFactory.eligiendoCentroFuente();
                    }
                },function(){
                    toastr.error(noSeEcontro);
                }
            );
        }
    }

}
/* EOF */