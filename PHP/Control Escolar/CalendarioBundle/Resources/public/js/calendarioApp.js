'use strict';
//los siguientes modulos estan incluidos en los otros bundles de control Escolar
var app = angular.module('controlEscolar'
        ,['baseControlEscolar'
        ,'ui.calendar'      // Para la app de calendarizacion
        , 'ui.bootstrap'    // Para el Ui de la plantilla y pickcalendar
        ,'ngResource'       // Para los servicicios REST
        , 'ngSanitize'      //
        , 'smart-table'     //
        , 'ui.bootstrap.datetimepicker']);


angular.module('controlEscolar').factory('httpInterceptor', function ($q, $rootScope, $log) {
    var loadingCount = 0;

    return {
        request: function (config) {
            //toastr.warning("Cargando datos ....", "Plan Anual");
            if(++loadingCount === 1) $rootScope.$broadcast('loading:progress');
            return config || $q.when(config);

        },

        response: function (response) {
            if(--loadingCount === 0) $rootScope.$broadcast('loading:finish');
            return response || $q.when(response);
        },

        responseError: function (response) {
            if(--loadingCount === 0) $rootScope.$broadcast('loading:finish');
            return $q.reject(response);
        }
    };
}).config(function ($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
    var realEncodeURIComponent = window.encodeURIComponent;
    return {
      'request': function(config) {
         window.encodeURIComponent = function(input) {
           return realEncodeURIComponent(input).split("%2B").join("+");
         };
         return config || $q.when(config);
      },
      'response': function(config) {
         window.encodeURIComponent = realEncodeURIComponent;
         return config || $q.when(config);
      }
    };
  });

});

var ocurrioError = "Ocurrio un error al realizar la operación solicitada ";

app.factory('eventoFactory',function(centroFactory){
    return {
        editarEvento         : function (){},
        removerHeaders       : function (){},
        nuevoEvento          : function(){},
        eventoSeleccionado   : {},
        editarSoloPorEvento  : function(){},
        fechaEventoModificable : true,
        modificarGrupoEventos  : false,
        reiniciarEventoFactory : function(){
                                    this.modificarGrupoEventos  = false;
                                    this.fechaEventoModificable = true;
                                    },
        mensajes              : {
                                 fechaLimite : function(){
                                     return "No es posible cambiar la actividad a la fecha, dia y hora inicial y final porque debe ser "
                                                                                                + centroFactory.HorasMinimasActualizarSession
                                                                                                + "hrs antes de que inicie la actividad"
                                    }
                                },
        eventoModificable     : function(condicional,fechaEvento,fechaHoraSistema,HorasMinimasActualizarSession){
            this.fechaEventoModificable = true;
            if(condicional){
                var diferenciaMiliSegundos = fechaEvento
                                             - fechaHoraSistema;
                var diferenciaHoras = ((diferenciaMiliSegundos/1000 //MiliSegundos
                                                             )/60   //Segundos
                                                             )/60;   //Minutos
                    if(diferenciaHoras < 0
                       || diferenciaHoras <= HorasMinimasActualizarSession){
                        this.fechaEventoModificable = false;
                        return false;
                    }
            }
            return true;
        }
    }
})
/*
 * Factoria para el control de oferta educativa
 */
app.factory('ofertaEducativaFactory',function(centroFactory){
    return {
        renderActividadesEnCalendario   : function (){},
        escenarioOferta                 : {},
        ofertaEducativa                 : {},
        irAFechaCalendario              : function (){},
        checkMostrarTodosLosEventos     : false,
        mensajeAlerta                   : "",
        mensajes                        : {
                                            noEventoDiaLaboral : "No se pueden registrar sesiones en un <strong>día no laboral</strong>",
                                            noEventoMako       : "No es posible modificar el evento"
                                            },
        diasFeriados                    : [],
        ofertaSoloLectura               : function (){return false;},
        verificarFechaCreacion          : function (date,eventoFactory,timeZone,fechaSistema){
                                            //Verifica que no sea un dia no laboral
                                            if(this.diasFeriados.filter(function(obj){ return obj.start.toString().substr(0, 10) == date.format("YYYY-MM-D");} ).length > 0){
                                                toastr.warning(this.mensajes.noEventoDiaLaboral);
                                                return true;
                                            }

                                            var fechaSeleccionada   = date._d;


                                            var fechaActual             = new Date(fechaSistema.getFullYear(),
                                                                                   fechaSistema.getMonth(),
                                                                                   fechaSistema.getDate(),
                                                                                   fechaSistema.getHours() + parseInt(centroFactory.HorasMinimasActualizarSession),
                                                                                   fechaSistema.getMinutes(),
                                                                                   0,0);



                                            if(fechaSeleccionada < fechaActual){
                                                        toastr.warning("No es posible programar una actividad en esta fecha y hora.");
                                                        return true;
                                            }
                                            var tmp                     = this.ofertaEducativa.fecha_inicio.split("-");
                                            var ofertaFechaInicioDate = new Date(tmp[0], parseInt(tmp[1]) -1, tmp[2],0,0,0);

                                            tmp                     = this.ofertaEducativa.fecha_fin.split("-");
                                            var ofertaFechaFinalDate = new Date(tmp[0], parseInt(tmp[1]) -1, tmp[2],23,59,59);


                                            if(fechaSeleccionada >= ofertaFechaInicioDate && fechaSeleccionada <= ofertaFechaFinalDate){
                                                eventoFactory.nuevoEvento(date);

                                                $("#idModalEventos").modal("show");
                                            }else{
                                              toastr.warning("Fecha fuera de rango para el periodo de la programación CE de: <strong>" + this.ofertaEducativa.fecha_inicio + "</strong> al <strong>" + this.ofertaEducativa.fecha_fin + "</strong>");
                                            }
                                        },
        evaluaVista                     : function(view){
                                            var ofertaFechaInicioDate  = new Date(this.ofertaEducativa.fecha_inicio);
                                            var ofertaFechaFinDate     = new Date(this.ofertaEducativa.fecha_fin);
                                            if (view.end > ofertaFechaFinDate){
                                                  this.mensajeAlerta = "La programación CE \"" + this.ofertaEducativa.nombre + "\" finaliza el " + this.ofertaEducativa.fecha_fin ;
                                                  return false;
                                            }
                                            else {
                                                  this.mensajeAlerta = "";
                                            }

                                            if (view.start < ofertaFechaInicioDate) {

                                                this.mensajeAlerta = "La programación CE \"" + this.ofertaEducativa.nombre + "\" inicia el " + this.ofertaEducativa.fecha_inicio;
                                                return false;
                                            }
                                              else {
                                                  this.mensajeAlerta = "";
                                            }
                                        },
     evaluaDiasPublicarOfertaCentro     : function(){
                                            var ofertaFechaAltaDate      = '';
                                            if(this.ofertaEducativa.fecha_sincronizacion != null){
                                                var ofertaFechaAltaDate      = convertirFechaYMD(this.ofertaEducativa.fecha_sincronizacion.split("T")[0]);
                                            }
                                            var ultimoDiaPublicar        = ofertaFechaAltaDate;
                                            ultimoDiaPublicar.setDate(ultimoDiaPublicar.getDate()+ parseInt(centroFactory.diasPublicarOfertaCentro));
                                            var date1                    = convertirFechaYMD(centroFactory.fechaSistema);
                                            var date2                    = ultimoDiaPublicar;
                                            var timeDiff                 = Math.abs(date2.getTime() - date1.getTime());
                                            var diffDays                 = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                            var hoy                      = convertirFechaYMD(centroFactory.fechaSistema);

                                            this.claseAlertaDiasPublicar = "alert alert-warning warning-calendario";

                                            if (getSoloFecha(ofertaFechaAltaDate) == getSoloFecha(hoy) && this.ofertaEducativa.publicado == null){

                                              this.mensajeAlertaDiasPublicar = "Empieza a trabajar con el Calendario de actividades del periodo siguiente";

                                            }else{

                                                if (getSoloFecha(ultimoDiaPublicar) == getSoloFecha(hoy) && this.ofertaEducativa.publicado == null){

                                                  this.mensajeAlertaDiasPublicar = "Último día para que publiques el calendario";

                                                }else{

                                                  if (ultimoDiaPublicar > hoy && this.ofertaEducativa.publicado == null){

                                                    this.mensajeAlertaDiasPublicar = "Tienes " + diffDays + " días restantes para publicar el calendario";

                                                  }else{

                                                    if (ultimoDiaPublicar < hoy && this.ofertaEducativa.publicado == null){

                                                      this.mensajeAlertaDiasPublicar = "Esta Programación ya debio haber sido publicada";
                                                      this.claseAlertaDiasPublicar = "alert alert-danger warning-calendario";

                                                    }else{

                                                      this.mensajeAlertaDiasPublicar = "";

                                                    }


                                                  }
                                                }
                                            }

                                        },
      evaluaDiasPublicarOfertaCentral     : function(){


                                            var ofertaFechaInicioDate     = convertirFechaYMD(this.ofertaEducativa.fecha_inicio);
                                            var diasRestantes = convertirFechaYMD(this.ofertaEducativa.fecha_inicio);
                                            diasRestantes.setDate(diasRestantes.getDate() - parseInt(centroFactory.diasPublicarOfertaCentral));
                                            //diasRestantes = getSoloFecha(diasRestantes);
                                            var hoy = convertirFechaYMD(centroFactory.fechaSistema);
                                            var date1 = hoy;
                                            var date2 = diasRestantes;
                                            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                            this.claseAlertaDiasPublicar = "alert alert-warning warning-calendario";


                                            if (diasRestantes < hoy && this.ofertaEducativa.sincronizado == false){

                                              this.mensajeAlertaDiasPublicar = "Esta Programación ya debio haber sido publicada a los centros";
                                              this.claseAlertaDiasPublicar = "alert alert-danger warning-calendario";

                                            }else{

                                                if (getSoloFecha(diasRestantes) == getSoloFecha(hoy) && this.ofertaEducativa.sincronizado == false){

                                                  this.mensajeAlertaDiasPublicar = "Último día para que publiques la Programación a los centros";

                                                }else{

                                                  if (diasRestantes > hoy && this.ofertaEducativa.sincronizado == false){

                                                    this.mensajeAlertaDiasPublicar = "Quedan " + diffDays + " días restantes para publicar la programación";

                                                  }else{

                                                    this.mensajeAlertaDiasPublicar = "";

                                                  }

                                                }
                                            }

                                        },
            puedePublicarOferta         : function(){
                                            if (this.tipoProgramacionCE == 'extemporanea'){
                                                return false;
                                            }
                                            if(this.ofertaEducativa.hasOwnProperty("_oferta_educativa_estatus")){
                                                if(this.ofertaEducativa._oferta_educativa_estatus.nombre == "Publicado"){
                                                    return true;
                                                }else{
                                                    return false;
                                                }
                                            }
                                            return true;
                                        }
        }
});
//factory para el paginado de de reporte grupos y facilitadores
app.factory("paginadorFactory",function(){
  var tableState = {
      pagination:{
        start:0,
        numberOfPages:10,
        number:10},
      setInicio: function(start){
        tableState.start = start;
        return tableState;
      },
      setItems: function(itemsPorPagina){
        tableState.number = itemsPorPagina;
        return tableState;
      },
      setPaginas: function(paginas){
        tableState.numberOfPages = paginas;
        return tableState;
      }
  }
  return tableState;

  //var tablaEstatus = {
  //}
});
//Factory para la inyeccion de un facilitador , id nombre,grupo
//y funciones
app.factory("facilitadorFuncionesFactory",function(){
  return{
    contarPaginasFacilitador : function(){},
    pipeReporteFacilitador : function(){},
    getReporteFacilitador : function(){},
    facilitador : {
      id:null,
      nombre:null,
      grupo:null
    }
  }
})
/*
 * Factoria para el control de escenarios en oferta educativa
 */
app.factory('escenarioFactory',function(){
    return {
        cargaEscenarios   : function (){},
        diasNoLaborablesPorFecha : function (){}
    }
})

/*
 * Factoria para el control de los filtros en calendarizacion y oferta educativa
 */
app.factory('filtrosFactory',function(){
    return {
        cargaFiltroInicialCalendario   : function (){},
        filtroInicial                  : "",
        aula                           : 0,
        facilitador                    : 0
    }
})

/*
 * Factoria para las estadisticas de calendarizacion
 */
app.factory('estadisticaFactory',function(){
    return {
        actividades_obligatorias_faltantes               : 0,
        total_actividades_calendarizadas                 : 0,
        total_actividades_oferta_educativa               : 0
    }
})

/*
 * Factoria para las estadisticas de calendarizacion y carga de parametros
 */
var ruta = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
app.factory('centroFactory',function(centroParametrosService){
    return {
        mostrarCalendario           : false,
        centroActual                : {
                                        tipo    : 'local',
                                        ip      : ruta
                                      },
         horarioCentro               : {
                                     apertura : {},
                                    cierre   : {}
                                     },
         HorasMinimasActualizarSession : {},
         eligiendoCentroFuente           : function(){},//La funcion es sobre cargada dependiendo del centro elegido
        cargaInicialCalendarizacion : function(){},
        cargaInicialFiltros         : function(){},
        cargaInicialEvento          : function(){},
        datosInformativos:{
            categoriasActividadNoAcademica : [],
            categoriasActividadAcademica : []
        },
        cargaInicialParametros      : function cargaInicialParametros(data){
                                       this.horarioCentro.apertura =  parseInt(data.filter(function(parametro){return (parametro.nombre == "hora_apertura_centro")})[0].valor);
                                       this.horarioCentro.cierre =  parseInt(data.filter(function(parametro){return (parametro.nombre == "hora_cierre_centro")})[0].valor);
                                       this.HorasMinimasActualizarSession =  data.filter(function(parametro){return (parametro.nombre == "numero_horas_minimas_recorrido")})[0].valor;
                                       this.diasPublicarOfertaCentro = data.filter(function(parametro){return (parametro.nombre == "dias_publicar_oferta_centro")})[0].valor;
                                       this.diasPublicarOfertaCentral = data.filter(function(parametro){return (parametro.nombre == "dias_publicar_oferta_central")})[0].valor;
                                        }
    }
})

/*
 * Función que recibe una valor de tipo date y regresa unicamente la fecha
 * en formato YYYY-MM-DD
 * @param {date} valor
 * @returns {String}
 */
function getSoloFecha(valor){
      var mesFinal              =  ((valor.getMonth()  + 1) <10) ? ("0"+(valor.getMonth()  + 1)): (valor.getMonth()  + 1);
      var diaFinal              =  ( valor.getDate() <10 )       ? ("0" + valor.getDate())      : valor.getDate();
      var fin                   =  valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;
      return fin;
    }

/**
 * Función que convierte fechas de formato YYYY-mm-dd
 * a date (para no tener prblemas en firefox)
 * @param {fecha} es un string con esta estrucura YYY-mm-dd
 * @return {Date} regresa una variable de tipo date
 */
function convertirFechaYMD(fecha){
    var tmp                     = fecha.split("-");
    return new Date(tmp[0], parseInt(tmp[1]) -1, tmp[2],0,0,0);

}


/**
 * Modal/Popup Draggable
 * @description Se añade funcionalidad
 */
$(function(){
    $("#idModalEventos").draggable({
        keyboard: false,
        show: false,
        backdrop: true
    });
});



(function(ng) {
/*
  @description Directiva para aplicar filtros de select en smartable
  @author: Isaed Borrego, Jaime Hernández
  @date: Septiembre 14 2015
*/
app.directive('stSelectDistinct', [function() {
      return {
        restrict: 'E',
        require: '^stTable',
        scope: {
          collection: '=',
          predicate: '@',
          predicateExpression: '='
        },
        template: '<select ng-model="selectedOption" ng-change="optionChanged(selectedOption)" ng-options="opt for opt in distinctItems"></select>',
        link: function(scope, element, attr, table) {
          var getPredicate = function() {
            var predicate = scope.predicate;
            if (!predicate && scope.predicateExpression) {
              predicate = scope.predicateExpression;
            }
            return predicate;
          }

          scope.$watch('collection', function(newValue) {
            var predicate = getPredicate();

            if (newValue) {
              var temp = [];
              scope.distinctItems = ['Todos'];

              angular.forEach(scope.collection, function(item) {
                var value = item[predicate];

                if (value && value.trim().length > 0 && temp.indexOf(value) === -1) {
                  temp.push(value);
                }
              });
              temp.sort();

              scope.distinctItems = scope.distinctItems.concat(temp);
              scope.selectedOption = scope.distinctItems[0];
              scope.optionChanged(scope.selectedOption);
            }
          }, true);

          scope.optionChanged = function(selectedOption) {
            var predicate = getPredicate();

            var query = {};

            query.distinct = selectedOption;

            if (query.distinct === 'Todos') {
              query.distinct = '';
            }

            table.search(query, predicate);
          };
        }
      }
    }])
/*
  @description Directiva para aplicar filtros en smartable
  @author: Isaed Borrego, Jaime Hernández
  @date: Septiembre 14 2015
*/

app.filter('customFilter', ['$filter', function($filter) {
      var filterFilter = $filter('filter');
      var standardComparator = function standardComparator(obj, text) {
        text = ('' + text).toLowerCase();
        return ('' + obj).toLowerCase().indexOf(text) > -1;
      };

      return function customFilter(array, expression) {
        function customComparator(actual, expected) {

          var isBeforeActivated = expected.before;
          var isAfterActivated = expected.after;
          var isLower = expected.lower;
          var isHigher = expected.higher;
          var higherLimit;
          var lowerLimit;
          var itemDate;
          var queryDate;

          if (ng.isObject(expected)) {




            //exact match
            if (expected.distinct) {
              if (!actual || actual.toLowerCase() !== expected.distinct.toLowerCase()) {
                return false;
              }

              return true;
            }

            //matchAny
            if (expected.matchAny) {
              if (expected.matchAny.all) {
                return true;
              }

              if (!actual) {
                return false;
              }

              for (var i = 0; i < expected.matchAny.items.length; i++) {
                if (actual.toLowerCase() === expected.matchAny.items[i].toLowerCase()) {
                  return true;
                }
              }

              return false;
            }

            //date range
            if (expected.before || expected.after) {
              try {
                if (isBeforeActivated) {
                  higherLimit = expected.before;

                  itemDate = new Date(actual);
                  queryDate = new Date(higherLimit);

                  if (itemDate > queryDate) {
                    return false;
                  }
                }

                if (isAfterActivated) {
                  lowerLimit = expected.after;


                  itemDate = new Date(actual);
                  queryDate = new Date(lowerLimit);

                  if (itemDate < queryDate) {
                    return false;
                  }
                }

                return true;
              } catch (e) {
                return false;
              }

            } else if (isLower || isHigher) {
              //number range
              if (isLower) {
                higherLimit = expected.lower;

                if (actual > higherLimit) {
                  return false;
                }
              }

              if (isHigher) {
                lowerLimit = expected.higher;
                if (actual < lowerLimit) {
                  return false;
                }
              }

              return true;
            }
            //etc

            return true;

          }
          return standardComparator(actual, expected);
        }

        var output = filterFilter(array, expression, customComparator);
        return output;
      };
    }]);

})(angular);

angular.module("smart-table").directive('onFilter', function () {
    return {
        require: '^stTable',
        scope: {
            onFilter: '='
        },
        link: function (scope, element, attr, ctrl) {

            scope.$watch(function () {
                scope.onFilter(ctrl);
                return ctrl.tableState().search;
            }, function (newValue, oldValue) {
                scope.onFilter(ctrl);
            }, true);
        }
    };
  });