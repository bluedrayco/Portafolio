
function indexOfertaEducativaCtrl(
        $scope
        ,ofertaEducativaService
        ,$filter
        ) {
    $scope.ofertasEducativas                    = [];
    $scope.permitirCrearOferta                  = false;
    $scope.ofertaEducativa                      = {};
    $scope.parametrosOfertaEducativa            = {};
    $scope.tmpOfertaEducativa                   = {};
    $scope.tipoAccion                           = 1;
    $scope.fechaInicial                         = null;
    $scope.modoEdicionOfertaEducativa = false;

    $scope.nueva                                = function(){
        $scope.eventoSeleccionado.evento_id     = 9;
        $('#idModalEventos').modal("hide");
        $scope.eventoSeleccionado               = {}
    }

    $scope.init                         = function() {
        $scope.list();
    }


    $scope.editaOfertaEducativa         = function(ofertaEducativa){

        $scope.modoEdicionOfertaEducativa = true;
        $scope.ofertaEducativa                  = ofertaEducativa;
        $scope.ofertaEducativa.fecha_inicio     = $filter('date')($scope.ofertaEducativa.fecha_inicio, "yyyy-MM-dd");
        $scope.ofertaEducativa.fecha_fin     = $filter('date')($scope.ofertaEducativa.fecha_fin, "yyyy-MM-dd");

        $scope.tmpOfertaEducativa               = angular.copy(ofertaEducativa);
        $scope.tipoAccion                       = 2;

    }

    $scope.cancelarActualizacion        = function(){

      $scope.modoEdicionOfertaEducativa = false;
        if($scope.ofertasEducativas.indexOf($scope.ofertaEducativa) >=0){
        $scope.ofertasEducativas[$scope.ofertasEducativas.indexOf($scope.ofertaEducativa)] = $scope.tmpOfertaEducativa;
      }
      $scope.inicializa();
      $scope.calculaFechaInicial();
    }

    $scope.inicializa                   = function(){
        $scope.ofertaEducativa          = {};
        $scope.tipoAccion               = 1;
        $scope.formOfertaEducativa.$setPristine();
    }

    $scope.update                       = function(){

        var m                           = loader.modal();
        var data                        = {};
        data.fecha_inicio               = $scope.ofertaEducativa.fecha_inicio;
        data.fecha_final                =  $scope.ofertaEducativa.fecha_fin;
        data.nombre                     = $scope.ofertaEducativa.nombre;

      if ($scope.ofertaEducativa.fecha_fin > $scope.ofertaEducativa.fecha_inicio){

            ofertaEducativaService.restResource.update(
                  {"oferta_educativa_id": $scope.ofertaEducativa.oferta_educativa_id},data,
                  function(result){
                     m.remove();
                     toastr.success("Se ha modificado la programación CE correctamente", "Programación CE");
                     $scope.inicializa();
                     $scope.calculaFechaInicial();
                  },
                  function(){
                      m.remove();
                    toastr.error(ocurrioError+": modificar programación CE.");
                  }
            );

        }else{

          m.remove();
          toastr.error("No se puede modificar la programación CE. La fecha de inicio es mayor a la fecha fin");

        }
    }

    $scope.remove                       = function(ofertaEducativa){

        dialogConfirm("Eliminar programación CE", "¿Está seguro de eliminar esta Programación CE?", function() {
            var m                       = loader.modal();
            ofertaEducativaService.restResource.remove(
                {"oferta_educativa_id": ofertaEducativa.oferta_educativa_id},{},
                function(result){
                   m.remove();
                   $scope.ofertasEducativas.splice($scope.ofertasEducativas.indexOf(ofertaEducativa), 1);
                   toastr.success("Se ha eliminado la programación CE Adecuadamente", "Programación CE");
                   $scope.inicializa();
                    $scope.calculaFechaInicial();

                },
                function(){
                    m.remove();
                  toastr.error(ocurrioError+": eliminar programación CE.");
                }
            );
        });
    }

    /**
     * Opción para listar las ofertas educativas
     * Cunsumimos el api rest para obtenerlos.
     */
    $scope.list = function() {


        ofertaEducativaService.restResource.get(
                        {},
                        function(result){
                           $scope.ofertasEducativas = result;
                           $scope.ofertasEducativas2 = [].concat($scope.ofertasEducativas);
                           $scope.permitirCrearOferta = true;
                           $scope.calculaFechaInicial();
                        },
                        function(){
                          toastr.error(ocurrioError+": traer programación CE.");

                        }
                    );
    }


    $scope.calculaFechaInicial    = function(){
      var fechaActual         = null;
      var tmp                 = null;
      $scope.fechaInicial = new Date();
      for (var a =  0; a < $scope.ofertasEducativas.length; a++){
        fechaActual   = $scope.getFechaObjectFromString($scope.ofertasEducativas[a].fecha_fin);
        if(a == 0){
          $scope.fechaInicial = fechaActual;
        }
        if(fechaActual > $scope.fechaInicial){
          $scope.fechaInicial = fechaActual;
        }

      }
      //agregamos 1 dia para que  sea el dia en que realmente se puede empezar la oferta educativa
      //$scope.fechaInicial = $scope.fechaInicial.addDays(1);
     $scope.fechaInicial.setDate($scope.fechaInicial.getDate() +  1);
     $scope.ofertaEducativa.fecha_inicio  = $scope.getSoloFecha($scope.fechaInicial);

    }

    $scope.getFechaObjectFromString = function(fecha){
      var tmpfecha = fecha.replace(/[T]+(.)+/g, "");
      var tmp      = tmpfecha.split("-");
      var fecha    = new Date(tmp[0],(parseInt(tmp[1])-1), tmp[2]);
      return fecha;

    }



    /**
     * Opción para agregar una nueva oferta educativa
     * COnsumimos el api rest  para agregarlo
     */
    $scope.add = function() {
        var m = loader.modal();
        var data = {
                    "nombre"        : $scope.ofertaEducativa.nombre,
                    "fecha_inicio"   : $scope.ofertaEducativa.fecha_inicio,
                    "fecha_final"   : $scope.ofertaEducativa.fecha_fin,

          };

             ofertaEducativaService.restResource.add(
                        data,
                        function(result){
                           m.remove();
                           toastr.success("Se ha agregado correctamente la programación CE", "Programación CE");
                           $scope.ofertasEducativas.unshift (result);
                           $scope.ofertasEducativas2 = [];
                           $scope.ofertasEducativas2 = [].concat($scope.ofertasEducativas);
                           $scope.ofertaEducativa    = {};
                           $scope.formOfertaEducativa.$setPristine();
                            toastr.success("Se ha agregado correctamente la programación CE", "Programación CE");
                            $scope.inicializa();
                             
                             $scope.calculaFechaInicial();
                        },
                        function(){
                            m.remove();
                          toastr.error(ocurrioError+": Agregar programación CE.");
                        }
                    );
    }

    $scope.selectDatePicker = function (newDate, oldDate,tipo_fecha) {

        var fecha = new Date(newDate);
        var mes   = ((fecha.getMonth() +1) <10)?"0" +(fecha.getMonth() +1): (fecha.getMonth() +1);
        var dia   = ((fecha.getDate()) <10)?"0" +(fecha.getDate()): (fecha.getDate());
        var fecha_actual = new Date();
        if(fecha < fecha_actual){
            $scope.ofertaEducativa.fecha_inicio = "";
            toastr.error("Fecha invalida");
            
        }else{
            if(newDate < $scope.fechaInicial){
                $scope.ofertaEducativa.fecha_inicio = "";
                toastr.warning("Tu fecha de Inicio no puede ser menor a " + $scope.getSoloFecha($scope.fechaInicial));
            }
            else{
                $scope.ofertaEducativa.fecha_inicio  = fecha.getFullYear() + "-" + mes + "-" + dia;
            }
        }
    }

   $scope.selectDatePickerCalendarioInicio = function (newDate, oldDate,tipo_fecha) {

        var fecha         = new Date(newDate);
        var mes           = ((fecha.getMonth() +1) <10)?"0" +(fecha.getMonth() +1): (fecha.getMonth() +1);
        var dia           = ((fecha.getDate()) <10)?"0" +(fecha.getDate()): (fecha.getDate());
        var fecha_actual  = $scope.getSoloFecha(new Date());
        var fecha_enviada = $scope.getSoloFecha(newDate);
        var fecha_limite  =  $scope.getSoloFecha($scope.fechaInicial);
        var fecha_fin_actual = $scope.ofertaEducativa.fecha_fin;

        

        if(fecha_enviada < fecha_actual){
            $scope.ofertaEducativa.fecha_inicio = oldDate;
            toastr.error("Fecha invalida");
            
        }else{
            if(fecha_enviada > fecha_fin_actual){
                $scope.ofertaEducativa.fecha_inicio = oldDate;
                toastr.warning("Tu fecha de Inicio no puede ser mayor a " + fecha_fin_actual + " ya que seleccionaste esta fecha como fecha de fin");
            }else{
            if(fecha_enviada < fecha_limite && $scope.modoEdicionOfertaEducativa == false){
            //if(fecha_enviada < fecha_limite){
                  $scope.ofertaEducativa.fecha_inicio = oldDate;
                  toastr.warning("Tu fecha de Inicio no puede ser menor a " + fecha_limite);
              }else{
                  $scope.ofertaEducativa.fecha_inicio  = fecha.getFullYear() + "-" + mes + "-" + dia;
              }
            }
        }
         
    }

   $scope.selectDatePickerCalendarioFin = function (newDate, oldDate,tipo_fecha) {

        var fecha = new Date(newDate);
        var mes   = ((fecha.getMonth() +1) <10)?"0" +(fecha.getMonth() +1): (fecha.getMonth() +1);
        var dia   = ((fecha.getDate()) <10)?"0" +(fecha.getDate()): (fecha.getDate());
        var fecha_actual = new Date();
        var fecha_actual  = $scope.getSoloFecha(new Date());
        var fecha_enviada = $scope.getSoloFecha(newDate);
        var fecha_limite  =  $scope.getSoloFecha($scope.fechaInicial);
        var fecha_inicio_actual = $scope.ofertaEducativa.fecha_inicio;
        

        if(fecha_enviada < fecha_actual){
            $scope.ofertaEducativa.fecha_fin = oldDate;
            toastr.error("Fecha invalida");
        }else{
          if(fecha_enviada < fecha_inicio_actual){
                $scope.ofertaEducativa.fecha_fin = oldDate;
                toastr.warning("Tu fecha de Fin no puede ser menor a " + fecha_inicio_actual + " ya que seleccionaste esta fecha como fecha de inicio");
          }else{
             if(fecha_enviada < fecha_limite && $scope.modoEdicionOfertaEducativa == false){
             //if(fecha_enviada < fecha_limite){
                  $scope.ofertaEducativa.fecha_fin = oldDate;
                  toastr.warning("Tu fecha de Fin no puede ser menor a " + fecha_limite);
              }else{
                  $scope.ofertaEducativa.fecha_fin  = fecha.getFullYear() + "-" + mes + "-" + dia;
              }
          }
        }
         
    }

     $scope.getSoloFecha = function(valor){
      var mesFinal              =  ((valor.getMonth()  + 1) <10) ? ("0"+(valor.getMonth()  + 1)): (valor.getMonth()  + 1);
      var diaFinal              =  ( valor.getDate() <10 )       ? ("0" + valor.getDate())      : valor.getDate();
      var fin                   =  valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;
      return fin;
    }


}
/* EOF */