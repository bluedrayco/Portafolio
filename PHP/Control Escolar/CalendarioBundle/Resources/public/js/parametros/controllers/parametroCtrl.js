
function parametroCtrl(
            $scope
            ,$filter
            ,parametroService
            ,centroFactory
             ) {

    $scope.diasFeriados              = [];
    $scope.tipoAccion                = 1;
    $scope.parametroEdicion          = {};
    $scope.tmpDiaFeriado             = {};

    /**
     * Funcion inicial
     * @return void
     */
    $scope.init = function(ambiente,parametroDesdeCentral) {
        $scope.corporativo             = (ambiente =="central") ? true: false;
        $scope.parametroDesdeCentral   = parametroDesdeCentral;
        if(!$scope.parametroDesdeCentral)
            $scope.list();
        centroFactory.eligiendoCentroFuente = $scope.eligiendoCentroFuente;
    }



    $scope.editaParametro  = function(dia){
      $scope.tmpDiaFeriado  = angular.copy(dia);
      $scope.diaFeriado     = dia;
      $scope.tipoAccion     = 2;
    }

/* Funcion para editar un parametro */

    $scope.editarParametro = function(parametro, localidad) {

        var localidad = localidad;
        $scope.tipoModal = "editar";

        parametroService.restResource.getParametro(
            {
                parametro_id    : parametro,
                hostCentro      : limpiarUri(centroFactory.centroActual.ip)
             },
            function(data){
                  $scope.parametroEdicion = data;
                  $scope.tituloModal = "Editar Parametro " + $scope.parametroEdicion.nombre;
                  if (localidad = "central"){$('#idModalEdicionParametro').modal('show');}
                  if (localidad = "local"){$('#idModalEdicionParametroCentro').modal('show');}
            },function() {
                  toastr.error(ocurrioError + ': No existe el parámetro --> ' + parametro);
            }
        );


    }

    /* Funcion para llamar al modal de agregar nuevo parametro */

     $scope.agregarParametro = function(parametro) {

        $scope.tipoModal = "agregar";
        $scope.parametroEdicion                  = {};
        $scope.parametroEdicion.activo           = false;
        $scope.parametroEdicion.puede_publicar   = false;
        $scope.tituloModal = "Agregar Nuevo Parámetro";
        $('#idModalEdicionParametro').modal('show');


    }


    /**
     * Opción para listar los parametros
     * Cunsumimos el api rest para obtenerlos.
     */
    $scope.list = function() {


        parametroService.restResource.get(
                        {
                            hostCentro      : limpiarUri(centroFactory.centroActual.ip)
                        },
                        function(result){
                           $scope.parametros = result;
                           $scope.array_parametros = [].concat($scope.parametros);

                        },
                        function(){
                          toastr.error(ocurrioError+": Obtener la lista de parámetros");
                        }
                    );
    }

    /**
     * Opción para agregar una nuevo parametro
     *
     */
    $scope.addParametro = function() {
      var m                             = loader.modal();
      parametroService.restResource.add(
          {
              hostCentro      : limpiarUri(centroFactory.centroActual.ip)
          },
          $scope.parametroEdicion,
          function(result){
             m.remove();
             toastr.success("Se ha agregado el parámetro exitosamente", "Parametrización");
             $('#idModalEdicionParametro').modal('hide');
             $scope.list();

          },
          function(){
              m.remove();
            toastr.error(ocurrioError+": Agregar parámetro.");
          }
      );
    }

    /*
     * Función que sirve en calendarizacion desde central y ejecuta las accioens cuando es llamada}
     * Se llama a list en este caso
     * @returns {undefined}
     */
    $scope.eligiendoCentroFuente = function(){
        $scope.parametros = [];
        $scope.list();
    }


    /**
     * Actualizar un parametro
     * @return void
     */
    $scope.updateParametro = function(){
      var m                             = loader.modal();

      parametroService.restResource.update(
            {
                parametro_id    : $scope.parametroEdicion.parametro_id,
                hostCentro      : limpiarUri(centroFactory.centroActual.ip)
            },
            $scope.parametroEdicion
            ,
          function(result){
             m.remove();
             toastr.success("Se ha modificado el parámetro", "Parametrizacion");
             $('#idModalEdicionParametro').modal('hide');
             $('#idModalEdicionParametroCentro').modal('hide');

             $scope.list();
          },
          function(){
              m.remove();
            toastr.error(ocurrioError+": modificar parámetro.");
          }
      );

    }

     $scope.CambiarIconoActivo = function(){

        $scope.parametroEdicion.activo = $scope.parametroEdicion.activo === false ? true: false ;

    }

     $scope.CambiarIconoPuedePublicar = function(){

        $scope.parametroEdicion.puede_publicar = $scope.parametroEdicion.puede_publicar === false ? true: false ;

    }

    $scope.CambiarIconoModificaCentro = function(){
        $scope.parametroEdicion.modifica_centro = $scope.parametroEdicion.modifica_centro === false ? true: false ;

    }

/* Funcion para eliminar un parametro */

    $scope.eliminarParametro =  function(parametro){
      dialogConfirm("Eliminar parámetro", "¿Confirmas la eliminación de este parámetro?", function() {
        var m                             = loader.modal();
        parametroService.restResource.remove(
            {
                parametro_id    : parametro,
                hostCentro      : limpiarUri(centroFactory.centroActual.ip)
            },{},
            function(result){
               m.remove();
               toastr.success("Se ha eliminado el parámetro exitosamente", "Parametrización");
               $scope.list();

            },
            function(){
                m.remove();
              toastr.error(ocurrioError+": eliminar parámetro.");
            }
        );
      });

    }

    $scope.publicarParametros = function(){

      var m = loader.modal();

      parametroService.restResource.update(
            {
                hostCentro      : limpiarUri(centroFactory.centroActual.ip)
            },
            $scope.parametroEdicion
            ,
          function(result){
             m.remove();
             toastr.success("Se ha modificado el parámetro", "Parametrizacion");
             $('#idModalEdicionParametro').modal('hide');
             $('#idModalEdicionParametroCentro').modal('hide');

             $scope.list();
          },
          function(){
              m.remove();
            toastr.error(ocurrioError+": modificar parámetro.");
          }
      );

    }

}
/* EOF */