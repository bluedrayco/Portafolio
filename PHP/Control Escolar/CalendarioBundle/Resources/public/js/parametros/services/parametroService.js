/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('parametroService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/parametros', { },{
        get:{
            url: dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/parametros",
            method: 'GET',
            isArray:true,
            transformResponse : function(data, headerrs){
                result = angular.fromJson(data).data;
                return result.parametros;
            }
        },
        getParametro:{
            method : 'GET',
            url : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/parametros/:parametro_id",
            isArray : false,
            transformResponse : function(data, headerrs){
                var result = [];
                result = angular.fromJson(data).data;
                return result.parametro;
            }
        },
        add: {                                                                                         
            method : 'POST',
            url : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/parametros",
            transformResponse : function(data, headerrs){
                var result = [];
                result = angular.fromJson(data).data;
                return result.parametros;
            }
        },
        update: {
            method : 'PUT',
            url : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/parametros/:parametro_id",
            isArray : true,
            transformResponse : function(data, headerrs){
                var result = [];
                result = angular.fromJson(data).data;
                return result.parametros;
            }
        },
        remove: {
            method : 'DELETE',
            url : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/parametros/:parametro_id",
            transformResponse : function(data, headerrs){
                var result = [];
                result = angular.fromJson(data).data;
                return result.parametros;
            }
        },
        /**
        * Función que hace el update y envío de parametros a centros
        **/
        publicarParametros: {
            method : 'PUT',
            url : dinamicUri(":hostCentro")+"/calendarizacion/api/entity/calendario/enviaparametrosacentros",
            isArray : false,
            transformResponse : function(data, headerrs){
                var result  = [];
                result      = angular.fromJson(data);
                return result.data;
            }
        }   
    });
});