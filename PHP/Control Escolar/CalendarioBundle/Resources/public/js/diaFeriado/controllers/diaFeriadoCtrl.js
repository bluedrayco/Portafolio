
function diaFeriadoCtrl($scope,$filter,diaFeriadoService) {

    $scope.diasFeriados              = [];
    $scope.tipoAccion                = 1;
    $scope.corporativo               = ($("#ambiente").attr("value")=="central")?true: false;
    $scope.tmpDiaFeriado             = {};



    /**
     * Funcion inicial
     * @return void
     */
    $scope.init = function() {
        $scope.list();
    }



    $scope.editaDiaFeriado  = function(dia){
      $scope.tmpDiaFeriado  = angular.copy(dia);
      $scope.diaFeriado     = dia;
      $scope.tipoAccion     = 2;
    }


    /**
     * Opción para listar las ofertas educativas
     * Cunsumimos el api rest para obtenerlos.
     */
    $scope.list = function() {


        diaFeriadoService.restResource.get(
                        {},
                        function(result){
                           $scope.diasFeriados = result;
                           $scope.diasFeriados2 = result;

                        },
                        function(){
                          toastr.error(ocurrioError+": Obtener la lista de  días no laborables");
                        }
                    );
    }

    /**
     * Opción para agregar una nueva oferta educativa
     * COnsumimos el api rest  para agregarlo
     */
    $scope.add = function() {
      var m                             = loader.modal();
      $scope.diaFeriado.puede_publicar  = true;
      $scope.diaFeriado.corporativo     = $scope.corporativo;

      diaFeriadoService.restResource.add(
          $scope.diaFeriado,
          function(result){
             m.remove();
             $scope.diasFeriados.unshift(result);
             toastr.success("Se ha agregado día no laborable correctamente", "Día no laborable");
             $scope.iniciaElementos();
          },
          function(){
              m.remove();
            toastr.error(ocurrioError+": Agregar oferta educativa.");
          }
      );
    }


    /**
     * Actualizamos un elemento de dia feriado
     * @return void
     */
    $scope.update = function(){
      var m                             = loader.modal();
      $scope.diaFeriado.puede_publicar  = true;
      $scope.diaFeriado.corporativo     = $scope.corporativo;

      diaFeriadoService.restResource.update(
          {"dia_feriado_id": $scope.diaFeriado.dia_feriado_id},$scope.diaFeriado,
          function(result){
             m.remove();
             toastr.success("Se ha modificado día no laborable correctamente", "Día no laborable");
             $scope.iniciaElementos();

          },
          function(){
              m.remove();
            toastr.error(ocurrioError+": modificar día feriado.");
          }
      );

    }

    /**
     * Inicializamos el catalog de dias feriados
     * @return void
     */
    $scope.inicializa = function(){

      if($scope.diasFeriados.indexOf($scope.diaFeriado) >=0){
        $scope.diasFeriados[$scope.diasFeriados.indexOf($scope.diaFeriado)] = $scope.tmpDiaFeriado;
      }

    }

    $scope.iniciaElementos = function(){
        $scope.tipoAccion                = 1;
        $scope.diaFeriado                = {};
        $scope.formDiaFeriado.$setPristine();

    }


    $scope.remove =  function(diaFeriado){
      dialogConfirm("Eliminar día no laborable", "¿Confirmas la eliminación de este día no laborable?", function() {
        var m                             = loader.modal();
        diaFeriadoService.restResource.remove(
            {"dia_feriado_id": diaFeriado.dia_feriado_id},$scope.diaFeriado,
            function(result){
               m.remove();
               $scope.diasFeriados.splice($scope.diasFeriados.indexOf(diaFeriado), 1);
               toastr.success("Se ha eliminado día no laborable correctamente", "Día no laborable");
               $scope.iniciaElementos();

            },
            function(){
                m.remove();
              toastr.error(ocurrioError+": eliminar día feriado.");
            }
        );
      });

    }


    $scope.selectDatePicker = function (newDate, oldDate,tipo_fecha) {

        var fecha = new Date(newDate);

        var mes   = ((fecha.getMonth() +1) <10)?"0" +(fecha.getMonth() +1): (fecha.getMonth() +1);
        var dia   = ((fecha.getDate()) <10)?"0" +(fecha.getDate()): (fecha.getDate());
        var fecha_actual = new Date();
        if(fecha < fecha_actual){
            $scope.diaFeriado.fecha ="";
            toastr.error("Fecha invalida");

        }else{
            $scope.diaFeriado.fecha= $scope.getSoloFecha(fecha);
        }


    }

    $scope.getSoloFecha = function(valor){
      var mesFinal              =  ((valor.getMonth()  + 1) <10)?("0"+(valor.getMonth()  + 1)):(valor.getMonth()  + 1);
      var diaFinal              =  ( valor.getDate() <10 )?("0" + valor.getDate()):valor.getDate();
      var fin                   =  valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;
      return fin;

    }


}
/* EOF */