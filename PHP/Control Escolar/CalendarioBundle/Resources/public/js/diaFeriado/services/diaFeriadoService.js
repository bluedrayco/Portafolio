
/*
 * Servicio que obtiene y gestiona las diferentes presentaciones de la oferta educativa
 */
app.service ('diaFeriadoService',function($http, $resource){
    this.restResource = $resource('/calendarizacion/api/entity/calendario/diasferiados?filter=diaferiado.activo=true&order=fecha|desc', { },{
        get:       {
                    method: 'GET',
                    isArray:true,
                    transformResponse : function(data, headerrs){

                        var result                          = [];
                        var valor                           = null;
                        var mesFinal                        = null;
                        var diaFinal                        = null;
                        var fin                             = null;
                        result                              = angular.fromJson(data).data;

                        for(var i = 0; i < result.diasferiados.length; i++){
                            valor                           = new Date(result.diasferiados[i].fecha);
                            mesFinal                        = ((valor.getMonth()  + 1) <10)?("0"+(valor.getMonth()  + 1)):(valor.getMonth()  + 1);
                            diaFinal                        = ( valor.getDate() <10 )?("0" + valor.getDate()):valor.getDate();
                            fin                             = valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;
                            result.diasferiados[i].fecha    = fin;
                        }
                        return result.diasferiados;
                    }
                },


        add:    {
                    method              : 'POST',
                    url                 : "/calendarizacion/api/entity/calendario/diaferiado",
                    transformResponse   : function(data, headerrs){
                        var result                  = [];
                        result                      = angular.fromJson(data).data;
                        valor                       = new Date(result.diaferiado.fecha);
                        mesFinal                    = ((valor.getMonth()  + 1) <10)?("0"+(valor.getMonth()  + 1)):(valor.getMonth()  + 1);
                        diaFinal                    = ( valor.getDate() <10 )?("0" + valor.getDate()):valor.getDate();
                        fin                         = valor.getFullYear()  + "-" + mesFinal  + "-" + diaFinal;
                        result.diaferiado.fecha     = fin;
                        return result.diaferiado;
                        }
                },

        update: {
                    method              : 'PUT',
                    url                 : "/calendarizacion/api/entity/calendario/diaferiado/:dia_feriado_id",
                    transformResponse   : function(data, headerrs){
                        var result  = [];
                        result      = angular.fromJson(data).data;
                        return result.diaferiado;
                    }
                },

        remove: {
                    method              : 'DELETE',
                    url                 : "/calendarizacion/api/entity/calendario/diaferiado/:dia_feriado_id",
                    transformResponse   : function(data, headerrs){
                        var result  = [];
                        result      = angular.fromJson(data).data;
                        return result.diaferiado;
                    }

        }
    });

});