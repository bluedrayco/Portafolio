<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;
use \DateTime;

/**
 * Repositorio para generar consultas particulares en la tabla EventoActividadOfertaCentro
 * @author Roberto Monroy <roberto.monroy@enova.mx>
 */

class EventoActividadOfertaCentroRepository extends GenericRepository{

    /**
     * Encontramos los Eventos de las las Actividades a partir de un Escenario y una Oferta Educativa
     * @param  [array] $params [Array que debe contener el key "escenario_id, oferta_educativa_id, fecha_inicio, fecha_final"]
     * @return [array]         [Array de Actividades (solo los datos basicos)], como mejora se coloco el horario de la Actividad
     */
    public function findEventosForCalendar($params){
        $result = array();

        $dql            = "SELECT

                                    OA.oferta_actividad_centro_id
                                FROM
                                    ControlEscolarCalendarioBundle:Ocupacion                       O
                                LEFT JOIN
                                    O.OfertaActividadCentro                                        OA
                                LEFT JOIN
                                    O.OfertaEducativaCentro                                        OEC
                                    WHERE
                                        O.OfertaActividadCentro IS NOT NULL
                                    AND
                                        OEC.oferta_educativa_centro_id = :oferta_educativa_centro_id

                                    AND O.activo = true
                          ";
        $query          = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('oferta_educativa_centro_id'  , $params["oferta_educativa_centro_id"]);
        $actividades    = $query->getArrayResult();
        if(count($actividades)<=0){
            $actividades [] = array("oferta_actividad_centro_id"=>0);
        }






        $dql            = " SELECT
                                    'oferta'                                                                                                AS tipo,
                                    EA.evento_centro_id                                                                                     AS evento_id,
                                    OA.oferta_actividad_centro_id                                                                           AS oferta_actividad_id,
                                    OA.oferta_actividad_centro_id                                                                           AS oferta_actividad_centro_id,
                                    OA.obligatoria                                                                                          AS obligatorio,
                                    OA.extemporanea                                                                                         AS extemporanea,
                                    OA.control_acceso                                                                                       AS control_acceso,
                                    EA.fecha_evento                                                                                         AS start,
                                    OA.fecha_fin                                                                                            AS fecha_final,
                                    OA.fecha_inicio                                                                                         AS fecha_inicio,
                                    EA.hora_inicio,
                                    EA.hora_fin,




                                    CASE WHEN A IS null THEN AA.nombre                            ELSE A.nombre                       END   AS title,
                                    CASE WHEN A IS null THEN AA.descripcion                       ELSE A.descripcion                  END   AS descripcion,
                                    CASE WHEN A IS null THEN AAC.nombre                           ELSE AC.nombre                      END   AS categoria,
                                    CASE WHEN A IS null THEN AAC.actividad_academica_categoria_id ELSE AC.categoria_id                END   AS categoria_id,
                                    CASE WHEN A IS null THEN CONCAT(AAC.estilo,'')                ELSE CONCAT(AC.estilo,'')           END   AS className,
                                    CASE WHEN A IS null THEN true                                 ELSE false                          END   AS es_academica,
                                    CASE WHEN A IS null THEN AA.actividad_academica_id            ELSE A.actividad_id                 END   AS id_actividad,
                                    CASE WHEN A IS null THEN OAE.actividad_academica_esquema_id   ELSE 0                              END   AS esquema_id,
                                    CASE WHEN :oferta_educativa_centro_id <> OEC.oferta_educativa_centro_id THEN true ELSE false      END   AS externo,
                                    CASE WHEN :oferta_educativa_centro_id <> OEC.oferta_educativa_centro_id THEN OEC.nombre ELSE 'false' END   AS nombreOfertaExterna


                            FROM
                                    ControlEscolarCalendarioBundle:EventoActividadOfertaCentro      EA
                            LEFT JOIN
                                    EA.OfertaActividadCentro                                        OA
                            LEFT JOIN
                                    OA.ActividadAcademicaEsquema                                    OAE
                            LEFT JOIN
                                    OA.OfertaEducativaCentro                                        OEC
                            LEFT JOIN
                                    OA.ActividadAcademica                                           AA
                            LEFT JOIN
                                    OA.Actividad                                                    A
                            LEFT JOIN
                                    AA.ActividadAcademicaCategoria                                  AAC
                            LEFT JOIN
                                    A.ActividadCategoria                                            AC

                            WHERE
                                    (EA.fecha_evento  BETWEEN :fecha_inicio AND :fecha_final)
                                AND
                                    OA.oferta_actividad_centro_id NOT IN (:actividades)
                                AND
                                    OEC.oferta_educativa_centro_id = :oferta_educativa_centro_id
                          ";





    $query              = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('oferta_educativa_centro_id'                   , $params["oferta_educativa_centro_id"]);
    $query->setParameter('actividades'                                  , $actividades );
    $query->setParameter('fecha_inicio'                                 , $params["fecha_inicio"]);
    $query->setParameter('fecha_final'                                  , $params["fecha_final"]);
    //echo $query->getSQL();
    $result = $query->getArrayResult();





    $dql2               = " SELECT

                                    OAH.hora_inicio                         AS hora_inicio,
                                    OAH.hora_fin                            AS hora_fin,
                                    OAH.dia_semana                          AS dia_semana

                            FROM
                                    ControlEscolarCalendarioBundle:HorarioOfertaCentro OAH

                            LEFT JOIN
                                    OAH.OfertaActividadCentro                          OA
                            WHERE
                                    OA.oferta_actividad_centro_id=:oferta_actividad_centro_id
                         ";
        $query2  = $this->getEntityManager()->createQuery($dql2);



        foreach ($result as $key => $value){

            $fecha_evento                                                   = $result[$key]["start"]->format("Y-m-d ");
            $result[$key]["hora_inicio"]                                    = $result[$key]["hora_inicio"]->format("H:i");
            $result[$key]["hora_fin"]                                       = $result[$key]["hora_fin"]->format("H:i");
            $result[$key]["start"]                                          = $fecha_evento . $result[$key]["hora_inicio"];
            $result[$key]["end"]                                            = $fecha_evento . $result[$key]["hora_fin"];
            $result[$key]["durationEditable"]                               = false;
            $result[$key]["editable"]                                       = false;
            $result[$key]["facilitador_id"]                                 = null;
            $result[$key]["aula_id"]                                        = null;

            $query2->setParameter('oferta_actividad_centro_id',$result[$key]['oferta_actividad_id']);
            $result[$key]["_horarios"]                                      = $query2->getArrayResult();
            foreach($result[$key]["_horarios"] as &$obj){
                $obj["hora_inicio"]                                         = $obj["hora_inicio"]->format("H:i");
                $obj["hora_fin"]                                            = $obj["hora_fin"]->format("H:i");
            }

            //obtener los horarios de la actividad

        }

    //print_r($result);
    //exit();

    return $result;
    }

}
?>