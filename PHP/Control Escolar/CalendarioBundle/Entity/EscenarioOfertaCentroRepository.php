<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla escenario
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class EscenarioOfertaCentroRepository extends GenericRepository{ 

/**
 * método que encuentra los centros y grupos vinculados a un escenario oferta
 * @param mixed $obj objeto que contiene el escenario_oferta_id con el cual obtendremos los grupos y centros relacionados
 * @return mixed objeto que contiene un escenarioofertacentro con los grupos y centros relacionados con este
 */    
    public function findCentrosYGruposByEscenarioOfertaId($obj){
        $dql="SELECT 
              eo.escenario_oferta_id,eo.fecha_alta
              FROM ControlEscolarCalendarioBundle:EscenarioOferta eo 
              WHERE 
              eo.activo=true AND eo.escenario_oferta_id=:escenario_oferta_id";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter('escenario_oferta_id', $obj['escenario_oferta_id']);        
        $escenariooferta = $query->getOneOrNullResult();
        $dql="SELECT 
              Distinct (c.centro_id )centro_id,c.nombre,c.descripcion
              FROM ControlEscolarCalendarioBundle:EscenarioOfertaCentro eoc
                LEFT JOIN eoc.Centro c
                LEFT JOIN eoc.EscenarioOferta eo 
              WHERE 
              eoc.activo=true AND eo.escenario_oferta_id=:escenario_oferta_id 
              AND c IS NOT NULL AND c.activo=true
              ORDER BY c.nombre";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter('escenario_oferta_id', $obj['escenario_oferta_id']);        
        $centros = $query->getResult();
        $dql="SELECT 
              Distinct (g.grupo_id )grupo_id,g.nombre,g.descripcion
              FROM ControlEscolarCalendarioBundle:EscenarioOfertaCentro eoc
                LEFT JOIN eoc.Grupo g
                LEFT JOIN eoc.EscenarioOferta eo 
              WHERE 
              eoc.activo=true AND eo.escenario_oferta_id=:escenario_oferta_id 
              AND g IS NOT NULL AND g.activo=true
              ORDER BY g.nombre";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter('escenario_oferta_id', $obj['escenario_oferta_id']);      
        $grupos = $query->getResult();
        $escenariooferta['centros']=$centros==NULL?array():$centros;
        $escenariooferta['grupos']=$grupos==NULL?array():$grupos;
        return $escenariooferta;
    }   
/**
 * método que encuentra el centro vinculado a un escenario oferta
 * @param mixed $obj objeto que contiene el escenario_oferta_id con el cual obtendremos el centro vinculado y un centro_id
 * @return mixed objeto que contiene un escenarioofertacentro con los centros relacionados con este
 */   
    public function findByEscenarioOfertaIdCentroId($obj){
        
        $dql="SELECT 
                eoc
              FROM ControlEscolarCalendarioBundle:EscenarioOfertaCentro eoc
                LEFT JOIN eoc.EscenarioOferta eo
                LEFT JOIN eoc.Centro c
              WHERE 
                eoc.activo=true 
                AND eo.escenario_oferta_id=:escenario_oferta_id 
                AND c.centro_id=:centro_id";
        $query=$this->getEntityManager()->createQuery($dql);

        $query->setParameter('escenario_oferta_id' , $obj['escenario_oferta_id']);
        $query->setParameter('centro_id'           , $obj['centro_id']);      
        $escenariooferta = $query->getResult();   
        return $escenariooferta;
    }
    
/**
 * método que encuentra el grupo vinculado a un escenario oferta
 * @param mixed $obj objeto que contiene el escenario_oferta_id con el cual obtendremos el grupo vinculado y un grupo_id
 * @return mixed objeto que contiene un escenarioofertacentro con los grupos relacionados con este
 */     
    public function findByEscenarioOfertaIdGrupoId($obj){
        
        $dql="SELECT 
                eoc
              FROM ControlEscolarCalendarioBundle:EscenarioOfertaCentro eoc
                LEFT JOIN eoc.EscenarioOferta eo
                LEFT JOIN eoc.Grupo g
              WHERE 
                eoc.activo=true 
                AND eo.escenario_oferta_id=:escenario_oferta_id 
                AND g.grupo_id=:grupo_id";
        $query=$this->getEntityManager()->createQuery($dql);

        $query->setParameter('escenario_oferta_id' , $obj['escenario_oferta_id']);
        $query->setParameter('grupo_id'           , $obj['grupo_id']);      
        $escenariooferta = $query->getResult();   
        return $escenariooferta;       
    }
}