<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla Oferta Actividad
 * @author Silvio Bravo Cado <silvio.bravo@enova.mx>
 */
class OfertaActividadRepository extends GenericRepository{ 
    
    /**
     * Encontramos las Actividades(Academicas y No) a partir de un OfertaEscenarioID
     * @param  [array] $params [Array que debe contener el key "escenario_oferta_id"]
     * @return [array]         [Array de Actividades (solo los datos basicos)]
     */
    public function findActividades($params){
        $result = array();
        $dql    = " SELECT  
                            
                            partial OA.{oferta_actividad_id     , fecha_inicio  , obligatoria   , fecha_fin },
                            partial AA.{actividad_academica_id  , nombre        , descripcion   , objetivo, duracion_horas_presenciales,  duracion_horas_practicas_requeridas,  duracion_sesion_minima, duracion_sesion_ideal, duracion_sesion_maxima },
                            partial A.{actividad_id,nombre      , descripcion},
                            partial EO.{escenario_oferta_id},
                            partial OFED.{oferta_educativa_id},
                            partial ES.{nombre, escenario_id},
                            AAE
                            


                            
                    FROM 
                            ControlEscolarCalendarioBundle:OfertaActividad OA

                    LEFT JOIN 
                            OA.ActividadAcademica           AA
                    LEFT JOIN
                            OA.Actividad                    A
                    LEFT JOIN
                            OA.EscenarioOferta              EO
                    LEFT JOIN 
                            OA.ActividadAcademicaEsquema    AAE
                    LEFT JOIN 
                            EO.Escenario                    ES
                    LEFT JOIN 
                            EO.OfertaEducativa              OFED
                    
                    

                    WHERE 
                            ES.escenario_id=:escenario_id
                        AND 
                            OFED.oferta_educativa_id=:oferta_educativa_id
                        AND 
                            (:fecha_inicio BETWEEN OA.fecha_inicio   AND OA.fecha_fin)
                 ";

        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('escenario_id'         , $params["escenario_id"]);
        $query->setParameter('oferta_educativa_id'  , $params["oferta_educativa_id"]);
        $query->setParameter('fecha_inicio'         , $params["fecha_inicio"]);
        //die($query->getSql());
        return $query->getArrayResult();
    }


}
?>