<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * EventoActividadOfertaCentro
 * @ExclusionPolicy("all")
 */
class EventoActividadOfertaCentro
{
    /**
     * @var integer
     * @Expose
     */
    private $evento_centro_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_evento;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_fin;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;
 
    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro
     * @Expose
     */
    private $OfertaActividadCentro;


    /**
     * Get evento_centro_id
     *
     * @return integer 
     */
    public function getEventoCentroId()
    {
        return $this->evento_centro_id;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return EventoActividadOfertaCentro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_evento
     *
     * @param \DateTime $fechaEvento
     * @return EventoActividadOfertaCentro
     */
    public function setFechaEvento($fechaEvento)
    {
        $this->fecha_evento = $fechaEvento;

        return $this;
    }

    /**
     * Get fecha_evento
     *
     * @return \DateTime 
     */
    public function getFechaEvento()
    {
        return $this->fecha_evento;
    }

    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return EventoActividadOfertaCentro
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return EventoActividadOfertaCentro
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return EventoActividadOfertaCentro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->UsuarioAlta = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set OfertaActividadCentro
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro $ofertaActividadCentro
     * @return EventoActividadOfertaCentro
     */
    public function setOfertaActividadCentro(\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro $ofertaActividadCentro = null)
    {
        $this->OfertaActividadCentro = $ofertaActividadCentro;

        return $this;
    }

    /**
     * Get OfertaActividadCentro
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro 
     */
    public function getOfertaActividadCentro()
    {
        return $this->OfertaActividadCentro;
    }
}
