<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Parametro
 * @ExclusionPolicy("all")
 */
class Parametro
{
    /**
     * @var integer
     * @Expose
     */
    private $parametro_id;

    /**
     * @var string
     * @Expose
     */
    private $nombre;
    
    /**
     * @var string
     * @Expose
     */
    private $etiqueta;

    /**
     * @var integer
     * @Expose
     */
    private $tipo;

    /**
     * @var string
     * @Expose
     */
    private $valor;

    /**
     * @var boolean
     * @Expose
     */
    private $puede_publicar;


    /**
     * @var boolean
     * @Expose
     */
    private $modifica_centro;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;
    
    /**
     * Get parametro_id
     *
     * @return integer 
     */
    public function getParametroId()
    {
        return $this->parametro_id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Parametro
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Set etiqueta
     *
     * @param string $etiqueta
     * @return Parametro
     */
    public function setEtiqueta($etiqueta)
    {
        $this->etiqueta = $etiqueta;

        return $this;
    }

    /**
     * Get etiqueta
     *
     * @return string 
     */
    public function getEtiqueta()
    {
        return $this->etiqueta;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return Parametro
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return Parametro
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set puede_publicar
     *
     * @param boolean $puedePublicar
     * @return Parametro
     */
    public function setPuedePublicar($puedePublicar)
    {
        $this->puede_publicar = $puedePublicar;

        return $this;
    }

    /**
     * Get puede_publicar
     *
     * @return boolean 
     */
    public function getPuedePublicar()
    {
        return $this->puede_publicar;
    }

    /**
     * Set modifica_centro
     *
     * @param boolean $modificaCentro
     * @return Parametro
     */
    public function setModificaCentro($modificaCentro)
    {
        $this->modifica_centro = $modificaCentro;

        return $this;
    }

    /**
     * Get modifica_centro
     *
     * @return boolean 
     */
    public function getModificaCentro()
    {
        return $this->modifica_centro;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Parametro
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return Parametro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return Parametro
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return Parametro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return Parametro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
}
