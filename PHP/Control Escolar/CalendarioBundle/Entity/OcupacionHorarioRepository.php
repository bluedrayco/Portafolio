<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla ocupacion horario
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OcupacionHorarioRepository extends GenericRepository{ 
    
    function findHorariosByOcupacionId($params){
        $result = array();
        $dql    = " SELECT OH"
                . " FROM ControlEscolarCalendarioBundle:OcupacionHorario OH"
                . " LEFT JOIN OH.Ocupacion O"
                . " WHERE O.ocupacion_id=:ocupacion_id";
                            
        
        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ocupacion_id',$params["ocupacion_id"]);
        $result = $query->getResult();
        return $result;
    }
    
}