<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Ocupacion
 * @ExclusionPolicy("all")
 */
class Ocupacion
{
    /**
     * @var integer
     * @Expose
     */
    private $ocupacion_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_fin;

    /**
     * @var string
     * @Expose
     */
    private $clave_grupo;

    /**
     * @var integer
     * @Expose
     */
    private $numero_inscritos;

    /**
     * @var integer
     * @Expose
     */
    private $numero_interesados;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro
     * @Expose
     */
    private $OfertaActividadCentro;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademica
     * @Expose
     */
    private $ActividadAcademica;

    /**
     * @var \ControlEscolar\PlanAnualBundle\Entity\Actividad
     * @Expose
     */
    private $Actividad;

    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademicaEsquema
     * @Expose
     */
    private $ActividadAcademicaEsquema;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OcupacionEstatus
     * @Expose
     */
    private $OcupacionEstatus;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro
     */
    private $OfertaEducativaCentro;

    /**
     * @var integer
     * @Expose
     */
    private $numero_movimientos;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Aula
     * @Expose
     */
    private $Aula;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Facilitador
     * @Expose
     */
    private $Facilitador;

    /**
     * Nombre de la actividad academica y clave del grupo.
     * @return string La representación en texto de la ocupacion.
     */
    public function __toString()
    {
        if (is_object($this->getActividadAcademica())) {
            return $this->getActividadAcademica()->getNombre().' - '.$this->getClaveGrupo();
        } elseif (is_object($this->getActividad())) {
            return $this->getActividad()->getNombre().' - '.$this->getClaveGrupo();
        } else {
            return $this->getClaveGrupo();
        }
    }

    /**
     * Get ocupacion_id
     *
     * @return integer
     */
    public function getOcupacionId()
    {
        return $this->ocupacion_id;
    }

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return Ocupacion
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Set fecha_fin
     *
     * @param \DateTime $fechaFin
     * @return Ocupacion
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;

        return $this;
    }

    /**
     * Get fecha_fin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /**
     * Set clave_grupo
     *
     * @param string $claveGrupo
     * @return Ocupacion
     */
    public function setClaveGrupo($claveGrupo)
    {
        $this->clave_grupo = $claveGrupo;

        return $this;
    }

    /**
     * Get clave_grupo
     *
     * @return string
     */
    public function getClaveGrupo()
    {
        return $this->clave_grupo;
    }

    /**
     * Set numero_inscritos
     *
     * @param integer $numeroInscritos
     * @return Ocupacion
     */
    public function setNumeroInscritos($numeroInscritos)
    {
        $this->numero_inscritos = $numeroInscritos;

        return $this;
    }

    /**
     * Get numero_inscritos
     *
     * @return integer
     */
    public function getNumeroInscritos()
    {
        return $this->numero_inscritos;
    }

    /**
     * Set numero_interesados
     *
     * @param integer $numeroInteresados
     * @return Ocupacion
     */
    public function setNumeroInteresados($numeroInteresados)
    {
        $this->numero_interesados = $numeroInteresados;

        return $this;
    }

    /**
     * Get numero_interesados
     *
     * @return integer
     */
    public function getNumeroInteresados()
    {
        return $this->numero_interesados;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Ocupacion
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return Ocupacion
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return Ocupacion
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set ActividadAcademica
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica
     * @return Ocupacion
     */
    public function setActividadAcademica(\Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica = null)
    {
        $this->ActividadAcademica = $actividadAcademica;

        return $this;
    }

    /**
     * Get ActividadAcademica
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademica
     */
    public function getActividadAcademica()
    {
        return $this->ActividadAcademica;
    }

    /**
     * Set Actividad
     *
     * @param \ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad
     * @return Ocupacion
     */
    public function setActividad(\ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad = null)
    {
        $this->Actividad = $actividad;

        return $this;
    }

    /**
     * Get Actividad
     *
     * @return \ControlEscolar\PlanAnualBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->Actividad;
    }

    /**
     * Set ActividadAcademicaEsquema
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademicaEsquema $actividadAcademicaEsquema
     * @return Ocupacion
     */
    public function setActividadAcademicaEsquema(\Core\CoreBundle\Entity\ActividadAcademicaEsquema $actividadAcademicaEsquema = null)
    {
        $this->ActividadAcademicaEsquema = $actividadAcademicaEsquema;

        return $this;
    }

    /**
     * Get ActividadAcademicaEsquema
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademicaEsquema
     */
    public function getActividadAcademicaEsquema()
    {
        return $this->ActividadAcademicaEsquema;
    }

    /**
     * Set OcupacionEstatus
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OcupacionEstatus $ocupacionEstatus
     * @return Ocupacion
     */
    public function setOcupacionEstatus(\ControlEscolar\CalendarioBundle\Entity\OcupacionEstatus $ocupacionEstatus = null)
    {
        $this->OcupacionEstatus = $ocupacionEstatus;

        return $this;
    }

    /**
     * Get OcupacionEstatus
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OcupacionEstatus
     */
    public function getOcupacionEstatus()
    {
        return $this->OcupacionEstatus;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return Ocupacion
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return Ocupacion
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set numero_movimientos
     *
     * @param integer $numeroMovimientos
     * @return Ocupacion
     */
    public function setNumeroMovimientos($numeroMovimientos)
    {
        $this->numero_movimientos = $numeroMovimientos;

        return $this;
    }

    /**
     * Get numero_movimientos
     *
     * @return integer
     */
    public function getNumeroMovimientos()
    {
        return $this->numero_movimientos;
    }

    /**
     * Set Aula
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Aula $aula
     * @return Ocupacion
     */
    public function setAula(\ControlEscolar\CalendarioBundle\Entity\Aula $aula = null)
    {
        $this->Aula = $aula;

        return $this;
    }

    /**
     * Get Aula
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Aula
     */
    public function getAula()
    {
        return $this->Aula;
    }

    /**
     * Set Aula
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Aula $facilitador
     * @return Ocupacion
     */
    public function setFacilitador(\ControlEscolar\CalendarioBundle\Entity\Facilitador $facilitador = null)
    {
        $this->Facilitador = $facilitador;

        return $this;
    }

    /**
     * Get Aula
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Facilitador
     */
    public function getFacilitador()
    {
        return $this->Facilitador;
    }


    /**
     * Set OfertaEducativaCentro
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro $ofertaEducativaCentro
     * @return Ocupacion
     */
    public function setOfertaEducativaCentro(\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro $ofertaEducativaCentro = null)
    {
        $this->OfertaEducativaCentro = $ofertaEducativaCentro;

        return $this;
    }

    /**
     * Get ActividadAcademica
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademica
     */
    public function getOfertaEducativaCentro()
    {
        return $this->OfertaEducativaCentro;
    }


    /**
     * Set OfertaActividadCentro
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro $ofertaActividadCentro
     * @return Ocupacion
     */
    public function setOfertaActividadCentro(\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro $ofertaActividadCentro = null)
    {
        $this->OfertaActividadCentro = $ofertaActividadCentro;

        return $this;
    }

    /**
     * Get OfertaActividadCentro
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro
     */
    public function getOfertaActividadCentro()
    {
        return $this->OfertaActividadCentro;
    }
}
