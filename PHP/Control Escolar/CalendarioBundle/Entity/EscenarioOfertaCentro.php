<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;


/**
 * EscenarioOfertaCentro
 * @ExclusionPolicy("all")
 */
class EscenarioOfertaCentro
{
    /**
     * @var integer
     * @Expose
     */
    private $escenario_oferta_centro_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\EscenarioOferta
     * @Expose
     */
    private $EscenarioOferta;

    /**
     * @var \Core\CoreBundle\Entity\Centro
     * @Expose
     */
    private $Centro;


    /**
     * Get escenario_oferta_centro_id
     *
     * @return integer 
     */
    public function getEscenarioOfertaCentroId()
    {
        return $this->escenario_oferta_centro_id;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return EscenarioOfertaCentro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return EscenarioOfertaCentro
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return EscenarioOfertaCentro
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return EscenarioOfertaCentro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set EscenarioOferta
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\EscenarioOferta $escenarioOferta
     * @return EscenarioOfertaCentro
     */
    public function setEscenarioOferta(\ControlEscolar\CalendarioBundle\Entity\EscenarioOferta $escenarioOferta = null)
    {
        $this->EscenarioOferta = $escenarioOferta;

        return $this;
    }

    /**
     * Get EscenarioOferta
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\EscenarioOferta 
     */
    public function getEscenarioOferta()
    {
        return $this->EscenarioOferta;
    }

    /**
     * Set Centro
     *
     * @param \Core\CoreBundle\Entity\Centro $centro
     * @return EscenarioOfertaCentro
     */
    public function setCentro(\Core\CoreBundle\Entity\Centro $centro = null)
    {
        $this->Centro = $centro;

        return $this;
    }

    /**
     * Get Centro
     *
     * @return \Core\CoreBundle\Entity\Centro 
     */
    public function getCentro()
    {
        return $this->Centro;
    }
    /**
     * @var boolean
     */
    private $activo;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return EscenarioOfertaCentro
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    /**
     * @var \Core\CoreBundle\Entity\Grupo
     */
    private $Grupo;


    /**
     * Set Grupo
     *
     * @param \Core\CoreBundle\Entity\Grupo $grupo
     * @return EscenarioOfertaCentro
     */
    public function setGrupo(\Core\CoreBundle\Entity\Grupo $grupo = null)
    {
        $this->Grupo = $grupo;

        return $this;
    }

    /**
     * Get Grupo
     *
     * @return \Core\CoreBundle\Entity\Grupo 
     */
    public function getGrupo()
    {
        return $this->Grupo;
    }
}
