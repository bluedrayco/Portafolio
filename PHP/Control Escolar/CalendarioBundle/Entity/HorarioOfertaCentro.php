<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * HorarioOfertaCentro
 * @ExclusionPolicy("all")
 */
class HorarioOfertaCentro
{
    /**
     * @var integer
     * @Expose
     */
    private $horario_oferta_id;

    /**
     * @var integer
     * @Expose
     */
    private $dia_semana;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_fin;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro
     * @Expose
     */
    private $OfertaActividadCentro;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;


    /**
     * Get horario_oferta_id
     *
     * @return integer 
     */
    public function getHorarioOfertaId()
    {
        return $this->horario_oferta_id;
    }

    /**
     * Set dia_semana
     *
     * @param integer $diaSemana
     * @return HorarioOfertaCentro
     */
    public function setDiaSemana($diaSemana)
    {
        $this->dia_semana = $diaSemana;

        return $this;
    }

    /**
     * Get dia_semana
     *
     * @return integer 
     */
    public function getDiaSemana()
    {
        return $this->dia_semana;
    }

    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return HorarioOfertaCentro
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return HorarioOfertaCentro
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return HorarioOfertaCentro
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return HorarioOfertaCentro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modifica
     *
     * @param \DateTime $fechaModificacion
     * @return HorarioOfertaCentro
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set OfertaActividadCentro
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro $ofertaActividadCentro
     * @return HorarioOfertaCentro
     */
    public function setOfertaActividadCentro(\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro $ofertaActividadCentro = null)
    {
        $this->OfertaActividadCentro = $ofertaActividadCentro;

        return $this;
    }

    /**
     * Get OfertaActividadCentro
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro 
     */
    public function getOfertaActividadCentro()
    {
        return $this->OfertaActividadCentro;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return HorarioOfertaCentro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return HorarioOfertaCentro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
}
