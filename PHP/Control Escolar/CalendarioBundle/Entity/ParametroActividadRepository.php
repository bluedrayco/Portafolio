<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla Parametro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class ParametroActividadRepository extends GenericRepository{

    public function findActividadByClaveParametro($params){
        $result = array();
           $dql            = "SELECT
                                   PA
                                FROM
                                    ControlEscolarCalendarioBundle:ParametroActividad  PA
                                LEFT JOIN
                                    PA.Parametro P
                                LEFT JOIN
                                    PA.ActividadAcademica AA
                                WHERE
                                    P.nombre = :nombre
                                    AND AA.actividad_academica_id=:actividad_id
                          ";
        $query          = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('nombre'  , $params["nombre_parametro"]);
        $query->setParameter('actividad_id'  , $params["actividad_id"]);
        $valorParametro = $query->getResult();
        return $valorParametro;
    }
}
