<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OcupacionHorario
 * @ExclusionPolicy("all")
 */
class OcupacionHorario
{
    /**
     * @var integer
     * @Expose
     */
    private $ocupacion_horario_id;

    /**
     * @var integer
     * @Expose
     */
    private $dia_semana;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_fin;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Ocupacion
     * @Expose
     */
    private $Ocupacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;


    /**
     * Get ocupacion_horario_id
     *
     * @return integer 
     */
    public function getOcupacionHorarioId()
    {
        return $this->ocupacion_horario_id;
    }

    /**
     * Set dia_semana
     *
     * @param integer $diaSemana
     * @return OcupacionHorario
     */
    public function setDiaSemana($diaSemana)
    {
        $this->dia_semana = $diaSemana;

        return $this;
    }

    /**
     * Get dia_semana
     *
     * @return integer 
     */
    public function getDiaSemana()
    {
        return $this->dia_semana;
    }

    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return OcupacionHorario
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return OcupacionHorario
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return OcupacionHorario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OcupacionHorario
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OcupacionHorario
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Ocupacion
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Ocupacion $ocupacion
     * @return OcupacionHorario
     */
    public function setOcupacion(\ControlEscolar\CalendarioBundle\Entity\Ocupacion $ocupacion = null)
    {
        $this->Ocupacion = $ocupacion;

        return $this;
    }

    /**
     * Get Ocupacion
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Ocupacion 
     */
    public function getOcupacion()
    {
        return $this->Ocupacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return OcupacionHorario
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OcupacionHorario
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
}
