<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * EventoActividadOferta
 * @ExclusionPolicy("all")
 */
class EventoActividadOferta
{
    /**
     * @var integer
     * @Expose
     */
    private $evento_actividad_oferta_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_evento;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_fin;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaActividad
     * @Expose
     */
    private $OfertaActividad;


    /**
     * Get evento_actividad_oferta_id
     *
     * @return integer 
     */
    public function getEventoActividadOfertaId()
    {
        return $this->evento_actividad_oferta_id;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return EventoActividadOferta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_evento
     *
     * @param \DateTime $fechaEvento
     * @return EventoActividadOferta
     */
    public function setFechaEvento($fechaEvento)
    {
        $this->fecha_evento = $fechaEvento;

        return $this;
    }

    /**
     * Get fecha_evento
     *
     * @return \DateTime 
     */
    public function getFechaEvento()
    {
        return $this->fecha_evento;
    }

    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return EventoActividadOferta
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return EventoActividadOferta
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return EventoActividadOferta
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set OfertaActividad
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaActividad $ofertaActividad
     * @return EventoActividadOferta
     */
    public function setOfertaActividad(\ControlEscolar\CalendarioBundle\Entity\OfertaActividad $ofertaActividad = null)
    {
        $this->OfertaActividad = $ofertaActividad;

        return $this;
    }

    /**
     * Get OfertaActividad
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaActividad 
     */
    public function getOfertaActividad()
    {
        return $this->OfertaActividad;
    }
}
