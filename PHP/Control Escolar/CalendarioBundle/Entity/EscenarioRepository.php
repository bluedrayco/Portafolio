<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla escenario
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class EscenarioRepository extends GenericRepository{ 
    
    function findIdsEscenarioByIdsGrupos($params){
        //print_r($params);
        $result = array();
        $dql    = " SELECT  
                            E.escenario_id escenario_id
                    FROM 
                            ControlEscolarCalendarioBundle:EscenarioOferta EO

                    LEFT JOIN 
                            EO.Escenario                       E
                    LEFT JOIN 
                            E.Grupo                            G   
                    LEFT JOIN 
                            EO.OfertaEducativa                 OE
                    LEFT JOIN
                            E.TipoEscenario                    TE
                    WHERE  
                            OE.oferta_educativa_id = :oferta_educativa_id 
                        AND
                            G.grupo_id IN (:grupo_ids)
                        AND
                            TE IS NOT NULL
                            
                  ";
                            
        
        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('grupo_ids',$params["grupo_ids"]);
        $query->setParameter('oferta_educativa_id',$params["oferta_educativa_id"]);
        $result = $query->getArrayResult();
//        print_r($params);
        $arregloEscenarios=array();
        foreach($result as $escenario){
            $arregloEscenarios[]=$escenario['escenario_id'];
        }
        return $arregloEscenarios;
    }
    
}