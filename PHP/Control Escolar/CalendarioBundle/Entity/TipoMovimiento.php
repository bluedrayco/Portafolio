<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * TipoMovimiento
 * @ExclusionPolicy("all")
 */
class TipoMovimiento
{
    /**
     * @var integer
     * @Expose
     */
    private $tipo_movimiento_id;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var string
     * @Expose
     */
    private $clave;

    /**
     * @var string
     * @Expose
     */
    private $nombre;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModificacion;


    /**
     * Get tipoMovimientoId
     *
     * @return integer
     */
    public function getTipoMovimientoId()
    {
        return $this->tipo_movimiento_id;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return TipoMovimiento
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set clave
     *
     * @param string $clave
     *
     * @return TipoMovimiento
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoMovimiento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     *
     * @return TipoMovimiento
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return TipoMovimiento
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set usuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     *
     * @return TipoMovimiento
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get usuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set usuarioModificacion
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModificacion
     *
     * @return TipoMovimiento
     */
    public function setUsuarioModificacion(\Core\UserBundle\Entity\Usuario $usuarioModificacion = null)
    {
        $this->UsuarioModificacion = $usuarioModificacion;

        return $this;
    }

    /**
     * Get usuarioModificacion
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModificacion()
    {
        return $this->UsuarioModificacion;
    }
}

