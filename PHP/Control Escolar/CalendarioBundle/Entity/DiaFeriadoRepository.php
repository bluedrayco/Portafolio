<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla dia feriado
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class DiaFeriadoRepository extends GenericRepository{ 
    
    /**
     * Obtención de Días Feriados con el formato de: {title,start,end y classname}
     * @return mixed objeto con los dias feriados
     */
    public function findDiasWithFormat(){
        $dql    = "SELECT
                        DF.nombre as title,
                        DF.fecha as start
                        
                   FROM 
                    ControlEscolarCalendarioBundle:DiaFeriado DF
                    WHERE 
                        DF.activo=true   
                  ";
        $query  = $this->getEntityManager()->createQuery($dql);
        $diasFeriados = $query->getArrayResult();
        $x=0;
        foreach ($diasFeriados as $diaFeriado){
            $diasFeriados[$x]['start']=$diaFeriado["start"]->format("Y-m-d");
            $diasFeriados[$x]['end']=$diaFeriado["start"]->format("Y-m-d");
            $diasFeriados[$x]['className']='diaFeriado';
            $x++;
        }
        return $diasFeriados;
    }

     /**
     * Obtención de Días Feriados por fecha
     * @return mixed objeto con los dias feriados
     */
    public function findDiasWithDate($obj){
        $dql    = "SELECT
                        DF.nombre,
                        DF.fecha
                   FROM 
                    ControlEscolarCalendarioBundle:DiaFeriado DF
                    WHERE 
                        DF.activo=true
                    AND
                        DF.fecha >= :fecha_inicio AND DF.fecha <= :fecha_fin
                  ";

        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('fecha_inicio'        , $obj['fecha_inicio']);
        $query->setParameter('fecha_fin'           , $obj['fecha_fin']); 
        //$diasFeriados = $query->getArrayResult();   
        $diasFeriados = $query->getResult();   

        return $diasFeriados;
    }
}