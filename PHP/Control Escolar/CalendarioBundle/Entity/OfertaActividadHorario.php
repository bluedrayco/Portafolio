<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OfertaActividadHorario
 * @ExclusionPolicy("all")
 */
class OfertaActividadHorario
{
    /**
     * @var integer
     * @Expose
     */
    private $oferta_actividad_horario_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_fin;

    /**
     * @var integer
     * @Expose
     */
    private $dia_semana;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaActividad
     * @Expose
     */
    private $OfertaActividad;


    /**
     * Get escenario_oferta_centro_id
     *
     * @return integer 
     */
    public function getOfertaActividadHorarioId()
    {
        return $this->oferta_actividad_horario_id;
    }

    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return OfertaActividadHorario
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return OfertaActividadHorario
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set dia_semana
     *
     * @param integer $diaSemana
     * @return OfertaActividadHorario
     */
    public function setDiaSemana($diaSemana)
    {
        $this->dia_semana = $diaSemana;

        return $this;
    }

    /**
     * Get dia_semana
     *
     * @return integer 
     */
    public function getDiaSemana()
    {
        return $this->dia_semana;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OfertaActividadHorario
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OfertaActividadHorario
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return OfertaActividadHorario
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OfertaActividadHorario
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set OfertaActividad
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaActividad $ofertaActividad
     * @return OfertaActividadHorario
     */
    public function setOfertaActividad(\ControlEscolar\CalendarioBundle\Entity\OfertaActividad $ofertaActividad = null)
    {
        $this->OfertaActividad = $ofertaActividad;

        return $this;
    }

    /**
     * Get OfertaActividad
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaActividad 
     */
    public function getOfertaActividad()
    {
        return $this->OfertaActividad;
    }
    /**
     * @var boolean
     */
    private $activo;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return OfertaActividadHorario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
}
