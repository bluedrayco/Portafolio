<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * CentroOfertaPublica
 * @ExclusionPolicy("all")
 */
class CentroOfertaPublica
{
    /**
     * @var integer
     * @Expose
     */
    private $centro_oferta_publica_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \Core\CoreBundle\Entity\Centro
     * @Expose
     */
    private $Centro;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaActividad
     * @Expose
     */
    private $OfertaActividad;


    /**
     * Get centro_oferta_publica_id
     *
     * @return integer 
     */
    public function getCentroOfertaPublicaId()
    {
        return $this->centro_oferta_publica_id;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return CentroOfertaPublica
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return CentroOfertaPublica
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return CentroOfertaPublica
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return CentroOfertaPublica
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return CentroOfertaPublica
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set Centro
     *
     * @param \Core\CoreBundle\Entity\Centro $centro
     * @return CentroOfertaPublica
     */
    public function setCentro(\Core\CoreBundle\Entity\Centro $centro = null)
    {
        $this->Centro = $centro;

        return $this;
    }

    /**
     * Get Centro
     *
     * @return \Core\CoreBundle\Entity\Centro 
     */
    public function getCentro()
    {
        return $this->Centro;
    }

    /**
     * Set OfertaActividad
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaActividad $ofertaActividad
     * @return CentroOfertaPublica
     */
    public function setOfertaActividad(\ControlEscolar\CalendarioBundle\Entity\OfertaActividad $ofertaActividad = null)
    {
        $this->OfertaActividad = $ofertaActividad;

        return $this;
    }

    /**
     * Get OfertaActividad
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaActividad 
     */
    public function getOfertaActividad()
    {
        return $this->OfertaActividad;
    }
}
