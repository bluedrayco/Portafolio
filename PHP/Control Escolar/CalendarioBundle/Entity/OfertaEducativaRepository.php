<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla OfertaEducativa
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OfertaEducativaRepository extends GenericRepository{

    /**
     * obtención de la última oferta educativa que contiene los siguientes casos: este en Estatus 1, Sincronizado en False y activo en True
     * @return mixed objeto OfertaEducativa solicitada
     */
    public function findOfertaEducativaActiva(){
        $dql="SELECT
              partial oe.{nombre,oferta_educativa_id,fecha_inicio,fecha_fin,sincronizado},oee ,p
              FROM ControlEscolarCalendarioBundle:OfertaEducativa oe
                LEFT JOIN oe.OfertaEducativaEstatus oee
                LEFT JOIN oe.Periodo p
              WHERE
                oe.activo=true
                AND oe.sincronizado=false
                AND oee.oferta_educativa_estatus_id=1
                ORDER BY oe.oferta_educativa_id DESC";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setMaxResults(1);
        $escenariooferta = $query->getResult();

        return (!$escenariooferta)?false:$escenariooferta[0];
    }

    public function findStoreProcedureGeneratePaquetes($obj){
        $sql="SELECT
                *
              FROM calendario.publica_oferta_educativa(?,?)";
        $rsm=new \Doctrine\ORM\Query\ResultSetMapping();

        $query = $this->getEntityManager()->createNativeQuery($sql,$rsm);
        $query->setParameter(1, $obj['oferta_educativa_id']);
        $query->setParameter(2, $obj['usuario_id']);
        $result=$query->getResult();
        $dql="SELECT
                COUNT(cop.centro_oferta_publica_id)
              FROM ControlEscolarCalendarioBundle:CentroOfertaPublica cop";
        $query=$this->getEntityManager()->createQuery($dql);
        if($query->getResult()[0][1]!=0){
            return true;
        }
        return false;
    }
}