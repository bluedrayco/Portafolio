<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla TipoMovimiento
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class TipoMovimientoRepository extends GenericRepository{ 

}
