<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OcupacionReserva
 * @ExclusionPolicy("all")
 */
class OcupacionReserva
{
    /**
     * @var integer
     * @Expose
     */
    private $ocupacion_reserva_id;

    /**
     * @var integer
     * @Expose
     */
    private $reserva_id;

    /**
     * @var integer
     * @Expose
     */
    private $numero_asistentes;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var fecha_evento
     * @Expose
     */
    private $fecha_evento;
    
    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $hora_fin;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Facilitador
     * @Expose
     */
    private $Facilitador;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Aula
     * @Expose
     */
    private $Aula;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Ocupacion
     * @Expose
     */
    private $Ocupacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;


    /**
     * Get ocupacion_reserva_id
     *
     * @return integer 
     */
    public function getOcupacionReservaId()
    {
        return $this->ocupacion_reserva_id;
    }

    /**
     * Set reserva_id
     *
     * @param integer $reservaId
     * @return OcupacionReserva
     */
    public function setReservaId($reservaId)
    {
        $this->reserva_id = $reservaId;

        return $this;
    }

    /**
     * Get reserva_id
     *
     * @return integer 
     */
    public function getReservaId()
    {
        return $this->reserva_id;
    }

    /**
     * Set numero_asistentes
     *
     * @param integer $numeroAsistentes
     * @return OcupacionReserva
     */
    public function setNumeroAsistentes($numeroAsistentes)
    {
        $this->numero_asistentes = $numeroAsistentes;

        return $this;
    }

    /**
     * Get numero_asistentes
     *
     * @return integer 
     */
    public function getNumeroAsistentes()
    {
        return $this->numero_asistentes;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return OcupacionReserva
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OcupacionReserva
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OcupacionReserva
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Facilitador
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Facilitador $facilitador
     * @return OcupacionReserva
     */
    public function setFacilitador(\ControlEscolar\CalendarioBundle\Entity\Facilitador $facilitador = null)
    {
        $this->Facilitador = $facilitador;

        return $this;
    }

    /**
     * Get Facilitador
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Facilitador 
     */
    public function getFacilitador()
    {
        return $this->Facilitador;
    }

    /**
     * Set Ocupacion
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Ocupacion $ocupacion
     * @return OcupacionReserva
     */
    public function setOcupacion(\ControlEscolar\CalendarioBundle\Entity\Ocupacion $ocupacion = null)
    {
        $this->Ocupacion = $ocupacion;

        return $this;
    }

    /**
     * Get Ocupacion
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Ocupacion 
     */
    public function getOcupacion()
    {
        return $this->Ocupacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return OcupacionReserva
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OcupacionReserva
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set reserva_id
     *
     * @param \DateTime $reservaId
     * @return OcupacionReserva
     */
    public function setFechaEvento($fechaEvento)
    {
        $this->fecha_evento = $fechaEvento;

        return $this;
    }

    /**
     * Get fecha_evento
     *
     * @return \DateTime 
     */
    public function getFechaEvento()
    {
        return $this->fecha_evento;
    }

    /**
     * Set hora_inicio
     *
     * @param \DateTime $horaInicio
     * @return OcupacionReserva
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get hora_inicio
     *
     * @return \DateTime 
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set hora_fin
     *
     * @param \DateTime $horaFin
     * @return OcupacionReserva
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get hora_fin
     *
     * @return \DateTime 
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }
    
    /**
     * Set Aula
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Aula $aula
     * @return OcupacionReserva
     */
    public function setAula(\ControlEscolar\CalendarioBundle\Entity\Aula $aula = null)
    {
        $this->Aula = $aula;

        return $this;
    }

    /**
     * Get Aula
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Aula 
     */
    public function getAula()
    {
        return $this->Aula;
    }
}
