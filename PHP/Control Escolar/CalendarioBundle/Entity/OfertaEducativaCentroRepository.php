<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla OfertaEducativaCentro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OfertaEducativaCentroRepository extends GenericRepository{ 

    public function findEstadisticas($datos){
        $dql="SELECT 
               count(o.ocupacion_id)
              FROM ControlEscolarCalendarioBundle:Ocupacion o
                JOIN o.OfertaActividadCentro oac WITH oac is not null
                LEFT JOIN oac.OfertaEducativaCentro oec
              WHERE 
               oac.obligatoria=true
               And oec.oferta_educativa_centro_id=:oferta_id
               AND oec.activo = true
                AND o.activo= true";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter('oferta_id', $datos['oferta_educativa_id']);
        $totalActividadesAsignadas = $query->getArrayResult();
        
         $dql="SELECT 
               count(oac.oferta_actividad_centro_id)
              FROM ControlEscolarCalendarioBundle:OfertaActividadCentro oac
                LEFT JOIN oac.OfertaEducativaCentro oec
              WHERE  %s 
              oec.oferta_educativa_centro_id=:oferta_id";
         
        $query=$this->getEntityManager()->createQuery(sprintf($dql,'oac.obligatoria=true And '));
        $query->setParameter('oferta_id', $datos['oferta_educativa_id']);
        $totalActividadesTotales = $query->getArrayResult();
        $query=$this->getEntityManager()->createQuery(sprintf($dql,''));
        $query->setParameter('oferta_id', $datos['oferta_educativa_id']);
        $totalActividadesOfertaEducativa = $query->getResult();
//        print_r($totalActividadesAsignadas[0][1]);
//        exit();
        $actividadesFaltantes=$totalActividadesTotales[0][1]-$totalActividadesAsignadas[0][1];
        $totalActividadesOfertaEducativatmp = $totalActividadesOfertaEducativa[0][1];
        $flagAula=false;
        $flagFacilitador=false;
        $opciones="";
        if($datos['aula_id']==0 && $datos['facilitador_id']==0){
            $opciones= '';
        }else{
            if($datos['aula_id']!=0){
                $flagAula=true;
                $opciones = ' AND a.aula_id=:aula_id';
            }
            if($datos['facilitador_id']!=0){
                $flagFacilitador=true;
                $opciones = $opciones.' AND '.'f.facilitador_id=:facilitador_id';
            }   
        }
        $dql="SELECT 
               count(o.ocupacion_id)
              FROM ControlEscolarCalendarioBundle:Ocupacion o
                LEFT JOIN o.OfertaEducativaCentro oec
                LEFT JOIN o.Aula a 
                LEFT JOIN o.Facilitador f
              WHERE 
               oec.oferta_educativa_centro_id=:oferta_id
               AND o.activo=true
               ".$opciones;
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter('oferta_id', $datos['oferta_educativa_id']);
        if($flagAula){
           $query->setParameter('aula_id', $datos['aula_id']); 
        }
        if($flagFacilitador){
            $query->setParameter('facilitador_id', $datos['facilitador_id']); 
        }
//        echo "query--------->  ".$opciones;
//        exit();
        $totalActividadesCalendarizadas = $query->getResult();
        
        
        $arreglo=array(
            'actividades_obligatorias_faltantes'=>$actividadesFaltantes,
            'total_actividades_calendarizadas'  =>$totalActividadesCalendarizadas[0][1],
            'total_actividades_oferta_educativa' =>$totalActividadesOfertaEducativatmp
        );
        return $arreglo;        
    }
}
