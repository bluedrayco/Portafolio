<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Parametro Actividad
 * @ExclusionPolicy("all")
 */
class ParametroActividad
{
    /**
     * @var integer
     * @Expose
     */
    private $parametro_actividad_id;


    /**
     * @var string
     * @Expose
     */
    private $valor;


    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademica
     */
    private $ActividadAcademica;

    /**
     * @var ControlEscolar\CalendarioBundle\Entity\Parametro
     */
    private $Parametro;



    /**
     * Get parametro_actividad_id
     *
     * @return integer
     */
    public function getParametroActividadId()
    {
        return $this->parametro_actividad_id;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return Parametro
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }
    /**
     * Get valor
     *
     * @return boolean
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Parametro
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return Parametro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }


    /**
     * Set ActividadAcademica
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademica $ActividadAcademica
     * @return Parametro
     */
    public function setActividadAcademica(\Core\CoreBundle\Entity\ActividadAcademica $ActividadAcademica= null)
    {
        $this->ActividadAcademica = $ActividadAcademica;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademica
     */
    public function getActividadAcademica()
    {
        return $this->ActividadAcademica;
    }


    /**
     * Set Parametro
     *
     * @param ControlEscolar\CalendarioBundle\Entity\Parametro $Parametro
     * @return Parametro
     */
    public function setParametro(\ControlEscolar\CalendarioBundle\Entity\Parametro $Parametro= null)
    {
        $this->Parametro = $Parametro;

        return $this;
    }

    /**
     * Get Parametro
     *
     * @return ControlEscolar\CalendarioBundle\Entity\Parametro
     */
    public function getParametro()
    {
        return $this->Parametro;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return Parametro
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return Parametro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return Parametro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
}
