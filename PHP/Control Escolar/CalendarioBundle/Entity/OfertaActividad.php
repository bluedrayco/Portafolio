<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OfertaActividad
 * @ExclusionPolicy("all")
 */
class OfertaActividad
{
    /*
     * Contructor de la clase
     */
    public function __construct() {
        //Inicializa  el control acceso con un array vacio
        $this->control_acceso = array(
            'obligatorio'=>array(),
            'solo_lectura'=>array()
            );
    }

   /**
     * @var integer
     * @Expose
     */
    private $oferta_actividad_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_fin;

    /**
     * @var boolean
     * @Expose
     */
    private $obligatoria;

    /**
     * @var boolean
     * @Expose
     */
    private $sincronizado;

    /**
     * @var boolean
     * @Expose
     */
    private $puede_publicar;

    /**
     * @var boolean
     * @Expose
     */
    private $extemporanea;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\EscenarioOferta
     * @Expose
     */
    private $EscenarioOferta;

    /**
     * @var \ControlEscolar\PlanAnualBundle\Entity\Actividad
     * @Expose
     */
    private $Actividad;

    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademica
     * @Expose
     */
    private $ActividadAcademica;

    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademicaEsquema
     * @Expose
     */
    private $ActividadAcademicaEsquema;

    /**
     * @var array
     * @Expose
     */
    private $control_acceso;

    /**
     * Get oferta_actividad_id
     *
     * @return integer
     */
    public function getOfertaActividadId()
    {
        return $this->oferta_actividad_id;
    }

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return OfertaActividad
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Set fecha_fin
     *
     * @param \DateTime $fechaFin
     * @return OfertaActividad
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;

        return $this;
    }

    /**
     * Get fecha_fin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /**
     * Set obligatoria
     *
     * @param boolean $obligatoria
     * @return OfertaActividad
     */
    public function setObligatoria($obligatoria)
    {
        $this->obligatoria = $obligatoria;

        return $this;
    }

    /**
     * Get obligatoria
     *
     * @return boolean
     */
    public function getObligatoria()
    {
        return $this->obligatoria;
    }

    /**
     * Set extemporanea
     *
     * @param boolean $extemporanea
     * @return OfertaActividad
     */
    public function setExtemporanea($extemporanea)
    {
        $this->extemporanea = $extemporanea;

        return $this;
    }

    /**
     * Get extemporanea
     *
     * @return boolean
     */
    public function getExtemporanea()
    {
        return $this->extemporanea;
    }

    /**
     * Set sincronizado
     *
     * @param boolean $sincronizado
     * @return OfertaActividad
     */
    public function setSincronizado($sincronizado)
    {
        $this->sincronizado = $sincronizado;

        return $this;
    }

    /**
     * Get puede_publicar
     *
     * @return boolean
     */
    public function getPuedePublicar()
    {
        return $this->puede_publicar;
    }

    /**
     * Set puede_publicar
     *
     * @param boolean $puede_publicar
     * @return OfertaActividad
     */
    public function setPuedePublicar($puede_publicar)
    {
        $this->puede_publicar = $puede_publicar;

        return $this;
    }

    /**
     * Get sincronizado
     *
     * @return boolean
     */
    public function getSincronizado()
    {
        return $this->sincronizado;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OfertaActividad
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OfertaActividad
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return OfertaActividad
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OfertaActividad
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set EscenarioOferta
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\EscenarioOferta $escenarioOferta
     * @return OfertaActividad
     */
    public function setEscenarioOferta(\ControlEscolar\CalendarioBundle\Entity\EscenarioOferta $escenarioOferta = null)
    {
        $this->EscenarioOferta = $escenarioOferta;

        return $this;
    }

    /**
     * Get EscenarioOferta
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\EscenarioOferta
     */
    public function getEscenarioOferta()
    {
        return $this->EscenarioOferta;
    }

    /**
     * Set Actividad
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Actividad $actividad
     * @return OfertaActividad
     */
    public function setActividad(\ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad = null)
    {
        $this->Actividad = $actividad;

        return $this;
    }

    /**
     * Get Actividad
     *
     * @return \ControlEscolar\PlanAnualBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->Actividad;
    }

    /**
     * Set ActividadAcademica
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica
     * @return OfertaActividad
     */
    public function setActividadAcademica(\Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica = null)
    {
        $this->ActividadAcademica = $actividadAcademica;

        return $this;
    }

    /**
     * Get ActividadAcademica
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademica
     */
    public function getActividadAcademica()
    {
        return $this->ActividadAcademica;
    }

    /**
     * Set ActividadAcademicaEsquema
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademicaEsquema $actividadAcademicaEsquema
     * @return OfertaActividad
     */
    public function setActividadAcademicaEsquema(\Core\CoreBundle\Entity\ActividadAcademicaEsquema $actividadAcademicaEsquema = null)
    {
        $this->ActividadAcademicaEsquema = $actividadAcademicaEsquema;

        return $this;
    }

    /**
     * Get ActividadAcademicaEsquema
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademicaEsquema
     */
    public function getActividadAcademicaEsquema()
    {
        return $this->ActividadAcademicaEsquema;
    }
    /**
     * @var boolean
     */
    private $activo;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return OfertaActividad
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /*
     * Función que setea el acceso de tipo obligatorio
     */
    public function addAccesoObligatorio($acceso)
    {
            $this->addAcceso($acceso,'obligatorio');
    }
    /*
     * Función que setea el acceso de tipo obligatorio
     */
    public function removeAccesoObligatorio($acceso)
    {
            $this->removeAcceso($acceso,'obligatorio');
    }

    /**
     * Función para remover un acceso
     *
     * @return Object OfertaActividad
     */
    public function removeAcceso($acceso,$tipo)
    {
        //Evakya su existe el elemento en el array de control de acceso de
        //acuerdo al tipo dado, lo elimina si existe
        if (false !== $key = array_search(strtoupper($acceso), $this->control_acceso[$tipo], true)) {
            $controlAuxiliar = $this->control_acceso[$tipo];
            unset($controlAuxiliar[$key]);
            $this->control_acceso[$tipo] = array_values($controlAuxiliar);
        }
        return $this;
    }

    /**
     * Regresa el acceso
     *
     * @return array control_acceso
     */
    public function getControlAcceso()
    {
        return $this->control_acceso;
    }



    /**
     * Settea un array de acceso completo el acceso
     *
     * @return Object OfertaActividad
     */
    public function setControlAcceso($controlAcceso)
    {
        $this->control_acceso = $controlAcceso;
        return $this;
    }

    /**
     * Función para agregar un acceso obligatorio
     *
     * @return
     */
     public function addAcceso($acceso,$tipo)
    {
        $acceso = strtoupper($acceso);
        if(in_array($tipo, $this->control_acceso, true)){
            $this->control_acceso[$tipo] = array();
        }

        if (!in_array($acceso, $this->control_acceso[$tipo], true)) {
                $this->control_acceso[$tipo][] = $acceso;
        }

        return $this;
    }

    /**
     * Settea un array de acceso completo el acceso con valores por default
     *
     * @return Object OfertaActividad
     */
    public function setDefaultControlAcceso()
    {
        $this->control_acceso = array(
            'obligatorio'=>array(),
            'solo_lectura'=>array()
            );
        return $this;
    }
}
