<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Movimiento
 * @ExclusionPolicy("all")
 */
class Movimiento
{
    /**
     * @var integer
     * @Expose
     */
    private $movimiento_id;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var string
     * @Expose
     */
    private $movimientos;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var integer
     * @Expose
     */
    private $referencia;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModificacion;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\TipoMovimiento
     * @Expose
     */
    private $TipoMovimiento;

    /**
     * @var \Core\CoreBundle\Entity\Tabla
     * @Expose
     */
    private $Tabla;


    /**
     * Get movimientoId
     *
     * @return integer
     */
    public function getMovimientoId()
    {
        return $this->movimiento_id;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Movimiento
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set movimientos
     *
     * @param string $movimientos
     *
     * @return Movimiento
     */
    public function setMovimientos($movimientos)
    {
        $this->movimientos = $movimientos;

        return $this;
    }

    /**
     * Get movimientos
     *
     * @return string
     */
    public function getMovimientos()
    {
        return $this->movimientos;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     *
     * @return Movimiento
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Movimiento
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set referencia
     *
     * @param integer $referencia
     *
     * @return Movimiento
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return integer
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set usuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     *
     * @return Movimiento
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get usuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set usuarioModificacion
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModificacion
     *
     * @return Movimiento
     */
    public function setUsuarioModificacion(\Core\UserBundle\Entity\Usuario $usuarioModificacion = null)
    {
        $this->UsuarioModificacion = $usuarioModificacion;

        return $this;
    }

    /**
     * Get usuarioModificacion
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModificacion()
    {
        return $this->UsuarioModificacion;
    }

    /**
     * Set tipoMovimiento
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\TipoMovimiento $tipoMovimiento
     *
     * @return Movimiento
     */
    public function setTipoMovimiento(\ControlEscolar\CalendarioBundle\Entity\TipoMovimiento $tipoMovimiento = null)
    {
        $this->TipoMovimiento = $tipoMovimiento;

        return $this;
    }

    /**
     * Get tipoMovimiento
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\TipoMovimiento
     */
    public function getTipoMovimiento()
    {
        return $this->TipoMovimiento;
    }

    /**
     * Set tabla
     *
     * @param \Core\CoreBundle\Entity\Tabla $tabla
     *
     * @return Movimiento
     */
    public function setTabla(\Core\CoreBundle\Entity\Tabla $tabla = null)
    {
        $this->Tabla = $tabla;

        return $this;
    }

    /**
     * Get tabla
     *
     * @return \Core\CoreBundle\Entity\Tabla
     */
    public function getTabla()
    {
        return $this->Tabla;
    }
}
