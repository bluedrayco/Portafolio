<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OfertaEducativaCentro
 * @ExclusionPolicy("all")
 */
class OfertaEducativaCentro
{
    /**
     * @var integer
     * @Expose
     */
    private $oferta_educativa_centro_id;
    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_fin;

    /**
     * @var string
     * @Expose
     */
    private $clave_centro;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var boolean
     * @Expose
     */
    private $publicado;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_sincronizacion;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_publicacion;

    /**
     * @var \ControlEscolar\PlanAnualBundle\Entity\Periodo
     * @Expose
     */
    private $Periodo;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var string
     * @Expose
     */
    private $nombre;

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return OfertaEducativaCentro
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Set fecha_fin
     *
     * @param \DateTime $fechaFin
     * @return OfertaEducativaCentro
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;

        return $this;
    }

    /**
     * Get fecha_fin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /**
     * Set clave_centro
     *
     * @param string $claveCentro
     * @return OfertaEducativaCentro
     */
    public function setClaveCentro($claveCentro)
    {
        $this->clave_centro = $claveCentro;

        return $this;
    }

    /**
     * Get clave_centro
     *
     * @return string
     */
    public function getClaveCentro()
    {
        return $this->clave_centro;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return OfertaEducativaCentro
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set publicado
     *
     * @param boolean $publicado
     * @return OfertaEducativaCentro
     */
    public function setPublicado($publicado)
    {
        $this->publicado = $publicado;

        return $this;
    }

    /**
     * Get publicado
     *
     * @return boolean
     */
    public function getPublicado()
    {
        return $this->publicado;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OfertaEducativaCentro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OfertaEducativaCentro
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set fecha_publicacion
     *
     * @param \DateTime $fechaPublicacion
     * @return OfertaEducativaCentro
     */
    public function setFechaPublicacion($fechaPublicacion)
    {
        $this->fecha_publicacion = $fechaPublicacion;

        return $this;
    }

    /**
     * Get fecha_publicacion
     *
     * @return \DateTime
     */
    public function getFechaPublicacion()
    {
        return $this->fecha_publicacion;
    }

    /**
     * Set Periodo
     *
     * @param \ControlEscolar\PlanAnualBundle\Entity\Periodo $periodo
     * @return OfertaEducativaCentro
     */
    public function setPeriodo(\ControlEscolar\PlanAnualBundle\Entity\Periodo $periodo = null)
    {
        $this->Periodo = $periodo;

        return $this;
    }

    /**
     * Get Periodo
     *
     * @return \ControlEscolar\PlanAnualBundle\Entity\Periodo
     */
    public function getPeriodo()
    {
        return $this->Periodo;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return OfertaEducativaCentro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OfertaEducativaCentro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }



    /**
     * Get oferta_educativa_centro_id
     *
     * @return integer
     */
    public function getOfertaEducativaCentroId()
    {
        return $this->oferta_educativa_centro_id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return OfertaEducativaCentro
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fecha_sincronizacion
     *
     * @param \DateTime $fechaSincronizacion
     * @return OfertaEducativa
     */
    public function setFechaSincronizacion($fechaSincronizacion)
    {
        $this->fecha_sincronizacion = $fechaSincronizacion;

        return $this;
    }

    /**
     * Get fecha_sincronizacion
     *
     * @return \DateTime
     */
    public function getFechaSincronizacion()
    {
        return $this->fecha_sincronizacion;
    }
}
