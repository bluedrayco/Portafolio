<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;
use Doctrine\ORM\Query\ResultSetMapping;
/**
 * Repositorio para generar consultas particulares en la tabla Ocupacion
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OcupacionRepository extends GenericRepository{

    /**
     * obtención de la última oferta educativa que contiene los siguientes casos: este en Estatus 1, Sincronizado en False y activo en True
     * @return mixed objeto OfertaEducativa solicitada
     */
    public function findOfertaEducativaActiva(){
        $dql="SELECT
              partial oe.{nombre,oferta_educativa_id,fecha_inicio,fecha_fin,sincronizado},oee,p
              FROM ControlEscolarCalendarioBundle:OfertaEducativa oe
                LEFT JOIN oe.OfertaEducativaEstatus oee
                LEFT JOIN oe.Periodo p
              WHERE
                oe.activo=true
                AND oe.sincronizado=false
                AND oee.oferta_educativa_estatus_id=1
                ORDER BY oe.oferta_educativa_id DESC";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setMaxResults(1);
        $escenariooferta = $query->getArrayResult();

        return (!$escenariooferta)?false:$escenariooferta[0];
    }

    /**
     * Buscando eventos de actividades calendarizadas desde centro.
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function findEventosForCalendar($params){
        $result = array();


        $dql            = "";/*" SELECT
                                    'centro'                                                                                                AS tipo,
                                    OCR.ocupacion_reserva_id                                                                                AS evento_id,
                                    OA.oferta_actividad_centro_id                                                                           AS oferta_actividad_id,
                                    OA.obligatoria                                                                                          AS obligatorio,
                                    OCR.fecha_evento                                                                                        AS start,
                                    O.fecha_fin                                                                                             AS fecha_final,
                                    AU.aula_id                                                                                              AS aula_id,
                                    FA.facilitador_id                                                                                       AS facilitador_id,
                                    OCR.hora_inicio,
                                    OCR.hora_fin,




                                    CASE WHEN A IS null THEN AA.nombre                            ELSE A.nombre                       END   AS title,
                                    CASE WHEN A IS null THEN AA.descripcion                       ELSE A.descripcion                  END   AS descripcion,
                                    CASE WHEN A IS null THEN AAC.nombre                           ELSE AC.nombre                      END   AS categoria,
                                    CASE WHEN A IS null THEN AAC.actividad_academica_categoria_id ELSE AC.categoria_id                END   AS categoria_id,
                                    CASE WHEN A IS null THEN CONCAT(AAC.estilo,'')                ELSE CONCAT(AC.estilo,'')           END   AS className,
                                    CASE WHEN A IS null THEN true                                 ELSE false                          END   AS es_academica,
                                    CASE WHEN A IS null THEN AA.actividad_academica_id            ELSE A.actividad_id                 END   AS id_actividad,
                                    CASE WHEN A IS null THEN OAE.actividad_academica_esquema_id   ELSE 0                              END   AS esquema_id,
                                    CASE WHEN :oferta_educativa_centro_id <> OEC.oferta_educativa_centro_id THEN true ELSE false      END   AS externo
                            FROM
                                    ControlEscolarCalendarioBundle:OcupacionReserva                 OCR
                            LEFT JOIN
                                    OCR.Ocupacion                                                   O
                            LEFT JOIN
                                    O.OfertaActividadCentro                                         OA
                            LEFT JOIN
                                    OA.ActividadAcademicaEsquema                                    OAE
                            LEFT JOIN
                                    OA.OfertaEducativaCentro                                        OEC
                            LEFT JOIN
                                    OA.ActividadAcademica                                           AA
                            LEFT JOIN
                                    OA.Actividad                                                    A
                            LEFT JOIN
                                    AA.ActividadAcademicaCategoria                                  AAC
                            LEFT JOIN
                                    A.ActividadCategoria                                            AC
                            LEFT JOIN
                                    O.Aula                                                          AU
                            LEFT JOIN
                                    OCR.Facilitador                                                 FA

                            WHERE
                                    (OCR.fecha_evento  BETWEEN :fecha_inicio AND :fecha_final)
                                AND
                                    OCR.activo= true

                          ";*/
        /** Evaluamos si es filtrado por facilitador_id o aula_id */

        if($params["facilitador_id"]!=null){
            $dql .=  "  AND FA.facilitador_id = :facilitador_id    ";
        }

        if($params["aula_id"]!=null){
            $dql .=  " AND AU.aula_id = :aula_id    ";
        }

        //echo $dql;

        /** Fin de Evaluar si es filtrado por facilitador_id o aula_id */




    $query              = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('oferta_educativa_centro_id'                   , $params["oferta_educativa_centro_id"]);
    $query->setParameter('fecha_inicio'                                 , $params["fecha_inicio"]);
    $query->setParameter('fecha_final'                                  , $params["fecha_final"]);

    if($params["facilitador_id"]!=null){
        $query->setParameter('facilitador_id'                           , $params["facilitador_id"]);
    }

    if($params["aula_id"]!=null){
        $query->setParameter('aula_id'                                  , $params["aula_id"]);
    }


    //echo $query->getSQL();
    $result = $query->getArrayResult();





    $dql2               = " SELECT

                                    OAH.hora_inicio                         AS hora_inicio,
                                    OAH.hora_fin                            AS hora_fin,
                                    OAH.dia_semana                          AS dia_semana

                            FROM
                                    ControlEscolarCalendarioBundle:HorarioOfertaCentro OAH

                            LEFT JOIN
                                    OAH.OfertaActividadCentro                          OA
                            WHERE
                                    OA.oferta_actividad_centro_id=:oferta_actividad_centro_id
                         ";
        $query2  = $this->getEntityManager()->createQuery($dql2);



        foreach ($result as $key => $value){

            $fecha_evento                                                   = $result[$key]["start"]->format("Y-m-d ");
            $result[$key]["hora_inicio"]                                    = $result[$key]["hora_inicio"]->format("H:i");
            $result[$key]["hora_fin"]                                       = $result[$key]["hora_fin"]->format("H:i");
            $result[$key]["start"]                                          = $fecha_evento . $result[$key]["hora_inicio"];
            $result[$key]["end"]                                            = $fecha_evento . $result[$key]["hora_fin"];
            $result[$key]["durationEditable"]                               = false;
            $result[$key]["editable"]                                       = false;


            $query2->setParameter('oferta_actividad_centro_id',$result[$key]['oferta_actividad_id']);
            $result[$key]["_horarios"]                                      = $query2->getArrayResult();
            foreach($result[$key]["_horarios"] as &$obj){
                $obj["hora_inicio"]                                         = $obj["hora_inicio"]->format("H:i");
                $obj["hora_fin"]                                            = $obj["hora_fin"]->format("H:i");
            }

            //obtener los horarios de la actividad

        }

    //print_r($result);
    //exit();

    return $result;
    }

    public function findEventosByOcupacionId($obj){
        //obtener eventos OcupacionReserva
        $dql='SELECT ORES'
           . ' FROM ControlEscolarCalendarioBundle:OcupacionReserva ORES '
           . ' LEFT JOIN ORES.Ocupacion O '
           . ' WHERE O.ocupacion_id=:ocupacion_id'
           . ' AND O.activo=true'
           . ' AND ORES.activo=true';
        $query              = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ocupacion_id'                   , $obj["ocupacion_id"]);
        $eventos = $query->getResult();

        //obtener los Horarios de la Ocupacion
        $dql2= 'SELECT OH'
                . ' FROM ControlEscolarCalendarioBundle:OcupacionHorario OH '
                . ' LEFT JOIN OH.Ocupacion O '
                . ' WHERE O.ocupacion_id=:ocupacion_id'
                . ' AND O.activo=true'
                . ' AND OH.activo=true';
        $query2              = $this->getEntityManager()->createQuery($dql2);
        $query2->setParameter('ocupacion_id'                   , $obj["ocupacion_id"]);
        $horarios = $query2->getResult();

        //obtener la Ocupacion
        $dql3= 'SELECT O'
                . ' FROM ControlEscolarCalendarioBundle:Ocupacion O '
                . ' WHERE O.ocupacion_id=:ocupacion_id'
                . ' AND O.activo=true';
        $query3              = $this->getEntityManager()->createQuery($dql3);
        $query3->setParameter('ocupacion_id'                   , $obj["ocupacion_id"]);
        $ocupacion = $query3->getResult();

        $eventosIds=array();
        foreach($eventos as $evento){
            $eventosIds[]=$evento->getReservaId();
        }
        //obtener los eventos de reserva20
        $dql4= 'SELECT C'
                . ' FROM ControlEscolarReservaBundle:Calendario C'
                . ' WHERE C.calendario_id IN (:reserva_id)';
        $query4              = $this->getEntityManager()->createQuery($dql4);
        $query4->setParameter('reserva_id'                   , $eventosIds);
        $calendarios = $query4->getResult();

        $result =array();
        $result = array_merge($ocupacion,$result);
        $result = array_merge($horarios,$result);
        $result = array_merge($eventos,$result);
        $result = array_merge($calendarios,$result);

        return $result;
    }


    public  function findStoreProcedureCancelacion($params){

        try{
                $sql       = "SELECT
                                *
                              FROM calendario.cancela_ocupacion(?)";

                $rsm       = new ResultSetMapping();
                $query     = $this->getEntityManager()->createNativeQuery($sql,$rsm);
                $query->setParameter(1, $params["ocupacion_id"]);
                $result    =$query->getResult();
        }
        catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
        return $result;
    }

     /**
     * Se obtiene toda la oferta educativa programada para el reporte de grupos en CE CENTRAL
     * @author Isaed Martinez, Luis González
     * @fecha 2015-07-08
     * @return
     */
    public function findReporteGrupos($obj){
        $GLOBALS['kernel']->getContainer()->get('logger')->info("\nEntro a FINDRPTGPOS");
        try {

            $parametros=array();
            //echo $parametros['nombre_centro']."OK";
            //echo $obj['like'];

                $sql = "
                      SELECT '".$obj['nombre_centro']."' as nombre_centro,
                       CASE
                            WHEN ccaa.nombre is null THEN pca.nombre
                            ELSE ccaa.nombre
                        END curso_nombre,
                        CASE
                            WHEN ccaa.nombre is null THEN 'Actividad No Academica'
                            ELSE 'Actividad Academica'
                        END tipo_actividad,
                        cco.clave_grupo clave_grupo,
                        TRIM(ccf.nombre) || ' ' || TRIM(ccf.apellido_paterno) || ' ' || TRIM(ccf.apellido_materno) nombre_facilitador,
                        cca.nombre aula_nombre,
                        cco.fecha_inicio::date fecha_inicio,
                        cco.fecha_fin::date fecha_fin,
                        ARRAY_TO_STRING( ARRAY_AGG(ctoh.dia_semana || '=>' || TO_CHAR(ctoh.hora_inicio, 'HH24:MI') || ' - ' || TO_CHAR(ctoh.hora_fin, 'HH24:MI')),',' ) dias_de_curso,
                        CASE
                            WHEN cco.fecha_fin::date < now()::date THEN 'Grupo finalizado'
                            WHEN cco.fecha_inicio::date > now()::date THEN 'Grupo próximo'
                            WHEN cco.fecha_inicio::date <= now()::date AND now()::date <= cco.fecha_fin::date THEN 'Grupo iniciado'
                        END as estatus_curso
                      FROM
                        calendario.cat_ocupacion cco
                      LEFT JOIN comun.cat_actividad_academica ccaa ON (ccaa.actividad_academica_id = cco.actividad_academica_id)
                      LEFT JOIN calendario.cat_aula cca ON (cca.aula_id = cco.aula_id)
                      LEFT JOIN calendario.cat_facilitador ccf ON (ccf.facilitador_id = cco.facilitador_id)
                      LEFT JOIN pa.cat_actividad pca ON (pca.actividad_id = cco.actividad_id)
                      LEFT JOIN calendario.tra_ocupacion_horario ctoh ON (ctoh.ocupacion_id = cco.ocupacion_id)
                      WHERE cco.activo = 'true'
                    ";

            if(array_key_exists('like', $obj) && $obj['like'] != ''){
                $parametros['megusta'] = "%".$obj['like']."%";
                $sql = $sql."AND
                        (LOWER(ccaa.nombre) LIKE LOWER(:megusta) OR LOWER(pca.nombre) LIKE LOWER(:megusta) OR
                        LOWER(TRIM(ccf.nombre) || ' ' || TRIM(ccf.apellido_paterno) || ' ' || TRIM(ccf.apellido_materno)) LIKE LOWER(:megusta) OR
                        LOWER(cco.clave_grupo) LIKE LOWER(:megusta) OR LOWER(cca.nombre) LIKE LOWER(:megusta)
                        )
                        ";
            }else if(array_key_exists('where', $obj)){
                $parametros['whereis'] = $obj['where'];
                $sql = $sql."AND
                        cco.fecha_inicio <= :whereis
                        ";
            }
            if(array_key_exists('search', $obj)){

                if($obj['search'] == "finalizado"){
                    $sql.=" AND cco.fecha_fin::date < now()::date
                    ";
                }else if($obj['search'] == "proximo"){
                    $sql.=" AND cco.fecha_inicio::date > now()::date
                    ";
                }else if($obj['search'] == "iniciado"){
                    $sql.=" AND cco.fecha_inicio::date <= now()::date AND now()::date <= cco.fecha_fin::date
                    ";
                }
            }
            $sql = $sql."GROUP BY
                    cco.ocupacion_id,
                    ccaa.nombre,
                    pca.nombre,
                    cca.nombre,
                    TRIM(ccf.nombre) || ' ' || TRIM(ccf.apellido_paterno) || ' ' || TRIM(ccf.apellido_materno),
                    cco.fecha_inicio::date,
                    cco.fecha_fin::date,
                    cco.clave_grupo,
                    cco.activo
                ";
            if(array_key_exists('order', $obj)){
                switch ($obj['order']) {
                    case 'clave_grupo':
                        $sql.="ORDER BY cco.clave_grupo ";
                        break;
                    case 'curso_nombre':
                        $sql.="ORDER BY ccaa.nombre ";
                        break;
                    case 'nombre_facilitador':
                        $sql.="ORDER BY (TRIM(ccf.nombre) || ' ' || TRIM(ccf.apellido_paterno) || ' ' || TRIM(ccf.apellido_materno)) ";
                        break;
                    case 'aula_nombre':
                        $sql.="ORDER BY cca.nombre ";
                        break;
                    case 'fecha_inicio':
                        $sql.="ORDER BY cco.fecha_inicio ";
                        break;
                    case 'fecha_fin':
                        $sql.="ORDER BY cco.fecha_fin ";
                        break;
                    
                }
                if($obj['reverse'] == "true"){
                    $sql.=" DESC
                    ";
                }
            }

            if(array_key_exists('limit', $obj)){
                $parametros['limite'] = $obj['limit'];
                $sql = $sql."LIMIT :limite
                        ";
            }
            if(array_key_exists('offset', $obj)){
                $parametros['inicio'] = $obj['offset'];
                $sql = $sql."OFFSET :inicio
                        ";
            }
            $sql = $sql.";";

            $stmt = $this->getEntityManager()->getConnection()->prepare($sql);

            $stmt->execute($parametros);
            $result = $stmt->fetchAll();
            //$result['sql'] = $sql;
            if(array_key_exists('count', $obj)){
                $result = count($result);
            }

         }
        catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

     /**
     * Se obtiene toda la oferta educativa programada para el reporte de grupos en CE CENTRAL
     * @author Isaed Martinez, Luis González
     * @fecha 2015-07-08
     * @modify samuel.abad@enova.mx 2015-12-17
     * @return
     */
    public function findReporteGruposByFacilitadores($obj){
        try {
            $parametros=array();

            $sql = "
                    SELECT
                ccf.facilitador_id,
                TRIM(ccf.nombre) || ' ' || TRIM(ccf.apellido_paterno) || ' ' || TRIM(ccf.apellido_materno) nombre_facilitador,
                SUM(CASE WHEN EXTRACT(YEAR FROM cco.fecha_inicio) = EXTRACT(YEAR FROM NOW()) AND EXTRACT(MONTH FROM cco.fecha_inicio) = EXTRACT(MONTH FROM NOW()) THEN 1 ELSE 0 END) tot_mes_actual,
                SUM(CASE WHEN EXTRACT(YEAR FROM cco.fecha_inicio) = EXTRACT(YEAR FROM NOW()) AND EXTRACT(MONTH FROM cco.fecha_inicio) > EXTRACT(MONTH FROM NOW()) AND cco.fecha_inicio::date >=  NOW()  THEN 1 ELSE 0 END) proximos_meses,
                SUM(CASE WHEN cco.fecha_inicio::date >=  (NOW() - INTERVAL '2 Months')::date AND cco.fecha_inicio::date < date_trunc('month', NOW())::date THEN 1 ELSE 0 END) tot_hace_2_meses,
                SUM(CASE WHEN cco.fecha_inicio::date >=  (NOW() - INTERVAL '5 Months')::date AND cco.fecha_inicio::date < date_trunc('month', NOW())::date THEN 1 ELSE 0 END) tot_ult_6_meses,
                SUM(CASE WHEN EXTRACT(YEAR FROM cco.fecha_inicio) =  EXTRACT(YEAR FROM NOW()) THEN 1 ELSE 0 END) tot_este_anio,
                SUM(CASE WHEN cco.fecha_inicio IS NOT NULL THEN 1 ELSE 0 END) totales
              FROM
                calendario.cat_ocupacion cco
              LEFT JOIN comun.cat_actividad_academica ccaa ON (ccaa.actividad_academica_id = cco.actividad_academica_id)
              LEFT JOIN pa.cat_actividad pca ON (pca.actividad_id = cco.actividad_id)
              LEFT JOIN calendario.cat_aula cca ON (cca.aula_id = cco.aula_id)
              LEFT JOIN calendario.cat_facilitador ccf ON (ccf.facilitador_id = cco.facilitador_id)
              WHERE cco.activo
            ";
            if(array_key_exists('like', $obj)){
                $parametros['megusta'] = "%".$obj['like']."%";
                $sql = $sql."AND
                        (LOWER(TRIM(ccf.nombre) || ' ' || TRIM(ccf.apellido_paterno) || ' ' || TRIM(ccf.apellido_materno)) LIKE LOWER(:megusta)
                        )
                        ";
            }
            $sql = $sql."GROUP BY ccf.facilitador_id
            ";
            if(array_key_exists('order', $obj)){
                if($obj['order'] == "nombre_facilitador"){
                    $sql =$sql." ORDER BY ccf.nombre ";

                }
                if($obj['reverse'] == "true"){
                    $sql = $sql." DESC
                    ";

                }
            }

            if(array_key_exists('limit', $obj)){
                $parametros['limite'] = $obj['limit'];
                $sql = $sql." LIMIT :limite
                        ";
            }
            if(array_key_exists('offset', $obj)){
                $parametros['inicio'] = $obj['offset'];
                $sql = $sql." OFFSET :inicio
                        ";
            }
            $sql = $sql.";";

            $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
            $stmt->execute($parametros);

            $result = $stmt->fetchAll();
            if(array_key_exists('count', $obj)){
                $result = count($result);
            }
         }
        catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

     /**
     * Se obtiene toda la oferta educativa programada para el reporte de grupos en CE CENTRAL
     * @author Luis González
     * @fecha 2015-07-08
     * @modify samuel.abad@enova.mx 2015-12-17
     * @return
     */
    public function findReporteGruposByFacilitador($obj){
        try {
            $parametros=array();

            $sql = "
                    SELECT '".$obj['nombre_centro']."' as nombre_centro,
                    CASE
                    WHEN ccaa.nombre is null THEN pca.nombre
                    ELSE ccaa.nombre
                    END curso_nombre,
                cco.clave_grupo clave_grupo,
                cco.fecha_inicio::date fecha_inicio,
                cco.fecha_fin::date fecha_fin
              FROM
                calendario.cat_ocupacion cco
              LEFT JOIN comun.cat_actividad_academica ccaa ON (ccaa.actividad_academica_id = cco.actividad_academica_id)
              LEFT JOIN calendario.cat_aula cca ON (cca.aula_id = cco.aula_id)
              LEFT JOIN pa.cat_actividad pca ON (pca.actividad_id = cco.actividad_id)
              LEFT JOIN calendario.cat_facilitador ccf ON (ccf.facilitador_id = cco.facilitador_id)
              WHERE cco.activo AND ccf.facilitador_id =
            ";
            $sql .= $obj['facilitador_id'];
            switch ( trim($obj['tipo_grupo']) ) {
                case 'tot_mes_actual':
                    $sql .= " AND EXTRACT(YEAR FROM cco.fecha_inicio) = EXTRACT(YEAR FROM NOW()) AND EXTRACT(MONTH FROM cco.fecha_inicio) = EXTRACT(MONTH FROM NOW()) ";
                    break;
                case 'proximos_meses':
                    $sql .= " AND EXTRACT(YEAR FROM cco.fecha_inicio) = EXTRACT(YEAR FROM NOW()) AND EXTRACT(MONTH FROM cco.fecha_inicio) > EXTRACT(MONTH FROM NOW()) AND cco.fecha_inicio::date >=  NOW() ";
                    break;
                case 'tot_hace_2_meses':
                    $sql .= " AND cco.fecha_inicio::date >=  NOW() - INTERVAL '2 Months' AND cco.fecha_inicio::date < date_trunc('month', NOW())::date ";
                    break;
                case 'tot_ult_6_meses':
                    $sql .= " AND cco.fecha_inicio::date >=  NOW() - INTERVAL '5 Months' AND cco.fecha_inicio::date < date_trunc('month', NOW())::date ";
                    break;
                case 'tot_este_anio':
                    $sql .= " AND EXTRACT(YEAR FROM cco.fecha_inicio) =  EXTRACT(YEAR FROM NOW()) ";
                    break;
                case 'totales':
                    $sql .= " AND cco.fecha_inicio IS NOT NULL";
                    break;
            }
            if(array_key_exists('like', $obj)){
                $parametros['megusta'] = "%".$obj['like']."%";
                $sql = $sql." AND
                        (LOWER(ccaa.nombre) LIKE LOWER(:megusta))
                        ";
            }else if(array_key_exists('where', $obj)){
                $parametros['whereis'] = $obj['where'];
                $sql = $sql." AND
                        cco.fecha_inicio <= :whereis
                        ";
            }
            else if(array_key_exists('order', $obj)){
                if($obj['order']=="fecha_inicio"){
                    $sql.=" ORDER BY cco.fecha_inicio ";
                }else if($obj['order']=="fecha_fin"){
                    $sql.=" ORDER BY cco.fecha_fin ";
                }else if($obj['order']=="clave_grupo"){
                    $sql.=" ORDER BY ccaa.clave ";
                }else if($obj['order']=="curso_nombre"){
                    $sql.=" ORDER BY ccaa.nombre ";
                }

                if($obj['reverse']=="true"){
                    $sql .=" DESC ";
                }
            }

            if(array_key_exists('limit', $obj)){
                $parametros['limite'] = $obj['limit'];
                $sql = $sql." LIMIT :limite
                        ";
            }
            if(array_key_exists('offset', $obj)){
                $parametros['inicio'] = $obj['offset'];
                $sql = $sql." OFFSET :inicio
                        ";
            }


            $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
            $stmt->execute($parametros);

            $result = $stmt->fetchAll();
            if(array_key_exists('count', $obj)){
                $result = count($result);
            }

         }
        catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

}

/*
*/
