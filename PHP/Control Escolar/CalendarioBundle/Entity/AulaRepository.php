<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla Aula
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class AulaRepository extends GenericRepository{ 

    public function findAulaNoInArray($params){
        $dql    = " SELECT  
                            A
                    FROM 
                            ControlEscolarCalendarioBundle:Aula A

                    WHERE   A.activo  = true 

                    AND
                            A.aula_id NOT IN (:aulas)
                 ";

        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('aulas',$params["aulas"]);
        return $query->getArrayResult();
    }

}
