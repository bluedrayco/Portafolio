<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;
use \DateTime;

/**
 * Repositorio para generar consultas particulares en la tabla Oferta Actividad
 * @author Silvio Bravo Cado <silvio.bravo@enova.mx>
 */
class EventoActividadOfertaRepository extends GenericRepository{

    /**
     * Encontramos los Eventos de las las Actividades a partir de un Escenario y una Oferta Educativa
     * @param  [array] $params [Array que debe contener el key "escenario_id, oferta_educativa_id, fecha_inicio, fecha_final"]
     * @return [array]         [Array de Actividades (solo los datos basicos)], como mejora se coloco el horario de la Actividad
     */
    public function findEventosForCalendar($params){
        $result = array();
        $dql    = " SELECT

                            EA.fecha_evento     AS start,
                            EA.evento_actividad_oferta_id,
                            EA.hora_inicio,
                            EA.hora_fin,
                            CASE WHEN TE.clave ='todos' THEN false  ELSE CASE WHEN TE.clave ='grupo'  THEN false ELSE true END END  AS escenario_individual,
                            CASE WHEN TE.clave ='todos' THEN true   ELSE false END                                                  AS escenario_todos,
                            TE.clave            AS tipo_escenario,
                            ES.escenario_id,
                            OA.obligatoria      AS obligatorio,
                            OA.extemporanea     AS extemporanea,
                            OA.fecha_fin        AS fecha_final,
                            OA.fecha_inicio        AS fecha_inicio,
                            OA.oferta_actividad_id,
                            OA.puede_publicar      AS puede_publicar,
                            OA.sincronizado        AS sincronizado,
                            OA.control_acceso      AS control_acceso,


                            CASE WHEN A.actividad_id IS null THEN AA.nombre                            ELSE A.nombre                       END AS title,
                            CASE WHEN A.actividad_id IS null THEN AA.descripcion                       ELSE A.descripcion                  END AS descripcion,
                            CASE WHEN A.actividad_id IS null THEN AAC.nombre                           ELSE AC.nombre                      END AS categoria,
                            CASE WHEN A.actividad_id IS null THEN CONCAT(AAC.estilo,'')                ELSE CONCAT(AC.estilo,'')           END AS className,
                            CASE WHEN A.actividad_id IS null THEN true                                 ELSE false                          END AS es_academica,

                            CASE WHEN A.actividad_id IS null THEN AA.actividad_academica_id            ELSE A.actividad_id                 END AS id_actividad,
                            CASE WHEN A.actividad_id IS null THEN OAE.actividad_academica_esquema_id   ELSE 0                              END AS esquema_id,
                            CASE WHEN :oferta_educativa_id <> OFED.oferta_educativa_id THEN true ELSE false                   END AS externo,
                            CASE WHEN :oferta_educativa_id <> OFED.oferta_educativa_id THEN OFED.nombre ELSE 'false' END   AS nombreOfertaExterna

                    FROM
                            ControlEscolarCalendarioBundle:EventoActividadOferta EA

                    LEFT JOIN
                        EA.OfertaActividad              OA
                    LEFT JOIN
                        OA.ActividadAcademicaEsquema    OAE
                    LEFT JOIN
                        OA.EscenarioOferta              EO
                    LEFT JOIN
                        EO.Escenario                    ES
                    LEFT JOIN
                        ES.TipoEscenario                TE
                    LEFT JOIN
                        EO.OfertaEducativa              OFED
                    LEFT JOIN
                        OA.ActividadAcademica           AA
                    LEFT JOIN
                        OA.Actividad                    A
                    LEFT JOIN
                        AA.ActividadAcademicaCategoria  AAC
                    LEFT JOIN
                        A.ActividadCategoria            AC

                    WHERE
                        (EA.fecha_evento BETWEEN :fecha_inicio AND :fecha_final)
                    AND
                        ES.escenario_id          IN (:escenario_ids)
                  ";

        $query  = $this->getEntityManager()->createQuery($dql);

        $query->setParameter('escenario_ids'        , $params["escenario_ids"]);
        $query->setParameter('oferta_educativa_id'  , $params["oferta_educativa_id"]);
        $query->setParameter('fecha_inicio'         , $params["fecha_inicio"]);
        $query->setParameter('fecha_final'          , $params["fecha_final"]);

        $result = $query->getArrayResult();

        //creamos el segundo query para o btener los horarios de las ofertas actividades

        $dql2    = " SELECT

                            OAH.hora_inicio    AS hora_inicio,
                            OAH.hora_fin       AS hora_fin,
                            OAH.dia_semana     AS dia_semana

                    FROM
                            ControlEscolarCalendarioBundle:OfertaActividadHorario OAH

                    LEFT JOIN
                            OAH.OfertaActividad              OA
                    WHERE
                            OA.oferta_actividad_id          =:oferta_actividad_id
                            AND
                            OAH.activo = true";
        $query2  = $this->getEntityManager()->createQuery($dql2);

        /**
         * Ajustamos el array de salida para que contenga los parametros exactos que se esperan en el calendario :)
         */
        foreach ($result as $key => $value) {
            $fecha_evento                     = $result[$key]["start"]->format("Y-m-d ");
            $result[$key]["hora_inicio"]      = $result[$key]["hora_inicio"]->format("H:i");
            $result[$key]["hora_fin"]         = $result[$key]["hora_fin"]->format("H:i");
            $result[$key]["start"]            = $fecha_evento . $result[$key]["hora_inicio"];
            $result[$key]["end"]              = $fecha_evento . $result[$key]["hora_fin"];
            $result[$key]["durationEditable"] = false;
            $result[$key]["editable"]         = false;

            //obtener los horarios de la actividad
            $query2->setParameter('oferta_actividad_id', $result[$key]['oferta_actividad_id']);
            $result[$key]["_horarios"] = $query2->getArrayResult();

            foreach ($result[$key]["_horarios"] as &$obj) {
                $obj["hora_inicio"]        = $obj["hora_inicio"]->format("H:i");
                $obj["hora_fin"]           = $obj["hora_fin"]->format("H:i");
            }
        }

        return $result;
    }


    /**
     * Encontramos los Eventos de las las Actividades a partir de un Escenario y una Oferta Educativa
     * @param  [array] $params [Array que debe contener el key "escenario_id, oferta_educativa_id, fecha_inicio, fecha_final"]
     * @return [array]         [Array de Actividades (solo los datos basicos)], como mejora se coloco el horario de la Actividad
     */
    public function findEventosForCalendarTodos($params){
        $result = array();
        $dql    = " SELECT

                            EA.fecha_evento     AS start,
                            EA.evento_actividad_oferta_id,
                            EA.hora_inicio,
                            EA.hora_fin,
                            CASE WHEN TE.clave ='todos' THEN false  ELSE CASE WHEN TE.clave ='grupo'  THEN false ELSE true END END  AS escenario_individual,
                            CASE WHEN TE.clave ='todos' THEN true   ELSE false END                                                  AS escenario_todos,
                            TE.clave            AS tipo_escenario,
                            ES.escenario_id,
                            OA.obligatoria      AS obligatorio,
                            OA.control_acceso      AS control_acceso,
                            OA.fecha_fin        AS fecha_final,
                            OA.fecha_inicio        AS fecha_inicio,
                            OA.oferta_actividad_id,



                            CASE WHEN A.actividad_id IS null THEN AA.nombre                            ELSE A.nombre                       END AS title,
                            CASE WHEN A.actividad_id IS null THEN AA.descripcion                       ELSE A.descripcion                  END AS descripcion,
                            CASE WHEN A.actividad_id IS null THEN AAC.nombre                           ELSE AC.nombre                      END AS categoria,
                            CASE WHEN A.actividad_id IS null THEN CONCAT(AAC.estilo,'')                ELSE CONCAT(AC.estilo,'')           END AS className,
                            CASE WHEN A.actividad_id IS null THEN true                                 ELSE false                          END AS es_academica,

                            CASE WHEN A.actividad_id IS null THEN AA.actividad_academica_id            ELSE A.actividad_id                 END AS id_actividad,
                            CASE WHEN A.actividad_id IS null THEN OAE.actividad_academica_esquema_id   ELSE 0                              END AS esquema_id,
                            CASE WHEN :oferta_educativa_id <> OFED.oferta_educativa_id THEN true ELSE false                   END AS externo,
                            CASE WHEN :oferta_educativa_id <> OFED.oferta_educativa_id THEN OFED.nombre ELSE 'false' END   AS nombreOfertaExterna

                    FROM
                            ControlEscolarCalendarioBundle:EventoActividadOferta EA

                    LEFT JOIN
                        EA.OfertaActividad              OA
                    LEFT JOIN
                        OA.ActividadAcademicaEsquema    OAE
                    LEFT JOIN
                        OA.EscenarioOferta              EO
                    LEFT JOIN
                        EO.Escenario                    ES
                    LEFT JOIN
                        ES.TipoEscenario                TE
                    LEFT JOIN
                        EO.OfertaEducativa              OFED
                    LEFT JOIN
                        OA.ActividadAcademica           AA
                    LEFT JOIN
                        OA.Actividad                    A
                    LEFT JOIN
                        AA.ActividadAcademicaCategoria  AAC
                    LEFT JOIN
                        A.ActividadCategoria            AC

                    WHERE
                        (EA.fecha_evento BETWEEN :fecha_inicio AND :fecha_final)
                    AND
                        ES.escenario_id          IN (:escenario_ids)
                  ";

        $query  = $this->getEntityManager()->createQuery($dql);

        $query->setParameter('escenario_ids'        , $params["escenario_ids"]);
        $query->setParameter('oferta_educativa_id'  , $params["oferta_educativa_id"]);
        $query->setParameter('fecha_inicio'         , $params["fecha_inicio"]);
        $query->setParameter('fecha_final'          , $params["fecha_final"]);

        $result = $query->getArrayResult();

        //creamos el segundo query para o btener los horarios de las ofertas actividades

        $dql2    = " SELECT

                            OAH.hora_inicio    AS hora_inicio,
                            OAH.hora_fin       AS hora_fin,
                            OAH.dia_semana     AS dia_semana

                    FROM
                            ControlEscolarCalendarioBundle:OfertaActividadHorario OAH

                    LEFT JOIN
                            OAH.OfertaActividad              OA
                    WHERE
                            OA.oferta_actividad_id          =:oferta_actividad_id
                            AND
                            OAH.activo = true";
        $query2  = $this->getEntityManager()->createQuery($dql2);

        /**
         * Ajustamos el array de salida para que contenga los parametros exactos que se esperan en el calendario :)
         */
        foreach ($result as $key => $value) {
            $fecha_evento                     = $result[$key]["start"]->format("Y-m-d ");
            $result[$key]["hora_inicio"]      = $result[$key]["hora_inicio"]->format("H:i");
            $result[$key]["hora_fin"]         = $result[$key]["hora_fin"]->format("H:i");
            $result[$key]["start"]            = $fecha_evento . $result[$key]["hora_inicio"];
            $result[$key]["end"]              = $fecha_evento . $result[$key]["hora_fin"];
            $result[$key]["durationEditable"] = false;
            $result[$key]["editable"]         = false;

            //obtener los horarios de la actividad
            $query2->setParameter('oferta_actividad_id', $result[$key]['oferta_actividad_id']);
            $result[$key]["_horarios"] = $query2->getArrayResult();

            foreach ($result[$key]["_horarios"] as &$obj) {
                $obj["hora_inicio"]        = $obj["hora_inicio"]->format("H:i");
                $obj["hora_fin"]           = $obj["hora_fin"]->format("H:i");
            }
        }

        return $result;
    }


}
