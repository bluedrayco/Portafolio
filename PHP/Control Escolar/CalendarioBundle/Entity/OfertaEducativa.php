<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OfertaEducativa
 * @ExclusionPolicy("all")
 */
class OfertaEducativa
{
    /**
     * @var integer
     * @Expose
     */
    private $oferta_educativa_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_fin;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_sincronizacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var string
     * @Expose
     */
    private $nombre;

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return OfertaEducativa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Get oferta_educativa_id
     *
     * @return integer 
     */
    public function getOfertaEducativaId()
    {
        return $this->oferta_educativa_id;
    }

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return OfertaEducativa
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Set fecha_sincronizacion
     *
     * @param \DateTime $fechaSincronizacion
     * @return OfertaEducativa
     */
    public function setFechaSincronizacion($fechaSincronizacion)
    {
        $this->fecha_sincronizacion = $fechaSincronizacion;

        return $this;
    }

    /**
     * Get fecha_sincronizacion
     *
     * @return \DateTime 
     */
    public function getFechaSincronizacion()
    {
        return $this->fecha_sincronizacion;
    }
    
    /**
     * Set fecha_fin
     *
     * @param \DateTime $fechaFin
     * @return OfertaEducativa
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;

        return $this;
    }

    /**
     * Get fecha_fin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return OfertaEducativa
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OfertaEducativa
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OfertaEducativa
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return OfertaEducativa
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OfertaEducativa
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
    /**
     * @var \ControlEscolar\PlanAnualBundle\Entity\Periodo
     * @Expose
     */
    private $Periodo;


    /**
     * Set Periodo
     *
     * @param \ControlEscolar\PlanAnualBundle\Entity\Periodo $periodo
     * @return OfertaEducativa
     */
    public function setPeriodo(\ControlEscolar\PlanAnualBundle\Entity\Periodo $periodo = null)
    {
        $this->Periodo = $periodo;

        return $this;
    }

    /**
     * Get Periodo
     *
     * @return \ControlEscolar\PlanAnualBundle\Entity\Periodo 
     */
    public function getPeriodo()
    {
        return $this->Periodo;
    }
    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;


    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return OfertaEducativa
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }
    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaEstatus
     * @Expose
     */
    private $OfertaEducativaEstatus;


    /**
     * Set OfertaEducativaEstatus
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaEstatus $ofertaEducativaEstatus
     * @return OfertaEducativa
     */
    public function setOfertaEducativaEstatus(\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaEstatus $ofertaEducativaEstatus = null)
    {
        $this->OfertaEducativaEstatus = $ofertaEducativaEstatus;

        return $this;
    }

    /**
     * Get OfertaEducativaEstatus
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaEstatus 
     */
    public function getOfertaEducativaEstatus()
    {
        return $this->OfertaEducativaEstatus;
    }
    /**
     * @var boolean
     * @Expose
     */
    private $sincronizado;


    /**
     * Set sincronizado
     *
     * @param boolean $sincronizado
     * @return OfertaEducativa
     */
    public function setSincronizado($sincronizado)
    {
        $this->sincronizado = $sincronizado;

        return $this;
    }

    /**
     * Get sincronizado
     *
     * @return boolean 
     */
    public function getSincronizado()
    {
        return $this->sincronizado;
    }
}
