<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla OfertaEducativa
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OcupacionReservaRepository extends GenericRepository{

    /**
     * obtención de la última oferta educativa que contiene los siguientes casos: este en Estatus 1, Sincronizado en False y activo en True
     * @return mixed objeto OfertaEducativa solicitada
     */
    public function findOfertaEducativaActiva(){
        $dql="SELECT
              partial oe.{nombre,oferta_educativa_id,fecha_inicio,fecha_fin,sincronizado},oee,p
              FROM ControlEscolarCalendarioBundle:OfertaEducativa oe
                LEFT JOIN oe.OfertaEducativaEstatus oee
                LEFT JOIN oe.Periodo p
              WHERE
                oe.activo=true
                AND oe.sincronizado=false
                AND oee.oferta_educativa_estatus_id=1
                ORDER BY oe.oferta_educativa_id DESC";
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setMaxResults(1);
        $escenariooferta = $query->getArrayResult();

        return (!$escenariooferta)?false:$escenariooferta[0];
    }

    /**
     * Buscando eventos de actividades calendarizadas desde centro.
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function findEventosForCalendar($params){
        $result = array();


        $dql            = " SELECT
                                    'centro'                                                                                                 AS tipo,
                                    OCR.ocupacion_reserva_id                                                                                 AS evento_id,
                                    O.ocupacion_id                                                                                           AS oferta_actividad_id,
                                    O.ocupacion_id                                                                                           AS ocupacion_id,
                                    OCR.fecha_evento                                                                                         AS start,
                                    O.fecha_fin                                                                                              AS fecha_final,
                                    O.fecha_inicio                                                                                           AS fecha_inicio,
                                    O.ocupacion_id                                                                                           AS id_principal,
                                    AU.aula_id                                                                                               AS aula_id_actividad,
                                    AUE.nombre                                                                                               AS aula_nombre_actividad,
                                    FA.facilitador_id                                                                                        AS facilitador_id_actividad,
                                    AUE.aula_id                                                                                              AS aula_id_evento,
                                    FAE.facilitador_id                                                                                       AS facilitador_id_evento,
                                    FAE.nombre                                                                                               AS facilitador_nombre_evento,
                                    FAE.apellido_paterno                                                                                     AS facilitador_apellido_paterno_evento,
                                    FAE.apellido_materno                                                                                     AS facilitador_apellido_materno_evento,
                                    OA.oferta_actividad_centro_id                                                                            AS oferta_actividad_centro_id,
                                    OA.extemporanea                                                                                          AS extemporanea,
                                    OCR.hora_inicio,
                                    OCR.hora_fin,
                                    O.numero_movimientos,
                                    O.clave_grupo                                                                                            AS clave_grupo,
                                    CASE WHEN OA IS null THEN false                                ELSE true                            END   AS basado_oferta,
                                    CASE WHEN OA IS null THEN false                                ELSE OA.obligatoria                 END   AS obligatorio,
                                    OA.control_acceso                                                                                        AS control_acceso,
                                    CASE WHEN A  IS null THEN AA.nombre                            ELSE A.nombre                       END   AS title,
                                    CASE WHEN A  IS null THEN AA.descripcion                       ELSE A.descripcion                  END   AS descripcion,
                                    CASE WHEN A  IS null THEN AAC.nombre                           ELSE AC.nombre                      END   AS categoria,
                                    CASE WHEN A  IS null THEN AAC.actividad_academica_categoria_id ELSE AC.categoria_id                END   AS categoria_id,
                                    CASE WHEN A  IS null THEN CONCAT(AAC.estilo,'')                ELSE CONCAT(AC.estilo,'')           END   AS className,
                                    CASE WHEN A  IS null THEN true                                 ELSE false                          END   AS es_academica,
                                    CASE WHEN A  IS null THEN AA.actividad_academica_id            ELSE A.actividad_id                 END   AS id_actividad,
                                    CASE WHEN A  IS null THEN OAE.actividad_academica_esquema_id   ELSE 0                              END   AS esquema_id,
                                    CASE WHEN :oferta_educativa_centro_id <> OEC.oferta_educativa_centro_id THEN true ELSE false       END   AS externo,
                                    CASE WHEN :oferta_educativa_centro_id <> OEC.oferta_educativa_centro_id THEN OEC.nombre ELSE 'false' END   AS nombreOfertaExterna

                            FROM
                                ControlEscolarCalendarioBundle:OcupacionReserva                             OCR
                                        LEFT JOIN
                                            OCR.Aula                                                        AUE
                                        LEFT JOIN
                                            OCR.Facilitador                                                 FAE
                                        LEFT JOIN
                                            OCR.Ocupacion                                                   O
                                        LEFT JOIN
                                            O.Aula                                                          AU
                                        LEFT JOIN
                                            O.Facilitador                                                   FA
                                        LEFT JOIN
                                            O.OfertaActividadCentro                                         OA
                                        LEFT JOIN
                                            O.ActividadAcademicaEsquema                                     OAE
                                        LEFT JOIN
                                            O.OfertaEducativaCentro                                         OEC
                                        LEFT JOIN
                                            O.ActividadAcademica                                            AA
                                            LEFT JOIN
                                            AA.ActividadAcademicaCategoria                                  AAC
                                        LEFT JOIN
                                            O.Actividad                                                     A
                                            LEFT JOIN
                                                A.ActividadCategoria                                        AC

                            WHERE
                                    (OCR.fecha_evento  BETWEEN :fecha_inicio AND :fecha_final)
                                AND
                                    OCR.activo= true
                                AND
                                    O.activo= true




                          ";
        /** Evaluamos si es filtrado por facilitador_id o aula_id */

        if($params["facilitador_id"]!=null){
            $dql .=  "  AND FAE.facilitador_id = :facilitador_id    ";
        }

        if($params["aula_id"]!=null){
            $dql .=  " AND AUE.aula_id = :aula_id    ";
        }

        //echo $dql;

        /** Fin de Evaluar si es filtrado por facilitador_id o aula_id */




    $query              = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('oferta_educativa_centro_id'                   , $params["oferta_educativa_centro_id"]);
    $query->setParameter('fecha_inicio'                                 , $params["fecha_inicio"]);
    $query->setParameter('fecha_final'                                  , $params["fecha_final"]);

    if($params["facilitador_id"]!=null){
        $query->setParameter('facilitador_id'                           , $params["facilitador_id"]);
    }

    if($params["aula_id"]!=null){
        $query->setParameter('aula_id'                                  , $params["aula_id"]);
    }


    //echo $query->getSQL();
    $result = $query->getArrayResult();





    $dql2               = " SELECT

                                    OAH.hora_inicio                         AS hora_inicio,
                                    OAH.hora_fin                            AS hora_fin,
                                    OAH.dia_semana                          AS dia_semana

                            FROM
                                    ControlEscolarCalendarioBundle:OcupacionHorario OAH

                            LEFT JOIN
                                    OAH.Ocupacion                                      OA
                            WHERE
                                    OA.ocupacion_id=:ocupacion_id
                            AND
                                    OAH.activo= true
                         ";
        $query2  = $this->getEntityManager()->createQuery($dql2);



        foreach ($result as $key => $value){

            $fecha_evento                                                   = $result[$key]["start"]->format("Y-m-d ");
            $result[$key]["hora_inicio"]                                    = $result[$key]["hora_inicio"]->format("H:i");
            $result[$key]["hora_fin"]                                       = $result[$key]["hora_fin"]->format("H:i");
            $result[$key]["start"]                                          = $fecha_evento . $result[$key]["hora_inicio"];
            $result[$key]["end"]                                            = $fecha_evento . $result[$key]["hora_fin"];
            $result[$key]["durationEditable"]                               = false;
            $result[$key]["editable"]                                       = true;


            $query2->setParameter('ocupacion_id',$result[$key]['oferta_actividad_id']);
            $result[$key]["_horarios"]                                      = $query2->getArrayResult();
            foreach($result[$key]["_horarios"] as &$obj){
                $obj["hora_inicio"]                                         = $obj["hora_inicio"]->format("H:i");
                $obj["hora_fin"]                                            = $obj["hora_fin"]->format("H:i");
            }

            //obtener los horarios de la actividad

        }

    //print_r($result);
    //exit();

    return $result;
    }

    public function findReservasByOcupacionId($params){
        $result = array();
        $dql    = " SELECT ORES"
                . " FROM ControlEscolarCalendarioBundle:OcupacionReserva ORES"
                . " LEFT JOIN ORES.Ocupacion O"
                . " WHERE O.ocupacion_id=:ocupacion_id"
                . " AND O.activo=true"
                . " AND ORES.activo=true";

        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ocupacion_id',$params["ocupacion_id"]);
        $result = $query->getResult();

        $arregloEventosIds=array();
        foreach($result as $evento){
          $arregloEventosIds[] = $evento->getReservaId();
        }

        $dql2   = " SELECT EV"
                . " FROM ControlEscolarReservaBundle:Calendario EV"
                . " WHERE EV.calendario_id IN(:eventos_ids)";

        $query2 = $this->getEntityManager()->createQuery($dql2);
        $query2->setParameter('eventos_ids', $arregloEventosIds);
        $eventosCalendario= $query2->getResult();

        $arregloEventos= array();
        $arregloEventos= array_merge($result,$eventosCalendario);

        return $arregloEventos;
    }

    public function findReservasByArregloOcupacionReservaId($params){
        $dql   = " SELECT EV"
                . " FROM ControlEscolarReservaBundle:Calendario EV"
                . " WHERE EV.calendario_id IN(:eventos_ids)";

        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('eventos_ids', $params['ids']);
        $eventosCalendario= $query->getResult();

        return $eventosCalendario;
    }

    public function findOcupacionesReservasByOcupacionId($params){
        $result = array();
        $dql    = " SELECT ORES"
                . " FROM ControlEscolarCalendarioBundle:OcupacionReserva ORES"
                . " LEFT JOIN ORES.Ocupacion O"
                . " WHERE O.ocupacion_id=:ocupacion_id"
                . " AND O.activo=true"
                . " AND ORES.activo=true";

        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('ocupacion_id',$params["ocupacion_id"]);
        $result = $query->getResult();
        return $result;
    }

    public function findEventosFromFecha($params){
//        print_r($params);
//        exit();
       $result = array();
       $dql    = " SELECT ORES"
               . " FROM ControlEscolarCalendarioBundle:OcupacionReserva ORES "
               . " LEFT JOIN ORES.Ocupacion O"
               . " WHERE O.ocupacion_id = :ocupacionId"
               . " AND ORES.fecha_evento >= :fechaReserva "
               . " AND ORES.activo = true";
       $query  = $this->getEntityManager()->createQuery($dql);
       $query->setParameter('ocupacionId', $params['ocupacion_id']);
       $query->setParameter('fechaReserva', $params['fecha']);
       $result = $query->getResult();
       return $result;
    }

    public function findOcupacionReservaByOfertaEducativaCentroYFacilitador($params){
       $dql    = " SELECT ORES"
               . " FROM ControlEscolarCalendarioBundle:OcupacionReserva ORES "
               . " LEFT JOIN ORES.Ocupacion O"
               . " LEFT JOIN O.OfertaEducativaCentro OEC"
               . " WHERE OEC = :ofertaEducativaCentro"
               . " AND ORES.Facilitador = :facilitador "
               . " AND ORES.activo = true";
       $query  = $this->getEntityManager()->createQuery($dql);
       $query->setParameter('ofertaEducativaCentro', $params['OfertaEducativaCentro']);
       $query->setParameter('facilitador', $params['Facilitador']);
       $result = $query->getResult();
       return $result;
    }
}