<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * DiaFeriado
 * @ExclusionPolicy("all")
 */
class DiaFeriado
{
    /**
     * @var integer
     * @Expose
     */
    private $dia_feriado_id;

    /**
     * @var string
     * @Expose
     */
    private $nombre;

    /**
     * @var string
     * @Expose
     */
    private $descripcion;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha;

    /**
     * @var boolean
     * @Expose
     */
    private $puede_publicar;

    /**
     * @var boolean
     * @Expose
     */
    private $activo;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var boolean
     * @Expose
     */
    private $corporativo;
    
    /**
     * Get dia_feriado_id
     *
     * @return integer 
     */
    public function getDiaFeriadoId()
    {
        return $this->dia_feriado_id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return DiaFeriado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return DiaFeriado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return DiaFeriado
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set puede_publicar
     *
     * @param boolean $puedePublicar
     * @return DiaFeriado
     */
    public function setPuedePublicar($puedePublicar)
    {
        $this->puede_publicar = $puedePublicar;

        return $this;
    }

    /**
     * Get puede_publicar
     *
     * @return boolean 
     */
    public function getPuedePublicar()
    {
        return $this->puede_publicar;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return DiaFeriado
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return DiaFeriado
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return DiaFeriado
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return DiaFeriado
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return DiaFeriado
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set corporativo
     *
     * @param boolean $corporativo
     * @return Parametro
     */
    public function setCorporativo($corporativo)
    {
        $this->corporativo = $corporativo;

        return $this;
    }

    /**
     * Get corporativo
     *
     * @return boolean 
     */
    public function getCorporativo()
    {
        return $this->corporativo;
    }
}
