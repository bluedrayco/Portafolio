<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * OfertaActividadCentro
 * @ExclusionPolicy("all")
 */
class OfertaActividadCentro
{
     /*
     * Contructor de la clase
     */
    public function __construct() {
        //Inicializa  el control acceso con un array vacio
        $this->control_acceso = array(
            'obligatorio'=>array(),
            'solo_lectura'=>array()
            );
    }


    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademica
     * @Expose
     */
    private $ActividadAcademica;

    /**
     * @var \ControlEscolar\PlanAnualBundle\Entity\Actividad
     * @Expose
     */
    private $Actividad;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro
     * @Expose
     */
    private $OfertaEducativaCentro;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var boolean
     * @Expose
     */
    private $obligatoria;

    /**
     * @var boolean
     * @Expose
     */
    private $extemporanea;
    
    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademicaEsquema
     * @Expose
     */
    private $ActividadAcademicaEsquema;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_inicio;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_fin;

    /**
     * @var array
     * @Expose
     */
    private $control_acceso;

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return OfertaActividadCentro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return OfertaActividadCentro
     */
    public function setFechaModificacion($fechaModifica)
    {
        $this->fecha_modificacion = $fechaModifica;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set ActividadAcademica
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica
     * @return OfertaActividadCentro
     */
    public function setActividadAcademica(\Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica = null)
    {
        $this->ActividadAcademica = $actividadAcademica;

        return $this;
    }

    /**
     * Get ActividadAcademica
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademica
     */
    public function getActividadAcademica()
    {
        return $this->ActividadAcademica;
    }

    /**
     * Set Actividad
     *
     * @param \ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad
     * @return OfertaActividadCentro
     */
    public function setActividad(\ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad = null)
    {
        $this->Actividad = $actividad;

        return $this;
    }

    /**
     * Get Actividad
     *
     * @return \ControlEscolar\PlanAnualBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->Actividad;
    }

    /**
     * Set OfertaActividadCentro
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro $ofertaEducativaCentro
     * @return OfertaActividadCentro
     */
    public function setOfertaEducativaCentro(\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro $ofertaEducativaCentro = null)
    {
        $this->OfertaEducativaCentro = $ofertaEducativaCentro;

        return $this;
    }

    /**
     * Get OfertaActividadCentro
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro
     */
    public function getOfertaEducativaCentro()
    {
        return $this->OfertaEducativaCentro;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return OfertaActividadCentro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return OfertaActividadCentro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
    /**
     * @var integer
     */
    private $oferta_actividad_centro_id;


    /**
     * Get oferta_actividad_centro_id
     *
     * @return integer
     */
    public function getOfertaActividadCentroId()
    {
        return $this->oferta_actividad_centro_id;
    }

    /**
     * Set obligatoria
     *
     * @param boolean $obligatoria
     * @return OfertaActividadCentro
     */
    public function setObligatoria($obligatoria)
    {
        $this->obligatoria = $obligatoria;

        return $this;
    }

    /**
     * Get obligatoria
     *
     * @return boolean
     */
    public function getObligatoria()
    {
        return $this->obligatoria;
    }

    /**
     * Set extemporanea
     *
     * @param boolean $extemporanea
     * @return OfertaActividad
     */
    public function setExtemporanea($extemporanea)
    {
        $this->extemporanea = $extemporanea;

        return $this;
    }

    /**
     * Get extemporanea
     *
     * @return boolean
     */
    public function getExtemporanea()
    {
        return $this->extemporanea;
    }

    /**
     * Set ActividadAcademicaEsquema
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademicaEsquema $actividadAcademicaEsquema
     * @return OfertaActividadCentro
     */
    public function setActividadAcademicaEsquema(\Core\CoreBundle\Entity\ActividadAcademicaEsquema $actividadAcademicaEsquema = null)
    {
        $this->ActividadAcademicaEsquema = $actividadAcademicaEsquema;

        return $this;
    }

    /**
     * Get ActividadAcademicaEsquema
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademicaEsquema
     */
    public function getActividadAcademicaEsquema()
    {
        return $this->ActividadAcademicaEsquema;
    }

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return OfertaActividadCentro
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Set fecha_fin
     *
     * @param \DateTime $fechaFin
     * @return OfertaActividadCentro
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;

        return $this;
    }

    /**
     * Get fecha_fin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /*
     * Función que setea el acceso de tipo obligatorio
     */
    public function addAccesoObligatorio($acceso)
    {
            $this->addAcceso($acceso,'obligatorio');
    }
    /*
     * Función que setea el acceso de tipo obligatorio
     */
    public function removeAccesoObligatorio($acceso)
    {
            $this->removeAcceso($acceso,'obligatorio');
    }

    /**
     * Función para remover un acceso
     *
     * @return Object OfertaActividad
     */
    public function removeAcceso($acceso,$tipo)
    {
        //Evakya su existe el elemento en el array de control de acceso de
        //acuerdo al tipo dado, lo elimina si existe
        if (false !== $key = array_search(strtoupper($acceso), $this->control_acceso[$tipo], true)) {
            $controlAuxiliar = $this->control_acceso[$tipo];
            unset($controlAuxiliar[$key]);
            $this->control_acceso[$tipo] = array_values($controlAuxiliar);
        }
        return $this;
    }

    /**
     * Regresa el acceso
     *
     * @return array control_acceso
     */
    public function getControlAcceso()
    {
        return $this->control_acceso;
    }



    /**
     * Settea un array de acceso completo el acceso
     *
     * @return Object OfertaActividad
     */
    public function setControlAcceso($controlAcceso)
    {
        $this->control_acceso = $controlAcceso;
        return $this;
    }

    /**
     * Función para agregar un acceso obligatorio
     *
     * @return
     */
     public function addAcceso($acceso,$tipo)
    {
        $acceso = strtoupper($acceso);
        if(in_array($tipo, $this->control_acceso, true)){
            $this->control_acceso[$tipo] = array();
        }

        if (!in_array($acceso, $this->control_acceso[$tipo], true)) {
                $this->control_acceso[$tipo][] = $acceso;
        }

        return $this;
    }

    /**
     * Settea un array de acceso completo el acceso con valores por default
     *
     * @return Object OfertaActividad
     */
    public function setDefaultControlAcceso()
    {
        $this->control_acceso = array(
            'obligatorio'=>array(),
            'solo_lectura'=>array()
            );
        return $this;
    }


}
