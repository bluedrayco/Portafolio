<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * EscenarioOferta
 * @ExclusionPolicy("all")
 */
class EscenarioOferta
{
    /**
     * @var integer
     * @Expose
     */
    private $escenario_oferta_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\Escenario
     * @Expose
     */
    private $Escenario;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaEducativa
     * @Expose
     */
    private $OfertaEducativa;


    /**
     * Get escenario_oferta_id
     *
     * @return integer 
     */
    public function getEscenarioOfertaId()
    {
        return $this->escenario_oferta_id;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return EscenarioOferta
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return EscenarioOferta
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return EscenarioOferta
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return EscenarioOferta
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set Escenario
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\Escenario $escenario
     * @return EscenarioOferta
     */
    public function setEscenario(\ControlEscolar\CalendarioBundle\Entity\Escenario $escenario = null)
    {
        $this->Escenario = $escenario;

        return $this;
    }

    /**
     * Get Escenario
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Escenario 
     */
    public function getEscenario()
    {
        return $this->Escenario;
    }

    /**
     * Set OfertaEducativa
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaEducativa $ofertaEducativa
     * @return EscenarioOferta
     */
    public function setOfertaEducativa(\ControlEscolar\CalendarioBundle\Entity\OfertaEducativa $ofertaEducativa = null)
    {
        $this->OfertaEducativa = $ofertaEducativa;

        return $this;
    }

    /**
     * Get OfertaEducativa
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaEducativa 
     */
    public function getOfertaEducativa()
    {
        return $this->OfertaEducativa;
    }
    /**
     * @var boolean
     */
    private $activo;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return EscenarioOferta
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
}
