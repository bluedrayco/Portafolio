<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * SincronizaOfertaCentro
 * @ExclusionPolicy("all")
 */
class SincronizaOfertaCentro
{
    /**
     * @var integer
     * @Expose
     */
    private $sincroniza_oferta_centro_id;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_sincronizacion;

    /**
     * @var boolean
     * @Expose
     */
    private $completado;

    /**
     * @var string
     * @Expose
     */
    private $ultimo_mensaje_registrado;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;

    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaEducativa
     * @Expose
     */
    private $OfertaEducativa;

    /**
     * @var \Core\CoreBundle\Entity\Centro
     * @Expose
     */
    private $Centro;


    /**
     * Get sincroniza_oferta_centro_id
     *
     * @return integer 
     */
    public function getSincronizaOfertaCentroId()
    {
        return $this->sincroniza_oferta_centro_id;
    }

    /**
     * Set fecha_sincronizacion
     *
     * @param \DateTime $fechaSincronizacion
     * @return SincronizaOfertaCentro
     */
    public function setFechaSincronizacion($fechaSincronizacion)
    {
        $this->fecha_sincronizacion = $fechaSincronizacion;

        return $this;
    }

    /**
     * Get fecha_sincronizacion
     *
     * @return \DateTime 
     */
    public function getFechaSincronizacion()
    {
        return $this->fecha_sincronizacion;
    }

    /**
     * Set completado
     *
     * @param boolean $completado
     * @return SincronizaOfertaCentro
     */
    public function setCompletado($completado)
    {
        $this->completado = $completado;

        return $this;
    }

    /**
     * Get completado
     *
     * @return boolean 
     */
    public function getCompletado()
    {
        return $this->completado;
    }

    /**
     * Set ultimo_mensaje_registrado
     *
     * @param string $ultimoMensajeRegistrado
     * @return SincronizaOfertaCentro
     */
    public function setUltimoMensajeRegistrado($ultimoMensajeRegistrado)
    {
        $this->ultimo_mensaje_registrado = $ultimoMensajeRegistrado;

        return $this;
    }

    /**
     * Get ultimo_mensaje_registrado
     *
     * @return string 
     */
    public function getUltimoMensajeRegistrado()
    {
        return $this->ultimo_mensaje_registrado;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return SincronizaOfertaCentro
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return SincronizaOfertaCentro
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set Usuario
     *
     * @param \Core\UserBundle\Entity\Usuario $usuario
     * @return SincronizaOfertaCentro
     */
    public function setUsuario(\Core\UserBundle\Entity\Usuario $usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return SincronizaOfertaCentro
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }

    /**
     * Set OfertaEducativa
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaEducativa $ofertaEducativa
     * @return SincronizaOfertaCentro
     */
    public function setOfertaEducativa(\ControlEscolar\CalendarioBundle\Entity\OfertaEducativa $ofertaEducativa = null)
    {
        $this->OfertaEducativa = $ofertaEducativa;

        return $this;
    }

    /**
     * Get OfertaEducativa
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaEducativa 
     */
    public function getOfertaEducativa()
    {
        return $this->OfertaEducativa;
    }

    /**
     * Set Centro
     *
     * @param \Core\CoreBundle\Entity\Centro $centro
     * @return SincronizaOfertaCentro
     */
    public function setCentro(\Core\CoreBundle\Entity\Centro $centro = null)
    {
        $this->Centro = $centro;

        return $this;
    }

    /**
     * Get Centro
     *
     * @return \Core\CoreBundle\Entity\Centro 
     */
    public function getCentro()
    {
        return $this->Centro;
    }
    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;


    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return SincronizaOfertaCentro
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }
    /**
     * @var boolean
     */
    private $activo;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return SincronizaOfertaCentro
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    /**
     * @var \DateTime
     */
    private $fecha_alta2;


    /**
     * Set fecha_alta2
     *
     * @param \DateTime $fechaAlta2
     * @return SincronizaOfertaCentro
     */
    public function setFechaAlta2($fechaAlta2)
    {
        $this->fecha_alta2 = $fechaAlta2;

        return $this;
    }

    /**
     * Get fecha_alta2
     *
     * @return \DateTime 
     */
    public function getFechaAlta2()
    {
        return $this->fecha_alta2;
    }
}
