<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * MovimientoCentro
 * @ExclusionPolicy("all")
 */
class MovimientoCentro {
    
    /**
     * @var integer
     * @Expose
     */
    private $movimiento_centro_id;
    
    /**
     * @var boolean
     * @Expose
     */
    private $activo; 

    /**
     * @var DateTime
     * @Expose
     */
    private $fecha_movimiento; 
    
    /**
     * @var DateTime
     * @Expose
     */
    private $hora_movimiento;  
    
    /**
     * @var DateTime
     * @Expose
     */
    private $fecha_alta;  
    
    /**
     * @var string
     * @Expose
     */
    private $usuario_movimiento;  
    
    /**
     * @var string
     * @Expose
     */
    private $descripcion_movimiento;  
    
    /**
     * @var string
     * @Expose
     */
    private $clave_grupo;  
    
    /**
     * @var \Core\CoreBundle\Entity\ActividadAcademica
     * @Expose
     */
    private $ActividadAcademica;

    /**
     * @var \ControlEscolar\PlanAnualBundle\Entity\Actividad
     * @Expose
     */
    private $Actividad;
    
    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModificacion;
    
    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro
     * @Expose
     */
    private $OfertaEducativaCentro;
    
    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\TipoMovimiento
     * @Expose
     */
    private $TipoMovimiento; 
    
    /**
     * Get movimiento_centro_id
     *
     * @return integer 
     */
    public function getMovimientoCentroId()
    {
        return $this->movimiento_centro_id;
    }
    
    /**
     * Set ActividadAcademica
     *
     * @param \Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica
     * @return Movimiento
     */
    public function setActividadAcademica(\Core\CoreBundle\Entity\ActividadAcademica $actividadAcademica = null)
    {
        $this->ActividadAcademica = $actividadAcademica;

        return $this;
    }

    /**
     * Get ActividadAcademica
     *
     * @return \Core\CoreBundle\Entity\ActividadAcademica 
     */
    public function getActividadAcademica()
    {
        return $this->ActividadAcademica;
    }

    /**
     * Set Actividad
     *
     * @param \ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad
     * @return Movimiento
     */
    public function setActividad(\ControlEscolar\PlanAnualBundle\Entity\Actividad $actividad = null)
    {
        $this->Actividad = $actividad;

        return $this;
    }

    /**
     * Get Actividad
     *
     * @return \ControlEscolar\PlanAnualBundle\Entity\Actividad 
     */
    public function getActividad()
    {
        return $this->Actividad;
    }
    
    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return Movimiento
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta = null)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModificacion
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return Movimiento
     */
    public function setUsuarioModificacion(\Core\UserBundle\Entity\Usuario $usuarioModificacion = null)
    {
        $this->UsuarioModificacion = $usuarioModificacion;

        return $this;
    }

    /**
     * Get UsuarioModificacion
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModificacion()
    {
        return $this->UsuarioModificacion;
    }
    
    /**
     * Set OfertaEducativaCentro
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro $ofertaEducativaCentro
     * @return Movimiento
     */
    public function setOfertaEducativaCentro(\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro $ofertaEducativaCentro = null)
    {
        $this->OfertaEducativaCentro = $ofertaEducativaCentro;

        return $this;
    }

    /**
     * Get OfertaEducativaCentro
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro 
     */
    public function getOfertaEducativaCentro()
    {
        return $this->OfertaEducativaCentro;
    }
    
    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Movimiento
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    
    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return Movimiento
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }
    
    /**
     * Set fecha_movimiento
     *
     * @param \DateTime $fechaMovimiento
     * @return Movimiento
     */
    public function setFechaMovimiento($fechaMovimiento)
    {
        $this->fecha_movimiento = $fechaMovimiento;

        return $this;
    }

    /**
     * Get fecha_movimiento
     *
     * @return \DateTime 
     */
    public function getFechaMovimiento()
    {
        return $this->fecha_movimiento;
    }
    
    /**
     * Set hora_movimiento
     *
     * @param \DateTime $hora_movimiento
     * @return Movimiento
     */
    public function setHoraMovimiento($horaMovimiento)
    {
        $this->hora_movimiento = $horaMovimiento;

        return $this;
    }

    /**
     * Get hora_movimiento
     *
     * @return \DateTime 
     */
    public function getHoraMovimiento()
    {
        return $this->hora_movimiento;
    }
    
    /**
     * Set usuario_movimiento
     *
     * @param string $usuario_movimiento
     * @return Movimiento
     */
    public function setUsuarioMovimiento($usuario_movimiento)
    {
        $this->usuario_movimiento = $usuario_movimiento;

        return $this;
    }

    /**
     * Get usuario_movimiento
     *
     * @return string 
     */
    public function getUsuarioMovimiento()
    {
        return $this->usuario_movimiento;
    }
    
    /**
     * Set descripcion_movimiento
     *
     * @param string $descripcion_movimiento
     * @return Movimiento
     */
    public function setDescripcionMovimiento($descripcion_movimiento)
    {
        $this->descripcion_movimiento = $descripcion_movimiento;

        return $this;
    }

    /**
     * Get activo
     *
     * @return string
     */
    public function getDescripcionMovimiento()
    {
        return $this->descripcion_movimiento;
    }
    
    /**
     * Set clave_grupo
     *
     * @param string $clave_grupo
     * @return Movimiento
     */
    public function setClaveGrupo($clave_grupo)
    {
        $this->clave_grupo = $clave_grupo;

        return $this;
    }

    /**
     * Get clave_grupo
     *
     * @return string 
     */
    public function getClaveGrupo()
    {
        return $this->clave_grupo;
    }
    
    /**
     * Set TipoMovimiento
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\TipoMovimiento $TipoMovimiento
     * @return Movimiento
     */
    public function setTipoMovimiento(\ControlEscolar\CalendarioBundle\Entity\TipoMovimiento $TipoMovimiento = null)
    {
        $this->TipoMovimiento = $TipoMovimiento;

        return $this;
    }

    /**
     * Get TipoMovimiento
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\TipoMovimiento
     */
    public function getTipoMovimiento()
    {
        return $this->TipoMovimiento;
    }
}
