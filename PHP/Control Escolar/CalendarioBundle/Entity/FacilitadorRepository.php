<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Repositorio para generar consultas particulares en la tabla Facilitador
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class FacilitadorRepository extends GenericRepository{

    public function findFacilitadoresNoInArray($params){
        $dql    = " SELECT
                            F
                    FROM
                            ControlEscolarCalendarioBundle:Facilitador F

                    WHERE   F.activo  = true

                    AND
                            F.facilitador_id NOT IN (:facilitadores)
                 ";

        $query  = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('facilitadores',$params["facilitadores"]);
        return $query->getArrayResult();
    }

    /**
     * Obtención de Facilitadores para el reporte de facilitadores
     * Solo obtiene los facilitadores que estan activos o que tienen o tuvieron
     * un grupo
     * @return mixed objeto con los dias feriados
     */
    public function findFacilitadoresReporte(){
        $sql    = "SELECT
                        ccf.facilitador_id,
                        ccf.nombre,
                        ccf.email,
                        ccf.apellido_paterno,
                        ccf.apellido_materno,
                        ccf.activo,
                        ccf.fecha_alta,
                        ccf.fecha_modificacion
                        , (SELECT DISTINCT valor_origen FROM equivalencia.tra_equivalencia WHERE tabla_id IN (SELECT tabla_id FROM equivalencia.cat_tabla WHERE nombre='Mako::Facilitador') AND valor_destino = ccf.facilitador_id) id_facilitador_mako
                    FROM
                        calendario.cat_facilitador ccf
                    WHERE ccf.activo = true OR (ccf.activo=false AND ccf.facilitador_id IN (
                        SELECT
                            facilitador_id
                        FROM
                            calendario.tra_ocupacion_reserva
                    )) ORDER BY ccf.facilitador_id;
                 ";

        $stmt = $this->getEntityManager();
        return $stmt->getConnection()->fetchAll($sql);;
    }
}
