<?php

namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Escenario
 * @ExclusionPolicy("all")
 */
class Escenario
{
    /**
     * @var integer
     * @Expose
     */
    private $escenario_id;

    /**
     * @var string
     * @Expose
     */
    private $nombre;


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Escenario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_alta;

    /**
     * @var \DateTime
     * @Expose
     */
    private $fecha_modificacion;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioAlta;

    /**
     * @var \Core\UserBundle\Entity\Usuario
     */
    private $UsuarioModifica;


    /**
     * Get escenario_id
     *
     * @return integer 
     */
    public function getEscenarioId()
    {
        return $this->escenario_id;
    }

    /**
     * Set fecha_alta
     *
     * @param \DateTime $fechaAlta
     * @return Escenario
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fecha_alta = $fechaAlta;

        return $this;
    }

    /**
     * Get fecha_alta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    /**
     * Set fecha_modificacion
     *
     * @param \DateTime $fechaModificacion
     * @return Escenario
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fecha_modificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fecha_modificacion
     *
     * @return \DateTime 
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * Set UsuarioAlta
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioAlta
     * @return Escenario
     */
    public function setUsuarioAlta(\Core\UserBundle\Entity\Usuario $usuarioAlta)
    {
        $this->UsuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get UsuarioAlta
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->UsuarioAlta;
    }

    /**
     * Set UsuarioModifica
     *
     * @param \Core\UserBundle\Entity\Usuario $usuarioModifica
     * @return Escenario
     */
    public function setUsuarioModifica(\Core\UserBundle\Entity\Usuario $usuarioModifica = null)
    {
        $this->UsuarioModifica = $usuarioModifica;

        return $this;
    }

    /**
     * Get UsuarioModifica
     *
     * @return \Core\UserBundle\Entity\Usuario 
     */
    public function getUsuarioModifica()
    {
        return $this->UsuarioModifica;
    }
    /**
     * @var boolean
     * @Expose
     */
    private $activo;


    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Escenario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @var \Core\CoreBundle\Entity\Centro
     * @Expose
     */
    private $Centro;

    /**
     * Set Centro
     *
     * @param \Core\CoreBundle\Entity\Centro $centro
     * @return Escenario
     */
    public function setCentro(\Core\CoreBundle\Entity\Centro $centro = null)
    {
        $this->Centro = $centro;

        return $this;
    }

    /**
     * Get Centro
     *
     * @return \Core\CoreBundle\Entity\Centro 
     */
    public function getCentro()
    {
        return $this->Centro;
    }
    
    /**
     * @var \Core\CoreBundle\Entity\Grupo
     * @Expose
     */
    private $Grupo;
    
    /**
     * Set Grupo
     *
     * @param \Core\CoreBundle\Entity\Grupo $grupo
     * @return Escenario
     */
    public function setGrupo(\Core\CoreBundle\Entity\Grupo $grupo = null)
    {
        $this->Grupo = $grupo;

        return $this;
    }

    /**
     * Get Grupo
     *
     * @return \Core\CoreBundle\Entity\Grupo
     */
    public function getGrupo()
    {
        return $this->Grupo;
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * @var \ControlEscolar\CalendarioBundle\Entity\TipoEscenario
     * @Expose
     */
    private $TipoEscenario;

    /**
     * Set TipoEscenario
     *
     * @param \ControlEscolar\CalendarioBundle\Entity\TipoEscenario $tipoEscenario
     * @return Escenario
     */
    public function setTipoEscenario(\ControlEscolar\CalendarioBundle\Entity\TipoEscenario $tipoEscenario = null)
    {
        $this->TipoEscenario = $tipoEscenario;

        return $this;
    }

    /**
     * Get TipoEscenario
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\TipoEscenario
     */
    public function getTipoEscenario()
    {
        return $this->TipoEscenario;
    }
}
