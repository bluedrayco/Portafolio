<?php
namespace ControlEscolar\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Core\CoreBundle\Entity\GenericRepository;

/**
 * Repositorio para generar consultas particulares en la tabla Aula
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class SincronizaOfertaCentroRepository extends GenericRepository{ 


    public function findCentrosNoSincroniaOfertaCentro($params){

        $result = array();

        $dql            = "SELECT 
                                   
                                    C.centro_id
                                FROM 
                                    ControlEscolarCalendarioBundle:SincronizaOfertaCentro         SOC
                                LEFT JOIN
                                    SOC.OfertaEducativa                                           OE
                                LEFT JOIN
                                    SOC.Centro                                                     C
                                    WHERE
                                        OE.oferta_educativa_id = :oferta_educativa_id
                          ";
        $query          = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('oferta_educativa_id'  , $params["oferta_educativa_id"]);
        $centros        = $query->getArrayResult();
        if(count($centros)<=0){
            $centros [] = array("centro_id"=>0);
        }

        $dql             = " SELECT 
                                        C
                                FROM   
                                        CoreCoreBundle:Centro                                    C
                                WHERE 
                                        C.centro_id NOT IN (:centros)
                                ORDER BY 
                                        C.centro_id
                                        
                            ";
        $query          = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('centros'               , $centros);
        $centros        = $query->getResult();
        if(count($centros)<=0){
            $centros = array();
        }
        return $centros;


    }


}
