<?php
namespace ControlEscolar\CalendarioBundle\Tests\Business\Entity;

use Core\CoreBundle\Business\Entity\GenericRest as BGenericRest;
use ControlEscolar\CalendarioBundle\Business\Entity\ParametroActividad          as BParametroActividad;
use Core\CoreBundle\Tests\BaseTester;

class ParametroActividadTest extends BaseTester{

    public function testObtenerParametroPorActividad(){
        $parametro = 'numero_maximo_movimientos';
        $genericRestBusiness = new BGenericRest($this->em);
        $actividad = $genericRestBusiness->listarGenerico(
                'Core\CoreBundle\Entity\ActividadAcademica',
                array(),
                array('incluir_activo'=>true)
        );
        $actividad = $actividad['data']['actividadacademica'][0];

        //obtiene el valor del parametro numero_maximo_movimientos
        $movimientos_parametro = $genericRestBusiness->listarGenerico(
                'ControlEscolar\CalendarioBundle\Entity\Parametro',
                array('filter'=>"parametro.nombre='numero_maximo_movimientos'"),
                array('incluir_activo'=>true)
        );

        $parametroActividad = new BParametroActividad($this->em);
        //Revisa que regrese el valor de numero_maximo_movimientos
        $respuesta = $parametroActividad->obtenerParametroPorActividad($parametro, $actividad);

        $this->assertEquals($movimientos_parametro['data']['parametro'],$respuesta);

        //OBTENIENDO UNA ACTIVIDAD VALIDA
        $actividad = $genericRestBusiness->listarGenerico(
                'ControlEscolar\CalendarioBundle\Entity\ParametroActividad',
                array(),
                array('incluir_activo'=>true)
        );
        $actividad = $actividad['data']['parametroactividad'][0];
        //Revisa que regrese el valor de numero_maximo_movimientos
        $respuesta = $parametroActividad->obtenerParametroPorActividad($parametro, $actividad->getActividadAcademica());

        $this->assertEquals('13',$respuesta[0]->getValor());

    }
}

