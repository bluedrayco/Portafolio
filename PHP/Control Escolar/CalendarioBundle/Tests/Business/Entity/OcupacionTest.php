<?php
namespace ControlEscolar\CalendarioBundle\Tests\Business\Entity;

use ControlEscolar\CalendarioBundle\Business\Entity\Ocupacion as BOcupacion;
use Core\CoreBundle\Business\Entity\GenericRest as BGenericRest;
use Core\CoreBundle\Tests\BaseTester;

class OcupacionTest extends BaseTester {

    public function testReporteGrupos() {
        $ocupacionBusiness = new BOcupacion($this->em);
        $respuesta = $ocupacionBusiness->reporteGrupos(array());

        $this->assertTrue(array_key_exists("reporte", $respuesta['data']));
        $this->assertTrue(count($respuesta['data']) > 0);
        $this->assertTrue(count($respuesta['data']['reporte']) > 0);
    }

    public function testOfertaEducativaPublicada(){
        $ocupacionBusiness = new BOcupacion($this->em,$this->emMako);
        $genericRestBusiness = new BGenericRest($this->em);
        // prueba en la que obtenemos una ocupacion y obtenemos sus inscritos e interesados, se nos debera obtener una estructura con claves inscritos e interesados
        $ocupacion =$genericRestBusiness->listarGenerico(
                'ControlEscolar\CalendarioBundle\Entity\Ocupacion',
                array('limit'=>1),
                array('incluir_activo'=>true)
        );
        $respuesta=$ocupacionBusiness->getInscritosEInteresados($ocupacion['data']['ocupacion'][0]);
        $this->assertTrue(array_key_exists("inscritos", $respuesta)&&array_key_exists("interesados", $respuesta));
    }


    public function testGetAsistenciaEvento(){


            $ocupacionBusiness      = new BOcupacion($this->em,$this->emMako);
            $genericRestBusiness    = new BGenericRest($this->em);

            $respuestaListado       = $genericRestBusiness->listarGenerico(
                    'ControlEscolar\CalendarioBundle\Entity\OcupacionReserva',
                    array("filter"=>'ocupacionreserva.ocupacion_reserva_id= 605'),
                    array('incluir_activo'  =>true)
            );

            $evento                 = $respuestaListado["data"]["ocupacionreserva"][0];
            $respuesta              = $ocupacionBusiness->getAsistenciaEvento($evento);
            $this->assertTrue($evento->getNumeroAsistentes() > 1);
    }


    public function testValidaEliminacionAutomatica(){

            $ocupacionBusiness      = new BOcupacion($this->em,$this->emMako);
            $genericRestBusiness    = new BGenericRest($this->em);

            $respuestaListado       = $genericRestBusiness->listarGenerico(
                    'ControlEscolar\CalendarioBundle\Entity\Ocupacion',
                    array("filter"=>'ocupacion.ocupacion_id= 36'),
                    array('incluir_activo'  =>true)
            );

            $ocupacion              = $respuestaListado["data"]["ocupacion"][0];
            $respuesta              = $ocupacionBusiness->validaEliminacionAutomatica($ocupacion);
            echo "respuesta: " . $respuesta;
            $this->assertTrue($respuesta == 0);
    }

}

