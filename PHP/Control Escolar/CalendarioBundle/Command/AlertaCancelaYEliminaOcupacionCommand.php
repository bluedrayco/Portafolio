<?php
namespace ControlEscolar\CalendarioBundle\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use ControlEscolar\CalendarioBundle\Business\Entity\Ocupacion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Core\UserBundle\Entity\Usuario;

class AlertaCancelaYEliminaOcupacionCommand extends ContainerAwareCommand {
    private $db         = null;
    private $db2        = null;
    protected function configure() {
        $this->setName('enova:alertas:cancelacion_eliminacion')
             ->setDescription('Generamos las alertas para cancelacíon de ocupaciones');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->getDoctrine();
        $BOcupacion = new Ocupacion($this->db,$this->db2);
        $params = array();
        $params['usuario_id']=1;
        $GLOBALS['kernel']->getContainer()->get('logger')->info("Notificacion de Cancelaciones ........");
        $BOcupacion->cancelarOcupaciones($params);
        $GLOBALS['kernel']->getContainer()->get('logger')->info("Notificacion de Eliminaciones ........");
        $BOcupacion->eliminarOcupaciones($params);


    }

    private function getDoctrine() {
        if(is_null($this->db)) {
            $this->db  = $this->getContainer()->get('doctrine')->getManager();
        }
    }
}


?>