<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;

use ControlEscolar\CalendarioBundle\Entity\Movimiento as EMovimiento;
use ControlEscolar\CalendarioBundle\Entity\TipoMovimiento as ETipoMovimiento;

use ControlEscolar\CalendarioBundle\Business\Entity\TipoMovimiento as BTipoMovimiento;
use Core\CoreBundle\Business\Entity\Tabla                          as BTabla;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class Movimiento extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    public function crearMovimiento($nombre_tabla, $referencia, $tipo_movimiento, $movimientoJSON, $usuario_id) {
        //obtencion de relaciones
        try {
            $usuario = $this->getUsuarioById($usuario_id);

            $tipoMovimientoBusiness = new BTipoMovimiento($this->entityManager,$this->entityManagerMako);
            $tipoMovimiento         = $tipoMovimientoBusiness->getTipoMovimientoByClave($tipo_movimiento);

            if (!$tipoMovimiento) {
                $this->log->error("error al encontrar el tipo de movimiento");
                throw new DBALException("No se encontró la entidad TipoMovimiento con la clave {$tipo_movimiento}");
            }
            $tablaBusiness = new BTabla($this->entityManager,$this->entityManagerMako);
            $tabla         = $tablaBusiness->getTablaByNombreTabla($nombre_tabla);

            if (!$tabla) {
                $this->log->error("error al encontrar la tabla");
                throw new DBALException("No se encontró la entidad Tabla con nombre {$nombre_tabla}");
            }

            $movimiento = new EMovimiento();
            $movimiento->setActivo(true);
            $movimiento->setFechaAlta(new \DateTime());
            $movimiento->setFechaModificacion(new \DateTime());
            $movimiento->setMovimientos($movimientoJSON);
            $movimiento->setReferencia($referencia);
            $movimiento->setTabla($tabla);
            $movimiento->setTipoMovimiento($tipoMovimiento);
            $movimiento->setUsuarioAlta($usuario);
            $movimiento->setUsuarioModificacion($usuario);

            $this->entityManager->persist($movimiento);
            $this->entityManager->flush();
        } catch (DBALException $ex) {
            throw new DBALException($ex->getMessage());
        }
        return $movimiento;
    }

    public function obtenerMovimientos($nombreTabla, $referencia, $tipoMovimiento = null) {
        $datosBusqueda = array();
        $tablaBusiness = new BTabla($this->entityManager,$this->entityManagerMako);
        $tabla         = $tablaBusiness->getTablaByNombreTabla($nombreTabla);

        if (!$tabla) {
            $this->log->error("No se encontró la Tabla con el nombre {$nombreTabla}");
            return array();
        }

        $datosBusqueda['Tabla'] = $tabla;

        if ($tipoMovimiento) {
            $tipoMovimientoBusiness = new BTipoMovimiento($this->entityManager,$this->entityManagerMako);
            $tipoMovimiento         = $tipoMovimientoBusiness->getTipoMovimientoByClave($tipoMovimiento);

            if (!$tipoMovimiento) {
                $this->log->error("No se encontró el tipo movimiento con tipo {$tipoMovimiento}");
                return array();
            }

            $datosBusqueda['TipoMovimiento'] = $tipoMovimiento;
        }

        $datosBusqueda['referencia'] = $referencia;

        $movimientos = $this->getRepository(
            'ControlEscolarCalendarioBundle:Movimiento',
            $datosBusqueda,
            null,
            200,
            null,
            'findBy',
            array()
        );

        if (!$movimientos) {
            return array();
        }

        return $movimientos;
    }
}
