<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\EscenarioOfertaCentro as EEscenarioOfertaCentro;
use Core\CoreBundle\Entity\Centro                                as ECentro;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * Lógica de negocio de los eventos de un calendario
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class EscenarioOfertaCentro extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Listado de los EscenariosOfertaCentro
     * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
     * @return mixed objeto respuesta con los datos de los escenariosofertacentro
     */
    public function getCentrosyGruposByEscenarioOfertaId($escenario_oferta_id) {
        $centrosygrupos = $this->getRepository(
                "ControlEscolarCalendarioBundle:EscenarioOfertaCentro", array('escenario_oferta_id' => $escenario_oferta_id), null, null, null, 'findCentrosYGruposByEscenarioOfertaId', null);
        return $this->buildRespuesta(
                        null, array('centrosygrupos' => $centrosygrupos)
        );
    }

    /**
     * Eliminación de la relación de un EscenarioOferta con un Centro
     * @param int $escenario_oferta_id identificador del escenario oferta que esta vinculado al Centro a eliminar
     * @param int $centro_id Identificador del centro que esta vinculado al escenario oferta a eliminar
     * @return mixed objeto respuesta con un mensaje de exito o error
     */
    public function eliminarByCentroId($escenario_oferta_id, $centro_id) {
        $escenarioOfertaCentro = $this->getRepository(
                "ControlEscolarCalendarioBundle:EscenarioOfertaCentro", array(
            'escenario_oferta_id' => $escenario_oferta_id,
            'centro_id' => $centro_id
                ), null, null, null, 'findByEscenarioOfertaIdCentroId', null);

        if (!$escenarioOfertaCentro) {
            return $this->respuesta;
        }

        try {
            foreach ($escenarioOfertaCentro as $eoc) {
                $eoc->setActivo(false);
                $this->entityManager->flush();
            }
            $this->buildRespuesta('delete');
        } catch (DBALException $e) {
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }

    /**
     * Eliminación de la relación de un EscenarioOferta con un Grupo
     * @param int $escenario_oferta_id identificador del escenario oferta que esta vinculado al Grupo a eliminar
     * @param int $grupo_id Identificador del Grupo que esta vinculado al escenario oferta a eliminar
     * @return mixed objeto respuesta con un mensaje de exito o error
     */
    public function eliminarByGrupoId($escenario_oferta_id,$grupo_id){
        $escenarioOfertaCentro = $this->getRepository(
          "ControlEscolarCalendarioBundle:EscenarioOfertaCentro", array(
            'escenario_oferta_id' => $escenario_oferta_id,
            'grupo_id' => $grupo_id
                ), null, null, null, 'findByEscenarioOfertaIdGrupoId', null);

        if (!$escenarioOfertaCentro) {
            return $this->respuesta;
        }

        try {
            foreach ($escenarioOfertaCentro as $eoc) {
                $eoc->setActivo(false);
                $this->entityManager->flush();
            }
            $this->buildRespuesta('delete');
        } catch (DBALException $e) {
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }
        return $this->respuesta;
    }

    /**
     * Creación de un EscenarioOfertaCentro
     *
     * @param integer $obj[usuario_id] usuario que creará el EscenarioOfertaCentro
     * @param integer $obj[escenario_oferta_id] Escenario oferta que se vinculará con el centro
     * @param integer $obj[centro_id] centro que se vinculará con el escenario oferta
     * @param integer $obj[grupo_id] grupo que se vinculará con el escenario oferta
     * @return mixed objeto respuesta con un mensaje de exito o error y con el escenarioofertacentro
     */
    public function crearByCentroId($obj){
        $escenarioOfertaCentro = null;
        $usuario = $this->getRepository(
            'CoreUserBundle:Usuario',
            array('id' => $obj['usuario_id']),
            null,
            200
        );

        if (!$usuario) {
            $this->respuesta['data']['escenarioofertacentro'] = $escenarioOfertaCentro;

            return $this->respuesta;
        }

        $escenarioOferta = $this->getRepository(
            'ControlEscolarCalendarioBundle:EscenarioOferta',
            array('escenario_oferta_id' => $obj['escenario_oferta_id']),
            null,
            200
        );

        if (!$escenarioOferta) {
            $this->respuesta['data']['escenarioofertacentro'] = $escenarioOfertaCentro;

            return $this->respuesta;
        }

        if(array_key_exists("centro_id", $obj)){
            $centro = $this->getRepository(
                'CoreCoreBundle:Centro',
                array('centro_id' => $obj['centro_id']),
                null,
                200
            );

            if (!$centro) {
                $this->respuesta['data']['escenarioofertacentro'] = $escenarioOfertaCentro;

                return $this->respuesta;
            }
        }else{

            $grupo = $this->getRepository(
                'CoreCoreBundle:Grupo',
                array('grupo_id' => $obj['grupo_id']),
                null,
                200
            );

            if (!$grupo) {
                $this->respuesta['data']['escenarioofertacentro'] = $escenarioOfertaCentro;

                return $this->respuesta;
            }
        }
        $escenarioOfertaCentro=new EEscenarioOfertaCentro();
        $escenarioOfertaCentro->setActivo(true);
        if(array_key_exists("centro_id", $obj)){
            $escenarioOfertaCentro->setCentro($centro);
        }else{
            $escenarioOfertaCentro->setGrupo($grupo);
        }
        $escenarioOfertaCentro->setEscenarioOferta($escenarioOferta);
        $escenarioOfertaCentro->setUsuario($usuario);
        $escenarioOfertaCentro->setUsuarioModifica($usuario);
        $fecha=new \DateTime();
        $escenarioOfertaCentro->setFechaAlta($fecha);
        $escenarioOfertaCentro->setFechaModificacion($fecha);

        try {
            $this->entityManager->persist($escenarioOfertaCentro);

            $this->entityManager->flush();

            $this->buildRespuesta('post', array('escenarioofertacentro' => $escenarioOfertaCentro));
        } catch (DBALException $e) {
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }
        return $this->respuesta;
    }

    /**
     * obtencion de un EscenarioOferta , si no existe lo crea
     *
     * @param mixed    $escenarioOferta escenarioOferta que se vinculará
     * @param mixed    $objeto bojeto que se vinculara en un escenarioofertacentro , puede ser un centro o un grupo
     * @param mixed    $usuario usuario de creación
     * @param DateTime $fecha fecha de creación
     * @param string   $tipo tipo de escenario que se tiene: grupal|individual
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\EscenarioOfertaCentro EscenarioOfertaCentro obtenido|creado
     */
    public function obtenerEscenarioOfertaCentroByCentroOrByGrupoOrCrear($escenarioOferta,$objeto,$usuario,$fecha,$tipo){

        $condiciones = array();
        switch($tipo){
            case 'centro':
                $condiciones = array(
                    'EscenarioOferta' =>$escenarioOferta,
                    'Centro'          =>$objeto
                );
                $entidad = 'setCentro';
            break;
            case 'grupo':
                $condiciones = array(
                    'EscenarioOferta' =>$escenarioOferta,
                    'Grupo'          =>$objeto
                );
                $entidad = 'setGrupo';
            break;
        }

        $escenarioOfertaCentro = $this->getRepository(
            'ControlEscolarCalendarioBundle:EscenarioOfertaCentro',
            $condiciones,
            null,
            200,
            null,
            'findBy'
        );
        if (!$escenarioOfertaCentro) {
            $escenarioOfertaCentro = new EEscenarioOfertaCentro();
            $escenarioOfertaCentro->setActivo(true);
            $escenarioOfertaCentro->$entidad($objeto);
            $escenarioOfertaCentro->setEscenarioOferta($escenarioOferta);
            $escenarioOfertaCentro->setFechaAlta($fecha);
            $escenarioOfertaCentro->setFechaModificacion($fecha);
            $escenarioOfertaCentro->setUsuario($usuario);
            $escenarioOfertaCentro->setUsuarioModifica($usuario);
        }
        return $escenarioOfertaCentro;
    }
}
