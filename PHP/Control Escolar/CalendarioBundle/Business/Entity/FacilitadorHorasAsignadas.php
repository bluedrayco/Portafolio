<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Core\CoreBundle\Business\Entity\BusinessEntity;

class FacilitadorHorasAsignadas extends BusinessEntity {
    /**
     * Devuelve las horas asignadas a facilitadores
     * @param  integer $centro_id CENTRO_ID en MAKO
     * @param  integer $facilitador_id FACILITADOR_ID en CE
     * @param  integer $id_facilitador_mako FACILITADOR_ID en MAKO
     * @param  string $fecha_inicio Fecha de Inicio del rago de búsqueda
     * @param  string $fecha_fin Fecha de Fin del rago de búsqueda
     * @param  string $tipo_rpt Tipo de reporte a generar, a través del WS de SIE
     * @return Array            Array con los hash de horas asignadas a facilitadores
     */
    public function obtenerReporteFacilitadorHorasAsignadas($centro_id, $facilitador_id, $id_facilitador_mako, $fecha_inicio, $fecha_fin, $tipo_rpt) {
        // Se obtiene bandera para ejecutar consulta en PRODUCCIÓN o AMBIENTE DE PRUEBAS
        $test            = $GLOBALS['kernel']->getContainer()->getParameter('sie_ws_test') == true? '&test':'';
        // Se obtiene URI de ambiente de PRUEBAS o PROD
        $url             = $GLOBALS['kernel']->getContainer()->getParameter('sie_ws_server') . $GLOBALS['kernel']->getContainer()->getParameter('sie_ws_path');

        $params          = sprintf("centro=%d&facilitador=%d&id_facilitador_mako=%d&fecha_inicio=%s&fecha_fin=%s&tipo_rpt=%d", $centro_id,$facilitador_id, $id_facilitador_mako,$fecha_inicio, $fecha_fin, $tipo_rpt);
        $err_msg = "";
        try {
            $ch        = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url.'?'.$params . $test ); //Url together with parameters
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 15); //Timeout after 15 seconds
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result     = curl_exec($ch);

            if(curl_error($ch)){
                $err_msg = "Ocurrio  un error al intentar obtener la asistencia de un curso en mako el error fue el siguiente: " . curl_error($ch);
                $this->log->error($err_msg);
                curl_close($ch);
                return $this->buildRespuesta(
                    "",
                    $err_msg,
                    false,
                    404
                );
            }

            curl_close($ch);
            $tmp    = json_decode($result,true);
            $result = $tmp;
        } catch (\Exception $e) {
            $err_msg = "No fue posible conectarse a mako 1.4 para consultar por los inscritos a un curso, el error fue el siguiente: " . $e->getMessage();
            $this->log->error($err_msg);
            return $this->buildRespuesta(
                    "",
                    $err_msg,
                    false,
                    404
                );
        }
        return $this->buildRespuesta(
                    "",
                    $result,
                    false,
                    200
                );
    }

     /**
     * Devuelve los centros con la equivalencia correspondiente de mako
     * @return Array            Array con los centros
     */
    public function obtenerIdMakoCentro() {
        $centrosMako = array();

        $centrosMako = $this->getRepository(
                'CoreCoreBundle:Centro',
                array(),
                null,
                404,
                null,
                'findCentrosMakoId'
        );

        if (!$centrosMako) {
            $this->buildRespuesta(null, array('centrosMako' => array()),true,200);
            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('centrosMako' => $centrosMako));

    }

}
