<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaDates;

/**
 *  * Lógica de negocio de la manipulación de una OfertaEducativaCentro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class Aula extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de las Aulas
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de las Aulas
 */
    public function listar() {
        $aulas = array();

        $this->log->debug('Entrando al business de Aula');

        $aulas = $this->getRepository(
            'ControlEscolarCalendarioBundle:Aula',
            (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
            null,
            200,
            null,
            (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
            (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$aulas) {
            $this->respuesta['data']['aulas'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('aulas' => $aulas));
    }

}
