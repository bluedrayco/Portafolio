<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;

use ControlEscolar\CalendarioBundle\Entity\TipoMovimiento as ETipoMovimiento;
use Core\CoreBundle\Business\Entity\GenericRest           as BGenericRest;

use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class TipoMovimiento extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    public function getTipoMovimientoByClave($clave){
        $genericRestBusiness = new BGenericRest($this->entityManager,$this->entityManagerMako);
        $tipoMovimiento=$genericRestBusiness->listarGenerico('ControlEscolarCalendarioBundle:TipoMovimiento', array('filter'=>"tipomovimiento.clave='{$clave}'"),array('incluir_activo'=>true));
        return count($tipoMovimiento)>1?$tipoMovimiento['data']['controlescolarcalendariobundle:tipomovimiento'][0]:null;
    }
}
