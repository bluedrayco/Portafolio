<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use ControlEscolar\CalendarioBundle\Entity\OcupacionReserva as EOcupacionReserva;
use ControlEscolar\CalendarioBundle\Business\Entity\Ocupacion as BOcupacion;
use ControlEscolar\ReservaBundle\Business\Entity\Calendario as BCalendario;
use ControlEscolar\CalendarioBundle\Business\Entity\Parametro as BParametro;
use ControlEscolar\CalendarioBundle\Business\Entity\ParametroActividad as BParametroActividad;
use Core\CoreBundle\Business\Entity\Equivalencia as BEquivalencia;
use Core\CoreBundle\Business\Entity\Tabla as BTabla;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Core\UserBundle\Entity\Usuario;
use Core\SyncBundle\Business\Action\NotificacionEventoCentroACentralSync as BNotificacionEventoCentroACentralSync;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaTransformsToArray as LEnovaTransformsToArray;
use Core\SyncBundle\Business\Action\EventoMakoSync as BEventoMakoSync;
use Core\SyncBundle\Business\Action\CursoMakoSync as BCursoMakoSync;
use ControlEscolar\CalendarioBundle\Business\Entity\Movimiento as BMovimiento;
use Core\CoreBundle\Business\Entity\GenericRest as BGenericRest;
use Enova\Mail\Sendmail as LSendMail;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaEducativaCentro       as BOfertaEducativaCentro;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OcupacionReserva extends BusinessEntity {

    private $servicioId;
    private $estatusCalendario;

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
        $this->servicioId = 1;
        $this->estatusCalendario = 2;
        $this->centroId = $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro");
    }

    /**
     * Listado de Eventos de OcupacionReserva
     * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
     * @return mixed objeto respuesta con los datos de los eventos de OcupacionReserva
     */
    public function listar() {
        $ocupacionesReserva = array();

        $ocupacionesReserva = $this->getRepository(
                'ControlEscolarCalendarioBundle:OcupacionReserva', func_get_args()[0], null, 200, null, 'findEventosBy', array()
        );

        if (!$ocupacionesReserva) {
            $this->respuesta['data']['ocupacionesreserva'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('ocupacionesreserva' => $ocupacionesReserva));
    }

    /**
     * se obtiene un arreglo de fechas y se genera un arreglo de clave=fecha y valor=fecha
     * @param array $diasFeriados objeto con días feriados
     * @return array arreglo de dias
     */
    public function crearEventosCargaMasiva($obj) {
        $this->respuesta['code'] = 200;
        $this->respuesta['data'] = $obj;
        return $this->respuesta;
    }

    /**
     * Generamos los eventos en reserva20 y ocupación reserva
     * @param  object $facilitadorObj Objeto Facilitador
     * @param  object $ocupacionObj   Objeto Ocupación
     * @param  object $usuarioObj     Objeto Objeto Usuario de sesión
     * @param  array  $fechas         Array que contiene cada uno de los eventos en la sig estructura: fecha_inicio, fecha_fin, hora_inicio, hora_fin
     * @return boolean                true ok false error
     */
    public function generaEventos($aulaObj, $facilitadorObj, $ocupacionObj, $usuarioObj, $fechas) {
        try {
            $tablaBusiness = new BTabla($this->entityManager,$this->entityManagerMako);
            $tabla = $tablaBusiness->listar(array(
                'filter' => "tabla.nombre='Reserva20::Aula'",
            ));
            $equivalencia = $this->getRepository(
                    "CoreCoreBundle:Equivalencia", array(
                'Tabla' => $tabla['data']['tablas'],
                'valor_origen' => $aulaObj->getAulaId()
                    ), null, null, null, 'findBy', null
            );
            $arreglo = array();
            //Generamos la estructura a enviar a reserva20
            $reserva = array(
                "usuario_id" => $usuarioObj->getId(),
                "estatus_calendario_id" => $this->estatusCalendario,
                "servicio_id" => $this->servicioId,
                "cursos" => array(
                    "espacio_id" => $equivalencia[0]->getValorDestino(),
                    "centro_id" => $this->centroId,
                    "eventos" => $fechas
                )
            );
            $reserva20 = new BCalendario($this->entityManager,$this->entityManagerMako);
            $eventosReserva = $reserva20->crearArregloEventos($reserva);

            //Recorremos todos los eventos proporcionados por reserva20
            foreach ($eventosReserva as $evento) {

                $calendarioEvento = new EOcupacionReserva();
                $calendarioEvento->setActivo(true);
                $calendarioEvento->setFacilitador($facilitadorObj);
                $calendarioEvento->setAula($aulaObj);
                $calendarioEvento->setFechaAlta(new \DateTime());
                $calendarioEvento->setFechaEvento($evento["fecha_inicio"]);
                $calendarioEvento->setFechaModificacion(new \DateTime());
                $calendarioEvento->setHoraFin($evento["hora_fin"]);
                $calendarioEvento->setHoraInicio($evento["hora_inicio"]);
                $calendarioEvento->setNumeroAsistentes(0);
                $calendarioEvento->setOcupacion($ocupacionObj);
                $calendarioEvento->setReservaId($evento["reserva_id"]);
                $calendarioEvento->setUsuarioAlta($usuarioObj);
                $calendarioEvento->setUsuarioModifica($usuarioObj);
                $this->entityManager->persist($calendarioEvento);
                $arreglo[] = $calendarioEvento;
            }
            $this->entityManager->flush();
        } catch (DBALException $ex) {
            //throw new DBALException("Existe un translape con el Facilitador en la generacion de Eventos...");
            $tmp = explode("##calendario_exception##", $ex->getMessage());
            $this->log->error("No se pudieron crear los eventos: " . $tmp[1]);
            throw new DBALException($tmp[1]);
        }
        return $arreglo;
    }

    /**
     * Modificacion del Evento
     * @param integer $obj[usuario_id] identificador del usuario que modificara el evento
     * @param integer $obj[ocupacion_reserva_id] identificador de la ocupacion reserva que se modificara
     * @param integer $obj[facilitador_id] identificador del facilitador que cambiara
     * @param integer $obj[aula_id] identificador de la nueva aula
     * @param integer $obj[fecha_evento] nueva fecha del evento
     * @param integer $obj[hora_inicio] nueva hora de inicio evento
     * @param integer $obj[numero_asistentes] nuevo numero de asistentes del evento
     * @param integer $obj[hora_fin] nueva hora fin del evento
     * @return mixed objeto con respuesta afirmativa o erronea
     * @throws DBALException Excepcion de base de datos
     */
    public function actualizar($obj) {
        //exit();
        $flag = false; //bandera para determinar si debemos cambiar en reserva el evento
        $usuario = $this->getUsuarioById($obj['usuario_id']);
        if (!$usuario) {
            return $this->respuesta;
        }
        $ocupacionReserva = $this->obtenerOcupacionReservaById($obj['ocupacion_reserva_id']);
        if (!$ocupacionReserva) {
            return $this->respuesta;
        }


        $flag_notificacion_socios_inscritos = false;

        $this->entityManager->beginTransaction();
        try {

            if (array_key_exists('fecha_evento', $obj) && array_key_exists('hora_inicio', $obj)) {
                $fechaEvento = new \DateTime($ocupacionReserva->getFechaEvento()->format('Y-m-d') . ' ' . $ocupacionReserva->getHoraInicio()->format('H:i:s'));
                $fechaNueva = new \DateTime($obj['fecha_evento'] . ' ' . $obj['hora_inicio']);
                if ($fechaEvento != $fechaNueva) {
                    $validacionOcupacionReserva = $this->validaEvento($ocupacionReserva->getFechaEvento()->format('Y-m-d'), $ocupacionReserva->getHoraInicio()->format('H:i:s'), $obj['fecha_evento'], $obj['hora_inicio']);
                    switch ($validacionOcupacionReserva['opcion']) {
                        case 1:
                            $this->entityManager->rollback();
                            $parametroBusiness = new BParametro($this->entityManager,$this->entityManagerMako);
                            $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                            return $this->buildRespuesta("No es posible cambiar la sesión a la fecha, dia y hora inicial y final, la sesión ya ha pasado.", array('ocupacionreserva' => null), false, 404);
                            break;

                        case 2:
                            $this->entityManager->rollback();
                            $parametroBusiness = new BParametro($this->entityManager,$this->entityManagerMako);
                            $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                            return $this->buildRespuesta("No es posible cambiar la sesión a la fecha, dia y hora inicial y final porque debe ser mas de {$numeroHorasMinimasRecorrido} hrs de diferencia con respecto a la hora actual para ser modificada.", array('ocupacionreserva' => null), false, 404);
                            break;

                        case 3:
                            $this->entityManager->rollback();
                            $parametroBusiness = new BParametro($this->entityManager,$this->entityManagerMako);
                            $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                            return $this->buildRespuesta("No es posible cambiar la actividad a la fecha, dia y hora inicial y final porque debe ser " . $numeroHorasMinimasRecorrido . "hrs antes de que inicie la actividad", array('ocupacionreserva' => null), false, 404);
                            break;
                    }
                    $flag_notificacion_socios_inscritos = true;
                }
            }

            $ocupacionReservaAnterior = clone $ocupacionReserva; //salvamos ocupacion anterior para comparar si hubo alguna modificacion
            $parametroBusiness  = new BParametroActividad($this->entityManager);
            $numero_movimientos = $parametroBusiness->obtenerParametroPorActividad('numero_maximo_movimientos',$ocupacionReserva->getOcupacion()->getActividadAcademica());
            $movimientos = $numero_movimientos[0]->getValor();

            $arregloActualizacion = array();
            if ($ocupacionReserva->getOcupacion()->getNumeroMovimientos() >= $movimientos && $this->esCentro($obj['central'])) {
                $this->entityManager->rollback();
                return $this->buildRespuesta("Has realizado el # máximo de movimientos por Grupo, ya no tienes más movimientos autorizados", array(), false, 404);
            }
            $respuesta = $this->modificarReserva($ocupacionReserva, $obj, $arregloActualizacion);
            $flag = $respuesta['flag'];
            $ocupacionReserva = $respuesta['reserva'];
            if (!$this->compareEvent($ocupacionReserva, $ocupacionReservaAnterior)) {
                $this->entityManager->rollback();
                return $this->buildRespuesta("put", array('ocupacionreserva' => $ocupacionReserva), false, 200);
            }
            $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->entityManager,$this->entityManagerMako);
            if ($this->esCentro($obj['central']) &&$ofertaEducativaCentroBusiness->ofertaEducativaPublicada($ocupacionReserva->getOcupacion()->getOfertaEducativaCentro())) {
                $ocupacionReserva->getOcupacion()->setNumeroMovimientos((($ocupacionReserva->getOcupacion()->getNumeroMovimientos() + 1)));
            }
            //se analizan el numero de movimientos para saber si peude modificarse o no la ocupacion
            if ($flag) {
                $calendarioBusiness = new BCalendario($this->entityManager,$this->entityManagerMako);
                $arregloActualizacion['calendario_id'] = $ocupacionReserva->getReservaId();
                $arregloActualizacion['usuario_id'] = $obj['usuario_id'];
                $calendarioBusiness->actualizar($arregloActualizacion);
            }
            $ocupacionReserva->setUsuarioModifica($usuario);
            $ocupacionReserva->setFechaModificacion(new \DateTime());

            $eventoMakoSyncBusiness = new BEventoMakoSync($this->entityManager,$this->entityManagerMako);
            $respuesta = $eventoMakoSyncBusiness->sincronizarModificacionEventoWithMako($usuario, array(
                array(
                    'evento_nuevo' => $ocupacionReserva,
                    'evento_anterior' => $ocupacionReservaAnterior
                )
                    )
            );
            if ($respuesta['code'] == 404) {
                $this->entityManager->rollback();
                return $this->buildRespuesta('Error al modificar el evento, no se pudo sincronizar con mako 1.4', array(), false, 404);
            }
            /////////////////calibrar la ocupacion en caso de haber sido modificada, se actualiza y se sincroniza con mako 1.4
            $ocupacionAnterior = $ocupacionReserva->getOcupacion();
            $ocupacionBusiness = new BOcupacion($this->entityManager,$this->entityManagerMako);
            $respuesta = $ocupacionBusiness->calibrarFechasOcupacion($ocupacionAnterior, $usuario);
            if ($respuesta['respuesta']) {
                $cursoMakoBusiness = new BCursoMakoSync($this->entityManager,$this->entityManagerMako);
                $paquete = $cursoMakoBusiness->sincronizarCursoWithMako($usuario, $respuesta['ocupacion'], null, 'update', null, $ocupacionAnterior);
                if ($paquete['code'] == 404) {
                    $this->entityManager->rollback();
                    throw new DBALException('No se pudo sincronizar con mako 1.4 ');
                }
            }

            if($respuesta['ocupacion']->getOfertaEducativaCentro()->getPublicado()){
                //////////////////////////////////////////////////////////////////////
                $notificacionEventoCentroACentralSyncBusiness = new BNotificacionEventoCentroACentralSync($this->entityManager,$this->entityManagerMako);
                $paquete = $notificacionEventoCentroACentralSyncBusiness->sincronizarNotificacionModificacionEventoWithCentral($ocupacionReservaAnterior, $ocupacionReserva, $usuario);
                if ($paquete['code'] == 404) {
                    $this->entityManager->rollback();
                    return $this->buildRespuesta('Error al modificar el evento, no se pudo notificar a los usuarios involucrados', array(), false, 404);
                }
            }
            //$this->entityManager->flush();
            $this->registrarMovimientoEventoSiEsModificado($ocupacionReserva, $ocupacionReservaAnterior, $usuario);

            if ($flag_notificacion_socios_inscritos) {
                //echo "entre...";
                $ocupacionBusiness = new BOcupacion($this->entityManager,$this->entityManagerMako);
                $inscritos = $ocupacionBusiness->getInscritosEInteresados($ocupacionReserva->getOcupacion())['inscritos'];
                if (count($inscritos) != 0) {
                    //echo "entre...2";
                    $ocupacionBusiness->registrarMovimientoSociosInscritos($ocupacionReserva->getOcupacion(), $inscritos, $usuario, 'notificacion_evento_alumnos');
                    $paquetes = $this->enviarEmailsSociosInscritos($ocupacionReservaAnterior, $ocupacionReserva, $inscritos);
                }
            }
            $this->entityManager->commit();
            return $this->buildRespuesta(null, array('ocupacionreserva' => $ocupacionReserva));
        } catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta($ex->getMessage(), array('ocupacionreserva' => $ocupacionReserva), false, 404);
            return $this->respuesta;
        }
        catch (\Exception $ex){
            $this->entityManager->rollback();
            $this->buildRespuesta($ex->getMessage(),array('ocupacionreserva'=>$ocupacionReserva), false, 404);
            return $this->respuesta;
        }
        return $this->respuesta;
    }

    public function enviarEmailsSociosInscritos($eventoAnterior, $eventoNuevo, $inscritos) {
        if ($eventoNuevo->getOcupacion()->getActividad()) {
            $nombreCurso = $eventoNuevo->getOcupacion()->getActividad()->getNombre();
        } else {
            $nombreCurso = $eventoNuevo->getOcupacion()->getActividadAcademica()->getNombre();
        }
        $diaSemanaTmp = array(0 => 'Domingo', 1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado');
        $fechaAnterior = $eventoAnterior->getFechaEvento()->format('d-m-Y');
        $diaAnterior = $diaSemanaTmp[$eventoAnterior->getFechaEvento()->format('w')];
        $horaInicioAnterior = $eventoAnterior->getHoraInicio()->format('H:i');
        $horaFinAnterior = $eventoAnterior->getHoraFin()->format('H:i');

        $fechaNueva = $eventoNuevo->getFechaEvento()->format('d-m-Y');
        $diaNuevo = $diaSemanaTmp[$eventoNuevo->getFechaEvento()->format('w')];
        ;
        $horaInicioNueva = $eventoNuevo->getHoraInicio()->format('H:i');
        $horaFinNueva = $eventoNuevo->getHoraFin()->format('H:i');

        $cadenaTmp = "";
        //$cadena = "te informó que la actividad {$nombreCurso} en que te encuentras inscrito cambio de la fecha {$fechaAnterior}, {$diaAnterior}, {$horaInicioAnterior} {$horaFinAnterior} a {$fechaNueva}, {$diaNuevo}, {$horaInicioNueva} {$horaFinNueva}, por lo cual te invitamos a que consideres este cambio para que te presentes en tu centro en esta fecha y hora para continuar con tu curso. <br><br>Gracias por tu preferencia!.";
        $cadena = "Te informamos que el curso al que estas inscrito {$nombreCurso} ha cambiado de fecha o de hora.<br>En esta ocasión, lo realizarás el día {$fechaNueva} a las {$horaInicioNueva} horas. Después de esta fecha, regresará al horario programado.<br><br>Gracias por tu preferencia, ¡Te esperamos!";
        $this->log->info("---------------------------------MENSAJE para el socio inscrito: {$cadena}");
        //echo "\n\ncadena:  ".$cadena;
        $arregloEmails = array();
        $sendMailLib = new LSendMail();
//        print_r($inscritos);
//        exit();
        foreach ($inscritos as $inscrito) {
            if ($inscrito['email'] != null) {
                //echo "email->{$inscrito['email']}";
                $cadenaTmp = "Existe un cambio en tu curso. ¡Anótalo!.<br><br>Estimado socio: {$inscrito['nombre_completo']}. " . $cadena;
                try {
//                    echo "email->".$inscrito['email'];
                    $sendMailLib->send($inscrito['email'], 'Cambio fecha de curso', $cadenaTmp);
                } catch (\Exception $ex) {
                    $this->log->error("MENSAJE de error------->".$ex->getMessage());
                    throw new Exception($ex->getMessage());

                }

                //$notificacionActionBusiness->notificaPorMail('Cambio fecha de actividad', $cadenaTmp, $inscrito['email']);
            }
        }
    }

    public function esCentro($flag_central) {
        return !$flag_central;
    }

    public function validaEvento($fechaEvento, $horaInicioEvento, $fechaEventoNueva, $horaInicioNueva) {
        //echo "fecha {$fechaEventoNueva} {$horaInicioNueva}";
        $respuesta = array('respuesta' => true, 'opcion' => 0);
        $fechaInicioTmp = new \DateTime($fechaEventoNueva . ' ' . $horaInicioNueva);

        $fechaEventoActual = new \DateTime($fechaEvento . ' ' . $horaInicioEvento);

        //echo "entre...";
        $fechaActual = new \DateTime();
        $parametroBusiness = new BParametro($this->entityManager,$this->entityManagerMako);
        $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
        $diferenciaFechas = $fechaInicioTmp->diff($fechaActual);
        $horasDiferencia = (intval($diferenciaFechas->format('%a')) * 24) + intval($diferenciaFechas->format('%h'));
        if ($diferenciaFechas->format('%R') == '+') {
            $horasDiferencia = $horasDiferencia * -1;
        }
//        echo "horas {$horasDiferencia}";
//         echo "horas minimas recorrido: {$numeroHorasMinimasRecorrido}";
//        exit();
        //si no han superado el numero de horas validas
        if ($horasDiferencia < $numeroHorasMinimasRecorrido) {
            $respuesta['opcion'] = 3;
            $respuesta['respuesta'] = false;
            return $respuesta;
        }

        if ($fechaEventoActual < $fechaActual) {
            $respuesta['opcion'] = 1;
            $respuesta['respuesta'] = false;
            return $respuesta;
        }
        $diferenciaFechasEvento = $fechaEventoActual->diff($fechaActual);
        $horasDiferenciaEventoActual = (intval($diferenciaFechasEvento->format('%a')) * 24) + intval($diferenciaFechasEvento->format('%h'));
        if ($horasDiferenciaEventoActual < $numeroHorasMinimasRecorrido) {
            $respuesta['opcion'] = 2;
            $respuesta['respuesta'] = false;
            return $respuesta;
        }
        return $respuesta;
    }

    public function registrarMovimientoEventoSiEsModificado($eventoNuevo, $eventoAnterior, $usuario) {
        $stringer = new LEnovaTransformsToArray();
        $modificaciones = $stringer->obtenerModificacionesToArray($eventoAnterior, $eventoNuevo);
        $movimiento = null;
        if (!empty($modificaciones)) {
            $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
            $movimiento = $movimientoBusiness->crearMovimiento('\ControlEscolar\CalendarioBundle\Entity\Ocupacion', $eventoNuevo->getOcupacionReservaId(), 'modificacion_evento', json_encode($modificaciones), $usuario->getId());
        }
        return $movimiento;
    }

    public function modificarReserva($ocupacionReserva, $obj, &$arregloActualizacion) {
        try {

            if (array_key_exists('activo', $obj)) {
                $ocupacionReserva->setActivo($obj['activo']);
            }
            if (array_key_exists('facilitador_id', $obj)) {
                $facilitador = $this->getRepository(
                        "ControlEscolarCalendarioBundle:Facilitador", array('facilitador_id' => $obj['facilitador_id']), null, null, null, 'find', null
                );
                if (!$facilitador) {
                    throw new DBALException('No se encontro el facilitador con el id {$FacilitadorId}');
                }
                $ocupacionReserva->setFacilitador($facilitador);
            }
            if (array_key_exists('aula_id', $obj)) {
                $aula = $this->getRepository(
                        "ControlEscolarCalendarioBundle:Aula", array('aula_id' => $obj['aula_id']), null, null, null, 'find', null
                );
                if (!$aula) {
                    throw new DBALException('No se encontro el aula con el id {$AulaId}');
                }
                $ocupacionReserva->setAula($aula);
                $flag = true;
                $arregloActualizacion['espacio_id'] = $aula->getAulaId();
            }
            if (array_key_exists('fecha_evento', $obj)) {
                $ocupacionReserva->setFechaEvento(new \DateTime($obj['fecha_evento']));
                $flag = true;
                $arregloActualizacion['fecha_inicio'] = $obj['fecha_evento'];
                $arregloActualizacion['fecha_fin'] = $obj['fecha_evento'];
            }

            if (array_key_exists('hora_fin', $obj)) {
                $ocupacionReserva->setHoraFin(new \DateTime($obj['hora_fin']));
                $flag = true;
                $arregloActualizacion['hora_fin'] = $obj['hora_fin'];
            }
            if (array_key_exists('hora_inicio', $obj)) {
                $ocupacionReserva->setHoraInicio(new \DateTime($obj['hora_inicio']));
                $flag = true;
                $arregloActualizacion['hora_inicio'] = $obj['hora_inicio'];
            }
            if (array_key_exists('numero_asistentes', $obj)) {
                $ocupacionReserva->setNumeroAsistentes($obj['numero_asistentes']);
            }
            $this->entityManager->flush();
            return array('flag' => $flag, 'reserva' => $ocupacionReserva);
        } catch (DBALException $ex) {
            $tmp = explode("##calendario_exception##", $ex->getMessage());
            throw new DBALException($tmp[1]);
        }
    }

    public function obtenerOcupacionReservaById($ocupacionReservaId) {
        $ocupacionesReserva = $this->getRepository(
                'ControlEscolarCalendarioBundle:OcupacionReserva', array(
            'ocupacion_reserva_id' => $ocupacionReservaId
                ), null, 200, null, 'find', array()
        );

        if (!$ocupacionesReserva) {
            return null;
        }
        return $ocupacionesReserva;
    }

    /**
     * Obtenemos todos los eventos de  las reservas que se han realizado en centro.
     * @param  Object $ofertaEducativaObject OfertaEducativaCentro
     * @param  date   $fechaInicio           fecha Inicio
     * @param  [date  $fechaFinal            fecha final
     * @param  Object $facilitadorObj        Facilitador
     * @param  Object $aulaObj               Aula
     * @return Array                         Array de eventos
     */
    public function getEventos($ofertaEducativaObject, $fechaInicio, $fechaFinal, $aulaObj = null, $facilitadorObj = null) {

        $aulaId = ($aulaObj == null) ? null : $aulaObj->getAulaId();
        $facilitadorId = ($facilitadorObj == null) ? null : $facilitadorObj->getFacilitadorId();


        $eventos = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array(
            'fecha_inicio' => $fechaInicio,
            "fecha_final" => $fechaFinal,
            "oferta_educativa_centro_id" => $ofertaEducativaObject->getOfertaEducativaCentroId(),
            "aula_id" => $aulaId,
            "facilitador_id" => $facilitadorId
                ), null, 404, null, 'findEventosForCalendar', null
        );
        return (!$eventos) ? array() : $eventos;
    }

    public function eliminarEventos($ocupacionId) {
        $ocupacionesReserva = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array('ocupacion_id' => $ocupacionId), null, null, null, 'findReservasByOcupacionId', null
        );

        try {
            foreach ($ocupacionesReserva as $or) {
                $this->entityManager->remove($or);
            }
            $this->entityManager->flush();
        } catch (DBALException $e) {
            throw new DBALException("error al eliminar eventos...");
        }
    }

    public function compareEvent($origen, $destino) {
        //echo $this->getStringEventDifference($origen,$destino);
        if (
                ($origen->getFacilitador()->getFacilitadorId() != $destino->getFacilitador()->getFacilitadorId())
                OR ( $origen->getAula()->getAulaId() != $destino->getAula()->getAulaId())
                OR ( $origen->getFechaEvento()->format("Y-m-d") != $destino->getFechaEvento()->format("Y-m-d"))
                OR ( $origen->getHoraInicio()->format("H:i") != $destino->getHoraInicio()->format("H:i"))
                OR ( $origen->getHoraFin()->format("H:i") != $destino->getHoraFin()->format("H:i"))
        ) {
            return true;
        }
        return false;
    }

    public function getStringEventDifference($origen, $destino) {
        $cadena = "";
        if (($origen->getFacilitador()->getFacilitadorId() != $destino->getFacilitador()->getFacilitadorId())) {
            $cadena .=($cadena == "") ? "" : ", ";
            $cadena .= sprintf(
                    "el facilitador %s por el Facilitador %s", $this->getNombreFacilitador($origen->getFacilitador()), $this->getNombreFacilitador($destino->getFacilitador())
            );
        }

        if (($origen->getAula()->getAulaId() != $destino->getAula()->getAulaId())) {
            $cadena .=($cadena == "") ? "" : ", ";
            $cadena .= sprintf(
                    "el aula %s por el Aula %s", $origen->getAula()->getNombre(), $destino->getAula()->getNombre()
            );
        }

        if (
                ($origen->getFechaEvento()->format("Y-m-d") != $destino->getFechaEvento()->format("Y-m-d"))
                OR ( $origen->getHoraInicio()->format("H:i") != $destino->getHoraInicio()->format("H:i"))
                OR ( $origen->getHoraFin()->format("H:i") != $destino->getHoraFin()->format("H:i"))
        ) {
            $cadena .=($cadena == "") ? "" : ", ";
            $cadena .= sprintf(
                    "la fecha ''%s / %s - %s''  a la fecha ''%s / %s - %s''", $origen->getFechaEvento()->format("Y-m-d"), $origen->getHoraInicio()->format("H:i"), $origen->getHoraFin()->format("H:i"), $destino->getFechaEvento()->format("Y-m-d"), $destino->getHoraInicio()->format("H:i"), $destino->getHoraFin()->format("H:i")
            );
        }
//        echo $cadena;
//        exit();
        return $cadena;
    }

    public function getNombreFacilitador($facilitador) {
        return $facilitador->getNombre() . " " . $facilitador->getApellidoPaterno() . " " . $facilitador->getApellidoMaterno();
    }

    /**
     * Obtenemos el array de los eventos a partir de un objeto de tipo ocupacion
     * @param  Object $ocupacion Ocupacion
     * @param  array que permite ordenar los eventos. p.e.: ordenado por fecha de evento descendente, array('fecha_evento'=>'desc')
     * @return array             los eventos encontrados.
     */
    public function getReservaByOcupacion($ocupacion, $extraSearch= array('fecha_evento'=>'asc')) {
        $eventos = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array(
            'Ocupacion' => $ocupacion,
            'activo' => true
                ), null, null, null, 'findBy', array('extra_search' => $extraSearch)
        );
        return (!$eventos) ? array() : $eventos;
    }

    public function obtenerEventoById($ocupacionReservaId) {
        $genericRestBusiness = new BGenericRest($this->entityManager,$this->entityManagerMako);
        $tabla = $genericRestBusiness->listarGenerico('ControlEscolarCalendarioBundle:OcupacionReserva', array('filter' => "ocupacionreserva.ocupacion_reserva_id={$ocupacionReservaId}"), array('incluir_activo' => true));
        return count($tabla) > 1 ? $tabla['data']['controlescolarcalendariobundle:ocupacionreserva'][0] : null;
    }

    public function numeroHorasByArrayEventos($arregloEventos) {
        $numeroHoras = 0;
        foreach ($arregloEventos as $evento) {
            $horaInicio = $evento->getHoraInicio();
            $horaFin = $evento->getHoraFin();
            $diferencia = $horaFin->diff($horaInicio);
            $numeroHoras+=$diferencia->format('%H');
        }
        return $numeroHoras;
    }

    public function eliminarArregloEventos($eventos) {
        $arregloEventosIds = array();
        $arregloClonadoEventos = array();
        foreach ($eventos as $evento) {
            $arregloEventosIds[] = $evento->getReservaId();
        }
        $ocupacionesReserva = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array('ids' => $arregloEventosIds), null, null, null, 'findReservasByArregloOcupacionReservaId', null
        );

        try {
            foreach ($ocupacionesReserva as $or) {
                $this->entityManager->remove($or);
            }
            foreach ($eventos as $evento) {
                $arregloClonadoEventos[] = clone($evento);
                $this->entityManager->remove($evento);
            }
            $this->entityManager->flush();
        } catch (DBALException $e) {
            throw new DBALException("error al eliminar eventos...");
        }
        return $arregloClonadoEventos;
    }

    public function obtenerPrimerEventoByOcupacion($ocupacion) {
        $ocupacionReserva = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array('Ocupacion' => $ocupacion), null, null, null, 'findBy', array('extra_search' => array('ocupacion_reserva_id' => 'asc'))
        );
        return $ocupacionReserva[0];
    }

    public function obtenerPrimerYUltimoEventoByOcupacion($ocupacion) {
        $ocupacionReserva = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array('Ocupacion' => $ocupacion), null, null, null, 'findBy', array('extra_search' => array('fecha_evento' => 'asc'))
        );
        $primeraOcupacionReserva = null;
        $ultimaOcupacionReserva = null;
        if (count($ocupacionReserva) > 1) {
            $primeraOcupacionReserva = $ocupacionReserva[0];
            $ultimaOcupacionReserva = $ocupacionReserva[count($ocupacionReserva) - 1];
        } else {
            $primeraOcupacionReserva = $ocupacionReserva[0];
            $ultimaOcupacionReserva = $ocupacionReserva[0];
        }
        return array('primer_evento' => $primeraOcupacionReserva, 'ultimo_evento' => $ultimaOcupacionReserva);
    }

    public function obtenerEventosByOcupacionId($ocupacion) {

        $ocupacionReserva = $this->getRepository(
                "ControlEscolarCalendarioBundle:OcupacionReserva", array('Ocupacion' => $ocupacion), null, null, null, 'findBy', null
        );
        return $ocupacionReserva;
    }

    public function obtenerArrayEventosAnterioresNuevos($eventosAnteriores, $eventosNuevos) {
        $arregloEventosAnteriores = array();
        $arregloEventosNuevos = array();
        $eventoNuevoTmp = array();
        $eventoAnteriorTmp = array();
        $i = 0;
        foreach ($eventosAnteriores as $ev) {
            $eventoAnteriorTmp['fecha_evento'] = $ev->getFechaEvento()->format('Y-m-d');
            $eventoAnteriorTmp['hora_inicio'] = $ev->getHoraInicio()->format('H:i');
            $eventoAnteriorTmp['hora_fin'] = $ev->getHoraFin()->format('H:i');

            $eventoNuevoTmp['fecha_evento'] = $eventosNuevos[$i]->getFechaEvento()->format('Y-m-d');
            $eventoNuevoTmp['hora_inicio'] = $eventosNuevos[$i]->getHoraInicio()->format('H:i');
            $eventoNuevoTmp['hora_fin'] = $eventosNuevos[$i]->getHoraFin()->format('H:i');

            $arregloEventosAnteriores[] = $eventoAnteriorTmp;
            $arregloEventosNuevos[] = $eventoNuevoTmp;
            $i = $i + 1;
        }

        return array('eventos_anteriores' => $arregloEventosAnteriores, 'eventos_nuevos' => $arregloEventosNuevos);
    }

}
