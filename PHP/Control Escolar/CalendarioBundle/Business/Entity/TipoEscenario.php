<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\TipoEscenario as ETipoEscenario;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class TipoEscenario extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de los tipos de Escenario
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de los tipos de escenario
 */
    public function listar() {
        $tiposEscenario = array();

        $tiposEscenario = $this->getRepository(
                'ControlEscolarCalendarioBundle:TipoEscenario',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$tiposEscenario) {
            $this->respuesta['data']['tiposescenario'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('tiposescenario' => $tiposEscenario));
    }
}
