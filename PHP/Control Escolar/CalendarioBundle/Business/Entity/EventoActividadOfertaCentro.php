<?php
/***
 * Clase Business para  operaciones relacionadas a los eventos Sincronizados en el centro
 * @author silvio.bravo@enova.mx
 * @date   Enero 24 2015
 *
 */


namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\EventoActividadOfertaCentro as EEventoActividadOfertaCentro;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use \DateTime;
use \DateInterval;




class EventoActividadOfertaCentro extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Obtenemos todas las ofertas educativas
     * @param  [type] $ofertaEducativaObject [description]
     * @param  [type] $fechaInicio           [description]
     * @param  [type] $fechaFinal            [description]
     * @return [type]                        [description]
     */
    public function getEventos($ofertaEducativaObject, $fechaInicio, $fechaFinal){

        $eventos = $this->getRepository(
                             "ControlEscolarCalendarioBundle:EventoActividadOfertaCentro",
                             array(
                                    'fecha_inicio'                  => $fechaInicio,
                                    "fecha_final"                   => $fechaFinal,
                                    "oferta_educativa_centro_id"    => $ofertaEducativaObject->getOfertaEducativaCentroId()
                                ),
                             null,
                             404,
                             null,
                             'findEventosForCalendar',
                             null
                         );

        return (!$eventos)?array():$eventos;
    }

}
