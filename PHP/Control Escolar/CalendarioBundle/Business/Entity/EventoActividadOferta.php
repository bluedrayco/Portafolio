<?php
/***
 * Clase Business para  operaciones relacionadas a los eventos de las actividades de la Oferta Educativa
 * @author silvio.bravo@enova.mx
 * @date   Dic 09 2014
 *
 */


namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\EventoActividadOferta as EEventoActividadOferta;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use \DateTime;
use \DateInterval;

class EventoActividadOferta extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Generamos Array de fechas evaluando los dias feriados.
     *
     * @param  array  $horario      description
     * @param  int    $totalHoras   total de Horas que se deben
     * @param  date   $fechaInicio  Fecha en la que debe iniciar
     * @param  array  $diasFeriados array de fechas que representan un dia feriado, el key de cada opcion del array debe ser una fecha "2015-01-01"=>"x"
     *
     * @return array                Array de fechas
     */
    public function generaFechasEventos($horario, $totalHoras, $fechaInicio,$diasFeriados, $fechaFinal=null){



        $arreglo                    = ["fechaInicio"=>null, "fechaFinal"=>null, "fechas"=>array()];
        $contadorHoras              = 0;
        $fechaActual                = new DateTime($fechaInicio);
        $fechaActual                = new DateTime($fechaActual->format("Y-m-d"));
        $tmpFechaFinal              =($fechaFinal == null)?null:new DateTime((new DateTime($fechaFinal))->format("Y-m-d"));

        //Recorremos mientras tengamos horas dispobibles para sesiones
        while ( ( $fechaFinal==null && ($contadorHoras < $totalHoras) ) || ($fechaFinal!=null && ($tmpFechaFinal >= $fechaActual))){
            $diaSemana               = $fechaActual->format("w");                   // Obtenemos el Dia de la semana en formato (0-6)
            if(array_key_exists($fechaActual->format("Y-m-d"), $diasFeriados)){
                $fechaActual->add(new DateInterval('P1D'));                         // Si  la fecha cae en dia festivo se aumenta un dia y volvemos a entrar al while
                continue;
            }

            foreach ($horario as $dia) {                                            // Recorremos el horario del curso


                if(intval($dia["dia_semana"]) == intval($diaSemana)){               // Si el dia actual concuerda con uno de los dias que se requieren en el horario

                    if(count($arreglo["fechas"]) ==0){

                        $arreglo["fechaInicio"] = $fechaActual->format("Y-m-d ") . $dia["hora_inicio"];
                    }
                    $arreglo["fechaFinal"]      = $fechaActual->format("Y-m-d ") . $dia["hora_fin"];
                    $arreglo["fechas"][]        = array(
                                              "fecha"           => $fechaActual->format("Y-m-d"),
                                              "fecha_inicio"    => $fechaActual->format("Y-m-d"),
                                              "fecha_fin"       => $fechaActual->format("Y-m-d"),
                                              "inicio"          => $dia["hora_inicio"],
                                              "fin"             => $dia["hora_fin"],
                                              "hora_inicio"     => $dia["hora_inicio"],
                                              "hora_fin"        => $dia["hora_fin"]
                                            );

                    $fechaInicial           = new DateTime($fechaActual->format("Y-m-d ") . $dia["hora_inicio"]);
                    $fechaFinalHorario      = new DateTime($fechaActual->format("Y-m-d ") . $dia["hora_fin"]);
                    $intervalo              = $fechaInicial->diff($fechaFinalHorario);
                    $contadorHoras         += ((intval($intervalo->format("%h"))*60) + intval($intervalo->format("%i")) )/60;
                    break;
                }
            } //Fin de recorrer los horarios
            $fechaActual->add(new DateInterval('P1D'));                             //Aumentamos un dia para continuar analizando las fechas

        }//Fin de While de contador de Horas.
        $arreglo['totalHoras']=$contadorHoras;
        return $arreglo;
    }







    /**
     * Guardamos los eventos en la base de datos, a partir de un Horario definido.
     * @param  object  $ofertaActividad     Objeto que representa una ofertaActividad
      *@param  array   $vtfechas            array con objetos donde cada objeto contiene: fecha, inicio, fin
     * @param   object $usuario             usuario que representa la sesion
     * @param  boolean                      true si todo fue Ok o false si algo ha salido mal
     */
    public function generaEventos($ofertaActividad, $vtFechas, $usuario){
        //$vtFechas       = $this->generaFechasEventos($horario, $totalHoras, $fechaInicio,$diasFeriados);

        try{
                foreach ($vtFechas as $fecha) {

                    $evento     = new EEventoActividadOferta();
                    $evento->setFechaEvento(new \DateTime($fecha["fecha"]));
                    $evento->setHoraInicio(new \DateTime($fecha["inicio"]));
                    $evento->setHoraFin(new \DateTime($fecha["fin"]));
                    $evento->setFechaAlta(new \DateTime());
                    $evento->setOfertaActividad($ofertaActividad);
                    $evento->setUsuario($usuario);
                    $this->entityManager->persist($evento);
                }
                $this->entityManager->flush();
            }
            catch (DBALException $e) {
                    throw new Exception($e);
                    return false;
            }
        return true;
    }//generaEventos



    public function eliminaEventosDesdeOfertaActividad($ofertaActividad){
        try{
            $query= $this->entityManager->createQuery(
                    'DELETE ControlEscolarCalendarioBundle:EventoActividadOferta eao '
                    .'WHERE eao.OfertaActividad = :OfertaActividadId');
            $query->setParameter('OfertaActividadId',$ofertaActividad);
            $query->execute();
        }
        catch(DBALException $ex) {
          return false;
      }
      return true;
    }

}

?>