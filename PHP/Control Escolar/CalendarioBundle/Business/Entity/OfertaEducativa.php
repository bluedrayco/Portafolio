<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Enova\Mail\Sendmail;
use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\Escenario as EEscenario;
use ControlEscolar\CalendarioBundle\Entity\OfertaEducativa as EOfertaEducativa;
use Core\SyncBundle\Business\Action\SyncScheduler as BSyncScheduler;
use Core\SyncBundle\Business\Entity\SincroniaDB as BSincroniaDB;
use Core\SyncBundle\Business\Action\GeneraPaqueteSync as BGeneraPaquetesSync;
use Core\SyncBundle\Business\Action\ActividadMakoSync as BActividadMakoSync;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use ControlEscolar\PlanAnualBundle\Business\Entity\PlanAnual as BPlanAnual;
use ControlEscolar\CalendarioBundle\Business\Entity\DiaFeriado as BDiaFeriado;
use Core\CoreBundle\Lib\EnovaDates;
use ControlEscolar\CalendarioBundle\Entity\SincronizaOfertaCentro as ESincronizaOfertaCentro;
use Core\CoreBundle\Business\Entity\ActividadAcademica as BActividadAcademica;
use Core\NotificacionesBundle\Entity\Notificacion as ENotificacion;
use Core\NotificacionesBundle\Business\Entity\Notificacion as BNotificacionEntity;
use ControlEscolar\CalendarioBundle\Business\Entity\Parametro as BParametro;
use ControlEscolar\PlanAnualBundle\Business\Entity\Actividad as BActividad;

/**
 *  * Lógica de negocio de la manipulación de una OfertaEducativa
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OfertaEducativa extends BusinessEntity {

    const SINCRONIZACION_EXITOSA = 0;
    const SINCRONIZACION_PARCIAL = 1;
    const SINCRONIZACION_ERRONEA = 2;

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Obtención de la OfertaEducativa que cumpla que: este en Estatus 1, Sincronizado en False y activo en True
     * @return mixed objeto respuesta con el último registro de la OfertaEducativa que cumpla la condición anterior
     */
    public function obtenerOfertaEducativaActiva() {
        $ofertaEducativa = array();
        $ofertaEducativa = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativa', array(), null, 200, null, 'findOfertaEducativaActiva', array()
        );
        if (!$ofertaEducativa) {
            $this->respuesta['data']['ofertaeducativa'] = $ofertaEducativa;
            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('ofertaeducativa' => $ofertaEducativa));
    }

    /**
     * Actualización de datos de una OfertaEducativa
     * @param int $obj['oferta_educativa_id'] identificador de la OfertaEducativa a ser modificada
     * @param int $obj['oferta_educativa_estatus_id'] identificador del nuevo estatus de la OfertaEducativa
     * @param int $obj['periodo_id'] identificador del nuevo periodo de la OfertaEducativa
     * @param int $obj['fecha_inicio'] nueva fecha de inicio de la OfertaEducativa
     * @param int $obj['nombre'] nuevo nombre de la OfertaEducativa
     * @param int $obj['sincronizado'] nuevo estatus de sincronizacion de la OfertaEducativa
     * @return mixed objeto respuesta
     */
    public function actualizar($obj) {
        $ofertaEducativa = $this->getRepository(
                "ControlEscolarCalendarioBundle:OfertaEducativa", array('oferta_educativa_id' => $obj['oferta_educativa_id'])
        );

        if (!$ofertaEducativa) {
            return $this->respuesta;
        }

        if (array_key_exists('oferta_educativa_estatus_id', $obj)) {

            $ofertaEducativaEstatus = $this->getRepository(
                    "ControlEscolarCalendarioBundle:OfertaEducativaEstatus", array('oferta_educativa_estatus_id' => $obj['oferta_educativa_estatus_id']), null, null, null, 'find', null
            );

            if (!$ofertaEducativaEstatus) {
                $this->buildRespuesta('put');
                return $this->respuesta;
            }
            $ofertaEducativa->setOfertaEducativaEstatus($ofertaEducativaEstatus);
        }

        if (array_key_exists('nombre', $obj)) {
            $ofertaEducativa->setNombre($obj['nombre']);
        }

        if (array_key_exists('sincronizado', $obj)) {
            $ofertaEducativa->setSincronizado($obj['sincronizado']);
        }

        if (array_key_exists('fecha_inicio', $obj)) {

            $fechaInicio = \DateTime::createFromFormat('Y-m-d', $obj['fecha_inicio']);
            $ofertaEducativa->setFechaInicio($fechaInicio);
            //$tipoPeriodo=$ofertaEducativa->getPeriodo()->getNombre();
            //$comun=new EnovaDates();
            //$fechaCalculada=$comun->obtenerFechaFin($fechaInicio,$tipoPeriodo);
            //$ofertaEducativa->setFechaFin($fechaCalculada);
        }

        if (array_key_exists('fecha_final', $obj)) {

            $fechaFinal = \DateTime::createFromFormat('Y-m-d', $obj['fecha_final']);
            $ofertaEducativa->setFechaFin($fechaFinal);
            //$comun=new EnovaDates();
            //$fechaCalculada=$comun->obtenerFechaFin($fechaInicio,$tipoPeriodo);
            //$ofertaEducativa->setFechaFin($fechaCalculada);
        }

        if (array_key_exists('usuario_id', $obj)) {
            $usuario = $this->getRepository(
                    "CoreUserBundle:Usuario", array('id' => $obj['usuario_id']), null, null, null, 'find', null
            );

            if (!$usuario) {
                $this->buildRespuesta('put');
                return $this->respuesta;
            }
            $ofertaEducativa->setUsuarioModifica($usuario);
        }
        try {

            $this->entityManager->flush();
        } catch (DBALException $e) {
            $this->log->error($e->getMessage());
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }

        $this->buildRespuesta('put');
        return $this->respuesta;
    }

    /**
     * Eliminación de la una Oferta Educativa
     * @param int $oferta_educativa_id identificador de la Oferta Educativa a eliminar
     * @return mixed objeto respuesta con un mensaje de exito o error
     */
    public function eliminar($oferta_educativa_id) {
        $ofertaEducativa = $this->getRepository(
                "ControlEscolarCalendarioBundle:OfertaEducativa", array(
            'oferta_educativa_id' => $oferta_educativa_id,
                ), null, 200, null, 'find', null);

        if (!$ofertaEducativa) {
            return $this->respuesta;
        }

        try {
            $ofertaEducativa->setActivo(false);
            $this->entityManager->flush();
            $this->buildRespuesta('delete');
        } catch (DBALException $e) {
            $this->log->error($e->getMessage());
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }

    /**
     * Listado de las Ofertas Educativas
     * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
     * @return mixed objeto respuesta con los datos de las Ofertas Educativas
     */
    public function listar() {
        $ofertasEducativas = array();

        $ofertasEducativas = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativa',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$ofertasEducativas) {
            $this->respuesta['data']['ofertaseducativas'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('ofertaseducativas' => $ofertasEducativas));
    }

    /**
     * Creación de una OfertaEducativa
     * @param int $obj['usuario_id'] identificador del usuario que creará la OfertaEducativa
     * @param int $obj['nombre'] nombre de la OfertaEducativa
     * @param int $obj['fecha_inicio'] fecha de inicio de la OfertaEducativa
     * @return mixed objeto respuesta con los datos de la oferta educativa creada
     */
    public function crear($obj) {
        $usuario = $this->getRepository(
                "CoreUserBundle:Usuario", array('id' => $obj['usuario_id']), null, null, null, 'find', null
        );

        if (!$usuario) {
            $this->respuesta["data"]["ofertaeducativa"] = null;
            return $this->respuesta;
        }

        $ofertaEducativaEstatus = $this->getRepository(
                "ControlEscolarCalendarioBundle:OfertaEducativaEstatus", array('oferta_educativa_estatus_id' => 1), null, null, null, 'find', null
        );

        if (!$ofertaEducativaEstatus) {
            $this->respuesta["data"]["ofertaeducativa"] = null;
            return $this->respuesta;
        }

        $fecha = new \DateTime();
        $ofertaEducativa = new EOfertaEducativa();
        $ofertaEducativa->setActivo(true);
        $ofertaEducativa->setFechaAlta($fecha);
        $fechaInicio = \DateTime::createFromFormat('Y-m-d', $obj['fecha_inicio']);
        $ofertaEducativa->setFechaInicio($fechaInicio);
        $planAnual = $this->obtenerPlanAnualVigente();
        if ($planAnual == null) {
            $this->buildRespuesta(
                    'No se almacenó en la base de datos la Entidad porque no existe plan anual valido',
                    array('ofertaeducativa' => null),
                    200,
                    null,
                    'error'
            );
            return $this->respuesta;
        }
        $ofertaEducativa->setPeriodo($planAnual->getPeriodo());
        //$tipoPeriodo=$planAnual->getPeriodo()->getNombre();
        //$comun=new EnovaDates();
        //$fechaCalculada=$comun->obtenerFechaFin($fechaInicio,$tipoPeriodo);
        //$ofertaEducativa->setFechaFin($fechaCalculada);
        $fechaFinal = \DateTime::createFromFormat('Y-m-d', $obj['fecha_final']);
        $ofertaEducativa->setFechaFin($fechaFinal);
        $ofertaEducativa->setFechaModificacion($fecha);
        $ofertaEducativa->setNombre($obj['nombre']);
        $ofertaEducativa->setOfertaEducativaEstatus($ofertaEducativaEstatus);
        $ofertaEducativa->setSincronizado(false);
        $ofertaEducativa->setUsuario($usuario);
        $ofertaEducativa->setUsuarioAlta($usuario);
        $ofertaEducativa->setUsuarioModifica($usuario);
        try {
            $this->entityManager->persist($ofertaEducativa);
            $this->entityManager->flush();

            $this->buildRespuesta('post', array('ofertaeducativa' => $ofertaEducativa));
        } catch (DBALException $e) {
            $this->buildRespuesta(
                    'No se almacenó en la base de datos la Entidad OfertaEducativa', array('ofertaeducativa' => $ofertaEducativa), 200, null, 'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }

    /**
     * Obtención del plan anual vigente
     * @return mixed objeto respuesta con los datos del plan anual vigente
     */
    public function obtenerPlanAnualVigente() {
        $planAnualBusiness = new BPlanAnual($this->entityManager,$this->entityManagerMako);
        $obj['activo'] = 'true';
        $obj['operador'] = 'mayor';
        $obj['estatus_id'] = '9';
        $respuesta = $planAnualBusiness->obtenerByIdActivoIdOperadorIdStatusPlanAnualId($obj);
        $planAnual = $respuesta['data']['planesanuales'];
        $planAnual = (count($planAnual) > 1 or count($planAnual) == 0 ) ? null : $planAnual;
        if (count($planAnual) > 0) {
            $planAnual = ($planAnual[0]->getFlujo()->getEstado()->getEstadoId() != 10) ? null : $planAnual;
        }

        return $planAnual[0];
    }

    /**
     *  Obtención de la última fecha de la oferta educativa
     * @return mixed objeto respuesta con los datos de la fecha de la última oferta educativa
     */
    public function obtenerFechaUltimaOfertaEducativa() {
        $ofertaEducativa = $this->listar(array(
            'limit' => '1',
            'fields' => 'fecha_fin',
            'filter' => 'ofertaeducativa.activo=true',
            'order' => 'fecha_fin|asc'
        ));

        if (!$ofertaEducativa) {
            $fechaTemporal = new \DateTime();
        } else {
            $fechaTemporal = $ofertaEducativa['data']['ofertaseducativas'][0]['fecha_fin'];
        }
        $diaFeriadoBusiness = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $diasFeriados = $diaFeriadoBusiness->obtencionDiasFeriados();
        $fechaTemporal->add(new \DateInterval('P1D'));
        $flag = true;
        while ($flag) {
            if (array_key_exists($fechaTemporal->format('Y-m-d'), $diasFeriados) or $fechaTemporal->format('w') == 0) {
                $fechaTemporal->add(new \DateInterval('P1D'));
            } else {
                $flag = false;
            }
        }

        $this->buildRespuesta('get', array('fechadisponible' => array($fechaTemporal, $diasFeriados)));
        return $this->respuesta;
    }

    /**
     * Metodo que publica una oferta actividad vigente a los centros
     * @param type $obj
     * @return mixed objeto respuesta con los datos de la fecha de la última oferta educativa
     */
    public function publicar($obj = array("usuario_id" => 1)) {
        set_time_limit(0);
        $this->log->info("Entre a publicar...");
        $memoriaInicial = memory_get_usage (true);
        $tiempoFuncionPublicar = $this->getTime();
        $usuario = $this->getUsuarioById($obj['usuario_id']);
        $ofertaEducativa = $obj['oferta_educativa_id'] == 0 ? $this->obtenerUltimaOfertaEducativaVigente() : $this->getOfertaEducativaById($obj['oferta_educativa_id']);
        if (!$ofertaEducativa) {
            $this->buildRespuesta('No existe la oferta educativa', array());
            return $this->respuesta;
        }
        $tiempoStoreProcedure = $this->getTime();
        $respuestaStore = $this->ejecucionStoreProcedureGeneracionDeCentroOfertaPublica($ofertaEducativa->getOfertaEducativaId(), $usuario->getId());
        $this->log->debug("-------------->Tiempo total de ejecucion del store procedure de Generacion de centros oferta publica-> " . ($this->getTime() - $tiempoStoreProcedure));

        if (!$respuestaStore) {
            $this->buildRespuesta('No se pudo generar el paquete de Sincronizacion de la oferta vigente', array());
            return $this->respuesta;
        }
        try {
            $notificacion = $this->notificacionPublicacionOfertaEducativa($ofertaEducativa, $usuario);
            $tiempoEnvioSincronizacion = $this->getTime();
            $respuestaEnvio = $this->enviarOfertaEducativaACentros($ofertaEducativa, $notificacion, $usuario);
            $this->log->debug("------------>Tiempo total de ejecucion del envio a los centros la publicacion de la oferta educativa-> " . ($this->getTime() - $tiempoEnvioSincronizacion));
            switch ($respuestaEnvio['code']) {
                case OfertaEducativa::SINCRONIZACION_PARCIAL :
                    $this->log->error("No se pudieron sincronizar todos los centros, los centros que no se sincronizaron fueron los siguientes: {$respuestaEnvio['centros']}");
                    return $this->buildRespuesta('Solo se pudo sincronizar la Oferta Educativa con algunos centros.', array('ofertaeducativa' => $ofertaEducativa), false, 404);
                    break;
                case OfertaEducativa::SINCRONIZACION_ERRONEA :
                    $this->log->error("No se pudieron sincronizar ningun centro ya que no hay comunicacion con RabbitMQ.");
                    return $this->buildRespuesta('Hubo un error en la sincronizacion no se pudo sincronizar ningun centro', array('ofertaeducativa' => $ofertaEducativa), false, 404);
                    break;
            }
            //genero sincronizacion de actividades academicas con mako 1.4
            $actividadBusiness = new BActividad($this->entityManager);
            $tiempoSincronizaMako = $this->getTime();
            $respuesta = $actividadBusiness->sincronizarActividadesNoAcademicasWithMako($obj['usuario_id']);
            $this->log->debug("------------>Tiempo total de ejecucion envio actividades com mako-> " . ($this->getTime() - $tiempoSincronizaMako));
            if ($respuesta['code'] == 404) {
                return $this->buildRespuesta('No se pudieron sincronizar las Fichas Tecnicas con mako 1.4', array('ofertaeducativa' => $ofertaEducativa), false, 404);
            }
            //notifico a mis usuarios propios
            $notificacionBusiness = new BNotificacionEntity($this->entityManager,$this->entityManagerMako);
            $respuesta = $notificacionBusiness->creaNotificacionLocal($obj['usuario_id'], $notificacion->getAccionNotificacion()->getClaveAccionNotificacion(), $notificacion->getTipoNotificacion()->getClaveTipoNotificacion(), $notificacion->getTitulo(), $notificacion->getMensaje(), $notificacion->getUrl());
            if (!$respuesta) {
                return $this->buildRespuesta('No se pudo enviar la notificacion de la publicacion de la oferta educativa a los usuarios', array('ofertaeducativa' => $ofertaEducativa), false, 404);
            }
            $memoriaFinal = memory_get_usage (true);
            $this->log->debug("--------->Tiempo total de ejecucion la funcionalidad de publicar->" . ($this->getTime() - $tiempoFuncionPublicar)." con memoria inicial de {$memoriaInicial} y memoria final de {$memoriaFinal}");
            $this->entityManager->flush();
            return $this->buildRespuesta($this->respuesta["message"], array('ofertaeducativa' => $ofertaEducativa), null, 200);
        } catch (DBALException $ex) {
            $this->log->error($ex->getMessage());
            return $this->buildRespuesta($ex->getMessage(), array('ofertaeducativa' => $ofertaEducativa), null, 404);
        }
    }

    /**
     * Se genera el paquete para envio a sincronizar de un centro
     * @param OfertaEducativa $ofertaEducativa
     * @param Centro $centro centro al cual se le hara su paquete de informacion para sincronizar
     * @param mixed $actividadesAcademicasYNoAcademicas arreglo con actividades academicas y no academicas
     * @param array[OfertaActividadCentro] $ofertasActividadesCentro arreglo de OfertaActividadCentro que se enviaran al centro
     * @param array[Parametro] $parametrosCentral arreglo de parametros que solo podran ser modificados por central
     * @param array[Parametro] $parametrosCentro arreglo de parametros que podran ser modificados tanto por el centro como por central
     * @param array[DiaFeriado] $diasFeriados arreglo de dias feriados que llegaran al centro
     * @param mixed $notificacionCodificada notificacion que llegara al centro
     * @return mixed paquete para sincronizar
     */
    public function creacionPaquetePorCentro($ofertaEducativa, $centro, $actividadesAcademicasYNoAcademicas, $ofertasActividadesCentro, $parametrosCentral, $parametrosCentro, $diasFeriados, $notificacionCodificada) {
        $ofertaEducativaArreglo = $this->creacionArregloOfertaEducativa($ofertaEducativa, $centro);
        $ofertaActividadCentroArreglo = $this->creacionArregloOfertaActividadCentro($ofertaEducativa, $ofertasActividadesCentro);
        $horarioOfertaCentro = $this->creacionArregloHorarioOfertaCentro($ofertasActividadesCentro);
        $eventosOfertaCentro = $this->creacionArregloEventosActividadOferta($ofertasActividadesCentro);

        $elementos = array();
        foreach ($parametrosCentral as $pcentral){
           $elementos[]=$pcentral;
        }
        foreach($diasFeriados as $df){
            $elementos[]=$df;
        }
        foreach($actividadesAcademicasYNoAcademicas as $aa){
            $elementos[]=$aa;
        }
        $elementos[]=$ofertaEducativaArreglo;
        foreach ($ofertaActividadCentroArreglo as $oa){
            $elementos[]=$oa;
        }
        foreach($horarioOfertaCentro as $hoc){
            $elementos[]=$hoc;
        }
        foreach($eventosOfertaCentro as $eoc){
            $elementos[]=$eoc;
        }
        $paqueteBD = array(
            'service' => 'DB',
            'data' => array(
                'create' => $elementos,
                'noupdate' => $parametrosCentro
            )
        );

        $paqueteNotificacion = array(
            'service' => 'NOTIFY',
            'data' => array(
                'create' => $notificacionCodificada
            )
        );
        $datos = array($paqueteBD, $paqueteNotificacion);
        $paqueteEnvio = array(
            array(
                'centro_id' => $centro->getCentroId(),
                'datos' => $datos
            )
        );
        return $paqueteEnvio;
    }

    /**
     * Metodo para generacion de paquete de sincronizacion y envio a cada uno de los centros
     * @param OfertaEducativa $ofertaEducativa oferta educativa de la cual se generaran los paquetes
     * @param Notificacion $notificacion notificacion la cual se enciara de central a cada uno de los centros para que notifiquen a sus usuarios
     * @param Usuario $usuario usuario el cual genero la sincronizacion
     * @return mixed se obtiene un key de code, el cual peude tomar el valor de sincronizacion exitosa, parcial o erronea y un listado de centros los cuales no se sincronizaron en caso de haber sido sincronizacion parcial
     */
    public function enviarOfertaEducativaACentros(&$ofertaEducativa, $notificacion, $usuario) {
        $centros = $this->getCentrosActivos(); /////////////////////////////////////comentar////////////////////
        $sincronizacion = new BSync($this->entityManager,$this->entityManagerMako);
        $ofertaEstatus = $this->getEstatusOfertaEducativaById(3);
        $destinos = $this->getDestinos();
        $bandera = OfertaEducativa::SINCRONIZACION_EXITOSA;
        $cuentaErroneos = 0;
        $numeroCentros = count($centros);
        $cadenaCentros = "";

        //actualizamos la fecha de sincronizacion de las actividades no academicas
        $fechaActual = new \DateTime();
        $actividadBusiness = new BActividad($this->entityManager);
        $actividades = $actividadBusiness->listar()['data']["actividades"];
        foreach($actividades as $actividad){
            $actividad->setFechaPublicacion($fechaActual);
            $this->entityManager->flush();
        }
        
        $parametroBusiness = new BParametro($this->entityManager,$this->entityManagerMako);
        $diaFeriadoBusiness = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $actividadesAcademicasYNoAcademicas = $this->creacionArregloActividadesAcademicasYNoAcademicas();

        $generaPaquetesSyncBusiness = new BGeneraPaquetesSync($this->entityManager,$this->entityManagerMako);

        $filtroParametrosCentral = array(
            'filter' => 'parametro.modifica_centro=false',
        );
        $filtroParametrosCentro = array(
            'filter' => 'parametro.modifica_centro=true',
        );
        $parametrosCentral = $generaPaquetesSyncBusiness->generaPaqueteStandar($parametroBusiness->listar($filtroParametrosCentral)['data']['parametros']);
        $parametrosCentro = $generaPaquetesSyncBusiness->generaPaqueteStandar($parametroBusiness->listar($filtroParametrosCentro)['data']['parametros']);
        $diasFeriados = $generaPaquetesSyncBusiness->generaPaqueteStandar($diaFeriadoBusiness->listar()['data']['diasferiados'],$parametroBusiness->listar($filtroParametrosCentral)['data']['parametros']);
        $notificacionCodificada = $generaPaquetesSyncBusiness->generaPaqueteStandar(array($notificacion), false);

        $ofertaEducativa->setFechaSincronizacion(new \DateTime());

        foreach ($centros as $centro) {
            $tiempoInicio = $this->getTime();
            $this->log->debug("------centro-> {$centro->getNombre()}");
            $ofertasActividadesCentro = $this->obtenerOfertasActividadesCentro($centro);
            $tiempoInicioPaquete = $this->getTime();
            $paquete = $this->creacionPaquetePorCentro($ofertaEducativa, $centro, $actividadesAcademicasYNoAcademicas, $ofertasActividadesCentro, $parametrosCentral, $parametrosCentro, $diasFeriados, $notificacionCodificada);
            $tiempoFinalPaquete =$this->getTime();
            $this->log->debug("tiempo de construccion del paquete-> ".($tiempoFinalPaquete-$tiempoInicioPaquete));

            $datos = array(
                'usuario_id' => $usuario->getId(),
                'destino' => array($destinos[$centro->getCentroId()]),
                'data' => $paquete[0]['datos'],
                'tipo' => 'Sincronizacion Centros'
            );
            $tiempoInicioEnvio=$this->getTime();
            $respuesta = $sincronizacion->enviar($datos);
            $tiempoFinalEnvio =$this->getTime();
            $this->log->debug("tiempo de envio del paquete-> ".($tiempoFinalEnvio-$tiempoInicioEnvio));
            if ($respuesta['code'] != 200) {
                $ofertaEstatus = $this->getEstatusOfertaEducativaById(2);
                $bandera = OfertaEducativa::SINCRONIZACION_PARCIAL;
                $cuentaErroneos++;
                $cadenaCentros+="[{$centro->getNombre()}]-({$centro->getCentroId()}),";
            } else {
                $sincroniaCentro = new ESincronizaOfertaCentro();
                $sincroniaCentro->setUsuarioAlta($usuario);
                $sincroniaCentro->setCentro($centro);
                $sincroniaCentro->setOfertaEducativa($ofertaEducativa);
                $sincroniaCentro->setFechaSincronizacion(new \DateTime());
                $sincroniaCentro->setCompletado(true);
                $sincroniaCentro->setUltimoMensajeRegistrado("Iniciando Envio de datos");
                $sincroniaCentro->setFechaAlta(new \DateTime());
                $sincroniaCentro->setActivo(true);
                $sincroniaCentro->setFechaAlta2(new \DateTime());
                $this->entityManager->persist($sincroniaCentro);
                $this->entityManager->flush();
            }
            $tiempoFinal = $this->getTime();
            $this->log->debug("tiempo total en generacion y envio de paquete-> ".($tiempoFinal-$tiempoInicio));
        }
        $ofertaEducativa->setOfertaEducativaEstatus($ofertaEstatus);
        $ofertaEducativa->setSincronizado($bandera == OfertaEducativa::SINCRONIZACION_EXITOSA);

        $this->entityManager->persist($ofertaEducativa);
        $this->entityManager->flush();
        $response = array(
            'code' => $cuentaErroneos == $numeroCentros ? OfertaEducativa::SINCRONIZACION_ERRONEA : $bandera,
            'centros' => $cadenaCentros
        );
        return $response;
    }

    /**
     * Metodo que obtiene el tiempo de reloj en decimales.
     * @return tiempo transcurrido en microsegundos
     */
    function getTime() {
        list($useg, $seg) = explode(" ", microtime());
        return ((float) $useg + (float) $seg);
    }

    public function obtenerUltimaOfertaEducativaVigente() {
        $ofertaEducativa = $this->listar(array(
            'limit' => '1',
            'filter' => 'ofertaeducativa.activo=true and ofertaeducativa.sincronizado=false',
            'order' => 'oferta_educativa_id|desc'
        ));
        if (!$ofertaEducativa['data']['ofertaseducativas']) {
            return null;
        }
        return $ofertaEducativa['data']['ofertaseducativas'][0];
    }

    public function ejecucionStoreProcedureGeneracionDeCentroOfertaPublica($ofertaEducativaId, $usuarioId) {
        $respuesta = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativa', array(
            'oferta_educativa_id' => $ofertaEducativaId,
            'usuario_id' => $usuarioId
                ), null, 200, null, 'findStoreProcedureGeneratePaquetes', array()
        );
        if (!$respuesta) {
            return false;
        }
        return true;
    }

    private function getDestinos() {
        $destinos = array();
        $entidad = "\Core\SyncBundle\Business\Entity\Destino";

        $tablaObj = $this->getRepository(
                "CoreCoreBundle:Tabla", array(
            "ruta_entidad" => $entidad
                ), null, 200, null, "findBy"
        );


        if ($tablaObj == false) {
            return $destinos;
        }



        $result = $this->getRepository(
                "CoreCoreBundle:Equivalencia", array("Tabla" => $tablaObj), null, 200, null, "findBy"
        );

        foreach ($result as $equivalencia) {
            $destinos[$equivalencia->getValorDestino()] = $equivalencia->getValorOrigen();
        }
        ////$this->log->debug(print_r($destinos));
        return $destinos;
    }

    private function getEstatusOfertaEducativaById($id) {

        $estatusObj = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativaEstatus', array('oferta_educativa_estatus_id' => $id), null, 200, null, 'find', array()
        );
        return( (!$estatusObj) ? null : $estatusObj );
    }

    /*
     * Función que obtiene el objeto de oferta educativa por el id
     * @param int id identificador de la OfertaEducativa
     */
    private function getOfertaEducativaById($id) {
        $ofertaEducativaObj = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativa', array('oferta_educativa_id' => $id), null, 200, null, 'find', array()
        );
        return( (!$ofertaEducativaObj) ? null : $ofertaEducativaObj );
    }

    private function validaDestinos($arreglo, $destinos) {
        //$this->log->debug(print_r($destinos, true));
        foreach ($arreglo as $objeto) {
            if (!array_key_exists($objeto[0]["centro_id"], $destinos)) {
                //$this->log->debug($objeto[0]["centro_id"] . "-------------------------------------------");
                return false;
            }
        }
        return true;
    }

    public function creacionArregloActividadesAcademicasYNoAcademicas() {
        $generaPaquetesSyncBusiness = new BGeneraPaquetesSync($this->entityManager,$this->entityManagerMako);
        $actividadesAcademicas = array();
        $actividades = array();
        $esquemas = array();
        $relaciones = array();
        $actividadAcademicaBusiness = new BActividadAcademica($this->entityManager,$this->entityManagerMako);
        $actividadesAcademicas2 = $actividadAcademicaBusiness->obtenerActividadesPublicadas()["data"]["actividadesacademicas"];
        foreach ($actividadesAcademicas2 as $aa) {
            $actividadesAcademicas[] = $aa;
            $relacionesActividadAcademica = $actividadAcademicaBusiness->ObtenerRelacionesActividadAcademica($aa);

            foreach ($relacionesActividadAcademica['actividadAcademicaEsquema'] as $aae) {
                $relaciones[] = $aae;
            }
            foreach ($relacionesActividadAcademica['actividadAcademicaEtiqueta'] as $aae) {
                $relaciones[] = $aae;
            }
        }
        $actividades = $this->obtenerActividades();
        $periodos = $this->obtenerPeriodos();

        $objetos = array();
        foreach($periodos as $p){
            $objetos[]=$p;
        }
        foreach($esquemas as $e){
            $objetos[]=$e;
        }
        foreach($actividades as $a){
            $objetos[]=$a;
        }
        foreach($relaciones as $r){
            $objetos[]=$r;
        }
        foreach($actividadesAcademicas as $aa){
            $objetos[]=$aa;
        }

        //$objetos = array_merge($periodos, $esquemas, $actividades, $relaciones, $actividadesAcademicas);
        $objetosCodificados = $generaPaquetesSyncBusiness->generaPaqueteStandar($objetos);

        return $objetosCodificados;
    }

    public function obtenerActividades() {
        $result = $this->getRepository(
                "ControlEscolarPlanAnualBundle:Actividad", array("activo" => true), null, 200, null, "findBy"
        );
        if (!$result) {
            return array();
        }
        return $result;
    }

    public function obtenerPeriodos() {
        $result = $this->getRepository(
                "ControlEscolarPlanAnualBundle:Periodo", array("activo" => true), null, 200, null, "findBy"
        );
        return $result;
    }

    public function obtenerOfertasActividadesCentro($centro) {
        $ofertasActividadesCentro = $this->getRepository(
                'ControlEscolarCalendarioBundle:CentroOfertaPublica', array(
            'Centro' => $centro
                ), null, 200, null, 'findBy', array()
        );
        return $ofertasActividadesCentro;
    }

    public function creacionArregloOfertaEducativa($ofertaEducativa, $centro) {
        $datos = array(
            'id' => array(
                'valor' => $ofertaEducativa->getOfertaEducativaId(),
                'nombre' => 'oferta_educativa_centro_id',
                'propiedad' => 'OfertaEducativaCentroId'
            ),
            'Periodo' => array(
                'id' => array(
                    'valor' => $ofertaEducativa->getPeriodo()->getPeriodoId(),
                    'nombre' => 'periodo_id'
                ),
                'entity' => "\ControlEscolar\PlanAnualBundle\Entity\Periodo",
            ),
            'FechaInicio' => $ofertaEducativa->getFechaInicio(),
            'FechaSincronizacion' => $ofertaEducativa->getFechaSincronizacion(),
            'Publicado'=>false,
            'FechaFin' => $ofertaEducativa->getFechaFin(),
            'ClaveCentro' => $centro->getCentroId(),
            'Nombre' => $ofertaEducativa->getNombre()
        );
        $arreglo = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro',
            'data' => $datos
        );
        return $arreglo;
    }

    public function creacionArregloOfertaActividadCentro($ofertaEducativa, $ofertasActividadesCentro) {
        $arreglo = array();
        foreach ($ofertasActividadesCentro as $oac) {
            $arreglo[] = $this->creacionOfertaActividadCentroUnico($ofertaEducativa, $oac->getOfertaActividad());
        }
        return $arreglo;
    }

    public function creacionOfertaActividadCentroUnico($ofertaEducativa, $ofertaActividad) {
        if ($ofertaActividad->getActividadAcademica()) {
            $actividadAcademica = array(
                'id' => array(
                    'valor' => $ofertaActividad->getActividadAcademica()->getActividadAcademicaId(),
                    'nombre' => 'actividad_academica_id'
                ),
                'entity' => '\Core\CoreBundle\Entity\ActividadAcademica'
            );
        } else {
            $actividadAcademica = null;
        }

        if ($ofertaActividad->getActividadAcademicaEsquema()) {
            $actividadAcademicaEsquema = array(
                'id' => array(
                    'valor' => $ofertaActividad->getActividadAcademicaEsquema()->getActividadAcademicaEsquemaId(),
                    'nombre' => 'actividad_academica_esquema_id'
                ),
                'entity' => '\Core\CoreBundle\Entity\ActividadAcademicaEsquema'
            );
        } else {
            $actividadAcademicaEsquema = null;
        }

        if ($ofertaActividad->getActividad()) {
            $actividad = array(
                'id' => array(
                    'valor' => $ofertaActividad->getActividad()->getActividadId(),
                    'nombre' => 'actividad_id'
                ),
                'entity' => '\ControlEscolar\PlanAnualBundle\Entity\Actividad'
            );
        } else {
            $actividad = null;
        }

        $datos = array(
            'id' => array(
                'valor' => $ofertaActividad->getOfertaActividadId(),
                'nombre' => 'oferta_actividad_centro_id',
                'propiedad' => 'OfertaActividadCentroId'
            ),
            'Obligatoria' => ($ofertaActividad->getObligatoria() == false) ? 0 : true,
            'ActividadAcademica' => $actividadAcademica,
            'ActividadAcademicaEsquema' => $actividadAcademicaEsquema,
            'Actividad' => $actividad,
            'FechaInicio' => $ofertaActividad->getFechaInicio(),
            'FechaFin' => $ofertaActividad->getFechaFin(),
            'ControlAcceso' => $ofertaActividad->getControlAcceso(),
            'Extemporanea'  =>$ofertaActividad->getExtemporanea(),
            'OfertaEducativaCentro' => array(
                'id' => array(
                    'valor' => $ofertaEducativa->getOfertaEducativaId(),
                    'nombre' => 'oferta_educativa_centro_id'
                ),
                'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro'
            )
        );
        $arreglo = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro',
            'data' => $datos
        );
        return $arreglo;
    }

    public function creacionArregloHorarioOfertaCentro($ofertasActividadesCentro) {
        $arreglo = array();
        foreach ($ofertasActividadesCentro as $oac) {
            $arreglo = array_merge($this->creacionHorariosOfertaActividadUnico($oac->getOfertaActividad(), $oac), $arreglo);
        }
        return $arreglo;
    }

    public function creacionHorariosOfertaActividadUnico($ofertaActividad, $ofertaActividadCentro) {
        $ofertasActividadHorario = $this->obtenerOfertasActividadHorario($ofertaActividad);
        $arreglo = array();
        foreach ($ofertasActividadHorario as $oah) {
            $arreglo[] = $this->creacionObjetoHorarioOfertaActividad($ofertaActividad, $oah);
        }
        return $arreglo;
    }

    public function obtenerOfertasActividadHorario($ofertaActividad) {
        $ofertasActividadesHorario = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaActividadHorario', array(
            'OfertaActividad' => $ofertaActividad
                ), null, 200, null, 'findBy', array()
        );
        return $ofertasActividadesHorario;
    }

    public function creacionObjetoHorarioOfertaActividad($ofertaActividad, $ofertaActividadHorario) {
        $datos = array(
            'id' => array(
                'valor' => $ofertaActividadHorario->getOfertaActividadHorarioId(),
                'nombre' => 'horario_oferta_id',
                'propiedad' => 'HorarioOfertaId',
            ),
            'DiaSemana' => $ofertaActividadHorario->getDiaSemana(),
            'HoraInicio' => $ofertaActividadHorario->getHoraInicio(),
            'HoraFin' => $ofertaActividadHorario->getHoraFin(),
            'OfertaActividadCentro' => array(
                'id' => array(
                    'valor' => $ofertaActividad->getOfertaActividadId(),
                    'nombre' => 'oferta_actividad_centro_id'
                ),
                'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro'
            )
        );
        $arreglo = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\HorarioOfertaCentro',
            'data' => $datos
        );
        return $arreglo;
    }

    public function creacionArregloEventosActividadOferta($ofertasActividadesCentro) {
        $arreglo = $this->obtenerEventosActividadOferta($ofertasActividadesCentro);
        $resultado = array();
        foreach ($arreglo as $evento) {
            $resultado[] = $this->creacionObjetoEventoActividadOferta($evento);
        }
        return $resultado;
    }

    /* ################## EVENTOS #################### */

    public function obtenerEventosActividadOferta($ofertasActividad) {
        foreach ($ofertasActividad as $oac) {
            $arreglo[] = $oac->getOfertaActividad();
        }

        $eventosActividadOferta = $this->getRepository(
                'ControlEscolarCalendarioBundle:EventoActividadOferta', array(
            'OfertaActividad' => $arreglo
                ), null, 200, null, 'findBy', array('ignore_activo' => true)
        );
        return $eventosActividadOferta;
    }

    public function creacionObjetoEventoActividadOferta($eventoObjecto) {
        $datos = array(
            'id' => array(
                'valor' => $eventoObjecto->getEventoActividadOfertaId(),
                'nombre' => 'evento_centro_id',
                'propiedad' => 'EventoCentroId',
            ),
            'FechaEvento' => $eventoObjecto->getFechaEvento(),
            'FechaAlta' => $eventoObjecto->getFechaAlta(),
            'HoraInicio' => $eventoObjecto->getHoraInicio(),
            'HoraFin' => $eventoObjecto->getHoraFin(),
            'OfertaActividadCentro' => array(
                'id' => array(
                    'valor' => $eventoObjecto->getOfertaActividad()->getOfertaActividadId(),
                    'nombre' => 'oferta_actividad_centro_id'
                ),
                'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro'
            )
        );
        $arreglo = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\EventoActividadOfertaCentro',
            'data' => $datos
        );
        return $arreglo;
    }

    public function notificacionPublicacionOfertaEducativa($ofertaEducativa, $usuario) {
        $accionNotificacion = $this->getRepository(
                "CoreNotificacionesBundle:AccionNotificacion", array(
            "clave_accion_notificacion" => "publicacion_oferta_educativa"
                ), null, 200, null, "findBy"
        );

        if (!$accionNotificacion) {
            throw new DBALException("No se encontró la AccionNotificacion especificada");
        }
        $tipoNotificacion = $this->getRepository(
                "CoreNotificacionesBundle:TipoNotificacion", array(
            "clave_tipo_notificacion" => "push_mail"
                ), null, 200, null, "findBy"
        );

        if (!$tipoNotificacion) {
            throw new DBALException("No se encontró el TipoNotificacion especificado");
        }

        $notificacion = new ENotificacion();
        $notificacion->setAccionNotificacion($accionNotificacion[0]);
        $notificacion->setActivo(true);
        $notificacion->setCentroOrigen(null);
        $notificacion->setCentroDestino(null);
        $notificacion->setEnviado(false);
        $notificacion->setFechaAlta(new \DateTime());
        $notificacion->setFechaModificacion(new \DateTime());
        $notificacion->setFechaNotificacion(new \DateTime());

        //Evalua si ya se publico antes la oferta para indicar que es oferta educativa extemporanea
        $ofertaExtemporanea = $ofertaEducativa->getSincronizado()?" extemporánea":"";

        $notificacion->setMensaje("La Programación CE {$ofertaExtemporanea} '" . $ofertaEducativa->getNombre() . "' ha sido publicada exitosamente.<br>Movimiento realizado por: " . $usuario->getUsername());
        $notificacion->setTipoNotificacion($tipoNotificacion[0]);
        $notificacion->setTitulo("Publicación de una oferta educativa".$ofertaExtemporanea.": " . $ofertaEducativa->getNombre());
        $notificacion->setUrl("");
        $notificacion->setUsuarioAlta($usuario);
        $notificacion->setUsuarioModificacion($usuario);
        try {
            $this->entityManager->persist($notificacion);
            $this->entityManager->flush();
        } catch (DBALException $e) {
            $this->log->error($e->getMessage());
            throw new DBALException("No se pudo Almacenar la Notificación generada para la publicación " . $e->getMessage());
        }
        //$this->enviarMailNotificacion($notificacion);                ///////////////enviar el mail de publicacion a todos las personas escritas en accionNotificacionUsuario
        return $notificacion;
    }

    public function creacionDummy() {
        //oferta educativa que se bajara al centro
        $OfertaEducativaCentro = array(
            'id' => array(
                'valor' => 20,
                'nombre' => 'oferta_educativa_centro_id',
                'propiedad' => 'OfertaEducativaCentroId'
            ),
            'Periodo' => array(
                'id' => array(
                    'valor' => 2,
                    'nombre' => 'periodo_id'
                ),
                'entity' => "\ControlEscolar\PlanAnualBundle\Entity\Periodo",
            ),
            'FechaInicio' => new \DateTime(),
            'FechaFin' => new \DateTime(),
            'ClaveCentro' => 11,
            'UsuarioModifica' => array(
                'id' => array(
                    'valor' => 1,
                    'nombre' => 'id'
                ),
                'entity' => '\Core\CoreBundle\Entity\Usuario'
            )
        );
        //actividad que se bajara al centro
        $OfertaActividadCentro = array(
            'id' => array(
                'valor' => 140,
                'nombre' => 'oferta_actividad_centro_id',
                'propiedad' => 'OfertaActividadCentroId'
            ),
            'Obligatoria' => false,
            'ActividadAcademica' => null,
            'ActividadAcademicaEsquema' => null,
            'Actividad' => array(
                'id' => array(
                    'valor' => 155,
                    'nombre' => 'actividad_id'
                ),
                'entity' => '\ControlEscolar\PlanAnualBundle\Entity\Actividad'
            ),
            'OfertaEducativaCentro' => array(
                'id' => array(
                    'valor' => 20,
                    'nombre' => 'oferta_educativa_centro_id'
                ),
                'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro'
            )
        );
        //Horario de la actividad que se bajara al centro
        $HorarioOfertaCentro = array(
            'id' => array(
                'valor' => 6,
                'nombre' => 'horario_oferta_id',
                'propiedad' => 'HorarioOfertaId'
            ),
            'DiaSemana' => 3,
            'HoraInicio' => new \DateTime('10:00'),
            'HoraFin' => new \DateTime('11:00'),
            'OfertaActividadCentro' => array(
                'id' => array(
                    'valor' => 140,
                    'nombre' => 'oferta_actividad_centro_id'
                ),
                'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro'
            )
        );
        //carnita de create
        $carnita = array();
        $carnita[] = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaEducativaCentro',
            'data' => $OfertaEducativaCentro
        );
        $carnita[] = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro',
            'data' => $OfertaActividadCentro
        );
        $carnita[] = array(
            'entity' => '\ControlEscolar\CalendarioBundle\Entity\HorarioOfertaCentro',
            'data' => $HorarioOfertaCentro
        );

        //estructura de paquetes
        $paquete = array(
            'service' => 'DB',
            'data' => array(
                'create' => $carnita
            )
        );
        //datos dummy envío de paquetes
        return $datos = array(
            array(
                'centro_id' => 11,
                'datos' => $paquete
            )
        );
    }
}
