<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaDates;

/**
 *  * Lógica de negocio de la manipulación de un Facilitador
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class Facilitador extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de los Facilitadores
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de los Facilitadores
 */
    public function listar() {
        $facilitadores = array();

        $facilitadores = $this->getRepository(
                'ControlEscolarCalendarioBundle:Facilitador',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$facilitadores) {
            $this->respuesta['data']['facilitadores'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('facilitadores' => $facilitadores));
    }

    /**
     * Obtiene el facilitador por ID
     * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
     * @return mixed objeto respuesta con los datos de los Facilitadores
     */
    public function getFacilitadorById($facilitadorId){
        $facilitador = $this->getRepository(
            "ControlEscolarCalendarioBundle:Facilitador",
            array('facilitador_id' => $facilitadorId),
            null,
            null,
            null,
            'find',
            null
        );

        if (!$facilitador) {
            return null;
        }
        return $facilitador;
    }




    /**
    * Listado de los Facilitadores para el reporte de grupos
    * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
    * @return mixed objeto respuesta con los datos de los Facilitadores
    */
    public function listarFacilitadoresReporte() {
        $facilitadores = array();
        $facilitadores = $this->getRepository(
                'ControlEscolarCalendarioBundle:Facilitador',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                'findFacilitadoresReporte',
                null
        );

        if (!$facilitadores) {
            $this->respuesta['data']['facilitadores'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('facilitadores' => $facilitadores));
    }
}