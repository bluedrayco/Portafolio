<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaDates;

/**
 * Lógica de negocio de la manipulación de un Movimiento
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class MovimientoCentro extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    public function sincronizacionMovimientoCentral($usuario_id,$descripcion,$clave){
        $fecha_movimiento = new \DateTime();
        $hora_movimiento = new \DateTime();
        $usuario_movimiento = $this->getUsuarioById($usuario_id)->getNombre();
        $descripcion_movimiento = $descripcion;
        $clave_grupo = $clave;

    }
}