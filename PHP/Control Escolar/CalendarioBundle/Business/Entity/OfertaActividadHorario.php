<?php
/***
 * Clase Business para  operaciones relacionadas al horario de los eventos
 * @author silvio.bravo@enova.mx
 * @date   Dic 09 2014
 *
 */


namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\OfertaActividadHorario as EOfertaActividadHorario;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use \DateTime;
use \DateInterval;

class OfertaActividadHorario extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }


    /**
     * Guardamos en la base de datos el horario correspondiente a un curso
     * @param  object $ofertaActividad Objeto que representa una oferta Actividad.
     * @param  array  $horarios array que debe contener los siguientes keys  dia_semana, hora_inicio, hora_fin
     * @param  object $usuario objeto usuario que realiza la operación
     * @return boolean          true|false dependiendo del exito de la operacion
     */
    public function generaHorario($ofertaActividad, $horarios, $usuario){
        try{
            foreach ($horarios as $horario) {
                $OfertaActividadHorarioEntity = new EOfertaActividadHorario();
                $OfertaActividadHorarioEntity->setOfertaActividad($ofertaActividad);
                $OfertaActividadHorarioEntity->setUsuario($usuario);
                $OfertaActividadHorarioEntity->setHoraInicio(new \DateTime($horario["hora_inicio"]));
                $OfertaActividadHorarioEntity->setHoraFin(new \DateTime($horario["hora_fin"]));
                $OfertaActividadHorarioEntity->setDiaSemana($horario["dia_semana"]);
                $OfertaActividadHorarioEntity->setFechaAlta(new \DateTime());
                $OfertaActividadHorarioEntity->setActivo(true);
                $this->entityManager->persist($OfertaActividadHorarioEntity);
            }
            $this->entityManager->flush();
        }
        catch(DBALException $e) {
                    throw new Exception($e);
                    return false;
        }
        return true;
    }//generaHorario

    /**
     * Eliminamos todos los horarios que rengan relacionado a un objeto OfertaActividad
     * @param  object $ofertaActividad Objeto que representa una ofertaActividad
     * @return boolean                 true|false dependiendo del exito de la operacion
     */
    public function eliminaHorarioDesdeOfertaActividad($ofertaActividad){
        try{
            $query= $this->entityManager->createQuery(
                    'DELETE ControlEscolarCalendarioBundle:OfertaActividadHorario oah '
                    .'WHERE oah.OfertaActividad = :OfertaActividadId');
            $query->setParameter('OfertaActividadId',$ofertaActividad);
            $query->execute();
        }
        catch(DBALException $ex) {
            return false;
        }
        return true;
    }
}

?>