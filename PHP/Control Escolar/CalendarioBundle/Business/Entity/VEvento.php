<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\Escenario as EEscenario;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class VEvento extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de los Eventos de la vista
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de los escenarios
 */
    public function listar() {
        $eventos = array();

        $eventos = $this->getRepository(
                'ControlEscolarCalendarioBundle:VEvento',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$eventos) {
            $this->respuesta['data']['eventos'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('eventos' => $eventos));
    }

}
