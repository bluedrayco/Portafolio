<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\OcupacionHorario as EOcupacionHorario;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OcupacionHorario extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

     /**
     * Guardamos en la base de datos el horario correspondiente a un curso
     * @param  object $ocupacion ocupacion
     * @param  array  $horarios array que debe contener los siguientes keys  dia_semana, hora_inicio, hora_fin
     * @param  object $usuario objeto usuario que realiza la operación
     * @return boolean          true|false dependiendo del exito de la operacion
     */
    public function generaHorario($horarios, $usuario,$ocupacion){
        $arreglo=array();
        try{
            foreach ($horarios as $horario) {
                $ocupacionHorario            = new EOcupacionHorario();
                $ocupacionHorario->setActivo(true);
                $ocupacionHorario->setDiaSemana($horario['dia_semana']);
                $ocupacionHorario->setFechaAlta(new \DateTime());
                $ocupacionHorario->setFechaModificacion(new \DateTime());
                $ocupacionHorario->setHoraFin(new \DateTime($horario['hora_fin']));
                $ocupacionHorario->setHoraInicio(new \DateTime($horario['hora_inicio']));
                $ocupacionHorario->setOcupacion($ocupacion);
                $ocupacionHorario->setUsuarioAlta($usuario);
                $ocupacionHorario->setUsuarioModifica($usuario);
                $this->entityManager->persist($ocupacionHorario);
                $this->entityManager->flush();
                $arreglo[]=$ocupacionHorario;
            }
        }
        catch(DBALException $e) {
            throw new DBALException("Error al generar el Horario para la Actividad...");
        }
        return $arreglo;
        return true;
    }//generaHorario

    public function eliminarHorario($ocupacion_id){
        $ocupacionesHorario = $this->getRepository(
               "ControlEscolarCalendarioBundle:OcupacionHorario",
               array('ocupacion_id' => $ocupacion_id),
               null,
               null,
               null,
               'findHorariosByOcupacionId',
               null
        );
        try{
            foreach ($ocupacionesHorario as $oh){
                $this->entityManager->remove($oh);
            }
            $this->entityManager->flush();
        } catch (DBALException $e) {
            throw new DBALException("Error al eliminar el horario...");
        }
    }

    public function obtenerHorariosByOcupacion($ocupacion_id){
        $ocupacionesHorario = $this->getRepository(
               "ControlEscolarCalendarioBundle:OcupacionHorario",
               array('ocupacion_id' => $ocupacion_id),
               null,
               null,
               null,
               'findHorariosByOcupacionId',
               null
        );
        return $ocupacionesHorario;
    }



    /**
     * Comparamos 2 arraays de objetos de tipo OcupacionHorario devolviendo true/false en caso de que existan/no existan cambios.
     * @param  OcupacionHorario array $horarioOrigen
     * @param  OcupacionHorario array $horarioFinal
     * @return boolean
     */
    public function compareHorarioFromObjects($horarioOrigen, $horarioFinal){
      $vtInicio  = $this->convertHorarioObjectToArray($horarioOrigen);
      $vtFinal   = $this->convertHorarioObjectToArray($horarioFinal);
      $compareA  = $this->compareHorario($vtInicio  , $vtFinal);
      $compareB  = $this->compareHorario($vtFinal   , $vtInicio);

      return($compareA == false OR $compareB == false)?false: true;

    }



    private function compareHorario($origen, $destino){

       foreach ($origen as $keyOrigen => $valorOrigen) {
         if(!array_key_exists($keyOrigen, $destino) OR $destino[$keyOrigen]!=$valorOrigen ) {
            return true;
         }
       }
       return false;

    }





    private function convertHorarioObjectToArray($vtHorarios){
        $result = "";
        foreach ($vtHorarios as $horario) {
            $result["Dia-" .$horario->getDiaSemana()] = $horario->getHoraInicio()->format("H:i") . " a " . $horario->getHoraFin()->format("H:i");
        }
        return $result;

    }





    public function getHorarioString($vtHorarios){

      $horarios   = $this->convertHorarioObjectToArray($vtHorarios);
      $vtaux      = array();
      $cadena     = "";

      foreach ($horarios as $dia=> $horario) {
          $vtaux[$horario][] = $this->getDayOfWeek(intval(str_replace("Dia-","", $dia)), "short");
      }

      foreach ($vtaux as $key => $value) {

         $cadena  .= ($cadena=="")?"":", ";
         $cadena  .= implode(",",$value);
         $cadena  .= " de " . $key;

      }
      return  $cadena;
    }





    private function getDayOfWeek($dia, $tipo="normal"){

      $dias = array(
                      0=>array("normal"=>"Domingo"   , "short"=>"DO"),
                      1=>array("normal"=>"Lunes"     , "short"=>"LU"),
                      2=>array("normal"=>"Martes"    , "short"=>"MA"),
                      3=>array("normal"=>"Miercoles" , "short"=>"MI"),
                      4=>array("normal"=>"Jueves"    , "short"=>"JU"),
                      5=>array("normal"=>"Viernes"   , "short"=>"VI"),
                      6=>array("normal"=>"Sabado"    , "short"=>"SA")
                    );
      return ($dias[$dia][$tipo]);

    }



}
