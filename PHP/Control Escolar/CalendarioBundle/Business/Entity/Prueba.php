<?php
namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\CoreBundle\Entity\ActividadAcademica as EActividadAcademica;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
//use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaDates;

/**
 *  * Lógica de negocio de la manipulación de una OfertaEducativaCentro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class Prueba extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    public function prueba(){
//        echo "entre... :)";
//        exit();
        $this->entityManager->beginTransaction();
        try{
            $actividadAcademica = new EActividadAcademica();
            $actividadAcademica->setNombre("DSFG");
            $this->entityManager->persist($actividadAcademica);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $ex) {
            $this->entityManager->rollback();
            echo "Mensaje: {$ex->getMessage()}";
//            print_r($ex);
//            exit();

        }
    }
}
