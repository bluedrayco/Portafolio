<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaDates;
use Core\SyncBundle\Business\Action\NotificacionOfertaEducativaCentroACentral as BNotificacionOfertaEducativaCentroACentral;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaEducativaCentro as BOfertaEducativaCentro;
use ControlEscolar\CalendarioBundle\Business\Entity\Parametro             as BParametro;
use Core\CoreBundle\Business\Entity\GenericRest                           as BGenericRest;

/**
 * Lógica de negocio de la manipulación de una OfertaEducativaCentro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class OfertaEducativaCentro extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de las Ofertas Educativas centros
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de las Ofertas Educativas Centro
 */
    public function listar() {
        $ofertasEducativasCentros = array();

        $ofertasEducativasCentros = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativaCentro',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$ofertasEducativasCentros) {
            $this->respuesta['data']['ofertaseducativascentros'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('ofertaseducativascentros' => $ofertasEducativasCentros));
    }


    public function obtenerEstadisticas($oferta_educativa_id,$aula_id,$facilitador_id){
        $estadistica = array();

        $estadistica = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativaCentro',
                array(
                    'oferta_educativa_id' => $oferta_educativa_id,
                    'aula_id'             => $aula_id,
                    'facilitador_id'      => $facilitador_id
                ),
                null,
                200,
                null,
                'findEstadisticas',
                array()
        );

        if (!$estadistica) {
            $this->respuesta['data']['estadisticas'] = array();
            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('estadistica' => $estadistica));
    }

    public function publicacionCalendarizacion($oferta_educativa_id,$usuario_id){
        $usuario = $this->getUsuarioById($usuario_id);

        if (!$usuario) {
            return  $this->buildRespuesta(
                      "No se encontro el Usuario con el id {$usuario_id}",
                      array('ofertaactividadcentro' =>  array()),
                        false,
                        404
                   );
        }

        $ofertaEducativaCentro = $this->getOfertaEducativaCentroById($oferta_educativa_id);
        if (!$ofertaEducativaCentro) {
            return  $this->buildRespuesta(
                      "No se encontro la OfertaEducativaCentro el id {$oferta_educativa_id}",
                      array('ofertaactividadcentro' =>  array()),
                        false,
                        404
                   );
        }

        $this->entityManager->beginTransaction();
        try{
            $ofertaEducativaCentro->setFechaPublicacion(new \DateTime());
            $ofertaEducativaCentro->setPublicado(true);
            $notificacionCentroACentralBusiness = new BNotificacionOfertaEducativaCentroACentral($this->entityManager,$this->entityManagerMako);
            $respuestaSincroniacionCentral = $notificacionCentroACentralBusiness->sincronizarNotificacionModificacionWithCentral($ofertaEducativaCentro,$usuario);
            if($respuestaSincroniacionCentral['code']==404){
                $this->entityManager->rollback();
                return $this->buildRespuesta('Ocurrio un error al tratar de notificar con central ',array('ofertaeducativacentro'=>$ofertaEducativaCentro),false,404);
            }
            $this->entityManager->flush();
            $this->entityManager->commit();
            $this->buildRespuesta('Se publicó y notifico de la publicacion de la oferta educativa centro',array('ofertaeducativacentro'=>$ofertaEducativaCentro),null,200);
        } catch (DBALException $ex) {
            $this->entityManager->rollback();
            return $this->buildRespuesta('Ocurrio un error al tratar de publicar la calendarizacion: '.$ex->getMessage(),array('ocupacion'=>$ocupacion));
        }
        return $this->respuesta;
    }

    protected  function getOfertaEducativaCentroById($oferta_educativa_id){
        $ofertaEducativaCentro = $this->getRepository(
            'ControlEscolarCalendarioBundle:OfertaEducativaCentro',
            array('oferta_educativa_centro_id' => $oferta_educativa_id),
            null,
            200
        );

        if (!$ofertaEducativaCentro) {
            return null;
        }

        return $ofertaEducativaCentro;
    }

    public function obtenerHorasAsignadasFacilitadores($oferta_educativa_id){
        $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->entityManager,$this->entityManagerMako);
        $ofertaEducativaCentro = $ofertaEducativaCentroBusiness->getOfertaEducativaCentroById($oferta_educativa_id);

        if(!$ofertaEducativaCentro){
            return $this->buildRespuesta('No se pudo encontrar la Entidad OfertaEducativaCentro con el id especificado', array('asignaciones'=>array()), false, 404);
        }

        $genericREST    = new BGenericRest($this->entityManager,$this->entityManagerMako);
        $search         = array(
            'filter' => 'facilitador.activo in (true, false)',
        );
        $facilitadores = $genericREST->listarGenerico(
            'ControlEscolarCalendarioBundle:Facilitador',
            $search,
            array()
        )['data']['controlescolarcalendariobundle:facilitador'];

        $arregloFacilitador=array();
        $arregloAsignaciones=array();
        foreach($facilitadores as $facilitador){
            $arregloFacilitador['facilitador'] = $facilitador;
            $arregloFacilitador['horas_asignadas'] = $this->horasAsignadasFacilitador($ofertaEducativaCentro ,$facilitador);

            $arregloAsignaciones[]=$arregloFacilitador;
        }
        //exit();
        //$this->obtenerHorasDisponiblesOfertaEducativa($oferta_educativa_id);
        return $this->buildRespuesta("get", array('asignaciones'=>$arregloAsignaciones));

    }

    public function horasAsignadasFacilitador($ofertaEducativaCentro ,$facilitador){
        $eventos = $this->getRepository("ControlEscolarCalendarioBundle:OcupacionReserva",
            array(
                'Facilitador'=>$facilitador,
                'OfertaEducativaCentro' =>$ofertaEducativaCentro
                ),
            null,
            null,
            null,
            'findOcupacionReservaByOfertaEducativaCentroYFacilitador'
        );
        //return $eventos;
        if(!$eventos){
            return 0;
        }
        $horasAsignadas = 0;
        foreach($eventos as $evento){
            $horaInicioCentro = $evento->getHoraInicio();
            $horaFinCentro    = $evento->getHoraFin();
            $numeroHorasDiff = $horaInicioCentro->diff($horaFinCentro);
            $horasAsignadas= $horasAsignadas+$numeroHorasDiff->format('%h');
        }
        return $horasAsignadas;
    }

    private function obtenerHorasDisponiblesOfertaEducativa($oferta_educativa_id){
        $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->entityManager,$this->entityManagerMako);
        $ofertaEducativaCentro = $ofertaEducativaCentroBusiness->getOfertaEducativaCentroById($oferta_educativa_id);
        $parametroBusiness = new BParametro($this->entityManager,$this->entityManagerMako);
        $horaInicioCentro =new  \DateTime($parametroBusiness->obtenerParametroByTag("hora_apertura_centro")[0]->getValor());
        $horaFinCentro    =new  \DateTime($parametroBusiness->obtenerParametroByTag("hora_cierre_centro")[0]->getValor());

        $numeroHorasDiff = $horaInicioCentro->diff($horaFinCentro);
        $numeroHoras = $numeroHorasDiff->format('%h');
        echo "--->{$numeroHoras}";

        echo "------>".$this->obtenerDiasLaborablesEntreDosFechas(new \DateTime('2015-07-01'), new \DateTime('2015-07-31'));
    }

    private function obtenerDiasLaborablesEntreDosFechas($fechaInicio,$fechaFin){
        $numeroDiasLaborales = 0;
        $starDate = clone $fechaInicio;
        $endDate = clone $fechaFin;
        while($starDate->format('Y-m-d')!=$endDate->format('Y-m-d')){
             if($starDate->format('l')!= 'Sunday'){
                    $numeroDiasLaborales++;
             }
             $starDate->modify("+1 days");
        }
        if($starDate->format('l')!= 'Sunday'){
            $numeroDiasLaborales++;
        }
        return $numeroDiasLaborales;
    }

    /**
     * A partir de una fecha de inicio y una de fin se obtiene el número de Días feriados registrados eliminando Días feriados de domingo
     * @param DateTime $fechaInicio
     * @param DateTime $fechaFin
     * @return int
     */
    private function obtenerNumeroDiasFeriados($fechaInicio,$fechaFin){
        $genericREST    = new BGenericRest($this->entityManager,$this->entityManagerMako);
        $search         = array(
            'filter' => 'diaferiado.fecha          >= \''.$fechaInicio->format('d-m-Y').'\' AND '.
                        'diaferiado.fecha         <=  \''.$fechaFin->format('d-m-Y').'\' AND '.
                        'diaferiado.activo                 = true'
        );

        $diasFeriados = $genericREST->listarGenerico(
            'ControlEscolarCalendarioBundle:DiaFeriado',
            $search,
            array()
        )['data']['controlescolarcalendariobundle:diaferiado'];
        $numeroDiasLaborales = 0;
        foreach($diasFeriados as $diaFeriado){
             if($diaFeriado->getFecha()->format('l')!= 'Sunday'){
                    $numeroDiasLaborales++;
             }
        }
        return $numeroDiasLaborales;
    }

    /**
     * Metodo para analizar si una oferta educativa en centro esta publicada o no
     * @param OfertaActividadCentro $ofertaEducativaCentro oferta educativa que se analizara si esta publicada o no
     * @return boolean oferta educativa publicada sera true false en caso contrario
     */
    public function ofertaEducativaPublicada($ofertaEducativaCentro){
        return $ofertaEducativaCentro->getPublicado();
    }
}