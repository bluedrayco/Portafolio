<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;

use ControlEscolar\CalendarioBundle\Entity\Ocupacion                            as EOcupacion;
use ControlEscolar\CalendarioBundle\Business\Entity\Parametro                   as BParametro;
use ControlEscolar\CalendarioBundle\Business\Entity\ParametroActividad          as BParametroActividad;
use ControlEscolar\CalendarioBundle\Business\Entity\DiaFeriado                  as BDiaFeriado;
use ControlEscolar\CalendarioBundle\Business\Entity\Movimiento                  as BMovimiento;
use ControlEscolar\CalendarioBundle\Business\Entity\OcupacionHorario            as BOcupacionHorario;
use ControlEscolar\CalendarioBundle\Business\Entity\OcupacionReserva            as BOcupacionReserva;
use ControlEscolar\CalendarioBundle\Business\Entity\EventoActividadOferta       as BEventoActividadOferta;
use ControlEscolar\CalendarioBundle\Business\Entity\EventoActividadOfertaCentro as BEventoActividadOfertaCentro;
use ControlEscolar\CalendarioBundle\Business\Entity\Facilitador                 as BFacilitador;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaEducativaCentro       as BOfertaEducativaCentro;

use Core\SyncBundle\Business\Entity\Destino                                 as BDestino;
use Core\SyncBundle\Business\Action\CursoMakoSync                           as BCursoMakoSync;
use Core\SyncBundle\Business\Action\GeneraPaqueteSync                       as BGeneraPaquetesSync;
use Core\SyncBundle\Business\Action\NotificacionOcupacionCentroACentralSync as BNotificacionOcupacionCentroACentralSync;
use Core\SyncBundle\Command\ObtenAulasYFacilitadoresCommand                 as CAulaFacilitador;
use Core\NotificacionesBundle\Business\Action\Notificacion                  as ANotificacion;
use Enova\Mail\Sendmail                                                     as LSendMail;
use Core\CoreBundle\Business\Entity\GenericRest                             as BGenericRest;

use Core\NotificacionesBundle\Business\Entity\Notificacion                  as BNotificacionEntity;
use Core\UserBundle\Entity\Usuario;

use Core\CoreBundle\Lib\EnovaTransformsToArray as LEnovaTransformsToArray;
use Core\CoreBundle\Business\Entity\BusinessEntity;

// Uso de input y output para la ejecución del comando.
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

use Core\CoreBundle\Business\Entity\Tabla        as BTabla;
use Core\CoreBundle\Business\Entity\Equivalencia as BEquivalencia;
use Core\CoreBundle\Lib\EnovaDates               as LEnovaDates;

class Ocupacion extends BusinessEntity {
    private $ocupacionEstatusId;
    private $modificacionAula;
    private $modificacionFacilitador;
    private $horarioActual;
    private $horarioAnterior;

    const ALERTA_AUTOMATICA_ELIMINACION_ERROR                   = -1;
    const ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA               = 0;
    const ALERTA_AUTOMATICA_ELIMINACION_PRIMERA                 = 1;
    const ALERTA_AUTOMATICA_ELIMINACION_SEGUNDA                 = 2;
    const ALERTA_AUTOMATICA_ELIMINACION_TERCERA                 = 3;
    const ALERTA_AUTOMATICA_ELIMINACION_TERCERA_CON_INSCRITOS   = 4;


    /**
     * Constructor del Business de Ocupacion
     * @param \Doctrine\ORM\EntityManager $em instancia de doctrine para control escolar
     * @param \Doctrine\ORM\EntityManager $emMako instancia de doctrine para mako 1.4
     */
    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
        $this->ocupacionEstatusId      = 1;
        $this->modificacionAula        = false;
        $this->modificacionFacilitador = false;
        $this->horarioActual           = array();
        $this->horarioAnterior         = array();
    }

    public function sincronizarAulasYFacilitadoresWithMako($contenedor) {
        try {
            //Aqui ponemos la generación del comando
            $CAulaFacilitador   = new CAulaFacilitador();
            $input              = new ArgvInput(array());
            $output             = new ConsoleOutput();

            $CAulaFacilitador->setContainer($contenedor);
            $CAulaFacilitador->run($input, $output);

        } catch (\Exception $e) {
            throw new DBALException(
                "No logramos conectarnos con mako local 1.4 para la sincronizacion con Aulas y Facilitador"
            );
        }
    }

    /**
     *
     * @param array    $obj[horario]                arreglo de horarios en los que se dará el curso, la estructura es:
     *                                              {dia_semana, hora_inicio, hora_final}
     * @param DateTime $obj[fecha_fin]              fecha final en el que terminará la actividad academica o no academica
     * @param DateTime $obj[fecha_inicio]           fecha inicial en el que terminara la actividad academica o no academica
     * @param integer  $obj[usuario_id]             identificador del usuario que realizará la transacción.
     * @param integer  $obj[oferta_id]              identificador de la oferta educativa del centro
     * @param integer  $obj[actividad_academica_id] identificador de la actividad academica
     * @param integer  $obj[actividad_id]           identificador de la actividad no academica
     * @param integer  $obj[esquema_id]             identificador del esquema de la actividad academica
     * @param integer  $obj[facilitador_id]         identificador del facilitador que será el encargado de dar la actividad
     * @param integer  $obj[aula_id]                identificador del aula que será asignada a la actividad
     * @param integer  $obj[oferta_actividad_id]    identificador de la oferta actividad si es que se tiene
     * @return mixed                                objeto con el mensaje de exito o error y con la ocupacion creada.
     */
    public function crearEventosCargaMasiva($obj) {
        // Declaracion de business necesarios para la transaccion
        $BOcupacionReserva      = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BDiasFeriados          = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $BOcupacionHorario      = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);

        // Obtencion de datos necesarios para la transaccion
        $fechaActual = new \DateTime();
        $horario     = $obj['horario'];
        $fechaFinal  = $obj['fecha_final'];
        $fechaInicio = $obj['fecha_inicio'];
        $usuario     = $this->getUsuarioById($obj['usuario_id']);

        if (!$usuario) {
            return $this->respuesta;
        }

        $obj['ocupacion_estatus_id'] = $this->ocupacionEstatusId;

        $this->entityManager->beginTransaction();

        try {
            $this->sincronizarAulasYFacilitadoresWithMako($obj['contenedor']);
            $vtObjetos = $this->obtenerObjetosParaOfertaEducativaCentro(
                $obj['oferta_id'],
                $obj['actividad_academica_id'],
                $obj['actividad_id'],
                $obj['esquema_id'],
                $obj['facilitador_id'],
                $obj['aula_id'],
                $obj['ocupacion_estatus_id'],
                $obj['oferta_actividad_id']
            );

            // Asignacion del arreglo obtenido a sus correspondientes variables
            $ofertaEducativaCentro = $vtObjetos["ofertaEducativaCentro"];
            $actividad             = $vtObjetos["actividad"];
            $actividadAcademica    = $vtObjetos["actividadAcademica"];
            $facilitador           = $vtObjetos['facilitador'];
            $esquema               = $vtObjetos["esquema"];
            $ocupacionEstatus      = $vtObjetos['ocupacionEstatus'];
            $ofertaActividadCentro = $vtObjetos['ofertaActividadCentro'];
            $aula                  = $vtObjetos['aula'];
            $totalHoras            = ($vtObjetos["actividadAcademica"] != null) ?
                                        $actividadAcademica->getDuracionHorasPresenciales() :
                                        0;
            $totalHoras            = ceil(($totalHoras / 60));
            $diasFeriados          = $BDiasFeriados->obtencionDiasFeriados();

            // Generacion de fechas de eventos
            $fechas = $BEventoActividadOferta->generaFechasEventos(
                $horario,
                $totalHoras,
                $fechaInicio,
                $diasFeriados,
                $fechaFinal
            );

            if (count($fechas["fechas"]) <= 0) {
                $this->entityManager->rollback();
                $this->buildRespuesta(
                    'No se ha podido realizar la operación solicitada ya que no '.
                    'se encontro fecha para el horario seleccionado',
                    array(),
                    false,
                    404
                );
                return $this->respuesta;
            }


            $ocupacion = $this->creacionOcupacion(
                $fechaActual,
                $fechas["fechaInicio"],
                $fechas["fechaFinal"],
                $usuario,
                $facilitador,
                $actividad,
                $actividadAcademica,
                $esquema,
                $ocupacionEstatus,
                $ofertaEducativaCentro,
                $aula,
                $ofertaActividadCentro
            );

            $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
            $fechaNow                 = new \DateTime();
            $validacionOcupacion      = $ocupacionReservaBusiness->validaEvento(
                    $ocupacion->getFechaInicio()->format('Y-m-d'),
                    $ocupacion->getFechaInicio()->format('H:i:s'),
                    $ocupacion->getFechaInicio()->format('Y-m-d'),
                    $ocupacion->getFechaInicio()->format('H:i:s')
            );
            switch($validacionOcupacion['opcion']){
                case 1:
                    $this->entityManager->rollback();
                    $parametroBusiness           = new BParametro($this->entityManager,$this->entityManagerMako);
                    $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag(
                            'numero_horas_minimas_recorrido'
                            )[0]->getValor();

                    return $this->buildRespuesta(
                            'No es posible crear la actividad puesto que la fecha y '.
                            'hora inicial es menor a la fecha actual.',
                            array('ocupacion'=>null),
                            false,
                            404
                    );
                break;

                case 2:
                    $this->entityManager->rollback();
                    $parametroBusiness           = new BParametro($this->entityManager,$this->entityManagerMako);
                    $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag(
                            'numero_horas_minimas_recorrido'
                    )[0]->getValor();
                    return $this->buildRespuesta(
                            "No es posible crear la actividad porque debe ser mas de {$numeroHorasMinimasRecorrido} ".
                            "hrs de diferencia con respecto a la hora actual para ser modificada.",
                            array('ocupacion'=>null),
                            false,
                            404
                    );
                break;

                case 3:
                    $this->entityManager->rollback();

                    $parametroBusiness           = new BParametro($this->entityManager,$this->entityManagerMako);
                    $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag(
                            'numero_horas_minimas_recorrido'
                    )[0]->getValor();

                    return $this->buildRespuesta(
                            "No es posible crear la actividad porque su fecha y hora inicial deben ser ".
                            "{$numeroHorasMinimasRecorrido} hrs antes de que inicie la actividad",
                            array('ocupacion'=>null),
                            false,
                            404
                    );
                break;
            }

            // Generamos el Horario
            $BOcupacionHorario->generaHorario($horario, $usuario, $ocupacion);

            // Generamos los eventos con nosotros y reserva20w
            $eventos = $BOcupacionReserva->generaEventos($aula, $facilitador, $ocupacion, $usuario, $fechas["fechas"]);

            $cursoMakoBusiness = new BCursoMakoSync($this->entityManager,$this->entityManagerMako);
            $paquete = $cursoMakoBusiness->sincronizarCursoWithMako(
                $usuario,
                $ocupacion,
                $totalHoras,
                'create',
                $eventos
            );

            if ($paquete['code'] == 404) {
                $this->entityManager->rollback();
                return $this->buildRespuesta(
                    'Error al registrar la calendarización ya que no se ha podido sincronizar con mako 1.4',
                    array(),
                    false,
                    404
                );
            }

            $notificaciones = $this->enviarEmailFacilitador($ocupacion,$usuario);

            //generamos notificacion para ser sincronizada con central
            if ($ofertaEducativaCentro->getPublicado()) {
                $this->log->debug("*******entramos a la notificacion a central si es publicada...");
                $notificacionCentroACentralBusiness = new BNotificacionOcupacionCentroACentralSync(
                        $this->entityManager
                );
                $respuestaSincronizacionCentral = $notificacionCentroACentralBusiness->sincronizarNotificacionCreacionWithCentral(
                    $ocupacion,
                    $usuario
                );

                if ($respuestaSincronizacionCentral['code'] == 404) {
                    $this->entityManager->rollback();
                    return $this->buildRespuesta(
                        'No se pudo notificar con central.',
                        array('ocupacion' => $ocupacion)
                    );
                }
            }

            $this->entityManager->flush();
            $this->entityManager->commit();

            $this->buildRespuesta('post', array('ocupacion' => $ocupacion));
        } catch (DBALException $ex) {
            $this->log->error($ex->getMessage());
            $this->entityManager->rollback();
            $this->buildRespuesta($ex->getMessage(), array(), false, 404);
            return $this->respuesta;
        }catch (\Exception $ex) {
            $this->log->error($ex->getMessage());
            $this->entityManager->rollback();
            $this->buildRespuesta($ex->getMessage(), array(), false, 404);
            return $this->respuesta;
        }

        return $this->respuesta;
    }

    /**
     * Envío de email al facilitador que ya tiene un nuevo grupo que impartir, con su horario correspondiente.
     * @param Ocupacion $ocupacion objeto de tipo ocupacion que envía un email al facilitador que se asignó la ocupacion
     */
    public function enviarEmailFacilitador($ocupacion, $usuario){
        $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        //Obtenemos el primer y ultimo evento de la ocupación
        $eventos = $ocupacionReservaBusiness->obtenerPrimerYUltimoEventoByOcupacion($ocupacion);
        $ocupacionHorarioBusiness = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);
        //obtenemos los horarios que posee la ocupación
        $horarios = $ocupacionHorarioBusiness->obtenerHorariosByOcupacion($ocupacion->getOcupacionId());
        //nombre centro
        $claveCentro         = $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro");
        $centro              = $this->getCentroById($claveCentro);
        $nombre_corto_centro = $centro->getNombreCorto();
        $diaSemanaTmp = array(0=>'Domingo',1=>'Lunes',2=>'Martes',3=>'Miércoles',4=>'Jueves',5=>'Viernes',6=>'Sábado');
        $cadenaHorario = "";
        foreach($horarios as $horario){
            foreach($diaSemanaTmp as $clave=>$valor){
                if($clave==$horario->getDiaSemana()){
                    $cadenaHorario=$cadenaHorario."<b>".$valor."</b> : ".$horario->getHoraInicio()->format('H:i')." - ".$horario->getHoraFin()->format('H:i')."<br>";
                    break;
                }
            }

        }
        if ($ocupacion->getActividad()) {
            $nombreCurso = $ocupacion->getActividad()->getNombre();
        } else {
            $nombreCurso = $ocupacion->getActividadAcademica()->getNombre();
        }


        $fechaInicio    = $eventos['primer_evento']->getFechaEvento()->format('d-m-Y');
        $fechaTermino   = $eventos['ultimo_evento']->getFechaEvento()->format('d-m-Y');

        $cadenaTmp="";

        $cadena = "se te asignó el siguiente grupo a impartir en tu centro {$nombre_corto_centro}.<br>Grupo: {$nombreCurso} {$ocupacion->getClaveGrupo()} del día {$fechaInicio} a {$fechaTermino} con el siguiente horario:<br><br>{$cadenaHorario}";

        $this->log->debug("------------------------MENSAJE para el facilitador al que se le asignó la ocupación: {$cadena}");
        $sendMailLib = new LSendMail();
            if ($ocupacion->getFacilitador()->getEmail()!=null) {
                $cadenaTmp="{$ocupacion->getFacilitador()->getNombre()} {$ocupacion->getFacilitador()->getApellidoPaterno()} {$ocupacion->getFacilitador()->getApellidoMaterno()} ".$cadena;
                $cadenaTmp .= '<br>Movimiento realizado por: ' . $usuario->getUsername();
                try{
                    $sendMailLib->send($ocupacion->getFacilitador()->getEmail(),'Asignación de un grupo: ' . $ocupacion->__toString(), $cadenaTmp);
                    $this->log->debug("****************se envió email al facilitador****************");
                } catch (\Exception $ex) {
                    $this->log->error("MENSAJE de error_________>".$ex->getMessage());
                }
            }
    }
    /**
     * Obtenemos los objetos necesarios de OfertaEducativaCentro
     * @param integer $OfertaEducativaCentroId     identificador de la oferta educativa para el centro
     * @param integer $actividadAcademicaId        actividad academica id
     * @param integer $actividadId                 actividad no academica id
     * @param integer $esquemaId                   id de tra_actividad_academica_esquema
     * @param integer $facilitadorId               identificador del facilitador
     * @param integer $aulaId                      identificador del aula
     * @param integer $ocupacionEstatusId          identificador del estado de la ocupacion
     * @return array|boolean                       false si ocurrio un error, si es exito devuelve los objetos buscados
     */
    private function obtenerObjetosParaOfertaEducativaCentro(
        $OfertaEducativaCentroId,
        $actividadAcademicaId,
        $actividadId,
        $esquemaId,
        $facilitadorId,
        $aulaId,
        $ocupacionEstatusId,
        $ofertaActividadId,
        $ocupacionId = null
    ) {
        $vtObjetos = array(
            "ofertaEducativaCentro" => null,
            "actividadAcademica"    => null,
            "actividad"             => null,
            "esquema"               => null,
            'facilitador'           => null,
            'aula'                  => null,
            'ocupacionEstatus'      => null,
            'ofertaActividadCentro' => null,
            'ocupacion'             => null
        );

        if ($ocupacionId) {
            $ocupacion = $this->getRepository(
                "ControlEscolarCalendarioBundle:Ocupacion",
                array('ocupacion_id' => $ocupacionId),
                null,
                null,
                null,
                'find',
                null
            );

            if (!$ocupacion) {
                $this->buildRespuesta(
                    'No se encontro la Ocupacion con el id {$ocupacionId}',
                    array('ofertaactividadcentro' => array()),
                    false,
                    404
                );
                $this->log->error("No se encontro la Ocupacion con el id {$ocupacionId}");
                return $vtObjetos;
            }

            $vtObjetos["ocupacion"] = $ocupacion;
        }

        $facilitador = $this->getRepository(
            "ControlEscolarCalendarioBundle:Facilitador",
            array('facilitador_id' => $facilitadorId),
            null,
            null,
            null,
            'find',
            null
        );

        if (!$facilitador) {
            $this->buildRespuesta(
                "No se encontro el facilitador con el id {$FacilitadorId}",
                array('ofertaactividadcentro' =>  array()),
                false,
                404
            );
            $this->log->error("No se encontro el facilitador con el id {$FacilitadorId}");
            return $vtObjetos;
        }

        $vtObjetos["facilitador"] = $facilitador;

        $aula = $this->getRepository(
            "ControlEscolarCalendarioBundle:Aula",
            array('aula_id' => $aulaId),
            null,
            null,
            null,
            'find',
            null
        );

        if (!$aula) {
            $this->buildRespuesta(
                "No se encontro el Aula con el id {$aulaId}",
                array('ofertaactividadcentro' =>  array()),
                false,
                404
            );
            $this->log->error("No se encontro el Aula con el id {$aulaId}");
            return $vtObjetos;
        }

        $vtObjetos["aula"] = $aula;

        $ocupacionEstatus = $this->getRepository(
            "ControlEscolarCalendarioBundle:OcupacionEstatus",
            array('ocupacion_estatus_id' => $ocupacionEstatusId),
            null,
            null,
            null,
            'find',
            null
        );

        if (!$ocupacionEstatus) {
            $this->buildRespuesta(
                "No se encontro la OcupacionEstatus con el id {$ocupacionEstatusId}",
                array('ofertaactividadcentro' =>  array()),
                false,
                404
            );
            $this->log->error("No se encontro la OcupacionEstatus con el id {$ocupacionEstatusId}");
            return $vtObjetos;
        }

        $vtObjetos["ocupacionEstatus"] = $ocupacionEstatus;

        $ofertaEducativaCentro = $this->getRepository(
            "ControlEscolarCalendarioBundle:OfertaEducativaCentro",
            array('oferta_educativa_centro_id' => $OfertaEducativaCentroId),
            null,
            404,
            null,
            'find',
            null
        );

        if (!$ofertaEducativaCentro) {
            $this->buildRespuesta(
                "No se encontro el oferta Educativa con el id {$OfertaEducativaCentroId}",
                array('ofertaactividadcentro' =>  array()),
                false,
                404
            );
            $this->log->error("No se encontro el oferta Educativa con el id {$OfertaEducativaCentroId}");
            return $vtObjetos;
        }

        $vtObjetos["ofertaEducativaCentro"] = $ofertaEducativaCentro;

        if ($actividadId != null) { // Es una Actividad No Academica
            //Obtenemos el Objeto de la Actividad No Academica
            $actividad = $this->getRepository(
                "ControlEscolarPlanAnualBundle:Actividad",
                array("actividad_id"=>$actividadId),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$actividad) {
                $this->buildRespuesta(
                    "No se encontro la actividad  No academica con el id {$actividadId}",
                    array('ofertaactividadcentro' =>  array()),
                    false,
                    404
                );
                $this->log->error("No se encontro la actividad  No academica con el id {$actividadId}");
                return $vtObjetos;
            }
            $vtObjetos["actividad"] = $actividad;

        } else { // Se trata de una actividad Academica :D
            // Obtenemos el objeto de la actividad Academica.
            $actividadAcademica =  $this->getRepository(
                "CoreCoreBundle:ActividadAcademica",
                array("actividad_academica_id"=>$actividadAcademicaId),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$actividadAcademica) {
                $this->buildRespuesta(
                    "No se encontro la actividad academica con el id " .$actividadAcademicaId,
                    array('ofertaactividadcentro' =>  array()),
                    false,
                    404
                );
                $this->log->error("No se encontro la actividad academica con el id " .$actividadAcademicaId);
                return $vtObjetos;
            }

            $vtObjetos["actividadAcademica"] = $actividadAcademica;

            $esquema = $this->getRepository(
                "CoreCoreBundle:ActividadAcademicaEsquema",
                array("actividad_academica_esquema_id"=>$esquemaId),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$esquema) {
                $this->buildRespuesta(
                    "No se encontro la actividad academica esquema con el id ". $esquemaId,
                    array('ofertaactividadcentro' =>  array()),
                    false,
                    404
                );
                $this->log->error("No se encontro la actividad academica esquema con el id ". $esquemaId);
                return $vtObjetos;
            }

            $vtObjetos["esquema"] = $esquema;
        }

        if ($ofertaActividadId != null) {
            // Obtenemos el objeto de OfertaActividadCentro.
            $ofertaActividadCentro =  $this->getRepository(
                "ControlEscolarCalendarioBundle:OfertaActividadCentro",
                array("oferta_actividad_centro_id"=>$ofertaActividadId),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$ofertaActividadCentro) {
                $this->buildRespuesta(
                    "No se encontro la ofertaActividadCentro con el id " .$ofertaActividadId,
                    array('ofertaactividadcentro' =>  array()),
                    false,
                    404
                );
                $this->log->error("No se encontro la ofertaActividadCentro con el id " .$ofertaActividadId);
                return $vtObjetos;
            }

            $vtObjetos["ofertaActividadCentro"] = $ofertaActividadCentro;
        }

        return $vtObjetos;
    }

    /**
     * Metodo que crea a partir de los eventos obtenidos una ocupacion
     * @param DateTime $fecha fecha actual del sistema para crear la ocupacion
     * @param DateTime $fechaInicio fecha de inicio de la ocupacíon de la actividad en el aula con el facilitador
     * @param DateTime $fechaFin fecha de fin de la ocupacion de la actividad en el aula con el facilitador
     * @param Object $usuario usuario que genera la ocupacion
     * @param Object $facilitador facilitador que estará a cargo de la actividad que se impartira
     * @param Object  $actividad actividad no academica que se impartira en el aula
     * @param Object $actividadAcademica actividad academica que se impartira en el aula
     * @param Object $actividadAcademicaEsquema esquema de la actividad academica
     * @param Object $ocupacionEstatus estado de la ocupacion
     * @param Object $ofertaEducativaCentro oferta educativa  vigente que tiene el centro
     * @param Object $aula aula en la que se impartira la actividad
     * @param Object $ofertaActividadCentro oferta actividad ligada al evento
     * @return null|\ControlEscolar\CalendarioBundle\Entity\Ocupacion ocupacion creada o null
     * @throws \DBALException
     */
    public function creacionOcupacion(
        $fecha,
        $fechaInicio,
        $fechaFin,
        $usuario,
        $facilitador,
        $actividad,
        $actividadAcademica,
        $actividadAcademicaEsquema,
        $ocupacionEstatus,
        $ofertaEducativaCentro,
        $aula,
        $ofertaActividadCentro
    ) {
        try {
            $clave     = '';
            $ocupacion = new EOcupacion();

            if ($actividad) {
                $ocupacion->setActividad($actividad);
                $clave = $actividad->getClave();
            } elseif ($actividadAcademica) {
                $clave = $actividadAcademica->getClave();
                $ocupacion->setActividadAcademica($actividadAcademica);
                $ocupacion->setActividadAcademicaEsquema($actividadAcademicaEsquema);
            }

            $ocupacion->setActivo(true);
            $ocupacion->setAula($aula);
            $ocupacion->setClaveGrupo('');
            $ocupacion->setFacilitador($facilitador);
            $ocupacion->setFechaAlta($fecha);
            $ocupacion->setFechaFin(new \DateTime($fechaFin));
            $ocupacion->setFechaInicio(new \DateTime($fechaInicio));
            $ocupacion->setFechaModificacion($fecha);
            $ocupacion->setNumeroInscritos(0);
            $ocupacion->setNumeroInteresados(0);
            $ocupacion->setNumeroMovimientos(0);
            $ocupacion->setOcupacionEstatus($ocupacionEstatus);
            $ocupacion->setOfertaEducativaCentro($ofertaEducativaCentro);
            $ocupacion->setUsuarioAlta($usuario);
            $ocupacion->setUsuarioModifica($usuario);
            $ocupacion->setOfertaActividadCentro($ofertaActividadCentro);

            $this->entityManager->persist($ocupacion);
            $this->entityManager->flush();

            $ocupacion->setClaveGrupo($this->generarClaveOcupacion($clave, $ocupacion));

            $this->entityManager->flush();

            return $ocupacion;
        } catch (DBALException $e) {
            $this->log->error($e->getMessage());
            throw new DBALException("Error al crear la ocupación...");
        }
        return null;
    }

    public function obtencionEventosCentroOTodos(
        $tipo,
        $oferta_educativa_centro_id,
        $tipo,
        $fecha_inicio,
        $fecha_fin,
        $aula_id,
        $facilitador_id
    ) {
        $facilitador = null;
        $aula        = null;

        $ofertaEducativaCentro = $this->getRepository(
            "ControlEscolarCalendarioBundle:OfertaEducativaCentro",
            array('oferta_educativa_centro_id' => $oferta_educativa_centro_id ),
            null,
            404,
            null,
            'find',
            null
        );

        if (!$ofertaEducativaCentro) {
            $this->buildRespuesta(
                'No se encontro el oferta Educativa con el id {$OfertaEducativaCentroId}',
                array('eventos' =>  array()),
                false,
                404
            );
            return $this->respuesta;
        }

        if (($facilitador_id != 0) and ($tipo != 'oferta')) {
            $facilitador = $this->getRepository(
                "ControlEscolarCalendarioBundle:Facilitador",
                array('facilitador_id' => $facilitador_id),
                null,
                null,
                null,
                'find',
                null
            );

            if (!$facilitador) {
                $this->buildRespuesta(
                    'No se encontro el facilitador con el id {$FacilitadorId}',
                    array('eventos' =>  array()),
                    false,
                    404
                );
                return $this->respuesta;
            }
        }

        if (($aula_id != 0) and ($tipo != 'oferta')) {
            $aula = $this->getRepository(
                "ControlEscolarCalendarioBundle:Aula",
                array('aula_id' => $aula_id),
                null,
                null,
                null,
                'find',
                null
            );

            if (!$aula) {
                $this->buildRespuesta(
                    'No se encontro el Aula con el id {$aulaId}',
                    array('eventos' =>  array()),
                    false,
                    404
                );
                return $this->respuesta;
            }
        }

        $eventosOferta = array();
        $eventosCentro = array();

        switch($tipo) {
            case 'todos':
                $ocupacionReserva = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
                $eventosOferta    = $ocupacionReserva->getEventos(
                    $ofertaEducativaCentro,
                    $fecha_inicio,
                    $fecha_fin,
                    $aula,
                    $facilitador
                );
                $eventoActividadOferta = new BEventoActividadOfertaCentro($this->entityManager,$this->entityManagerMako);
                $eventosCentro         = $eventoActividadOferta->getEventos(
                    $ofertaEducativaCentro,
                    $fecha_inicio,
                    $fecha_fin
                );
                break;
            case 'oferta':
                $eventoActividadOferta = new BEventoActividadOfertaCentro($this->entityManager,$this->entityManagerMako);
                $eventosCentro         = $eventoActividadOferta->getEventos(
                    $ofertaEducativaCentro,
                    $fecha_inicio,
                    $fecha_fin
                );
                break;

            case 'centro':
                $ocupacionReserva = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
                $eventosOferta    = $ocupacionReserva->getEventos(
                    $ofertaEducativaCentro,
                    $fecha_inicio,
                    $fecha_fin,
                    $aula,
                    $facilitador
                );
                break;
        }

        $arregloEventos = array();
        $arregloEventos = array_merge($eventosCentro, $eventosOferta);

        $this->buildRespuesta(
            'Listado de Eventos',
            array('eventos' =>  $arregloEventos),
            false,
            200
        );

        return $this->respuesta;
    }

    /**
     * Eliminacion de ocupaciones por alertas automaticas
     * @param integer $params[usuario_id] usuario que eliminara las ocupaciones
     * @return mixed respuesta con message, code y data
     * @throws \Exception error en caso que exista un rollback
     */
    public function eliminarOcupaciones($params){
        $ocupacionesEliminadas      = array();
        $ocupacionesNoeliminadas    = array();
        $erroneas                   = array();
        $notificaciones             = array();
        $fechaActual                = new \DateTime();
        $formatoFecha               = 'Y-m-d G:i:s';
        $usuario                    = $this->getUsuarioById($params['usuario_id']);

        $genericRestBusiness    = new BGenericRest($this->entityManager);

        if (!$usuario) {
            return $this->respuesta;
        }

        $search = array(
            'filter' => 'ocupacion.fecha_fin             >= \''.$fechaActual->format('Y-m-d').'\' AND '.
                        'ocupacion.fecha_inicio          >= \'2016-05-01\'                        AND '.
                        'ocupacion.activo                 = true                                  AND '.
                        'ofertaeducativacentro.publicado  = true'
        );

        try{
            $ocupacionesObjs = $genericRestBusiness->listarGenerico(
                    'ControlEscolarCalendarioBundle:Ocupacion',
                    $search,
                    array()
            )['data']['controlescolarcalendariobundle:ocupacion'];

//            error_log(\Doctrine\Common\Util\Debug::dump($ocupacionesObjs['data']['controlescolarcalendariobundle:ocupacion']));
//            exit();
            $this->log->debug(
                'Se encontraron ('.
                count($ocupacionesObjs).
                ') ocupaciones para eliminar (o notificar). Con la busqueda: '.
                json_encode($search)
            );

            foreach ($ocupacionesObjs as $ocupacion) {
                $this->log->debug('******************Iniciando proceso de ocupacion: "'.$ocupacion.'"*************************');

                /**
                 * Saltar de revisar (de cualquier manera, notificaciones o eliminacion) esta ocupacion.
                 * Ya que no esta publicada!
                 */
                if (!$ocupacion->getOfertaEducativaCentro()->getPublicado()) {
                    $this->log->debug('La ocupacion: "'.$ocupacion.'" no esta publicada. No se hará nada.');
                    continue;
                }

                $validaEliminacion = $this->validaEliminacionAutomatica($ocupacion);


                $this->log->debug(
                    'Ocupacion: "'.$ocupacion.'". '.
                    'resultado de la validacion de la eliminacion('.$validaEliminacion.') '
                );

                switch ($validaEliminacion) {
                    case Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA:
                        $this->log->debug("la Ocupacion: {$ocupacion} no es candidata a ser eliminada... codigo: {$validaEliminacion}");
                        $ocupacionesNoeliminadas[]=array(
                            'ocupacion'            => $ocupacion,
                            'resultadoEliminacion' => "No cuenta con las condiciones para eliminarse... codigo de validacion: {$validaEliminacion}"
                        );
                        break;

                    case Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_PRIMERA:
                        $this->log->debug("la Ocupacion: {$ocupacion} cumple con el envio de la primera alerta de notificacion... codigo: {$validaEliminacion}");

                        $notificacion = new BNotificacionOcupacionCentroACentralSync(
                            $this->entityManager,
                            $this->entityManagerMako
                        );
                        $resultadoNotif = $notificacion->sincronizarNotificacionAlertaInscritosWithCentral(
                                $ocupacion,
                                $usuario,
                                'tiempo_primer_eliminacion'
                        );

                        $ocupacionesNoeliminadas[]=array(
                            'ocupacion'            => $ocupacion,
                            'resultadoEliminacion' => "Se envia primera alerta de Eliminacion... codigo de validacion: {$validaEliminacion}"
                        );
                        break;

                    case Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_SEGUNDA:
                        $this->log->debug("la Ocupacion: {$ocupacion} cumple con el envio de la primera alerta de notificacion... codigo: {$validaEliminacion}");

                        $notificacion = new BNotificacionOcupacionCentroACentralSync(
                            $this->entityManager,
                            $this->entityManagerMako
                        );
                        $resultadoNotif = $notificacion->sincronizarNotificacionAlertaInscritosWithCentral(
                                $ocupacion,
                                $usuario,
                                'tiempo_segunda_eliminacion'
                        );

                        $ocupacionesNoeliminadas[]=array(
                            'ocupacion'            => $ocupacion,
                            'resultadoEliminacion' => "Se envia primera alerta de Eliminacion... codigo de validacion: {$validaEliminacion}"
                        );
                        break;

                    case Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_TERCERA:
                        $this->log->debug("la Ocupacion: {$ocupacion} cumple con las condiciones y sera eliminada ya que no cuenta con socios inscritos... codigo: {$validaEliminacion}");

                        $ocupacionHorarioBusiness = new BOcupacionHorario($this->entityManager);
                        $ocupacionesHorario       = $ocupacionHorarioBusiness->obtenerHorariosByOcupacion(
                            $ocupacion->getOcupacionId()
                        );
                        $horario                  = $ocupacionHorarioBusiness->getHorarioString(
                            $ocupacionesHorario
                        );

                        $textoNotificacion = sprintf(
                            'Se ha detectado que en la actividad %s de '.
                            'fecha %s y horario %s, no tiene alumnos inscritos, '.
                            'ha sido eliminada automáticamente por el sistema.',
                            $ocupacion,
                            $ocupacion->getFechaInicio()->format('Y-m-d'),
                            $horario
                        );

                        $eliminacion = $this->eliminarOcupacion(
                            $ocupacion->getOcupacionId(),
                            $usuario->getId(),
                            $textoNotificacion,
                            true,
                            true,
                            'eliminacion_automatica'
                        );

                        $ocupacionesEliminadas[]=array(
                            'ocupacion'            => $ocupacion,
                            'resultadoEliminacion' => "La ocupacion fue eliminada... codigo de validacion: {$validaEliminacion}"
                        );
                        break;

                    case Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_TERCERA_CON_INSCRITOS:
                        $this->log->debug("la Ocupacion: {$ocupacion} cumple con el envio de la primera alerta de notificacion... codigo: {$validaEliminacion}");

                        $this->enviarEmailMinimoInscritos($ocupacion,$usuario);

                        $ocupacionesNoeliminadas[]=array(
                            'ocupacion'            => $ocupacion,
                            'resultadoEliminacion' => "Se envia primera alerta de Eliminacion... codigo de validacion: {$validaEliminacion}"
                        );
                        break;

                    case Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_ERROR:
                        $this->log->error("la Ocupacion: {$ocupacion} sufrio un error desconocido proveniente de la verificacion y se notificara a los encargados del sistema... codigo: {$validaEliminacion}");

                        $sendMailLib           = new LSendMail();
                        $emailAdministrador    = $GLOBALS['kernel']->getContainer()->getParameter("email_soporte");
                        $centroId              = $this->getCentroById(
                            $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro")
                        );
                        $centro = $this->getCentroById($centroId);
                        $sendMailLib->send(
                            $emailAdministrador,
                            "Error al validar la ocupacion {$ocupacion} por crones automaticos",
                            "Se detecto un error al procesar la ocupacion {$ocupacion} en el centro: {$centro->getNombre()} con  nombre corto: {$centro->getNombreCorto()}"
                        );

                        $erroneas[]=array(
                            'ocupacion'            => $ocupacion,
                            'resultadoEliminacion' => "Se tuvo un error desconocido al tratar de validar esta ocupacion... codigo de validacion: {$validaEliminacion}"
                        );
                        break;
                }
                $this->log->debug('****************termino del procesamiento de la ocupacion: "'.$ocupacion.'"**********************');
            }

        return $this->buildRespuesta(
            "Se proceso adecuadamente el cron automatico de eliminacion...",
            array(
                'parametros'     => array(
                    'fechaServidor' => $fechaActual->format($formatoFecha),
                    'busqueda'      => $search
                ),
                'noEliminadas'   => $ocupacionesNoeliminadas,
                'eliminadas'     => $ocupacionesEliminadas,
                'notificaciones' => $notificaciones,
                'erroneas'       => $erroneas
            )
        );
        } catch (\Exception $ex) {
            $this->log->error('[EXCEPTION]: @'.$ex->getLine().' "'.$ex->getMessage().'"');
            //throw new \Exception($e);
            return $this->buildRespuesta(
                'Hubo un error en el proceso de los crones automaticos de eliminacion.'.$ex->getMessage(),
                array(),
                false,
                200,
                null,
                'error'
            );
        }
        return $this->respuesta;
    }

    public function enviarEmailMinimoInscritos($ocupacion,$usuario){

        $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
        $numeroEnviosEmail = count(
            $movimientoBusiness->obtenerMovimientos(
                "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                $ocupacion->getOcupacionId(),
                "notificacion_minimo_inscritos_pasado_evento"
            )
        );
        if($numeroEnviosEmail>0){
            return true;
        }

        $respuesta = $this->enviarNotificacionesNumeroMinimoInscritosCentralYLocal($ocupacion,$usuario);

        if($respuesta['code']==404){
            throw new \Exception($respuesta['message']);
        }else{
            $movimiento = $movimientoBusiness->crearMovimiento('\ControlEscolar\CalendarioBundle\Entity\Ocupacion', $ocupacion->getOcupacionId(), 'notificacion_minimo_inscritos_pasado_evento', $respuesta['data']['mensaje'], $usuario->getId());
        }
    }

    public function enviarNotificacionesNumeroMinimoInscritosCentralYLocal($ocupacion,$usuario){
        $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        //Obtenemos el primer y ultimo evento de la ocupación
        $eventos = $ocupacionReservaBusiness->obtenerPrimerYUltimoEventoByOcupacion($ocupacion);
        $ocupacionHorarioBusiness = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);
        //obtenemos los horarios que posee la ocupación
        $horarios = $ocupacionHorarioBusiness->obtenerHorariosByOcupacion($ocupacion->getOcupacionId());
        //nombre centro
        $claveCentro         = $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro");
        $centro              = $this->getCentroById($claveCentro);
        $nombre_corto_centro = $centro->getNombreCorto();
        $diaSemanaTmp = array(0=>'Domingo',1=>'Lunes',2=>'Martes',3=>'Miércoles',4=>'Jueves',5=>'Viernes',6=>'Sábado');
        $cadenaHorario = "";
        foreach($horarios as $horario){
            foreach($diaSemanaTmp as $clave=>$valor){
                if($clave==$horario->getDiaSemana()){
                    $cadenaHorario=$cadenaHorario."<b>".$valor."</b> : ".$horario->getHoraInicio()->format('H:i')." - ".$horario->getHoraFin()->format('H:i')."<br>";
                    break;
                }
            }

        }

        $fechaInicio    = $eventos['primer_evento']->getFechaEvento()->format('d-m-Y');
        $fechaTermino   = $eventos['ultimo_evento']->getFechaEvento()->format('d-m-Y');
        if ($ocupacion->getActividad()) {
            $nombreCurso = $ocupacion->getActividad()->getNombre();
        } else {
            $nombreCurso = $ocupacion->getActividadAcademica()->getNombre();
        }
        $clave = $ocupacion->getClaveGrupo();

        $mensajeNotificacion = "Se ha detectado que la actividad {$nombreCurso} con clave {$clave} que inicia el {$fechaInicio} y con horario:<br><br>{$cadenaHorario}<br><br> No cumple con el mínimo de alumnos inscritos.";


        $notificacionCentroACentralBusiness = new BNotificacionOcupacionCentroACentralSync(
            $this->entityManager
        );

        $paquete = $notificacionCentroACentralBusiness->sincronizarNotificacionMinimoInscritosWithCentral(
            $ocupacion,
            $usuario,
            $mensajeNotificacion
        );

        if ($paquete['code'] == 404) {
            $this->log->error(
                    "No se pudo sincronizar el mensaje con Central o con Centro." . $e->getMessage()
            );
            return $this->buildRespuesta(
                'No se pudo sincronizar la notificación con Central o con Centro.',
                array('mensaje' =>''),
                false,
                404
            );
        }

        return $this->buildRespuesta('ok',array('mensaje'=>$mensajeNotificacion),true,200);
    }

    /**
     * Metodo para eliminar ocupacion ya sea de forma manual o automatica
     * @param integer $ocupacion_id identificador de ocupacion a eliminar
     * @param integer $usuario_id identificador del usuario que eliminara la ocupacion
     * @param string $textoNotificacion texto que tendra la notificacion
     * @param boolean $central flag para determinar si la conexion se hizo remota (desde central) o no
     * @param boolean $automatico flag para especificar si es activada por un cron para no volver a validar o no
     * @return mixed respuesta estandar del servicio rest con 404 o 200 (error o exito)
     */
    public function eliminarOcupacion($ocupacion_id, $usuario_id, $textoNotificacion = null,$central=true,$automatico=false,$tipoMovimiento='') {
        $usuario          = $this->getUsuarioById($usuario_id);
        $ocupacion        = $this->getOcupacionById($ocupacion_id);
        $puedeEliminar    = $automatico==false?$this->validaEliminacion($ocupacion_id, true):3;

        $this->entityManager->beginTransaction();
        $this->log->debug("&&&&&&&&&&&&&entre a la validacion de eliminar...".$puedeEliminar);
        if ($puedeEliminar >= 1) {  //se Puede eliminar.
            try {
                //validamos el numero de movimientos por si no se han pasado estos del limite permitido
                $parametroBusiness  = new BParametroActividad($this->entityManager);
                $numero_movimientos = $parametroBusiness->obtenerParametroPorActividad('numero_maximo_movimientos',$ocupacion->getActividadAcademica());
                $movimientos        = $numero_movimientos[0]->getValor();

                if ($ocupacion->getNumeroMovimientos() >= $movimientos && !$central) {
                    $this->log->debug("///////////////rollback numero de movimientos");
                    $this->entityManager->rollback();
                    return $this->buildRespuesta(
                        "No se pudo cancelar la ocupacion porque sobrepasó el número de Movimientos",
                        array(),
                        false,
                        404
                    );
                }
                // ------Cancelamos localmente. ------ //
                $this->log->debug("se cancela localmente...");
                $eliminado = $this->getRepository(
                    "ControlEscolarCalendarioBundle:Ocupacion",
                    array('ocupacion_id' => $ocupacion_id),
                    null,
                    null,
                    null,
                    'findStoreProcedureCancelacion',
                    null
                );
                $this->log->debug("resultado de eliminado...: {$eliminado}");
                // ------ Avisamos a Mako que cancele el curso. ------ //
                $this->log->debug("se sincroniza curso con mako 1.4");
                $cursoMakoBusiness = new BCursoMakoSync($this->entityManager,$this->entityManagerMako);

                $paquete           = $cursoMakoBusiness->sincronizarCursoWithMako(
                    $usuario,
                    $ocupacion,
                    0,
                    'delete',
                    array()
                );

                if ($paquete['code'] == 404) {
                    $this->log->debug("///////////////rollback no se pudo sincronizar con mako 1.4");
                    $this->entityManager->rollback();
                    return $this->buildRespuesta(
                        'no se pudo sincronizar con mako 1.4 la eliminación',
                        array(),
                        false,
                        404
                    );
                }

                $stringer           = new LEnovaTransformsToArray($this->entityManager,$this->entityManagerMako);
                $modificaciones     = $stringer->extractorDatos($ocupacion);
                $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
                $movimiento         = $movimientoBusiness->crearMovimiento(
                    '\ControlEscolar\CalendarioBundle\Entity\Ocupacion',
                    $ocupacion->getOcupacionId(),
                    $tipoMovimiento==''?'eliminacion':$tipoMovimiento,
                    json_encode($modificaciones),
                    $usuario_id
                );

                //Si esta publicada el calendario en el centro se notifica
                if ($ocupacion->getOfertaEducativaCentro()->getPublicado()){
                    //generamos notificacion para ser sincronizada con central
                    $notificacionCentroACentralBusiness = new BNotificacionOcupacionCentroACentralSync(
                        $this->entityManager
                    );

                    $paquete = $notificacionCentroACentralBusiness->sincronizarNotificacionEliminacionWithCentral(
                        $ocupacion,
                        $usuario,
                        $textoNotificacion
                    );

                    $this->log->debug("ya se sincronzio la notificacion con central  :).");
                }

                if ($paquete['code'] == 404) {
                    $this->log->debug("///////////////rollback error al tratar de enviar sincronizacion a central");
                    return $this->buildRespuesta(
                        'Ocurrio un error al tratar de modificar la Actividad',
                        array(),
                        false,
                        404
                    );
                }

                $this->buildRespuesta(
                    "La actividad se ha Eliminado correctamete",
                    array(),
                    true,
                    200
                );

                $this->entityManager->flush();
                $this->log->debug("///////////////commit eliminar ocupacion...............");
                $this->entityManager->commit();
                $this->log->debug("///////////////commit eliminar ocupacion...............");
            } catch (\Exception $e) {  //Ocurrio algun error al momento de realizar la eliminacion.
                $this->log->debug("///////////////rollback excepcion");
                $this->entityManager->rollback();
                $this->log->error(
                    "Ocurrio un error al intentar eliminar una ocupacion con el sig mensaje:" . $e->getMessage()
                );
                return $this->buildRespuesta(
                    "Ocurrio un Error al momento de eliminar esta actividad, por favor reportelo al ".
                    "personal de soporte",
                    array(),
                    false,
                    404
                );
            }

        } else { //No se puede eliminar la ocupación.
            $cadena = "";
            switch($puedeEliminar) {
                case 0:
                    $cadena = "Esta actividad no puede ser Eliminada ya que no cumple ".
                              "con las condiciones para eliminarse";
                    break;
                case -2:
                    $afluencia         = $this->getInscritosEInteresados($ocupacion);
                    $inscritos         = count($afluencia["inscritos"]);
                    $cadena = "Existen {$inscritos} socios inscritos en el Grupo: {$this->obtenerNombreOcupacion($ocupacion)} que ha solicitado Eliminar; no es posible eliminarlo con socios inscritos.";
            }
            return $this->buildRespuesta(
                $cadena,
                array(),
                false,
                404
            );
        }
        $this->log->debug("///////////////return...............");
        return $this->respuesta;
    }

    public function generarClaveOcupacion($clave, $ocupacion) {
        $numeroEconomico     = str_pad($ocupacion->getOcupacionId(), 5, "0", STR_PAD_LEFT);
        $claveCentro         = $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro");
        $centro              = $this->getCentroById($claveCentro);
        $nombre_corto_centro = $centro->getNombreCorto();
        $anio                = new \DateTime();
        $anio                = $anio->format('Y');

        return $clave.'-'.$nombre_corto_centro.'-'.$anio.'-'.$numeroEconomico;
    }

    public function obtenerNombreOcupacion($ocupacion) {
        if ($ocupacion->getActividad()) {
            return $ocupacion->getActividad()->getNombre();
        } else {
            return $ocupacion->getActividadAcademica()->getNombre();
        }
    }

    public function  modificarCargaMasiva($obj) {
        $BOcupacionReserva      = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BDiasFeriados          = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $BOcupacionHorario      = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);

        $fechaActual         = new \DateTime();
        $horario             = $obj['horario'];
        $this->horarioActual = $obj['horario'];
        $fechaFinal          = $obj['fecha_final'];
        $fechaInicio         = $obj['fecha_inicio'];
        $numeroInscritos     = $obj['numero_inscritos'];
        $numeroInteresados   = $obj['numero_interesados'];

        $usuario = $this->getUsuarioById($obj['usuario_id']);
        if (!$usuario) {
            return $this->respuesta;
        }

        $obj['ocupacion_estatus_id'] = $this->ocupacionEstatusId;

        $this->entityManager->beginTransaction();
        $this->log->info("empieza la transaccion...");
        try {
        $this->log->info("*********empiezo sincronizando con mako");
            $this->sincronizarAulasYFacilitadoresWithMako($obj['contenedor']);
        $this->log->info("*********termino sincronizando con mako");
        $this->log->info("*********voy por objetos");
            $vtObjetos                      = $this->obtenerObjetosParaOfertaEducativaCentro($obj['oferta_id'], $obj['actividad_academica_id'], $obj['actividad_id'], $obj['esquema_id'], $obj['facilitador_id'], $obj['aula_id'], $obj['ocupacion_estatus_id'],null, $obj['ocupacion_id']);
        $this->log->info("*********regreso con objetos");

            $ofertaEducativaCentro          = $vtObjetos["ofertaEducativaCentro"];
            $actividad                      = $vtObjetos["actividad"];
            $actividadAcademica             = $vtObjetos["actividadAcademica"];
            $facilitador                    = $vtObjetos['facilitador'];
            $esquema                        = $vtObjetos["esquema"];
            $ocupacionEstatus               = $vtObjetos['ocupacionEstatus'];
            $aula                           = $vtObjetos['aula'];
            $ocupacion                      = $vtObjetos['ocupacion'];

            $totalHoras                     = ($vtObjetos["actividadAcademica"]!=null)?$actividadAcademica->getDuracionHorasPresenciales():0;
            $totalHoras                     = ceil(($totalHoras/60));
            $diasFeriados                   = $BDiasFeriados->obtencionDiasFeriados();

            $parametroBusiness  = new BParametroActividad($this->entityManager);
            $numero_movimientos = $parametroBusiness->obtenerParametroPorActividad('numero_maximo_movimientos',$ocupacion->getActividadAcademica());

            $movimientos = $numero_movimientos[0]->getValor();

            if ($ocupacion->getNumeroMovimientos()>=$movimientos && $this->esCentro($obj['central'])) {
                $this->entityManager->rollback();
                return $this->buildRespuesta("Has realizado el # máximo de movimientos por Grupo, ya no tienes más movimientos autorizados",array(), false, 404);
            }
            $flag_notificacion_socios_inscritos = false;
            $fechaInicioTmp = new \DateTime($fechaInicio);
            if ($fechaInicioTmp!=$ocupacion->getFechaInicio()) {
                $respuestaValidacion = $this->validaRecorridoCurso($ocupacion,$fechaInicioTmp);
                if (!$respuestaValidacion['respuesta']) {
                    switch($respuestaValidacion['opcion']) {

                        case 3:
                            $this->entityManager->rollback();
                            $parametroBusiness   = new BParametro($this->entityManager,$this->entityManagerMako);
                            $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                            $this->buildRespuesta("No es posible cambiar la actividad a la fecha, dia y hora inicial y final porque debe ser {$numeroHorasMinimasRecorrido} hrs antes de que inicie la actividad", array(), false, 404);
                            return $this->respuesta;
                        break;
                    }
                }
                $flag_notificacion_socios_inscritos = true;
            }

            //salvamos el horario actual antes de ser eliminado
            $this->horarioAnterior=  $BOcupacionHorario->obtenerHorariosByOcupacion($ocupacion->getOcupacionId());
            $primerEventoAnterior    =  $BOcupacionReserva->obtenerPrimerEventoByOcupacion($ocupacion);
            //eliminar los horarios y eventos
            $BOcupacionHorario->eliminarHorario($ocupacion->getOcupacionId());
            $BOcupacionReserva->eliminarEventos($ocupacion->getOcupacionId());//eliminar eventos anteriores

            //generacion de fechas de eventos
            $fechas                         = $BEventoActividadOferta->generaFechasEventos($horario, $totalHoras, $fechaInicio, $diasFeriados, $fechaFinal);

            if (count($fechas["fechas"]) <=0) {
                $this->entityManager->rollback();
                $this->buildRespuesta('No se ha podido realizar la operación solicitada ya que no se encontro fecha para el horario seleccionado',array('respuesta' =>  false),false,404);
                return $this->respuesta;
            }


            $tmpocupacion                   = clone($ocupacion);
            $nuevaOcupacion=$this->modificacionOcupacion($ocupacion, $fechaActual, $fechas["fechaInicio"], $fechas["fechaFinal"], $usuario, $facilitador, $actividad, $actividadAcademica, $esquema, $ocupacionEstatus, $ofertaEducativaCentro, $aula, $numeroInscritos, $numeroInteresados);
            $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->entityManager,$this->entityManagerMako);
            if($this->esCentro($obj['central'])&&$ofertaEducativaCentroBusiness->ofertaEducativaPublicada($tmpocupacion->getOfertaEducativaCentro())){
                $nuevaOcupacion->setNumeroMovimientos((($nuevaOcupacion->getNumeroMovimientos()+1)));
                $this->entityManager->flush();
            }

            $horarios=$BOcupacionHorario->generaHorario($horario, $usuario, $nuevaOcupacion);                                            //Generamos el Horario
            $eventos=$BOcupacionReserva->generaEventos($aula, $facilitador, $nuevaOcupacion, $usuario, $fechas["fechas"]);            // Generamos los eventos con nosotros y reserva20w

            $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
            $fechaNow = new \DateTime();
            $horaNow = new \DateTime();
            $validacionOcupacion = $ocupacionReservaBusiness->validaEvento($tmpocupacion->getFechaInicio()->format('Y-m-d'),$tmpocupacion->getFechaInicio()->format('H:i:s'),$nuevaOcupacion->getFechaInicio()->format('Y-m-d'),$nuevaOcupacion->getFechaInicio()->format('H:i:s'));
            switch($validacionOcupacion['opcion']){
                case 1:
                    $this->entityManager->rollback();
                    $parametroBusiness   = new BParametro($this->entityManager,$this->entityManagerMako);
                    $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                    return $this->buildRespuesta("No es posible cambiar la actividad puesto que la fecha y hora inicial es menor a la fecha actual.", array('ocupacion'=>null), false, 404);
                break;

                case 2:
                    $this->entityManager->rollback();
                    $parametroBusiness   = new BParametro($this->entityManager,$this->entityManagerMako);
                    $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                    return $this->buildRespuesta("No es posible cambiar la actividad porque debe ser mas de {$numeroHorasMinimasRecorrido} hrs de diferencia con respecto a la hora actual para ser modificada.",array('ocupacion'=>null), false, 404);
                break;

                case 3:
                    $this->entityManager->rollback();
                    $parametroBusiness   = new BParametro($this->entityManager,$this->entityManagerMako);
                    $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
                    return $this->buildRespuesta("No es posible cambiar la actividad porque su fecha y hora inicial deben ser {$numeroHorasMinimasRecorrido} hrs antes de que inicie la actividad",array('ocupacion'=>null), false, 404);
                break;
            }
            $this->entityManager->flush();
            $primerEventoNuevo = $BOcupacionReserva->obtenerPrimerEventoByOcupacion($nuevaOcupacion);

            $generaPaquetesSyncBusiness = new BGeneraPaquetesSync($this->entityManager,$this->entityManagerMako);
            $cursoMakoBusiness = new BCursoMakoSync($this->entityManager,$this->entityManagerMako);
            $paquete = $cursoMakoBusiness->sincronizarCursoWithMako($usuario, $nuevaOcupacion, $fechas['totalHoras'],'update', $eventos, $tmpocupacion);

            if ($paquete['code']==404) {
                $this->entityManager->rollback();
                return $this->buildRespuesta('No se pudo sincronizar con mako 1.4 ',array('ocupacion'=>$ocupacion));
            }
            //salvamos el nuevo horario generado para la ocupacion
            $this->horarioActual = $BOcupacionHorario->obtenerHorariosByOcupacion($ocupacion->getOcupacionId());

             //generamos notificacion para ser sincronizada con central
            if ($ofertaEducativaCentro->getPublicado()) {
                //generamos notificacion para ser sincronizada con central
                $notificacionCentroACentralBusiness = new BNotificacionOcupacionCentroACentralSync($this->entityManager,$this->entityManagerMako);
                $respuestaSincroniacionCentral = $notificacionCentroACentralBusiness->sincronizarNotificacionModificacionWithCentral($tmpocupacion, $nuevaOcupacion, $this->modificacionAula, $this->modificacionFacilitador, $this->horarioAnterior, $this->horarioActual, $usuario);
                if ($respuestaSincroniacionCentral['code']==404) {
                    $this->entityManager->rollback();
                    return $this->buildRespuesta('No se pudo notificar con central ',array('ocupacion'=>$ocupacion));
                }
            }


            //registro de movimiento de ocupacion u Horario si estos son modificados
            $this->registrarMovimientoOcupacionSiEsModificado($nuevaOcupacion, $tmpocupacion, $usuario);
            $this->registrarMovimientoHorarioSiEsModificado($this->horarioAnterior, $this->horarioActual, $usuario, $nuevaOcupacion);

            if ($flag_notificacion_socios_inscritos) {
                //echo "entre...";
                $inscritos = $this->getInscritosEInteresados($ocupacion)['inscritos'];
                if (count($inscritos)!=0) {
                    //echo "entre...2";
                    $this->registrarMovimientoSociosInscritos($nuevaOcupacion,$inscritos,$usuario);
                    $paquetes = $this->enviarEmailsSociosInscritos($nuevaOcupacion,$inscritos);
                }
            }
            if($tmpocupacion->getFacilitador()!=$nuevaOcupacion->getFacilitador()){
                $this->enviarEmailFacilitador($nuevaOcupacion,$usuario);
            }
            $this->entityManager->commit();


            $this->buildRespuesta('put', array('ocupacion'=>$nuevaOcupacion));
        } catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->log->error($ex->getMessage());
            $this->buildRespuesta($ex->getMessage(),array('respuesta' =>  false),false,404);
            return $this->respuesta;
        }
        return $this->respuesta;

    }

    public function validaRecorridoCurso($ocupacion,$fechaInicioTmp) {
        $respuesta = array('respuesta'=>true,'opcion'=>0);
        if ($fechaInicioTmp->format('Y-m-d')!=$ocupacion->getFechaInicio()->format('Y-m-d')) {
            //echo "entre...";
            $fechaActual=new \DateTime();
            $fechaInicioTmp = new \DateTime($fechaInicioTmp->format('Y-m-d').' '.$ocupacion->getFechaInicio()->format('H:i'));
            $parametroBusiness   = new BParametro($this->entityManager,$this->entityManagerMako);
            $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
            $diferenciaFechas = $fechaInicioTmp->diff($fechaActual);
            $horasDiferencia = (intval($diferenciaFechas->format('%a'))*24)+intval($diferenciaFechas->format('%h'));
            //si no han superado el numero de horas validas
            if ($horasDiferencia<$numeroHorasMinimasRecorrido) {
                $respuesta['opcion']=3;
                $respuesta['respuesta']=false;
                return $respuesta;
            }
        }
        //si ya cuenta con socios inscritos
        $socios = $this->getInscritosEInteresados($ocupacion);
        if (count($socios['inscritos'])!=0) {
            $respuesta['opcion']=2;
            $respuesta['respuesta']=false;
        }
        //si el curso ha iniciado
        $fechaActual = new \DateTime();
        if ($ocupacion->getFechaInicio()<=$fechaActual) {
            $respuesta['opcion']=1;
            $respuesta['respuesta']=false;
        }
        return $respuesta;
    }

    public function validacionOcupacion($ocupacion,$fechaInicioTmp) {
        $respuesta = array('respuesta'=>true,'opcion'=>0);

            //echo "entre...";
            $fechaActual=new \DateTime();
            $fechaInicioTmp = new \DateTime($fechaInicioTmp->format('Y-m-d').' '.$ocupacion->getFechaInicio()->format('H:i'));
            $parametroBusiness   = new BParametro($this->entityManager,$this->entityManagerMako);
            $numeroHorasMinimasRecorrido = $parametroBusiness->obtenerParametroByTag('numero_horas_minimas_recorrido')[0]->getValor();
            $diferenciaFechas = $fechaInicioTmp->diff($fechaActual);
            $horasDiferencia = (intval($diferenciaFechas->format('%a'))*24)+intval($diferenciaFechas->format('%h'));
            if ($diferenciaFechas->format('%R')=='+') {
                $horasDiferencia=$horasDiferencia*-1;
            }
            //si no han superado el numero de horas validas
            if ($horasDiferencia<$numeroHorasMinimasRecorrido) {
                $respuesta['opcion']=3;
                $respuesta['respuesta']=false;
                return $respuesta;
            }
        return $respuesta;
    }





    public function registrarMovimientoOcupacionSiEsModificado($ocupacionNueva, $ocupacionAnterior, $usuario) {
        $stringer = new LEnovaTransformsToArray();
        $modificaciones=$stringer->obtenerModificacionesToArray($ocupacionAnterior, $ocupacionNueva);
        $movimiento=null;
        if (!empty($modificaciones)) {
            $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
            $movimiento = $movimientoBusiness->crearMovimiento('\ControlEscolar\CalendarioBundle\Entity\Ocupacion', $ocupacionNueva->getOcupacionId(), 'modificacion_ocupacion', json_encode($modificaciones), $usuario->getId());
        }
        return $movimiento;
    }

    public function registrarMovimientoHorarioSiEsModificado($horariosAnteriores, $horariosNuevos, $usuario, $ocupacion) {
        $ocupacionHorarioBusiness   = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);
        $movimiento=null;
        if (!$ocupacionHorarioBusiness->compareHorarioFromObjects($horariosAnteriores, $horariosNuevos)) {
            return null;
        }
        $cadena ="Horario de: ".$ocupacionHorarioBusiness->getHorarioString($horariosAnteriores)." a: ".$ocupacionHorarioBusiness->getHorarioString($horariosNuevos);
        $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
        $movimiento = $movimientoBusiness->crearMovimiento('\ControlEscolar\CalendarioBundle\Entity\Ocupacion', $ocupacion->getOcupacionId(), 'modificacion_horario', $cadena, $usuario->getId());
        return $movimiento;
    }

    public function esCentro($flag_central) {
        return !$flag_central;
    }

    public function modificacionOcupacion(
        $ocupacion,
        $fecha,
        $fechaInicio,
        $fechaFin,
        $usuario,
        $facilitador,
        $actividad,
        $actividadAcademica,
        $actividadAcademicaEsquema,
        $ocupacionEstatus,
        $ofertaEducativaCentro,
        $aula,
        $numeroInscritos,
        $numeroInteresados
    ) {
        try {
            $clave=null;
            if ($aula) {
                $ocupacion->setAula($aula);
                $this->modificacionAula=true;
            }
            if ($actividad) {
                $ocupacion->setActividad($actividad);
                $ocupacion->setActividadAcademica(null);
                $ocupacion->setActividadAcademicaEsquema(null);
                $clave = $actividad->getClave();
            } elseif ($actividadAcademica) {
                $ocupacion->setActividadAcademica($actividadAcademica);
                $ocupacion->setActividadAcademicaEsquema($actividadAcademicaEsquema);
                $ocupacion->setActividad(null);
                $clave = $actividadAcademica->getClave();
            }

            if ($fechaFin) {
                $ocupacion->setFechaFin(new \DateTime($fechaFin));
            }
            if ($fechaInicio) {
                $ocupacion->setFechaInicio(new \DateTime($fechaInicio));
            }
            if ($numeroInscritos) {
                $ocupacion->setNumeroInscritos($numeroInscritos);
            }
            if ($numeroInteresados) {
                $ocupacion->setNumeroInteresados($numeroInteresados);
            }
            if ($ocupacionEstatus) {
                $ocupacion->setOcupacionEstatus($ocupacionEstatus);
            }

            if ($ofertaEducativaCentro) {
                $ocupacion->setOfertaEducativaCentro($ofertaEducativaCentro);
            }
            if ($facilitador) {
                $ocupacion->setFacilitador($facilitador);
                $this->modificacionFacilitador = true;
            }
            if($clave){
                $ocupacion->setClaveGrupo($this->generarClaveOcupacion($clave, $ocupacion));
            }
            $ocupacion->setFechaModificacion($fecha);
            $ocupacion->setUsuarioModifica($usuario);

            $this->entityManager->flush();

        } catch (DBALException $e) {
            throw new DBALException("error al modificar la ocupacion...");
        }
        return $ocupacion;
    }


    public function getOcupacionById($ocupacion_id) {
        $ocupacion = $this->getRepository(
            'ControlEscolarCalendarioBundle:Ocupacion',
            array("ocupacion_id"=>$ocupacion_id),
            null,
            200,
            null,
            'find',
            array()
        );

        return $ocupacion;
    }

    /**
     * Validamos si una ocupacion puede o no ser eliminada.
     * @param  integer $ocupacionId Id de la ocupacion que se requiere evaluar
     * @return integer              0 => no es posible Cancelarla
     *                              1 => se puede eliminar y no se ha enviado notificación
     *                              2 => se puede eliminar y se envio  notificacion 1
     *                              3 => se puede eliminar y se envio notificacion 2
     *                             -1 => ocurrio un error.
     */
    public function validaCancelacion(
        $ocupacionId,
        &$primerAvisoCancelacionMovimientos = 0,
        &$segundoAvisoCancelacionMovimientos = 0
    ) {
        try {

            $ocupacion          = $this->getOcupacionById($ocupacionId);
            $fechaActual        = new \DateTime();
            $intervalo          = $fechaActual->diff($ocupacion->getFechaInicio());
            $tipo               = $intervalo->format("%R");
            $totalDias          = $intervalo->format("%a");
            $BOcupacionReserva = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
            $BMovimiento       = new BMovimiento($this->entityManager,$this->entityManagerMako);

            $afluencia          = $this->getInscritosEInteresados($ocupacion);
            $inscritos          = count($afluencia["inscritos"]);


            //basado en el requerimiento de Gaby que solicita: no se procesen ocupaciones para cancelacion si no tiene alumnos inscritos
            if($inscritos <=0){
                return 0;
            }
            //regla que dicta que solo se envien alertas para actividades Academicas
            if(is_object($ocupacion->getActividadAcademica()) == false ){
               return 0;
            }
            //La oferta ha sido publicada en el centro?
            if ($ocupacion->getOfertaEducativaCentro()->getPublicado() == false) {
                return 0;
            }

            //la ocupacion ya no esta activa y ya no la evaluaremos.
            if ($ocupacion->getActivo() == false){
                return 0;
            }
            //aun no inicia el curso.
            if ($tipo == "+") {
                return 0;
            }

            $parametroBusiness     = new BParametro($this->entityManager,$this->entityManagerMako);
            $tmpporcentaje         = $parametroBusiness->obtenerParametroByTag('porcentaje_inasistencia_cancelacion');
            $porcentajeCancelacion = $tmpporcentaje[0]->getValor();
            $eventos               = $BOcupacionReserva->getReservaByOcupacion($ocupacion, array('fecha_evento'=>'desc'));
            $totalEventos          = count($eventos);



            //Obtenemos los eventos que ya pasaron y que no tienen asistencia
            $eventosOcurridos       = $this->obtenerEventosTranscurridosSeguidos($eventos);
            $porcentajeEvento       = (100 / $totalEventos);
            $fechasEvento           = array();
            $fechaEvento            = "";



            // SE PUEDE CANCELAR
            if(  (($porcentajeEvento * count($eventosOcurridos['inasistencias'])) >= $porcentajeCancelacion)){

                    //Generamos hash que contiene como key la fecha de cada evento que ya paso y que no tiene asistencia
                    //Recorremos los eventos pasados que no tienen asistencia para generar el array por fechas :)
                    foreach ($eventosOcurridos['inasistencias'] as $evento) {
                        $fechaEvento = $evento->getFechaEvento()->format("Y-m-d");


                        if(!array_key_exists($fechaEvento, $fechasEvento)){
                            $fechasEvento[$fechaEvento] = 1;

                        }
                        else{
                            $fechasEvento[$fechaEvento]++;
                        }

                    }//FOREACH DE RECORRIDO DE EVENTOS CON INASISTENCIA.
                    //Fin de generar el hash con fechas de eventos pasados y sin asistencia

                //SI hoy NO es un dia en que se exista una sesion entonces no se podrá evaluar para alertar
                if(!array_key_exists($fechaActual->format("Y-m-d"), $fechasEvento)){
                    return 0;
                }

                $ultimoMovimiento = $this->getUltimoMovimientoOcupacion($ocupacion->getOcupacionId());
                if($ultimoMovimiento == false){
                    // NO cuenta con ningún Movimiento
                    return 1;
                }

                $fechaUltimoMovimiento = $ultimoMovimiento->getFechaAlta()->format("Y-m-d");

                //Evaluamos si el movimiento fue en las fechas de los eventos del 25%
                if(array_key_exists($fechaUltimoMovimiento, $fechasEvento)){
                    //El ultimo movimiento de cancelacion NO fue hoy?
                    if($fechaUltimoMovimiento != $fechaActual->format("Y-m-d")){
                        return $this->evaluaTipoAlertaCancelacionAEnviar($ultimoMovimiento);
                    }else{
                        //Se tienen mas de 1 sesión hoy?
                        if($fechasEvento[$fechaUltimoMovimiento] >1){

                            if($this->getNumeroMovimientosCancelacionHoyOcupacion($ocupacion->getOcupacionId()) < $fechasEvento[$fechaUltimoMovimiento]){
                                return $this->evaluaTipoAlertaCancelacionAEnviar($ultimoMovimiento);
                            }
                            else{
                                //Ya se enviaron todas las alertas de cancelacion (PARA EL DIA DE HOY) de esta ocupacion
                                return 0;
                            }

                        }
                        else{
                            // Solo se tiene una sesion para hoy y ya se envio una alerta el dia de hoy.
                            return 0;
                        }
                    }
                }
                else{
                    //NO corresponde a las fechas y entonces es alerta de primera vez.
                    return 1;
                }
            } // SE PUEDE ELIMINAR
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            return (-1);
        }
        return 0;
    }

    /**
     * Validamos si un curso puede o no eliminarse
     * @param  integer $ocupacionId el Id de la ocupacion que se quiere validar
     * @param  boolean $flag siempre va a false
     * @param  integer $primerAvisoEliminacionMovimientos numero de avisos de primera eliminacion (por referencia)
     * @param  integer $segundoAvisoEliminacionMovimientos numero de avisos de segunda eliminacion (por referencia)
     * @param  integer $automatico validacion por crones automaticos o no
     * @return [type]              0 => no se puede eliminar,
     *                             1 => se puede pero no se ha notificado,
     *                             2 => se puede y se notifico 1 ves,
     *                             3 => se puede y se notifico 2 veces.
     */
    public function validaEliminacion(
        $ocupacionId,
        $flag                                = false,
        &$primerAvisoEliminacionMovimientos  = 0,
        &$segundoAvisoEliminacionMovimientos = 0,
        $automatico = false
    ) {
        try {

            $parametroBusiness          = new BParametro($this->entityManager,$this->entityManagerMako);
            $minimo_inscritos           = $parametroBusiness->obtenerParametroByTag('minimo_inscritos')[0]->getValor();
            $tiempoAvisoUno             = $parametroBusiness->obtenerParametroByTag('tiempo_primer_eliminacion')[0]->getValor();
            $tiempoAvisoDos             = $parametroBusiness->obtenerParametroByTag('tiempo_segunda_eliminacion')[0]->getValor();
            $ocupacion                  = $this->getOcupacionById($ocupacionId);
            $fechaActual                = new \DateTime();
            $fechaCentral               = LEnovaDates::obtenerHoraCentral();
            $fechaOcupacion             = $ocupacion->getFechaInicio();
            $fechaFinOcupacion          = $ocupacion->getFechaFin();
            $diasANotificar             = $this->obtenerFechasAlertasDesdeFecha();
            $fechaOcupacionFormato      = $fechaOcupacion->format("Y-m-d");
            $ocupacionReservaBusiness   = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
            $horaCentral                = new \DateTime($fechaActual->format("Y-m-d") . "  " . $fechaCentral->format("H:i:s"));
            $horaOcupacion              = new \DateTime($fechaActual->format("Y-m-d") . "  " . $fechaOcupacion->format("H:i:s"));
            $horaTopeMaximo             = new \DateTime($fechaActual->format("Y-m-d") . " 18:00:00");
            $eventosOcupados            = $this->obtenerEventosTranscurridos($ocupacionReservaBusiness->getReservaByOcupacion($ocupacion));
            $eventosPasados             = count($eventosOcupados["pasados"]);

            $afluencia         = $this->getInscritosEInteresados($ocupacion);
            $inscritos         = count($afluencia["inscritos"]);

            //la ocupacion ya no esta activa y ya no la evaluaremos.

            if ($ocupacion->getActivo() == false){
                $this->log->debug("la ocupacion ya no esta activa y ya no la evaluaremos...");
                return 0;
            }

            //regla que dicta que solo se envien alertas para actividades Academicas
            if( $automatico== true && is_object($ocupacion->getActividadAcademica()) == false ){
                $this->log->debug("no es una actividad academica por lo cual no deben enviarse alertas");
               return 0;
            }


            // Validamos si la oferta no esta publicada.
            if ($ocupacion->getOfertaEducativaCentro()->getPublicado() == false) {
                $this->log->debug("la oferta educativa de la ocupacion no esta publicada....");
                if($inscritos > 0){
                    $this->log->debug("tiene alumnos inscritos...");
                    return -2;
                }
                $this->log->debug("no tiene alumnos inscritos");
                return 1;
            }

            //Si no es automatico, entonces:
            //Revisa que que haya sido creado en base a la oferta educativa
            // validando si es null la propiedad oferta_actividad_centro
            if($automatico == false && !is_null($ocupacion->getOfertaActividadCentro())){
                $this->log->debug("no es automatica y fue traida de la oferta educativa (tiene una OfertaActividadCentro asociada).");
                if($ocupacion->getOfertaActividadCentro()->getObligatoria() == true){
                    $this->log->debug("es una ocupacion obligatoria no se puede eliminar...");
                    return 0;
                }
            }


            //Automatico es siempre y cuando venga de una validacion por alertas automaticas
            //Validamos que la fecha de inicio de la ocupacion NO empieza dentro de 24 o  48 horas.o su primer sesion NO ha termino
            if( $automatico == true &&
                ($diasANotificar[$tiempoAvisoUno] != $fechaOcupacionFormato  &&
                $diasANotificar[$tiempoAvisoDos]  != $fechaOcupacionFormato  &&
                $eventosPasados <= 0)){
                $this->log->debug("la fecha de inicio de la ocupacion empieza en 24 o 48 horas y su primer sesion no ha terminado");
                return 0;
            }


            // Nueva validacion basada en fechas y ya no en itervalos como anteriormente estaba
            //Automatico es siempre y cuando venga de una validacion por alertas automaticas
            //Validamos si NO es la hora correcta para enviar la notificacion.
            if( $automatico == true &&
                ($fechaActual < $horaOcupacion  &&
                ( $horaCentral < $horaOcupacion  &&  $horaTopeMaximo > $horaCentral )  &&
                $eventosPasados <= 0)){
                $this->log->debug("es una alerta automatica por validacion de tope de horas y no intervalos...");
                return 0;
            }


            $BMovimiento = new BMovimiento($this->entityManager,$this->entityManagerMako);
            $primerAviso = count(
                $BMovimiento->obtenerMovimientos(
                    "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                    $ocupacionId,
                    "pre_eliminacion_1"
                )
            );
            $segundoAviso = count(
                $BMovimiento->obtenerMovimientos(
                    "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                    $ocupacionId,
                    "pre_eliminacion_2"
                )
            );
            $tercerAviso = count(
                $BMovimiento->obtenerMovimientos(
                    "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                    $ocupacionId,
                    "eliminacion"
                )
            );

            $primerAvisoEliminacionMovimientos  = $primerAviso;
            $segundoAvisoEliminacionMovimientos = $segundoAviso;


            //validamos si tiene inscritos y viene de la validacion del boton
            // en ese casi si tiene almenos 1 inscrito no podemos permitir eliminar.

            if($automatico == false && $inscritos >0){
                $this->log->debug("no es automatica y tiene inscritos no peude eliminarse valor 0");
                return 0;
            }

            //Tiene alumnos inscritos y supera o es igual al minimo aceptable
            if ($inscritos >= $minimo_inscritos) {
                $this->log->debug('tiene numero de inscritos y supera o es igual al minimo aceptable valor 0');
                return 0;
            }


            //EL Curso puede Eliminarse.
            if (($inscritos >= 0) and ($inscritos < $minimo_inscritos)) {
                $this->log->debug("el curso tiene menos del minimo de inscritos");
                if ($inscritos > 0 and $flag) {
                    $this->log->debug("el curso puede eliminarse valor -2");
                    return -2;
                }


                $intervaloAvisoUno  = $fechaOcupacion->diff(new \DateTime($diasANotificar[$tiempoAvisoUno] ));
                if ($primerAviso == 0  && intval($intervaloAvisoUno->format("%R%d")) >=0  ) {

                    return 1;
                }
                $intervaloAvisoDos  = $fechaOcupacion->diff(new \DateTime($diasANotificar[$tiempoAvisoDos]));
                if ($segundoAviso == 0  && intval($intervaloAvisoDos->format("%R%d")) >=0 ){

                    return 2;
                }

                if  ($segundoAviso >= 1 && $eventosPasados > 0  && $tercerAviso == 0) {
                    return 3;
                }
                if ($automatico == true) {
                    return 0;
                }

                return 1;
            }
        } catch (\Exception $e){
            error_log($e->getMessage());
            return (-1);
        }

        return 0;
    }

    /**
     * Buscamos los eventos que ya transcurrieron
     * @param  array $eventos Eventos de tipo OcupacionReserva
     * @return mixed Hash con estructura "inasistencias" => array(), "pasados"=>array() cada array contiene Objetos de tipo OcupacionReserva
     */
    public function obtenerEventosTranscurridos($eventos) {
        $fechaActual = new \DateTime();
        $result      = array(
            "inasistencias" => array(),
            "pasados"       => array()
        );
        $this->log->debug("======================= Eventos transcurridos para : " .  $fechaActual->format("Y-m-d H:i") . " ====================");

        foreach ($eventos as $evento) {
            $evento->getFechaEvento()->setTime($evento->getHoraFin()->format("H"),$evento->getHoraFin()->format("i"));
            $intervalo = $fechaActual->diff($evento->getFechaEvento());


            $this->log->debug("**** Fecha: " . $evento->getFechaEvento()->format("Y-m-d H:i") . " *** valor:" . $intervalo->format("%R"));
            $this->log->debug(print_r($intervalo, true));

            if (($intervalo->format("%R") == "-") or ($intervalo->format("%R") == "")) { //El evento ya paso.
                $result["pasados"][] = $evento;
                $this->getAsistenciaEvento($evento);

                if ($evento->getNumeroAsistentes()==0) {
                    $result["inasistencias"][]= $evento;
                }
            }
        }

        return ($result);
    }




    /**
     * Buscamos los eventos que ya transcurrieron y que son consecutivos, con el fin de evaluar las cancelaciones
     * @param  array $eventos Eventos de tipo OcupacionReserva
     * @return mixed Hash con estructura "inasistencias" => array(), "pasados"=>array() cada array contiene Objetos de tipo OcupacionReserva
     */
    public function obtenerEventosTranscurridosSeguidos($eventos) {
       $fechaActual      = new \DateTime();
       $inicioSecuencia  = false;
       $indiceActual     = -1;


       $result      = array(
           "inasistencias" => array(),
           "pasados"       => array()
       );
       $this->log->debug("======================= Eventos transcurridos para : " .  $fechaActual->format("Y-m-d H:i") . " ====================");

       foreach ($eventos as $indice => $evento) {
           $evento->getFechaEvento()->setTime($evento->getHoraFin()->format("H"),$evento->getHoraFin()->format("i"));
           $intervalo = $fechaActual->diff($evento->getFechaEvento());
           $this->log->debug("**** Fecha: " . $evento->getFechaEvento()->format("Y-m-d H:i") . " *** valor:" . $intervalo->format("%R"));
           if (($intervalo->format("%R") == "-") or ($intervalo->format("%R") == "")) {
              //El evento ya paso.
               if($inicioSecuencia === false){
                    //el evento ya paso entonces iniciamos la secuencia.
                   $inicioSecuencia = true;
               }
               $result["pasados"][] = $evento;
               $this->getAsistenciaEvento($evento);
               if ($evento->getNumeroAsistentes()==0) {
                    //Evento no tiene asistencia.
                   //Validamos si ya se inicio la secuencia continua.
                   if($indiceActual != ($indice -1) && $indiceActual!=-1){
                        break;
                   }
                   $result["inasistencias"][] = $evento;
               }
               elseif($inicioSecuencia == true){
                   break;
               }
               $indiceActual              = $indice;
           }
       }//Foreach
       return ($result);
   }



    /**
     * Obtenemos el numero de asistentes para un evento en especifico
     * @param  Object &$evento Objecto de tipo OcupacionReserva
     * @return [type]          [description]
     */
    public function getAsistenciaEvento(&$evento) {
        //Aqui consumimos el listado de asistencia de mako por curso. y evento.
        $resultado          = array();
        $destinoNombre      = 'Mako Local PV';
        $defaultAsistencia  = 10;
        $destinoBusiness    = new BDestino($this->entityManager);
        $destino            = $destinoBusiness->getDestinoByNombre($destinoNombre);
        $fecha              = $evento->getFechaEvento()->format("Y-m-d");
        $hora_inicio        = $evento->getHoraInicio()->format("H:i");
        $hora_final         = $evento->getHoraFin()->format("H:i");

        try {

        //Generamos la consulta que se conectara a Mako para obtener los valores requeridos.
            parent::loadInstanceEntityManagerMako();
            $query          = $this->entityManagerMako->getConnection()->prepare("
                SELECT
                        s.nombre_completo
                FROM grupo_alumnos ga
                JOIN socio s                    ON (s.id    = ga.alumno_id)
                JOIN grupo g                    ON (g.id    = ga.grupo_id  AND g.clave    = :clave_grupo)
                JOIN aula  a                    ON (a.id    = g.aula_id    AND a.centro_id = g.centro_id)
                JOIN horario hr                 ON (hr.id   = g.horario_id )

                INNER JOIN fechas_horario fh    ON (fh.horario_id = hr.id
                                                        AND fh.fecha::date  = :sesion_fecha
                                                        AND fh.hora_inicio  = :sesion_hora_inicio
                                                        AND fh.hora_fin     = :sesion_hora_final)
                INNER JOIN sesion_historico sh  ON ((fh.fecha + fh.hora_inicio + interval '10 mins', fh.fecha + fh.hora_fin - interval '10 mins')
                                                        OVERLAPS (sh.fecha_login,sh.fecha_logout) and sh.socio_id = s.id)
                LEFT JOIN computadora comp      ON (comp.centro_id  = g.centro_id       AND sh.ip = comp.ip)
                LEFT JOIN seccion sec           ON (sec.id          = comp.seccion_id   AND sec.centro_id = g.centro_id
                                                        AND sec.nombre = 'AULA ' || replace(trim(split_part(a.nombre,'AULA',2)), '0', ''))
                WHERE
                  ga.preinscrito=FALSE
                GROUP BY s.nombre_completo, fh.fecha, fh.hora_inicio, fh.hora_fin, s.usuario, g.id, s.id, ga.preinscrito
                ORDER BY nombre_completo, fh.fecha");


            $query->bindValue("clave_grupo"         ,$evento->getOcupacion()->getClaveGrupo());
            $query->bindValue("sesion_fecha"        ,$fecha);
            $query->bindValue("sesion_hora_inicio"  ,$hora_inicio);
            $query->bindValue("sesion_hora_final"   ,$hora_final);



            //Ejecutamos la consulta.
            $query->execute();
            $resultado=$query->fetchAll();


            //Asignamos el total de asistencia de ese evento dentro del objeto evento que recibimos por referencia.
            $evento->setNumeroAsistentes(count($resultado));
        } catch (\Exception $e) {
            $this->log->error(
                "No fue posible conectarse a mako 1.4 para consultar por los inscritos a un curso, ".
                "el error fue el siguiente: " . $e->getMessage()
            );
            $evento->setNumeroAsistentes($defaultAsistencia);
        }

        return $resultado;
    }

    /**
     * Devuelve el numero de inscritos e interesados a un curso
     * @param  Object $ocupacion Objeto de tipo Ocupacion
     * @return Array            Array con los hash inscritos, interesados
     */
    public function getInscritosEInteresados($ocupacion) {
        //Aqui hacemos conexion con Mako 1.4
        $result          = array("inscritos"=>array(), "interesados"=>array());
        $claveGrupo = $ocupacion->getClaveGrupo();
        $params          =array('clave_grupo'=>$claveGrupo);
        parent::loadInstanceEntityManagerMako();
        try {
            $query = $this->entityManagerMako->getConnection()->prepare("
                SELECT
                    s.nombre_completo,
                    s.email
                FROM
                    grupo_alumnos ga
                    INNER JOIN grupo g ON (g.id = ga.grupo_id AND g.clave = :clave_grupo)
                    INNER JOIN socio s ON (s.id = ga.alumno_id)
                WHERE
                    NOT ga.preinscrito ");
            $query->bindValue("clave_grupo",$claveGrupo);
            $query->execute();
            $resultado=$query->fetchAll();
            $result['inscritos']=$resultado;

            $query = $this->entityManagerMako->getConnection()->prepare("
                SELECT
                    s.nombre_completo,
                    s.email
                FROM
                    grupo_alumnos ga
                    INNER JOIN grupo g ON (g.id = ga.grupo_id AND g.clave = :clave_grupo)
                    INNER JOIN socio s ON (s.id = ga.alumno_id)
                WHERE
                    ga.preinscrito ");
            $query->bindValue("clave_grupo",$claveGrupo);
            $query->execute();
            $resultado=$query->fetchAll();
            $result['interesados']=$resultado;
        } catch (\Doctrine\DBAL\DBALException $e) {
            $error="No fue posible conectarse a mako 1.4 para consultar por los inscritos a un curso, el error fue el siguiente: " . $e->getMessage();
            $this->log->error(
                $error
            );
        }catch (\Exception $e) {
            $error="No fue posible conectarse a mako 1.4 para consultar por los inscritos a un curso, el error fue el siguiente: " . $e->getMessage();
            $this->log->error(
                $error
            );

        }
        return($result);
    }

    public function cancelarOcupaciones($params) {
        $ocupaciones      = array();
        $cancelaciones    = array();
        $notificaciones   = array();
        $NOnotificaciones = array();
        $fechaActual      = new \DateTime();
        $formatoFecha     = 'Y-m-d G:i:s';
        $usuario          = $this->getUsuarioById($params['usuario_id']);
        $genericREST      = new \Core\CoreBundle\Business\Entity\GenericRest($this->entityManager,$this->entityManagerMako);

        if (!$usuario) {
            return $this->respuesta;
        }

        $parametro_notificacion_inscritos = $this->getRepositoryBy(
            'ControlEscolarCalendarioBundle:Parametro',
            array('nombre' => 'minimo_inscritos'),
            null,
            200,
            null
        );

        if ($parametro_notificacion_inscritos) {
            $parametro_notificacion_inscritos = $parametro_notificacion_inscritos[0]->getValor();
        } else {
            return $this->respuesta;
        }

        $search = array(
            'filter' => 'ocupacion.fecha_inicio           < \''.$fechaActual->format($formatoFecha).      '\' AND '.
                        'ocupacion.fecha_inicio          >= \'2016-05-01\'                                    AND '.
                        'ocupacion.fecha_fin             >= \''.$fechaActual->format($formatoFecha).      '\' AND '.
                        'ocupacion.numero_inscritos       < '.intval($parametro_notificacion_inscritos).'     AND '.
                        'ocupacion.activo                 = true                                              AND '.
                        'ofertaeducativacentro.publicado  = true'
        );

        try {
            $ocupacionesObjs = $genericREST->listarGenerico(
                'ControlEscolarCalendarioBundle:Ocupacion',
                $search,
                array()
            );

            if (!isset($ocupacionesObjs['status']) or ($ocupacionesObjs['status'] !== true)) {
                $data = isset($ocupacionesObjs['data']['exception']) ? $ocupacionesObjs['data']['exception'] : array();
                $msg  = isset($ocupacionesObjs['data']['exception']) ?
                            $ocupacionesObjs['message'] :
                            'No se encontraron ocupaciones.';

                return $this->buildRespuesta(
                    $msg,
                    $data,
                    false,
                    200
                );
            } else {
                $ocupacionesObjs = $ocupacionesObjs['data']['controlescolarcalendariobundle:ocupacion'];
            }

            $this->log->info(
                'Se encontraron ('.
                count($ocupacionesObjs).
                ') ocupaciones para cancelar (o notificar). Con la busqueda: '.
                json_encode($search)
            );

            foreach ($ocupacionesObjs as $ocupacion) {
                $this->log->info('Procesando ocupacion: "'.$ocupacion.'"');

                /**
                 * Saltar de revisar (de cualquier manera, notificaciones o eliminacion) esta ocupacion.
                 * Ya que no esta publicada!
                 */
                if (!$ocupacion->getOfertaEducativaCentro()->getPublicado()) {
                    $this->log->info('No se puede procesar la ocupacion "'.$ocupacion.'" porque no esta publicada.');
                    continue;
                }

                $numeroDeMovimientosPrimeraNotificacion = 0;
                $numeroDeMovimientosSegundaNotificacion = 0;

                $tipo              = null;
                $validaCancelacion = $this->validaCancelacion(
                    $ocupacion->getOcupacionId(),
                    $numeroDeMovimientosPrimeraNotificacion,
                    $numeroDeMovimientosSegundaNotificacion
                );

                $this->log->debug(
                    'Ocupacion: "'.$ocupacion.'". '.
                    'validaCancelacion('.$validaCancelacion.') '.
                    'Movimientos1erNotif('.$numeroDeMovimientosPrimeraNotificacion.') '.
                    'Movimientos2daNotif('.$numeroDeMovimientosSegundaNotificacion.')'
                );

                switch ($validaCancelacion) {
                    case 0: // No cumple con las condiciones...
                        $ocupaciones[] = array(
                            'ocupacion'            => $ocupacion,
                            'resultadoCancelacion' => $this->buildRespuesta(
                                'No cumple con las condiciones para cancelarse.',
                                array(),
                                false,
                                200,
                                null,
                                'error'
                            )
                        );
                        break;

                    case 1: // Primer notificacion
                        // Si ya existen movimientos de primera notificacion, no continuar.
                        if ($numeroDeMovimientosPrimeraNotificacion != 0) {
                            $this->log->info(
                                'Ya se mando primera notificacion para la ocupacion: "'.$ocupacion.'". '.
                                'No se va a volver a mandar'
                            );
                            break;
                        }

                        $tipo = 'primer_notificacion';

                        //:FALLTHROUGH:

                    case 2: // Segunda notificacion
                        // Si ya existen movimientos de segunda notificacion, no continuar.
                        if ($numeroDeMovimientosSegundaNotificacion != 0) {
                            $this->log->info(
                                'Ya se mando segunda notificacion para la ocupacion: "'.$ocupacion.'". '.
                                'No se va a volver a mandar'
                            );
                            break;
                        }

                        if (is_null($tipo)) {
                            $tipo = 'segunda_notificacion';
                        }

                        $this->log->info('Enviando "'.$tipo.'" para la ocupacion: "'.$ocupacion.'"');

                        $notificacion = new BNotificacionOcupacionCentroACentralSync(
                            $this->entityManager
                        );

                        $resultadoNotif = $notificacion->sincronizarNotificacionAlertaInscritosCancelacionWithCentral(
                            $ocupacion,
                            $usuario,
                            $tipo
                        );

                        if (isset($resultadoNotif['status']) and $resultadoNotif['status']) {
                            $notificaciones[] = array(
                                'ocupacion'         => $ocupacion,
                                'resultadoNotif'    => $resultadoNotif,
                                'validaCancelacion' => $validaCancelacion
                            );
                        } else {
                            $NOnotificaciones[] = array(
                                'ocupacion'         => $ocupacion,
                                'resultadoNotif'    => $resultadoNotif,
                                'validaCancelacion' => $validaCancelacion
                            );
                        }
                        break;

                    case 3: // Se cancela
                        $this->log->info('Cancelando ocupacion: "'.$ocupacion.'"');
                        $ocupacionHorarioBusiness = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);
                        $ocupacionesHorario       = $ocupacionHorarioBusiness->obtenerHorariosByOcupacion(
                            $ocupacion->getOcupacionId()
                        );
                        $horario                  = $ocupacionHorarioBusiness->getHorarioString(
                            $ocupacionesHorario
                        );

                        $textoNotificacion = sprintf(
                            'Se ha detectado que en la actividad %s de '.
                            'fecha %s y horario %s, no se han registrado asistencias, '.
                            'ha sido cancelada automáticamente por el sistema.',
                            $ocupacion,
                            $ocupacion->getFechaInicio()->format('Y-m-d'),
                            $horario
                        );

                        $paramsCancelacion = array(
                            'usuario_id'        => $usuario->getId(),
                            'ocupacion_id'      => $ocupacion->getOcupacionId(),
                            'textoNotificacion' => $textoNotificacion,
                            'central'           => true
                        );

                        $cancelacion = $this->cancelacionOcupacion($paramsCancelacion);

                        if ($cancelacion['status'] === true) {
                            $cancelaciones[] = array(
                                'ocupacion'            => $ocupacion,
                                'resultadoCancelacion' => $cancelacion
                            );
                        } else {
                            $ocupaciones[] = array(
                                'ocupacion'            => $ocupacion,
                                'resultadoCancelacion' => $cancelacion
                            );
                        }
                        break;

                    default:
                        throw new \Exception(
                            'No se reconoce el valor devuelto por validaCancelacion(): "'.$validaCancelacion.'"'
                        );
                        break;
                }
            }

            return $this->buildRespuesta(
                null,
                array(
                    'parametros'       => array(
                        'fechaServidor' => $fechaActual->format($formatoFecha),
                        'busqueda'      => $search
                    ),
                    'NOnotificaciones' => $NOnotificaciones,
                    'notificaciones'   => $notificaciones,
                    'noCanceladas'     => $ocupaciones,
                    'canceladas'       => $cancelaciones
                )
            );

        } catch (\Exception $e) {
            throw new \Exception($e);
            return $this->buildRespuesta(
                'No se pudo cancelar la ocupacion.',
                array(),
                false,
                200,
                null,
                'error'
            );
        }

        return $this->respuesta;
    }

    public function cancelacionOcupacion($obj) {
        $ocupacion_id  = $obj['ocupacion_id'];
        $ocupacion     = $this->getOcupacionById($ocupacion_id);
        $usuario       = $this->getUsuarioById($obj['usuario_id']);

        $validacion    = $this->validaCancelacion($ocupacion_id);

        if ($validacion >= 1) {
            $this->entityManager->beginTransaction();

            try {
                //validamos el numero de movimientos por si no se han pasado estos del limite permitido
                $parametroBusiness  = new BParametroActividad($this->entityManager);
                $numero_movimientos = $parametroBusiness->obtenerParametroPorActividad('numero_maximo_movimientos',$ocupacion->getActividadAcademica());
                $movimientos        = $numero_movimientos[0]->getValor();

                if ($ocupacion->getNumeroMovimientos() >= $movimientos && $this->esCentro($obj['central'])) {
                    $this->entityManager->rollback();
                    return $this->buildRespuesta(
                        "Has realizado el # máximo de movimientos por Grupo, ya no tienes más movimientos autorizados",
                        array(),
                        false,
                        404
                    );
                }
                $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->entityManager,$this->entityManagerMako);
                if (isset($obj['central']) && $this->esCentro($obj['central']) && $ofertaEducativaCentroBusiness->ofertaEducativaPublicada($ocupacion->getOfertaEducativaCentro())) {
                    $ocupacion->setNumeroMovimientos($ocupacion->getNumeroMovimientos() + 1);
                    $this->entityManager->flush();
                }

                // Cancelamos localmente
                $eliminado = $this->getRepository(
                    "ControlEscolarCalendarioBundle:Ocupacion",
                    array('ocupacion_id' => $ocupacion_id),
                    null,
                    null,
                    null,
                    'findStoreProcedureCancelacion',
                    null
                );

                // Avisamos a Mako que cancele el curso
                $cursoMakoBusiness = new BCursoMakoSync($this->entityManager,$this->entityManagerMako);
                $paquete           = $cursoMakoBusiness->sincronizarCursoWithMako(
                    $usuario,
                    $ocupacion,
                    0,
                    'delete',
                    array()
                );

                if ($paquete['code'] == 404) {
                    $this->entityManager->rollback();
                    return $this->buildRespuesta(
                        'no se pudo sincronizar con mako 1.4 la cancelación',
                        array(),
                        false,
                        404
                    );
                }

                $stringer           = new LEnovaTransformsToArray($this->entityManager,$this->entityManagerMako);
                $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
                $modificaciones     = $stringer->extractorDatos($ocupacion);
                $movimiento         = $movimientoBusiness->crearMovimiento(
                    '\ControlEscolar\CalendarioBundle\Entity\Ocupacion',
                    $ocupacion->getOcupacionId(),
                    'cancelacion',
                    json_encode($modificaciones),
                    $obj['usuario_id']
                );

                // Notificamos a centros
                $notificacionCentroACentralBusiness = new BNotificacionOcupacionCentroACentralSync(
                    $this->entityManager
                );

                $paquete = $notificacionCentroACentralBusiness->sincronizarNotificacionCancelacionWithCentral(
                    $ocupacion,
                    $usuario,
                    $obj['textoNotificacion']
                );

                if ($paquete['code'] == 404) {
                    return $this->buildRespuesta(
                        'Ocurrio un error al tratar de cancelar la Actividad',
                        array(),
                        false,
                        404
                    );
                }

                $this->buildRespuesta(
                    "La actividad se ha cancelado correctamete",
                    array(),
                    true,
                    200
                );

                $this->entityManager->flush();
                $this->entityManager->commit();
            } catch (\Exception $e) {
                $this->entityManager->rollback();
                $this->log->error(
                    'Ocurrio un error al intentar de cancelar una ocupacion con el siguiente mensaje: "'.
                    $e->getMessage().'"'
                );

                return $this->buildRespuesta(
                    "Ocurrio un Error al momento de eliminar esta actividad, ".
                    "por favor reportelo al personal de soporte",
                    array(),
                    false,
                    404
                );
            }
        } else {
            $this->buildRespuesta(
                'No es posible cancelar el curso calendarizado',
                array(),
                false,
                404
            );
        }

        return $this->respuesta;
    }

    /**
     * Secciona una serie de eventos a partir de uan fecha en especifico
     * @param integer $ocupacionReservaId id del evento desde el cual se va a realizar el seccionado
     * @param DateTime $fechaInicioSeccion fecha de inicio desde la cual los eventos seccionados empezarán a crearse respetando su esquema
     * @param integer $usuarioId identificador del usuario que hace consumo del servicio rest
     * @param boolean $flag_central boolean que identifica si el consumo lo esta haciendo un centro o un central
     * @param integer $facilitadorId identificador  del facilitador el cual fue seccionado
     * @return array estructura de respuesta
     */
    public function seccionarOcupacion($ocupacionReservaId,$fechaInicioSeccion,$usuarioId,$flag_central,$facilitadorId) {

        $this->entityManager->beginTransaction();
        try{

            $ocupacionReservaBusiness      = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
            $eventoActividadOfertaBusiness = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);

            $BDiasFeriados                 = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
            $usuario                       = $this->getUsuarioById($usuarioId);
            $ocupacionReservaReferencia    = $ocupacionReservaBusiness->obtenerOcupacionReservaById($ocupacionReservaId);
            $ocupacionAnterior             = $ocupacionReservaReferencia->getOcupacion();
            $diasFeriados                  = $BDiasFeriados->obtencionDiasFeriados();
            $eventos                       = $this->obtenerEventosByIdOcupacionReservaFecha($ocupacionReservaId);
            $numeroHoras                   = $ocupacionReservaBusiness->numeroHorasByArrayEventos($eventos);
            $horario                       = $this->extractorHorarioByOcupacionReservaId($ocupacionReservaId);
            $arregloEventosNuevos          = $eventoActividadOfertaBusiness->generaFechasEventos($horario, $numeroHoras, $fechaInicioSeccion, $diasFeriados);
            $eventosAnteriores             = $ocupacionReservaBusiness->eliminarArregloEventos($eventos);//eliminar eventos anteriores

            //Valida que el facilitador nuevo traiga un un identificador
            // y obtiene el id del facilitador
            if($facilitadorId){
                $facilitadorBusiness = new BFacilitador($this->entityManager,$this->entityManagerMako);
                $facilitador = $facilitadorBusiness->getFacilitadorById($facilitadorId);
            }else{
                //En caso contrario, revisa la ocupación reserva actual y setea el mismo facilitador
                $facilitador = $ocupacionReservaReferencia->getOcupacion()->getFacilitador();
            }

            //validamos el numero de movimientos por si no se han pasado estos del limite permitido
            $parametroBusiness  = new BParametroActividad($this->entityManager);
            $numero_movimientos = $parametroBusiness->obtenerParametroPorActividad('numero_maximo_movimientos',$ocupacionReservaReferencia->getOcupacion()->getActividadAcademica());
            $movimientos = $numero_movimientos[0]->getValor();
            if($ocupacionAnterior->getNumeroMovimientos()>=$movimientos && $this->esCentro($flag_central)){
                throw new DBALException("Has realizado el # máximo de movimientos por Grupo, ya no tienes más movimientos autorizados");
            }
            //Se generan los nuevos eventos en base a las nuevas
            //fechas calculadas y datos de la ocupacion con nosotros
            //y reserva20
            $eventosNuevos = $ocupacionReservaBusiness->generaEventos(
                    $ocupacionReservaReferencia->getOcupacion()->getAula()
                    , $facilitador
                    , $ocupacionReservaReferencia->getOcupacion()
                    , $usuario, $arregloEventosNuevos["fechas"]
            );

            //se recalibra la fecha de inicio y fin de la ocupación por si esta haya sido afectada por el seccionado (dure mas la ocupacion o dure menos)
            $respuesta                = $this->calibrarFechasOcupacion($ocupacionAnterior, $usuario);

            $respuestaCalibracionFechas = $respuesta['respuesta'];

            //en una variable almacenamos la ocupacion traida anteriormente...
            $ocupacion = $respuesta['ocupacion'];
            $ofertaEducativaCentroBusiness = new BOfertaEducativaCentro($this->entityManager,$this->entityManagerMako);
            if($this->esCentro($flag_central) && $ofertaEducativaCentroBusiness->ofertaEducativaPublicada($ocupacion->getOfertaEducativaCentro())){
                $ocupacion->setNumeroMovimientos((($ocupacion->getNumeroMovimientos()+1)));
                $this->entityManager->flush();
            }

            //obtenemos el facilitador anterior para la notificacion
            $facilitadorAnterior = $ocupacion->getFacilitador();

            //Seteando a la ocupación
            // el ultimo facilitador seleccionado a la ocupación
            $ocupacion->setFacilitador($facilitador);
            $this->entityManager->flush();


            $this->registrarMovimientoSeccionadoOcupacion($ocupacion,$eventosAnteriores ,$eventosNuevos, $usuario);

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (DBALException $e) {
            $this->log->error("Ocurrio un error al intentar modificar un bloque de ocupacion con el error: " . $e->getMessage());
            $this->entityManager->rollback();
            return $this->buildRespuesta($e->getMessage(), array('ocupacion'=>null), false, 404);
        } catch (\Exception $e){
            $this->log->error("Ocurrio un error al intentar modificar un bloque de ocupacion con la Excepcion generica error: " . $e->getMessage());
            $this->entityManager->rollback();
            return $this->buildRespuesta($e->getMessage(), array('ocupacion'=>null), false, 404);
        }

        //transacciones para la notificacion de la segmentacion de la ocupacion y notificacion a socios inscritos
        $this->entityManager->beginTransaction();
        try{
            //generamos notificacion para ser sincronizada con central
            if($ocupacion->getOfertaEducativaCentro()->getPublicado()){
                $notificador = new BNotificacionOcupacionCentroACentralSync($this->entityManager,$this->entityManagerMako);
                $notificador->notificacionSegmentacionOcupacion($ocupacion,$facilitadorAnterior, $usuario,$fechaInicioSeccion, count($eventosNuevos));
            }


            if($respuestaCalibracionFechas){

                $cursoMakoBusiness = new BCursoMakoSync($this->entityManager,$this->entityManagerMako);
                $paquete = $cursoMakoBusiness->sincronizarCursoWithMako($usuario, $respuesta['ocupacion'], null,'update', null, $ocupacionAnterior);
                if ($paquete['code']==404) {
                    throw new DBALException('No se pudo sincronizar con mako 1.4');
                }

            }else{
                throw new DBALException("Ocurrio un error al intentar modificar un bloque de ocupacion con el error...");
            }

            //verificamos que existan usuarios inscritos para avisarles del cambio de la fecha del seccionado de la ocupación
            $inscritos = $this->getInscritosEInteresados($ocupacion)['inscritos'];
            if (count($inscritos)!=0) {
                $this->registrarMovimientoSociosInscritos($ocupacion,$inscritos,$usuario,'notificacion_evento_alumnos');
                $paquetes = $ocupacionReservaBusiness->enviarEmailsSociosInscritos($ocupacionReservaReferencia,$eventosNuevos[0],$inscritos);
            }

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (DBALException $e) {
            $this->log->error("Ocurrio un error al intentar notificar de la segmentacion o a los inscritos con el error: " . $e->getMessage());
            $this->entityManager->rollback();
            return $this->buildRespuesta($e->getMessage(), array('ocupacion'=>null), false, 404);
        } catch (\Exception $e){
            $this->log->error("Ocurrio un error al intentar notificar de la segmentacion o a los inscritos con la Excepcion generica error: " . $e->getMessage());
            $this->entityManager->rollback();
            return $this->buildRespuesta($e->getMessage(), array('ocupacion'=>null), false, 404);
        }catch (\Exception $e) {
            $this->log->error("Ocurrio un error al intentar modificar un bloque de ocupacion con el error: " . $e->getMessage());
            $this->entityManager->rollback();
            return $this->buildRespuesta($e->getMessage(), array('ocupacion'=>null), false, 404);
        }


        return $this->buildRespuesta('Se realizo la operación correctamente.', array('ocupacion'=>$respuesta['ocupacion']), true, 200);
    }

    public function calibrarFechasOcupacion($ocupacion,$usuario){
        try{
            $respuesta = array('ocupacion'=>$ocupacion,'respuesta'=>false);
            $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
            $respuestaEventos = $ocupacionReservaBusiness->obtenerPrimerYUltimoEventoByOcupacion($ocupacion);

            if(($respuestaEventos['primer_evento']->getFechaEvento()!=$ocupacion->getFechaInicio())||($respuestaEventos['ultimo_evento']->getFechaEvento()!=$ocupacion->getFechaFin())){

                $fechaInicioNueva = new \DateTime($respuestaEventos['primer_evento']->getFechaEvento()->format('Y-m-d').' '.$respuestaEventos['primer_evento']->getHoraInicio()->format('H:i:s'));
                $fechaFinNueva = new \DateTime($respuestaEventos['ultimo_evento']->getFechaEvento()->format('Y-m-d').' '.$respuestaEventos['ultimo_evento']->getHoraInicio()->format('H:i:s'));

                $nuevaOcupacion=$this->modificacionOcupacion($ocupacion, new \DateTime(),$fechaInicioNueva->format("Y-m-d H:i:s"),$fechaFinNueva->format("Y-m-d H:i:s"), $usuario, null, null, null, null, null, null, null, null, null);
                $this->entityManager->flush();
                $respuesta['ocupacion']=$ocupacion;
                $respuesta['respuesta']=true;
            }
        }catch(DBALException $ex){
            $this->log->error('Error al tratar de calibrar Fechas de la ocupación.'.$ex->getMessage());
            throw new DBALException('Error al tratar de calibrar Fechas de la ocupación.'.$ex->getMessage());
        }
        return $respuesta;
    }
    public function extractorHorarioByOcupacionReservaId($ocupacionReservaId){
        $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        $ocupacionReserva = $ocupacionReservaBusiness->obtenerEventoById($ocupacionReservaId);
        $ocupacionHorarioBusiness = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);
        $ocupacionHorario= $ocupacionHorarioBusiness->obtenerHorariosByOcupacion($ocupacionReserva->getOcupacion()->getOcupacionId());
        $arreglo = array();
        foreach($ocupacionHorario as $oh) {
            $horario= array();
            $horario['dia_semana']= $oh->getDiaSemana();
            $horario['hora_inicio']=$oh->getHoraInicio()->format('H:i');
            $horario['hora_fin']=$oh->getHoraFin()->format('H:i');
            $arreglo[]=$horario;
        }
        return $arreglo;
    }

    public function registrarMovimientoSeccionadoOcupacion($ocupacion,$eventosAnteriores ,$eventosNuevos, $usuario){
        $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        $modificaciones=$ocupacionReservaBusiness->obtenerArrayEventosAnterioresNuevos($eventosAnteriores, $eventosNuevos);
        $movimiento=null;
        if (!empty($modificaciones)) {
            $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
            $movimiento = $movimientoBusiness->crearMovimiento('\ControlEscolar\CalendarioBundle\Entity\Ocupacion', $ocupacion->getOcupacionId(), 'seccionado_ocupacion', json_encode($modificaciones), $usuario->getId());
        }
        return $movimiento;
    }

    public function obtenerEventosByIdOcupacionReservaFecha($ocupacionReservaId) {
        $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        $ocupacionReserva = $ocupacionReservaBusiness->obtenerEventoById($ocupacionReservaId);
        if (!$ocupacionReserva) {
            throw new DBALException("No se encontro la OcupacionReserva con Id {$ocupacionReservaId}");
        }
        $eventos = $this->getRepository(
            "ControlEscolarCalendarioBundle:OcupacionReserva",
            array('ocupacion_id' => $ocupacionReserva->getOcupacion()->getOcupacionId(),'fecha'=>$ocupacionReserva->getFechaEvento()->format('Y-m-d')),
            null,
            null,
            null,
            'findEventosFromFecha',
            null
        );

        return $eventos;
    }

    public function obtenerListadoInscritosEInteresados($obj) {
        $ocupacion = $this->getOcupacionById($obj['ocupacion_id']);
        if (!$ocupacion) {
            return $this->buildRespuesta("No se pudo encontrar la Ocupación con id: {$obj['ocupacion_id']}", array('respuesta'=>array()), false, 404);
        }
        $resultado = $this->getInscritosEInteresados($ocupacion);
        return $this->buildRespuesta('Se conecto correctamente con mako 1.4', array('respuesta'=>$resultado), true, 200);
    }

    public function validarEliminacionOcupacion($obj) {
        $ocupacion = $this->getOcupacionById($obj['ocupacion_id']);
        if (!$ocupacion) {
            return $this->buildRespuesta("No se pudo encontrar la Ocupación con id: {$obj['ocupacion_id']}", array('respuesta'=>array()), false, 404);
        }
        $respuesta = $this->validaEliminacion($obj['ocupacion_id']);
        $textoRespuesta = '';
        $codigo=0;
        $status = false;
        switch($respuesta) {
            case -1:
                $codigo = 404;
                $status = false;
                $textoRespuesta = "Existe un error en la base de datos.";
            break;
            case 0:
                $codigo = 404;
                $status = true;
                $textoRespuesta = "El curso no puede eliminarse";
            break;

            case 1:
            case 2:
            case 3:
                $codigo = 200;
                $status = true;
                $textoRespuesta = "El curso puede eliminarse";
            break;
        }
            return $this->buildRespuesta($textoRespuesta, array('respuesta'=>$respuesta),$status, $codigo);
    }

    public function validarCancelacionOcupacion($obj) {
        $ocupacion = $this->getOcupacionById($obj['ocupacion_id']);
        if (!$ocupacion) {
            return $this->buildRespuesta("No se pudo encontrar la Ocupación con id: {$obj['ocupacion_id']}", array('respuesta'=>array()), false, 404);
        }
        $respuesta = $this->validaCancelacion($obj['ocupacion_id']);
        $textoRespuesta = '';
        $codigo=0;
        $status = false;
        switch($respuesta) {
            case -1:
                $codigo = 200;
                $status = false;
                $textoRespuesta = "Existe un error en la base de datos.";
            break;
            case 0:
                $codigo = 200;
                $status = true;
                $textoRespuesta = "El curso no puede cancelarse";
            break;

            case 1:
            case 2:
            case 3:
                $codigo = 200;
                $status = true;
                $textoRespuesta = "El curso puede cancelarse";
            break;
        }
            return $this->buildRespuesta($textoRespuesta, array('respuesta'=>$respuesta),$status, $codigo);
    }


    public function enviarEmailsSociosInscritos($ocupacion,$inscritos) {
        $ocupacionReservaBusiness = new BOcupacionReserva($this->entityManager,$this->entityManagerMako);
        //Obtenemos el primer y ultimo evento de la ocupación
        $eventos = $ocupacionReservaBusiness->obtenerPrimerYUltimoEventoByOcupacion($ocupacion);
        $ocupacionHorarioBusiness = new BOcupacionHorario($this->entityManager,$this->entityManagerMako);
        //obtenemos los horarios que posee la ocupación
        $horarios = $ocupacionHorarioBusiness->obtenerHorariosByOcupacion($ocupacion->getOcupacionId());

        $diaSemanaTmp = array(0=>'Domingo',1=>'Lunes',2=>'Martes',3=>'Miércoles',4=>'Jueves',5=>'Viernes',6=>'Sábado');
        $cadenaHorario = "";
        foreach($horarios as $horario){
            foreach($diaSemanaTmp as $clave=>$valor){
                if($clave==$horario->getDiaSemana()){
                    $cadenaHorario=$cadenaHorario."<b>".$valor."</b> : ".$horario->getHoraInicio()->format('H:i')." - ".$horario->getHoraFin()->format('H:i')."<br>";
                    break;
                }
            }

        }
        if ($ocupacion->getActividad()) {
            $nombreCurso = $ocupacion->getActividad()->getNombre();
        } else {
            $nombreCurso = $ocupacion->getActividadAcademica()->getNombre();
        }


        $fechaInicio    = $eventos['primer_evento']->getFechaEvento()->format('d-m-Y');
        $fechaTermino   = $eventos['ultimo_evento']->getFechaEvento()->format('d-m-Y');

        $cadenaTmp="";
        $cadena = "Te informamos que el curso al que estas inscrito {$nombreCurso} ha cambiado de fecha o de hora.<br>Ahora lo realizarás el día {$fechaInicio} a {$fechaTermino} con el siguiente horario:<br><br>{$cadenaHorario}";

        $cadena = $cadena."<br>Gracias por tu preferencia, ¡Te esperamos!";

        $this->log->info("------------------------MENSAJE para el socio inscrito: {$cadena}");
        $arregloEmails = array();
        $sendMailLib = new LSendMail();
        foreach($inscritos as $inscrito) {
            if ($inscrito['email']!=null) {
                $cadenaTmp="Anota la nueva fecha/hora de tu curso.<br><br>Estimado socio: {$inscrito['nombre_completo']}. ".$cadena;
                try{
                    $sendMailLib->send($inscrito['email'],'Cambio fecha de curso', $cadenaTmp);
                } catch (\Exception $ex) {
                    $this->log->error("MENSAJE de error-------->".$ex->getMessage());
                }
            }
        }
    }

    public function registrarMovimientoSociosInscritos($nuevaOcupacion,$inscritos,$usuario,$tipo_movimiento='notificacion_alumnos') {
        if ($nuevaOcupacion->getActividad()) {
            $nombreCurso = $nuevaOcupacion->getActividad()->getNombre();
        } else {
            $nombreCurso = $nuevaOcupacion->getActividadAcademica()->getNombre();
        }
        $cadenaAlumnos = $this->getCadenaListadoAlumnosInscritos($inscritos);
        $cadena = "La ocupación {$nuevaOcupacion->getClaveGrupo()} con la actividad {$nombreCurso} ha cambiado la fecha de inicio a {$nuevaOcupacion->getFechaInicio()->format('d-m-Y')} y se han enviado notificaciones a los alumnos ".$cadenaAlumnos;
        $movimientoBusiness = new BMovimiento($this->entityManager,$this->entityManagerMako);
        $movimiento = $movimientoBusiness->crearMovimiento('\ControlEscolar\CalendarioBundle\Entity\Ocupacion', $nuevaOcupacion->getOcupacionId(), $tipo_movimiento, $cadena, $usuario->getId());
        return $movimiento;
    }


    public function getCadenaListadoAlumnosInscritos($inscritos) {
        $cadena = "";
        foreach ($inscritos as $alumno) {
            if ($alumno["email"]!=null) {
                $cadena.=($cadena=="")?"":",";
                $cadena.=sprintf("%s-%s", $alumno["nombre_completo"], $alumno["email"]);
            }
        }
        return $cadena;
    }


    /**
     * @@ LGL
     *
     * Genera la llamada al modelo para obtener el reporte de grupos calendarizados
     *
     * @return array estructura de respuesta
     */

    public function reporteGrupos($parametros) {

        //nombre centro
        $claveCentro         = $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro");
        $centro              = $this->getCentroById($claveCentro);
        if(!$centro){
            return $this->buildRespuesta(
                            'No existe el centro actual',
                            array('reporte'=>array()),
                            false,
                            404
                    );
        }
        $parametros['nombre_centro'] = $centro->getNombre();
        $reporteGrupos = $this->getRepository(
            "ControlEscolarCalendarioBundle:Ocupacion",
            $parametros,
            null,
            null,
            null,
            'findReporteGrupos',
            null
        );
        return $this->buildRespuesta('Se ha ejecutado la consulta exitosamente.', array('reporte'=>$reporteGrupos),true, 200);
    }

    /**
     * @@ LGL
     *
     * Genera la llamada al modelo para obtener el reporte de grupos calendarizados
     *
     * @return array estructura de respuesta
     */

    public function reporteGruposByFacilitadores($parametros) {
         $reporteGrupos = $this->getRepository(
            "ControlEscolarCalendarioBundle:Ocupacion",
            $parametros,
            null,
            null,
            null,
            'findReporteGruposByFacilitadores',
            null
        );
        return $this->buildRespuesta('Se ha ejecutado la consulta exitosamente.', array('reporte'=>$reporteGrupos),true, 200);
    }


    /**
     * @@ LGL
     *
     * Genera la llamada al modelo para obtener el reporte de grupos calendarizados
     *
     * @return array estructura de respuesta
     */

    public function reporteGruposByFacilitador($facilitador_id, $tipo_grupo,$obj) {

        $claveCentro         = $GLOBALS['kernel']->getContainer()->getParameter("identificador_centro");
        $centro              = $this->getCentroById($claveCentro);
        $nombre_corto_centro = $centro->getNombreCorto();

        $parametros = array_merge(array('facilitador_id' => $facilitador_id,'tipo_grupo' => $tipo_grupo, 'nombre_centro' => $nombre_corto_centro),$obj);

         $reporteGrupos = $this->getRepository(
            "ControlEscolarCalendarioBundle:Ocupacion",
            $parametros,
            null,
            null,
            null,
            'findReporteGruposByFacilitador',
            null
        );
        return $this->buildRespuesta('Se ha ejecutado la consulta exitosamente.', array('reporte'=>$reporteGrupos),true, 200);
    }

    /**
     * Retornamos un array de  las fechas que enviaremos alertas de 24 y 48 horas a partir de un dia determinado. p.e: hoy enviaremos
     * las alertas de mañana y pasado mañana.
     *
     * @param  DateTime $fechaActual
     * @return Array    con formato 24=>2015-10-01, 48=>2015-10-02. el key es 24 y 48 y el valor es fecha en formato Y-m-d
     */
    protected function obtenerFechasAlertasDesdeFecha( $fechaActual = null ){

        // Inicializamos variables que vamos a ocupar
        if( $fechaActual == null ){
            $fechaActual = new \DateTime();
        }
        $contador                       = 0;
        $ap                             = 0;
        $parametroBusiness              = new BParametro($this->entityManager,$this->entityManagerMako);
        $horasPrimeraNotificacion       = $parametroBusiness->obtenerParametroByTag('tiempo_primer_eliminacion')[0]->getValor();
        $horasSegundaNotificacion       = $parametroBusiness->obtenerParametroByTag('tiempo_segunda_eliminacion')[0]->getValor();
        $notificacionesArreglo          = array(0=>$horasSegundaNotificacion,1=>$horasPrimeraNotificacion);
        $fechaFormato                   = $fechaActual->format("Y-m-d");
        $resultado                      = array($horasPrimeraNotificacion=>$fechaFormato, $horasSegundaNotificacion=> $fechaFormato);
        $diaSemana                      = 0;
        $BDiaFeriado                    = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $diasFeriados                   = $BDiaFeriado->obtencionDiasFeriados();  //Obtenemos los dias feriados


        //Recorreremos los dias hasta que tengamos nuestro contador en 48 que representa las horas maximas que podemos  enviar una alerta.
        while ($contador < $horasPrimeraNotificacion){

            //Aumentamos un nuevo dia para saber si ese dia  es festivo o es fin de semana :)
            $fechaActual->add(new \DateInterval("P1D"));
            $diaSemana      = $fechaActual->format("N");
            $fechaFormato   = $fechaActual->format("Y-m-d");

            if(!array_key_exists($fechaFormato, $diasFeriados) && $diaSemana != 6 && $diaSemana != 7 ){

                //Es un dia habil  y  se deben enviar notificaciones de los  eventos de ese dia hoy.
                $contador               = $notificacionesArreglo[$ap];
                $resultado[$contador]   = $fechaFormato;
                $ap++;

            }

        }
        //Retornamos el array de los dias que corresponde que hoy enviemos una notificacion.
        return $resultado;

    }



    /**
     * Validamos si un GRUPO puede o No Eliminarse
     * @param  object  $ocupacion Objeto de tipo Ocupacion
     * @return [type]
     *                             0 => no se puede eliminar,
     *                             1 => se puede pero no se ha notificado,
     *                             2 => se puede y se notifico 1 ves,
     *                             3 => se puede y se notifico 2 veces.
     *                             4 => no se puede pero hay que notificar que no cubre con el minimo de inscritos.
     *                            -1 => Oocurrio un error al realizar la operacion
     */
    public function validaEliminacionAutomatica($ocupacion) {

        try {

            $parametroBusiness          = new BParametro($this->entityManager);




            $minimo_inscritos           = $parametroBusiness->obtenerParametroByTag('minimo_inscritos')[0]->getValor();
            $tiempoAvisoUno             = $parametroBusiness->obtenerParametroByTag('tiempo_primer_eliminacion')[0]->getValor();
            $tiempoAvisoDos             = $parametroBusiness->obtenerParametroByTag('tiempo_segunda_eliminacion')[0]->getValor();
            $ocupacionId                = $ocupacion->getOcupacionId();
            $fechaActual                = new \DateTime();
            $fechaCentral               = LEnovaDates::obtenerHoraCentral();
            $fechaOcupacion             = $ocupacion->getFechaInicio();
            $fechaFinOcupacion          = $ocupacion->getFechaFin();
            $diasANotificar             = $this->obtenerFechasAlertasDesdeFecha();
            $fechaOcupacionFormato      = $fechaOcupacion->format("Y-m-d");
            $ocupacionReservaBusiness   = new BOcupacionReserva($this->entityManager);
            $horaCentral                = new \DateTime($fechaActual->format("Y-m-d") . "  " . $fechaCentral->format("H:i:s"));
            $horaOcupacion              = new \DateTime($fechaActual->format("Y-m-d") . "  " . $fechaOcupacion->format("H:i:s"));
            $horaTopeMaximo             = new \DateTime($fechaActual->format("Y-m-d") . " 18:00:00");
            $eventosOcupados            = $this->obtenerEventosTranscurridos($ocupacionReservaBusiness->getReservaByOcupacion($ocupacion));
            $eventosPasados             = count($eventosOcupados["pasados"]);

            $afluencia                  = $this->getInscritosEInteresados($ocupacion);
            $inscritos                  = count($afluencia["inscritos"]);



            //// *********************** VALIDACIONES BASICAS PARA DETERMINAR SI EVALUAMOS A FONDO LA OCUPACION ************************* ///

            $this->log->debug("******* VALIDACION AUTOMATICA DE OCUPACION PARA GRUPO:" . $ocupacion . " *******");

            //LA OFERTA EN CENTRO ESTA PUBLICADA?
            if ($ocupacion->getOfertaEducativaCentro()->getPublicado() == false) {
                $this->log->debug("   La Oferta no esta publicada");
                return  Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
            }

            //LA OCUPACION ESTA ACTIVA?
            if ($ocupacion->getActivo() == false){
                $this->log->debug("   Ocupacion inactiva");
                return  Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
            }

            //LA OCUPACION ES UNA ACTIVIDAD ACADEMICA?
            if(is_object($ocupacion->getActividadAcademica()) == false ){
               $this->log->debug("   Ocupacion no es una actividad academica");
               return  Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
            }


            //TIENE ALUMNOS INSCRITOS Y SUPERA EL MINIMO DE INSCRITOS ESTABLECIDO?
            if ($inscritos >= $minimo_inscritos) {
                $this->log->debug("   Cubre con el minimo de inscritos");
                return  Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
            }


            // ********* ES MOMENTO DE EVALUAR LA OCUPACION? **************
            // LA FECHA DE INICIO DE LA OCUPACION EMPIEZA DENTRO DE 24 HRS?
            // LA FECHA DE INICIO DE LA OCUPACION EMPIEZA DENTRO DE 48 HRS?
            // NO HA PASADO POR LO MENOS 1 SESION DE ESTA OCUPACION?
            if( ($diasANotificar[$tiempoAvisoUno] != $fechaOcupacionFormato  &&
                $diasANotificar[$tiempoAvisoDos]  != $fechaOcupacionFormato  &&
                $eventosPasados <= 0)){

                $this->log->debug("   NO es la fecha adecuada para notificarla: primer alerta es el dia: " . $diasANotificar[$tiempoAvisoUno] . ", la segunda: " . $diasANotificar[$tiempoAvisoDos] . " y la fecha de la ocupacion es: {$fechaOcupacionFormato}");

                return  Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
            }


            // ES LA HORA INDICADA PARA EVALUAR SI ESTE GRUPO PUEDE O NO SER ELIMINADO?
            if( ( $fechaActual < $horaOcupacion  &&
                ( $horaCentral < $horaOcupacion  &&  $horaTopeMaximo > $horaCentral )  &&
                $eventosPasados <= 0)){
                $this->log->debug("   NO es la hora indicada, hora actual:{$fechaActual->format('H:i')}, hora de ocupacion: {$horaOcupacion->format('H:i')}, hora central{$horaCentral->format('H:i')}, hora maxima para notificar: {$horaTopeMaximo->format('H:i')}");
                return  Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
            }




            //REVISAMOS LOS MOVIMINETOS CON LOS QUE CUENTA ESTE GRUPO.
            $BMovimiento = new BMovimiento($this->entityManager);
            $primerAviso = count(
                $BMovimiento->obtenerMovimientos(
                    "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                    $ocupacionId,
                    "pre_eliminacion_1"
                )
            );
            $segundoAviso = count(
                $BMovimiento->obtenerMovimientos(
                    "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                    $ocupacionId,
                    "pre_eliminacion_2"
                )
            );
            $tercerAviso = count(
                $BMovimiento->obtenerMovimientos(
                    "\ControlEscolar\CalendarioBundle\Entity\Ocupacion",
                    $ocupacionId,
                    "eliminacion"
                )
            );



            // **************** VALIDAMOS EL GRUPO BASADO EN LOS INCRITOS **************** //

            if ($inscritos < $minimo_inscritos) {

                // EL GRUPO SE PUEDE ELIMINAR O ALERTAR PARA ELIMINAR.

                $this->log->debug("   **SE PUEDE ELIMINAR");


                //VALIDAMOS SI NO SE HA ENVIADO UNA ALERTA DE PRIMER AVISO DE ELIMINACION
                if ($primerAviso == 0   && (new \DateTime($fechaOcupacionFormato))  <= (new \DateTime($diasANotificar[$tiempoAvisoUno]))) {
                    $this->log->debug("   ----PRIMER ALERTA");
                    return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_PRIMERA;

                }


                //VALIDAMOS SI NO SE HA ENVIADO UNA ALERTA DE SEGUNDO AVISO DE ELIMINACION
                if ($primerAviso >= 1  && $segundoAviso == 0  &&  (new \DateTime($fechaOcupacionFormato)) <= (new \DateTime($diasANotificar[$tiempoAvisoDos])) ) {
                    $this->log->debug("   ----SEGUNDA ALERTA");
                    return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_SEGUNDA;
                }


                //VALIDAMOS SI NO SE HA ENVIADO UNA ALERTA DE TERCER AVISO DE ELIMINACION
                if  ( $primerAviso >= 1 && $segundoAviso >= 1   && $tercerAviso == 0  &&  $eventosPasados > 0  ) {



                    //VALIDAMOS SI EL GRUPO TIENE INSCRITOS
                    if($inscritos <= 0){    //NO TIENE INSCRITOS
                        $this->log->debug("   ----TERCER ALERTA Y NO TIENE INSCRITOS");
                        return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_TERCERA;
                    }
                    else{                   //TIENE INSCRITOS
                        $this->log->debug("   ----TERCER ALERTA Y SI TIENE INSCRITOS");
                        return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_TERCERA_CON_INSCRITOS;
                    }

                }
                // SI NO PASO POR NINGUNA VALIDACION ANTERIOR NO PODRAN ELIMINARLA.
                return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;

            }//FIN DE QUE SI SE PUEDE ELIMINAR

            $this->log->debug("*******FIN DE LA VALIDACION ******");


        } catch (\Exception $e){
            // SI OCURRE ALGUN ERROR GUARDAMOS EN LOG Y DEVOLVEMOS 0 PARA QUE NO PUEDA ELIMINARSE ESTA OCUPACION.
            $this->log->error($e->getMessage());
            return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_ERROR;
        }

        return Ocupacion::ALERTA_AUTOMATICA_ELIMINACION_RECHAZADA;
    }




    /**
     * Obtenemos el ultimo movimiento registrado en la tabla movimientos para una ocupacion especifica
     * @param  integer  Id de ocupacion
     * @return [false|Object de tipo:\ControlEscolar\CalendarioBundle\Entity\Ocupacion]
     * @throws  \Exception Si ocurre un error de programacion.
     * @throws  \DBALException Si ocurre un error en base de datos.
     */
    private function getUltimoMovimientoOcupacion($ocupacionId){

       $genericRestBusiness = new BGenericRest($this->entityManager);


        try{
            $resultado =$genericRestBusiness->listarGenerico(
                   'Core\CoreBundle\Entity\Tabla',
                   array('limit'=>1, 'filter'=>"tabla.nombre='\\ControlEscolar\\CalendarioBundle\\Entity\\Ocupacion'"),
                   array('incluir_activo'=>true)
           );

            if(!is_array($resultado["data"]["tabla"])){
                throw new \Exception("No encontramos el tipo de tabla adecuada para realizar la busqueda de movimientos");
            }

            $tabla = $resultado["data"]["tabla"][0];

           $resultado =$genericRestBusiness->listarGenerico(
                   'ControlEscolar\CalendarioBundle\Entity\Movimiento',
                   array('limit'=>1, 'filter'=>"movimiento.referencia=" . $ocupacionId . " and tabla.tabla_id=" . $tabla->getTablaId() . " and (tipomovimiento.clave='pre_cancelacion_1' or tipomovimiento.clave='pre_cancelacion_2' or tipomovimiento.clave='cancelacion')", "order"=>"fecha_alta|desc"),
                   array('incluir_activo'=>true)
           );

            if(!is_array($resultado["data"]["movimiento"])){
                return false;
            }

            return $resultado["data"]["movimiento"][0];

        } catch(\Exception $e){
          throw new \Exception($e->getMessage());
        } catch(\DBALException $e){
          throw new \Exception($e->getMessage());
        }
    }


    /**
     * Determinamos que tipo de alerta de cancelacion se debe de enviar apartir del ultimo movimineto registrado
     * @param  Object  Movimiento
     * @return int que puede ser:
     *           0 No se debe enviar alerta
     *           1 Se debe enviar alerta de primera
     *           2 Alerta de segunda
     *           3 Alerta de tercera.
     */
    private function evaluaTipoAlertaCancelacionAEnviar($movimiento){

        switch($movimiento->getTipoMovimiento()->getClave()){
            case "pre_cancelacion_1":
                return 2;
                break;
            case "pre_cancelacion_2":
                return 3;
                break;
            case "cancelacion":
                return 0;
                break;
        }
        return 1;

    }

    /**
     * Obtenemos el total de movimientos de cancelacion registrados de una ocupacion
     * @param  ocupacionId  Id de ocupacion
     * @return int Numero de movimientos de cancelacion encontrados.
     * @throws  \Exception Si ocurre un error de programacion.
     * @throws  \DBALException Si ocurre un error en base de datos.
     */
    private function getNumeroMovimientosCancelacionHoyOcupacion($ocupacionId){

        $genericRestBusiness = new BGenericRest($this->entityManager);
        $fechaMovimiento     = new \DateTime();

        try{
            $resultado =$genericRestBusiness->listarGenerico(
                   'Core\CoreBundle\Entity\Tabla',
                   array('limit'=>1, 'filter'=>"tabla.nombre='\\ControlEscolar\\CalendarioBundle\\Entity\\Ocupacion'"),
                   array('incluir_activo'=>true)
           );

            if(!is_array($resultado["data"]["tabla"])){

                throw new \Exception("No encontramos el tipo de tabla adecuada para realizar la busqueda de movimientos");
            }

            $tabla = $resultado["data"]["tabla"][0];

           $resultado =$genericRestBusiness->listarGenerico(
                   'ControlEscolar\CalendarioBundle\Entity\Movimiento',
                   array('count'=>true, 'filter'=>"movimiento.referencia=" . $ocupacionId . " and tabla.tabla_id=" . $tabla->getTablaId() . " and (tipomovimiento.clave='pre_cancelacion_1' or tipomovimiento.clave='pre_cancelacion_2' or tipomovimiento.clave='cancelacion') and (movimiento.fecha_alta>='" . $fechaMovimiento->format("Y-m-d") . " 00:01' and movimiento.fecha_alta <= '" . $fechaMovimiento->format("Y-m-d"). " 23:59')", "order"=>"fecha_alta|desc"),
                   array('incluir_activo'=>true)
           );



            return $resultado["data"]["movimiento"];

        } catch(\Exception $e){
          throw new \Exception($e->getMessage());
        } catch(\DBALException $e){
          throw new \Exception($e->getMessage());
        }


    }

}
