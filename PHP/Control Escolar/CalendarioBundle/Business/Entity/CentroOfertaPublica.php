<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\Escenario as EEscenario;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class CentroOfertaPublica extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Publicación de un calendario a los centros
 * @param integer $obj[escenario_oferta_id] identificador del EscenarioOferta que será publicado
 * @param integer $obj[usuario_id] Identificadoe del usuario que realizó la publicacion
 * @return mixed objeto respuesta con los datos de error o exito de la publicación del calendario
 */
    public function publicarCalendario($obj){
        $usuario = $this->getUsuarioById($obj['usuario_id']);
        $escenarioOferta = $this->getRepository(
                'ControlEscolarCalendarioBundle:EscenarioOferta',
                array(
                    'escenario_oferta_id'=>$obj['escenario_oferta_id']
                ),
                null,
                200,
                null,
                'findBy',
                array()
        );
        if (!$escenarioOferta) {
            $this->respuesta['data']['ofertas_actividades']=array();
            return $this->respuesta;
        }
        $ofertasActividades = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaActividad',
                array(
                    'EscenarioOferta'=>$escenarioOferta
                ),
                null,
                200,
                null,
                'findBy',
                array()
        );
        if (!$ofertasActividades) {
            $this->respuesta['data']['ofertas_actividades']=array();
            return $this->respuesta;
        }
        return $this->buildRespuesta("post", array('ofertas_actividades'=>  $ofertasActividades));

    }
}
