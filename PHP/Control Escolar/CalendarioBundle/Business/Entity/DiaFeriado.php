<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\Escenario as EEscenario;
use ControlEscolar\CalendarioBundle\Entity\DiaFeriado as EDiaFeriado;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use Core\SyncBundle\Business\Action\NotificacionDiaNoLaborableCentroACentral as BNotificacionDiaNoLaborableCentroACentral;


/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class DiaFeriado extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de los dias feriados
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de los dias feriados
 */
    public function listar() {
        $diasferiados = array();

        $diasferiados = $this->getRepository(
                'ControlEscolarCalendarioBundle:DiaFeriado',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$diasferiados) {
            $this->respuesta['data']['diasferiados'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('diasferiados' => $diasferiados));
    }
/**
 * se obtiene un arreglo de fechas y se genera un arreglo de clave=fecha y valor=fecha
 * @param array $diasFeriados objeto con días feriados
 * @return array arreglo de dias
 */
    public function obtencionDiasFeriados(){
        $diasFeriados=$this->listar(array('fields'=>'fecha'));
        $respuesta=array();
        foreach($diasFeriados['data']['diasferiados'] as $diaFeriado){
            $respuesta[$diaFeriado['fecha']->format('Y-m-d')]=$diaFeriado['fecha']->format('Y-m-d');
        }
        return $respuesta;
    }


/**
 * Listado de los dias feriados
 * @return mixed objeto respuesta con los datos de los dias feriados
 */
    public function obtenerDiasFeriadosFormateado() {
        $diasferiados = array();

        $diasferiados = $this->getRepository(
                'ControlEscolarCalendarioBundle:DiaFeriado',
                array(),
                null,
                404,
                null,
                'findDiasWithFormat'
        );

        if (!$diasferiados) {
            $this->buildRespuesta(null, array('diasferiados' => array()),true,200);
            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('diasferiados' => $diasferiados));
    }

    /**
    * Listado de los dias feriados por renglon de fecha
    * @return mixed objeto respuesta con los datos de los dias feriados
    */
    public function obtenerDiasFeriadosPorReglonDeFecha($fecha_inicio, $fecha_fin) {
        $diasferiados = array();
        $diasferiados = $this->getRepository(
                'ControlEscolarCalendarioBundle:DiaFeriado',
                array('fecha_inicio' => $fecha_inicio, 'fecha_fin' => $fecha_fin),
                null,
                404,
                null,
                'findDiasWithDate'
        );

        if (!$diasferiados) {
            $this->buildRespuesta(null, array('diasferiados' => array()),true,200);
            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('diasferiados' => $diasferiados));
    }


/**
 * Creación de un DiaFeriado
 * @return mixed objeto respuesta con los datos del dia feriado creado
 */

    public function crear($obj){
        $usuario = $this->getUsuarioById($obj['usuario_id']);

        if (!$usuario) {
            $this->respuesta["data"]["diaferiado"] = null;
            return $this->respuesta;
        }

        $fecha=new \DateTime();
        $diaFeriado=new EDiaFeriado();
        $diaFeriado->setActivo(true);
        $diaFeriado->setCorporativo($obj['corporativo']);
        $diaFeriado->setDescripcion($obj['descripcion']);
        $fechaDia = \DateTime::createFromFormat('Y-m-d', $obj['fecha']);
        $diaFeriado->setFecha($fechaDia);
        $diaFeriado->setFechaAlta($fecha);
        $diaFeriado->setFechaModificacion($fecha);
        $diaFeriado->setNombre($obj['nombre']);
        $diaFeriado->setPuedePublicar($obj['puede_publicar']);
        $diaFeriado->setUsuarioAlta($usuario);
        $diaFeriado->setUsuarioModifica($usuario);
        try {

            if ($obj['corporativo']==false){

                $notificacionCentroACentralBusiness = new BNotificacionDiaNoLaborableCentroACentral($this->entityManager,$this->entityManagerMako);
                $respuestaSincroniacionCentral = $notificacionCentroACentralBusiness->sincronizarNotificacionDiaNoLaborableWithCentral($diaFeriado,$usuario, 'creacion');

                if($respuestaSincroniacionCentral['code']==404){
                    $this->entityManager->rollback();
                    return $this->buildRespuesta('Ocurrio un error al tratar de notificar con central ',array('diaferiado' => $diaFeriado),false,404);
                }

            }

            $this->entityManager->persist($diaFeriado);
            $this->entityManager->flush();

            $this->buildRespuesta('post', array('diaferiado' => $diaFeriado));

        } catch (DBALException $e) {
            $this->buildRespuesta(
                'No se almacenó en la base de datos la Entidad DiaFeriado',
                array('diaferiado' => $diaFeriado),
                404,
                null,
                'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }

    public function actualizar($obj){
        $usuario = $this->getUsuarioById($obj['usuario_id']);
        $diaFeriado = $this->getRepository(
                'ControlEscolarCalendarioBundle:DiaFeriado',
                array(
                    'dia_feriado_id'=>$obj['dia_feriado_id']
                ),
                null,
                200,
                null,
                'find'
        );

        if (!$diaFeriado) {
            $this->respuesta['data']['diaferiado'] = array();

            return $this->respuesta;
        }
        $fecha = new \DateTime();
        if (!$usuario) {
            $this->respuesta["data"]["diaferiado"] = null;
            return $this->respuesta;
        }
        if(array_key_exists('corporativo', $obj)){
            $diaFeriado->setCorporativo($obj['corporativo']);
        }
        if(array_key_exists('descripcion', $obj)){
            $diaFeriado->setDescripcion($obj['descripcion']);
        }
        if(array_key_exists('fecha', $obj)){
            $fechaDia = \DateTime::createFromFormat('Y-m-d', $obj['fecha']);
            $diaFeriado->setFecha($fechaDia);
        }
        if(array_key_exists('nombre', $obj)){
            $diaFeriado->setNombre($obj['nombre']);
        }
        if(array_key_exists('puede_publicar', $obj)){
            $diaFeriado->setPuedePublicar($obj['puede_publicar']);
        }

        $diaFeriado->setFechaModificacion($fecha);
        $diaFeriado->setUsuarioModifica($usuario);
        try {

            if ($obj['corporativo']==false){

                $notificacionCentroACentralBusiness = new BNotificacionDiaNoLaborableCentroACentral($this->entityManager,$this->entityManagerMako);
                $respuestaSincroniacionCentral = $notificacionCentroACentralBusiness->sincronizarNotificacionDiaNoLaborableWithCentral($diaFeriado,$usuario, 'modificacion');

                if($respuestaSincroniacionCentral['code']==404){
                    $this->entityManager->rollback();
                    return $this->buildRespuesta('Ocurrio un error al tratar de notificar con central ',array('diaferiado' => $diaFeriado),false,404);
                }

            }

            $this->entityManager->flush();
            $this->buildRespuesta('OK', array());

        } catch (DBALException $e) {
            $this->buildRespuesta(
                'No se modifico en la base de datos la Entidad DiaFeriado',
                array(),
                404,
                null,
                'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }

    public function eliminar($obj){
        $usuario = $this->getUsuarioById($obj['usuario_id']);

        $diaFeriado = $this->getRepository(
               "ControlEscolarCalendarioBundle:DiaFeriado",
               array('dia_feriado_id' => $obj['dia_feriado_id']),
               null,
               null,
               null,
               'find',
               null
        );
        try{

            if ($diaFeriado->getCorporativo() ==false){

                $notificacionCentroACentralBusiness = new BNotificacionDiaNoLaborableCentroACentral($this->entityManager,$this->entityManagerMako);
                $respuestaSincroniacionCentral = $notificacionCentroACentralBusiness->sincronizarNotificacionDiaNoLaborableWithCentral($diaFeriado,$usuario, 'eliminacion');

                if($respuestaSincroniacionCentral['code']==404){
                    $this->entityManager->rollback();
                    return $this->buildRespuesta('Ocurrio un error al tratar de notificar con central ',array('diaferiado' => $diaFeriado),false,404);
                }

            }

            $this->entityManager->remove($diaFeriado);
            $this->entityManager->flush();
            $this->buildRespuesta('delete',array(),true,200);
        } catch (DBALException $e) {
            $this->buildRespuesta(
                        'Error al Eliminar un Dia no laborable ',
                        array(),
                        false,
                        404
            );
            return $this->respuesta;
        }
        return $this->respuesta;
    }
}
