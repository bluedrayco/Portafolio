<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\EscenarioOferta                         as EEscenarioOferta;
use ControlEscolar\CalendarioBundle\Entity\Escenario                               as EEscenario;
use ControlEscolar\CalendarioBundle\Entity\OfertaEducativa                         as EOfertaEducativa;

use ControlEscolar\CalendarioBundle\Business\Entity\Escenario                      as BEscenario;
use ControlEscolar\CalendarioBundle\Business\Entity\EscenarioOferta                as BEscenarioOferta;
use ControlEscolar\CalendarioBundle\Business\Entity\EscenarioOfertaCentro          as BEscenarioOfertaCentro;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * Clase Business para  operaciones relacionadas a Escenario Oferta
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 *
 */
class EscenarioOferta extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Obtención de un Escenario Oferta bajos el escenario (todos|grupal|individual)
     *
     * @param integer $obj[oferta_id] identificador de la oferta
     * @param string  $obj[tipo] puede contener las siguientes palabras dependiendo del tipo del escenario: [todos|centro|grupo|agrupacion]
     * @param integer $obj[id] identificador del centro|grupo en el caso que tipo sea diferente de "todos o agrupacion"
     *
     * @return mixed objeto respuesta con el escenariooferta localizado
     */
    public function obtenerEscenarioOfertaByOfertaIdCentroId($obj){
        $escenarioOferta=null;
        $usuario = $this->getUsuarioById($obj['usuario_id']);
        if (!$usuario) {
            $this->respuesta['data']['escenariooferta'] = $escenarioOferta;

            return $this->respuesta;
        }
        $objeto = null;
        $entidad=array();
        if($obj['tipo']!='todos' && $obj['tipo']!='agrupacion'){
            switch($obj['tipo']){
                case 'centro':
                    $entidad       ='CoreCoreBundle:Centro';
                    $identificador = 'centro_id';
                break;
                case 'grupo':
                    $entidad       = 'CoreCoreBundle:Grupo';
                    $identificador = 'grupo_id';
                break;
            }

            $objeto = $this->getRepository(
                    $entidad,
                    array($identificador=>$obj['id']),
                    null,
                    200,
                    null,
                    'find'
            );
            if (!$objeto) {
                $this->respuesta['data']['escenariooferta'] = $escenarioOferta;

                return $this->respuesta;
            }
        }

        $oferta = $this->getRepository(
                'ControlEscolarCalendarioBundle:OfertaEducativa',
                array('oferta_educativa_id'=>$obj['oferta_id']),
                null,
                200,
                null,
                'find'
        );
        if (!$oferta) {
            $this->respuesta['data']['escenariooferta'] = $escenarioOferta;

            return $this->respuesta;
        }
        $fecha= new \DateTime();
        $this->entityManager->beginTransaction();

        try{

            $escenarioBusiness = new BEscenario($this->entityManager,$this->entityManagerMako);

            $escenario = $escenarioBusiness->obtenerEscenarioOrCrear($objeto, $usuario, $fecha,$obj['tipo']);//posee tambien la funcionalidad de enviar el tipo de Escenario para tratarlos de forma independiente dependiendo si son todos|individual|grupal
            $escenarioOferta= $this->obtenerEscenarioOfertaOrCrear($escenario, $oferta, $usuario, $fecha);

            if($obj['tipo']!='todos' && $obj['tipo']!='agrupacion'){
                $escenarioOfertaCentroBusiness = new BEscenarioOfertaCentro($this->entityManager);
                $escenarioOfertaCentro = $escenarioOfertaCentroBusiness->obtenerEscenarioOfertaCentroByCentroOrByGrupoOrCrear($escenarioOferta,$objeto,$usuario,$fecha,$obj['tipo']);
            }
            $this->entityManager->flush();
            $this->entityManager->commit();
        }catch (DBALException $e) {
            $this->entityManager->rollBack();
            $this->buildRespuesta(
                    $e->getMessage(), array("escenariooferta" => $escenarioOferta), 404, null, 'error'
            );

            return $this->respuesta;
        }

        $this->buildRespuesta("post", array("escenariooferta" => $escenarioOferta));
        return $this->respuesta;
    }

    public function obtenerEscenarioOfertaOrCrear($escenario,$oferta,$usuario,$fecha){
        $escenarioOferta = $this->getRepository(
            'ControlEscolarCalendarioBundle:EscenarioOferta',
            array(
                'Escenario'          =>$escenario,
                'OfertaEducativa'    =>$oferta
                ),
            null,
            200,
            null,
            'findBy'
        );
        $respuesta=$escenarioOferta[0];
        if (!$escenarioOferta[0]) {
            $escenarioOferta = new EEscenarioOferta();
            $escenarioOferta->setActivo(true);
            $escenarioOferta->setEscenario($escenario);
            $escenarioOferta->setFechaAlta($fecha);
            $escenarioOferta->setFechaModificacion($fecha);
            $escenarioOferta->setOfertaEducativa($oferta);
            $escenarioOferta->setUsuarioAlta($usuario);
            $escenarioOferta->setUsuarioModifica($usuario);
            $this->entityManager->persist($escenarioOferta);
            $respuesta=$escenarioOferta;
        }

        return $respuesta;
    }
}
