<?php

/***
 * Clase Business para  operaciones relacionadas a las actividades de la Oferta Educativa
 * @author silvio.bravo@enova.mx
 * @date Dic 09 2014
 *
 */

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\OfertaActividadCentro                   as EOfertaActividadCentro;
use ControlEscolar\CalendarioBundle\Business\Entity\EventoActividadOferta    as BEventoActividadOferta;
use ControlEscolar\CalendarioBundle\Business\Entity\DiaFeriado               as BDiaFeriado;
use ControlEscolar\CalendarioBundle\Business\Entity\OfertaActividadHorarioCentro   as BOfertaActividadHorarioCentro;

use ControlEscolar\CalendarioBundle\Business\Entity\Escenario                as BEscenario;

use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

class OfertaActividadCentro extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

    /**
     * Obtenemos la estructura de las actividades a partir de un id de oferta educativa y de un escenario id
     * @param  [int] $ofertaEducativaId [oferta educativa]
     * @param  [int] $escenarioId       [escenario]
     * @param  [date] $fechaInicio      [Fecha inicio]
     * @param  [date] $fechaFinal       [Fecha Final]
     * @return [array]                  [estructura de datos de las actividades]
     */
    public function obtenDatos($ofertaEducativaId, $escenarioId, $fechaInicio, $fechaFinal){

        $arregloEscenariosIds   = [];

        if($escenarioId !="todos"){

            $escenario = $this->getRepository(
                "ControlEscolarCalendarioBundle:Escenario",
                array('escenario_id' => $escenarioId ),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$escenario) {
                return null;
            }



            $arregloEscenariosIds   = $this->obtenerIdsEscenariosPorTipoEscenario(
                $escenario,
                $ofertaEducativaId
            );

        }
        else{  //Buscamos los escenarios que coincidan con esta oferta educativa.


            $ofertaEducativa          = $this->getRepository(
                "ControlEscolarCalendarioBundle:OfertaEducativa",
                array('oferta_educativa_id' => $ofertaEducativaId ),
                null,
                404,
                null,
                'find',
                null
            );

            if (!$ofertaEducativa) {
                return null;
            }


            $tmpescenario           = $this->getRepository(
                "ControlEscolarCalendarioBundle:EscenarioOferta",
                array('OfertaEducativa' => $ofertaEducativa),
                null,
                404,
                null,
                'findBy',
                null
            );


            foreach ($tmpescenario as $key => $escenario) {

                $arregloEscenariosIds = array_merge(
                                                    $arregloEscenariosIds,
                                                    $arregloEscenariosIds = $this->obtenerIdsEscenariosPorTipoEscenario(
                                                        $escenario->getEscenario(),
                                                        $ofertaEducativaId
                                                    )
                );

            }

        }




        $OfertaEscenarioEventos = $this->getRepository(
            "ControlEscolarCalendarioBundle:EventoActividadOferta",
            array(
                'oferta_educativa_id' => $ofertaEducativaId,
                "escenario_ids"=>$arregloEscenariosIds,
                "fecha_inicio"=>$fechaInicio,
                "fecha_final"=>$fechaFinal
            ),
            null,
            200,
            null,
            'findEventosForCalendar',
            null
        );

       if (!$OfertaEscenarioEventos) {

            $this->respuesta["data"]["eventos"] = array();
           return $this->respuesta;
       }

       return $this->buildRespuesta(null, array('eventos' => $OfertaEscenarioEventos));
    }

    /**
     * Metodo para obtener un arreglo de ids de escenario basandose en las siguientes premisas:
     * Tipo "todos" se obtendrá el id del escenario Todos
     * Tipo "centro" se obtendrá el id del escenario Todos, el id individual y los ids de los grupos en los que se encuentra el centro agregado
     * Tipo "grupo" se obtendrá el id del escenario Todos además del id representativo del grupo
     * @param mixed $escenario objeto escenario con el que se determinará el tipo de escenario
     * @param integer $ofertaEducativaId oferta educativa a la que esta vinculada el escenario
     * @return array arreglo de identificadores que representan a los escenarios.
     */
    public function obtenerIdsEscenariosPorTipoEscenario($escenario,$ofertaEducativaId){
        $arregloEscenarios=array();
        switch($escenario->getTipoEscenario()->getClave()){

            case 'todos':
                $arregloEscenarios[]=$escenario->getEscenarioId();
            break;

            case 'centro':
                $arregloEscenarios[]=$escenario->getEscenarioId();
                $escenarioBusiness = new BEscenario($this->entityManager,$this->entityManagerMako);

                $idEscenarioTodos = $escenarioBusiness->obtenerIdEscenarioTodos();
                $arregloEscenarios[]=$idEscenarioTodos;

                $idsEscenarioGrupos=$escenarioBusiness->obtenerIdsEscenarioGruposByIdCentro($escenario->getCentro()->getCentroId(),$ofertaEducativaId);
                if(count($idsEscenarioGrupos)>0){
                    $arregloEscenarios=  array_merge($idsEscenarioGrupos,$arregloEscenarios);
                }
            break;

            case 'grupo':
                $arregloEscenarios[]=$escenario->getEscenarioId();
                $escenarioBusiness = new BEscenario($this->entityManager,$this->entityManagerMako);
                $idEscenarioTodos = $escenarioBusiness->obtenerIdEscenarioTodos();
                $arregloEscenarios[]=$idEscenarioTodos;
            break;
        }
        return $arregloEscenarios;
    }


    /**
     * registraActividad description
     * @param  date   $fechaInicio            Fecha en que debe iniciar la programación de los eventos para esta actividad
     * @param  int    $escenarioOfertaId      Id de escenario Oferta
     * @param  int    $actividadAcademicaId   Id de Actividad Academica, puede venir null si no es una actividad academica
     * @param  int    $actividadId            Id de Actividad no Academica, puede venir null si no es una actividad no academica
     * @param  array  $horario                array que contiene el horario de la actividad
     * @param  int    $esquemaId              Id del Esquema en el que se realizará la programación del evento
     * @param  date   $fechaFinal             Fecha en que debe finalizar la programación de los eventos (siempre y cuando sea una actividad No academica)
     * @param  int    $usuarioId              Id de Usuario de la Sesión
     * @return array                          revoldemos un objeto envuelto en un array
     */
    public function registraActividad($fechaInicio,
                                      $escenarioOfertaId,
                                      $actividadAcademicaId,
                                      $actividadId,
                                      $horario,
                                      $esquemaId,
                                      $obligatorio,
                                      $fechaFinal,
                                      $usuarioId,
                                      $controlAcceso
            ){


        //Obtenemos Objetos que serán ocupados en el proceso de registrar la oferta actividad.
        $vtObjetos      = $this->obtenerObjetosParaOfertaActividad($escenarioOfertaId, $actividadAcademicaId, $actividadId, $esquemaId);
        if(!$vtObjetos){
            $this->respuesta["data"]["ofertaactividad"] = array();
           return $this->respuesta;
        }


        //Obtenemos el Usuario que representa la sesión
        $usuario         = $this->getUsuarioById($usuarioId);
        if(!$usuario){
            return $this->respuesta;
        }


        $ofertaActividad        = new EOfertaActividad();
        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BDiasFeriados          = new BDiaFeriado($this->entityManager,$this->entityManagerMako);
        $BHorario               = new BOfertaActividadHorario($this->entityManager,$this->entityManagerMako);
        $escenarioOferta        = $vtObjetos["escenarioOferta"];
        $actividad              = $vtObjetos["actividad"];
        $actividadAcademica     = $vtObjetos["actividadAcademica"];
        $esquema                = $vtObjetos["esquema"];
        $totalHoras             = ($vtObjetos["actividadAcademica"]!=null)?$actividadAcademica->getDuracionHorasPresenciales():0;
        $totalHoras             = ceil(($totalHoras/60));//si por ejemplo, la duracion es de 20.5 horas (recordemos que se peuden aceptar decimales) se tomará el inmediato superior de la cantidad para la generacion de eventos
        $diasFeriados           = $BDiasFeriados->obtencionDiasFeriados();



        /** #########  Iniciamos La Transacción para toda la Operación  ######### */
        $this->entityManager->beginTransaction();

        try{


            $fechas         = $BEventoActividadOferta->generaFechasEventos($horario, $totalHoras,$fechaInicio, $diasFeriados,$fechaFinal);

            if(count($fechas["fechas"]) <=0){
                $this->entityManager->rollback();
                $this->buildRespuesta('No se ha podido realizar la operación solicitada ya que no se encontro fecha para el horario seleccionado',array('ofertaactividad' =>  false),false,404);
                return $this->respuesta;
            }

            // Generamos la oferta Actividad
            $ofertaActividad->setUsuario($usuario);
            $ofertaActividad->setEscenarioOferta($escenarioOferta);
            $ofertaActividad->setActividad($actividad);
            $ofertaActividad->setActividadAcademica($actividadAcademica);
            $ofertaActividad->setFechaInicio(new \DateTime($fechas["fechaInicio"]));
            $ofertaActividad->setFechaFin(new \DateTime($fechas["fechaFinal"]));
            $ofertaActividad->setFechaAlta(new \DateTime());
            $ofertaActividad->setActivo(true);
            $ofertaActividad->setActividadAcademicaEsquema($esquema);
            $ofertaActividad->setObligatoria($obligatorio);
            //Seteando el control de acceso proveniente de central
            $ofertaActividad->setControlAcceso($controlAcceso);
            $this->entityManager->persist($ofertaActividad);
            //Fin de generar la oferta Actividad

            //Generamos el horario.
            $BHorario->generaHorario($ofertaActividad,$horario,$usuario);

            //Generamos los eventos.
            $BEventoActividadOferta->generaEventos($ofertaActividad, $fechas["fechas"],$usuario);

            $this->entityManager->flush();
            $this->entityManager->commit();
            $this->buildRespuesta('post',array('ofertaactividad' => $ofertaActividad));
        }
        catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta('No se ha podido realizar la operación solicitada',array('ofertaactividad' =>  false),false,404);
        }
        return $this->respuesta;
    }




    /**
     * Obtenemos los objetos de actividad no academica o actividad academica y su esquema si es el caso
     * @param  int $actividadAcademicaId actividad academica id
     * @param  int $actividadId          actividad no academica id
     * @param  int $esquemaId            id de tra_actividad_academica_esquema
     * @return array|boolean             false si ocurrio un error
     */
    private function obtenerObjetosParaOfertaActividad($escenarioOfertaId,$actividadAcademicaId, $actividadId, $esquemaId){


        $vtObjetos       = array("escenarioOferta"=>null, "actividadAcademica"=>null, "actividad"=>null, "esquema"=>null);

        // Obtenemos el escenario Oferta

        $escenarioOferta = $this->getRepository(
                             "ControlEscolarCalendarioBundle:EscenarioOferta",
                             array('escenario_oferta_id' => $escenarioOfertaId ),
                             null,
                             404,
                             null,
                             'find',
                             null
                         );

        if(!$escenarioOferta){
            $this->buildRespuesta(
                      "No se encontro el Escenario Oferta con el id {$escenarioOfertaId}",
                      array('ofertaactividad' =>  array()),
                        false,
                        404
                   );
            $this->log->error("No se encontro el Escenario Oferta con el id {$escenarioOfertaId}");
            return $vtObjetos;
        }

        $vtObjetos["escenarioOferta"] = $escenarioOferta;

        if($actividadId != null){                                           //Es una Actividad No Academica


            //Obtenemos el Objeto de la Actividad No Academica
            $actividad              = $this->getRepository(
                                               "ControlEscolarPlanAnualBundle:Actividad",
                                               array("actividad_id"=>$actividadId),
                                               null,
                                               404,
                                               null,
                                               'find',
                                               null
                                            );
            if(!$actividad){
                $this->buildRespuesta(
                          "No se encontro la actividad  No academica con el id {$actividadId}",
                          array('ofertaactividad' =>  array()),
                            false,
                            404
                        );
                $this->log->error("No se encontro la actividad  No academica con el id {$actividadId}");
                return $vtObjetos;

            }
            $vtObjetos["actividad"] = $actividad;

        }
        else{                                                           //Se trata de una actividad Academica :D


            // Obtenemos el objeto de la actividad Academica.

            $actividadAcademica =  $this->getRepository(
                                              "CoreCoreBundle:ActividadAcademica",
                                              array("actividad_academica_id"=>$actividadAcademicaId),
                                              null,
                                              404,
                                              null,
                                              'find',
                                              null
                                            );
            if(!$actividadAcademica){

                $this->buildRespuesta(
                            'No se encontro la actividad academica con el id ' .$actividadAcademicaId,
                            array('ofertaactividad' =>  array()),
                            false,
                            404
                        );
                $this->log->error('No se encontro la actividad academica con el id ' .$actividadAcademicaId);
                return $vtObjetos;

            }
            $vtObjetos["actividadAcademica"] = $actividadAcademica;

            $esquema            = $this->getRepository(
                                            "CoreCoreBundle:ActividadAcademicaEsquema",
                                            array("actividad_academica_esquema_id"=>$esquemaId),
                                            null,
                                            404,
                                            null,
                                            'find',
                                            null
                                        );
            if(!$esquema){
                $this->buildRespuesta(
                            'No se encontro la actividad academica esquema con el id '. $esquemaId,
                            array('ofertaactividad' =>  array()),
                            false,
                            404
                        );
                $GLOBALS['kernel']->getContainer()->get('logger')->error('No se encontro la actividad academica esquema con el id '. $esquemaId);
                return $vtObjetos;
            }
            $vtObjetos["esquema"] = $esquema;
        }

        return $vtObjetos;

    }

/**
 * eliminación de una ofertaActividad en base a su identificador, eliminando eventos y horarios generados
 * @param integer $ofertaActividadId identificador de la oferta actividad a eliminar
 * @return mixed objeto con la respuesta positiva o negativa del borrado de la oferta actividad
 */

    public function eliminaOfertaActividad($ofertaActividadId){
        $ofertaActividad = $this->getRepository(
                             "ControlEscolarCalendarioBundle:OfertaActividad",
                             array('oferta_actividad_id' => $ofertaActividadId ),
                             null,
                             404,
                             null,
                             'find',
                             null
                         );
        if(!$ofertaActividad){
            $this->buildRespuesta('No se ha encontrado una Oferta Actividad con el Id proporcionado',array('ofertaactividad' =>  false),false,404);
            return $this->respuesta;
        }


        $BEventoActividadOferta = new BEventoActividadOferta($this->entityManager,$this->entityManagerMako);
        $BHorario               = new BOfertaActividadHorario($this->entityManager,$this->entityManagerMako);

        $this->entityManager->beginTransaction();

        try{
             $BEventoActividadOferta->eliminaEventosDesdeOfertaActividad($ofertaActividad);
             $BHorario->eliminaHorarioDesdeOfertaActividad($ofertaActividad);
             $this->entityManager->remove($ofertaActividad);
             $this->entityManager->flush();
             $this->entityManager->commit();
             $this->buildRespuesta('delete',array('ofertaactividad' =>  true));
        }
        catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta('Ocurrio un error al momento de eliminar la Oferta Actividad',array('ofertaactividad' =>  false),false,404);
        }

        return $this->respuesta;
    }

     /**
     * Actualizacion de una Oferta Actividad Centro su campo Obligatorio
     * @param  bool    $obj[obligatorio]             Obligatorio
     * @return array                                 devoldemos un objeto envuelto en un array
     */
    public function actualizar($obj){
        //Obtenemos el Usuario que representa la sesión
        $usuario         = $this->getUsuarioById($obj['usuario_id']);
        if(!$usuario){
            return $this->respuesta;
        }

        //obtenermos la OfertaActividad a modificar
        $ofertaActividadCentro = $this->getRepository(
             "ControlEscolarCalendarioBundle:OfertaActividadCentro",
             array('oferta_actividad_centro_id' => $obj['oferta_actividad_id'] ),
             null,
             404,
             null,
             'find',
             null
            );

        if(!$ofertaActividadCentro){
            $this->buildRespuesta("No se ha encontrado una Oferta Actividad
                                    Centro con el Id proporcionado",
                    array('ofertaactividadcentro' =>  false),false,404);
            return $this->respuesta;
        }
        $this->entityManager->beginTransaction();

        try{
            $fecha=new \DateTime();
            $ofertaActividadCentro->setFechaModificacion($fecha);

            if (array_key_exists('obligatorio', $obj)){
                $ofertaActividadCentro->setObligatoria($obj['obligatorio']);
            }
            if (array_key_exists('control_acceso', $obj)){
                $ofertaActividadCentro->setControlAcceso($obj['control_acceso']);
            }
            $this->entityManager->flush();

             $this->entityManager->commit();
             $this->buildRespuesta('put',array('ofertaactividadcentro' =>  $ofertaActividadCentro));
        }
        catch (DBALException $ex) {
            $this->entityManager->rollback();
            $this->buildRespuesta("Ocurrio un error al momento de actualizar la
                                   Oferta Actividad Centro"
                                   , array("ofertaactividadcentro" => $ofertaActividadCentro)
                                   , false,404);
        }
        return $this->respuesta;
    }
}
?>
