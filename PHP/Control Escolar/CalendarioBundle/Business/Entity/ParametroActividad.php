<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use ControlEscolar\CalendarioBundle\Entity\ParametroActividad as EParametroActividad;
use ControlEscolar\CalendarioBundle\Business\Entity\Parametro as BParametro;

/**
 *  * Lógica de negocio para obtener Parametros de Actividades
 * @author Jaime Henández Cruz <control.escolar.desarrollo@enova.mx>
 */
class ParametroActividad extends BusinessEntity {

    public function __construct(EntityManager $em) {
        $this->construct($em);
    }

    /**
    * Obtiene el parametro para una actividad en especifico que tenga
    * especificado un parametro en la tabla de ParametroActividad
    * @param string $clave_parametro clave del parametro
    * @param object $actividad Objeto de tipo ActividadAcademica
    * @return $valor_parametro_actividad string valor del parametro en la actividad
    */
    public function obtenerParametroPorActividad($clave_parametro,$actividad){
        $valor_parametro_actividad = false;
        if($actividad){
            //Obtiene el valor del parametro que se pide
            $valor_parametro_actividad = $this->getRepository(
                    'ControlEscolarCalendarioBundle:ParametroActividad'
                    , array(
                        'nombre_parametro'=> $clave_parametro
                        ,'actividad_id'     => $actividad->getActividadAcademicaId()
                        )
                    , null
                    , 200
                    , null
                    ,'findActividadByClaveParametro'
                    , array()
            );
        }
        //Si el valor del parametro para la actividad no existe entonces
        //Regresa el valor generico de la tabla de parámetros
        if(!$valor_parametro_actividad){
            $parametroBusiness           = new BParametro($this->entityManager);
            $valor_parametro_actividad  = $parametroBusiness->obtenerParametroByTag('numero_maximo_movimientos');
        }
        return $valor_parametro_actividad;

    }


}