<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use ControlEscolar\CalendarioBundle\Entity\Escenario as EEscenario;
use Core\CoreBundle\Entity\Centro                    as ECentro;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;

/**
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class Escenario extends BusinessEntity {

    public function __construct(EntityManager $em,EntityManager $emMako=null) {
        $this->construct($em,$emMako);
    }

/**
 * Listado de los Escenarios
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de los escenarios
 */
    public function listar() {
        $eventos = array();

        $eventos = $this->getRepository(
                'ControlEscolarCalendarioBundle:Escenario',
                (count(func_get_args()) > 0) ? func_get_args()[0] : array(),
                null,
                200,
                null,
                (count(func_get_args()) > 0) ? 'findByOpciones' : 'findBy',
                (count(func_get_args()) > 0) ? array() : array('ignore_activo' => true)
        );

        if (!$eventos) {
            $this->respuesta['data']['escenarios'] = array();

            return $this->respuesta;
        }

        return $this->buildRespuesta(null, array('escenarios' => $eventos));
    }

    /**
     * obtencion de un Escenario para un centro individual, si no existe lo crea
     *
     * @param mixed $objeto objeto que se vinculara a un escenario, dependiendo del tipo de escenario a buscar
     * @param mixed $usuario Usuario que se vincuilará a un escenario
     * @param DateTime $fecha Fecha de Creación del escenario al crearlo
     * @param string $tipo tipo de escenario a buscar, los posibles valores son: todos|individual|grupal
     *
     * @return \ControlEscolar\CalendarioBundle\Entity\Escenario Escenario obtenido|creado
     */
    public function obtenerEscenarioOrCrear($objeto,$usuario,$fecha,$tipo){
        $condiciones = array();
        $tipoEscenario = $this->getRepository(
            'ControlEscolarCalendarioBundle:TipoEscenario',
            array(
                'clave'=>$tipo,
                'activo'=>true
            ),
            null,
            200,
            null,
            'findBy'
        );
        if(!$tipoEscenario){
            return null;
        }
        switch($tipo){
            case 'todos':
            case 'agrupacion':
                $condiciones=array(
                    'TipoEscenario'=> $tipoEscenario,
                    'Centro'=>null,
                    'Grupo' =>null
                );
            break;
            case 'centro':
                $condiciones = array(
                    'Centro'=>$objeto,
                    'TipoEscenario'=> $tipoEscenario,
                    'Grupo' =>null
                );
            break;
            case 'grupo':
                $condiciones = array(
                    'Grupo'=>$objeto,
                    'TipoEscenario'=> $tipoEscenario,
                    'Centro'=>null,
                );
            break;
        }
        $escenario = $this->getRepository(
            'ControlEscolarCalendarioBundle:Escenario',
            $condiciones,
            null,
            200,
            null,
            'findBy'
        );
        if (!$escenario) {
            $escenario=new EEscenario();
            $escenario->setActivo(true);
            if($tipo=='centro'){
               $escenario->setCentro($objeto);
            }
            if($tipo=='grupo'){
                $escenario->setGrupo($objeto);
            }
            $escenario->setTipoEscenario($tipoEscenario[0]);
            $escenario->setFechaAlta($fecha);
            $escenario->setFechaModificacion($fecha);
            ($objeto == null) ? $escenario->setNombre("Todos - Escenario") : $escenario->setNombre($objeto->getNombre()." - Escenario");  // Si el escenario es null entonces el nombre es todos
            $escenario->setUsuarioAlta($usuario);
            $escenario->setUsuarioModifica($usuario);
            $this->entityManager->persist($escenario);
            $this->entityManager->flush();
        }
        if(is_array($escenario)){
            return $escenario[0];
        }else{
            return $escenario;
        }
    }

    public function obtenerIdEscenarioTodos(){
        $tipoEscenario = $this->getRepository(
                'ControlEscolarCalendarioBundle:TipoEscenario',
                array(
                    'clave'=> "todos"
                    ),
                null,
                200,
                null,
                'findBy',
                array()
        );

        if (!$tipoEscenario) {
            return null;
        }
        $escenario = $this->getRepository(
                'ControlEscolarCalendarioBundle:Escenario',
                array(
                    'TipoEscenario'=> $tipoEscenario
                    ),
                null,
                200,
                null,
                'findBy',
                array()
        );

        if (!$escenario) {
            return null;
        }

        return $escenario[0]->getEscenarioId();
    }

    public function obtenerIdsEscenarioGruposByIdCentro($centroId,$ofertaEducativaId){
        $gruposIds = $this->getRepository(
                'CoreCoreBundle:GrupoCentro',
                array(
                    'centro_id'=> $centroId
                    ),
                null,
                200,
                null,
                'findIdsGruposByIdCentro',
                array()
        );

        if (!$gruposIds) {
            return null;
        }
        $escenariosIds = $this->getRepository(
                'ControlEscolarCalendarioBundle:Escenario',
                array(
                    'grupo_ids'=> $gruposIds,
                    'oferta_educativa_id' => $ofertaEducativaId
                    ),
                null,
                200,
                null,
                'findIdsEscenarioByIdsGrupos',
                array()
        );

        if (!$escenariosIds) {
            return null;
        }
        return $escenariosIds;
    }
}
