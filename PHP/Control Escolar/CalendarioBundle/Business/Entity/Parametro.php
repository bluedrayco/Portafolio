<?php

namespace ControlEscolar\CalendarioBundle\Business\Entity;

use Doctrine\ORM\EntityManager;
use Core\SyncBundle\Business\Action\Sync as BSync;
use Core\UserBundle\Entity\Usuario;
use Core\CoreBundle\Business\Entity\BusinessEntity;
use Doctrine\DBAL\DBALException;
use Core\CoreBundle\Lib\EnovaDates;
use ControlEscolar\CalendarioBundle\Entity\Parametro               as EParametro;
use Core\CoreBundle\Business\Entity\GenericRest as BGeneric;

/**
 *  * Lógica de negocio de la manipulación de un Parametro
 * @author Roberto Leroy <roberto.monroy@enova.mx>
 */
class Parametro extends BusinessEntity {

    public function __construct(EntityManager $em, $kernel = null) {
        $this->construct($em);
        $this->container = $kernel;
    }

/**
 * Listado de los Parametros
 * @param mixed func_get_args()[0] arreglo de opciones procedentes del query param
 * @return mixed objeto respuesta con los datos de los Parametros
 */
    public function listar() {
        try{
            $parametros = array();

            $parametros = $this->getRepository(
                    'ControlEscolarCalendarioBundle:Parametro',
                    (count(func_get_args()[0]) > 0) ? func_get_args()[0] : array(),
                    null,
                    200,
                    null,
                    (count(func_get_args()[0]) > 0) ? 'findByOpciones' : 'findBy'
            );
            if (!$parametros) {
                $this->respuesta['data']['parametros'] = array();

                return $this->respuesta;
            }
        }catch(\Exception $e){
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );
            return $this->respuesta;
        }catch(\DBALException $ex){
            $this->buildRespuesta(
                    $ex->getMessage(), null, 404, null, 'error'
            );
            return $this->respuesta;
        }
        return $this->buildRespuesta(null, array('parametros' => $parametros));
    }


    public function obtenerParametroByTag($nombre_parametro){
        $parametro= $this->listar(
                        array(
                            'filter'=>"parametro.nombre='".$nombre_parametro."'"
                        )
                    );
        if($parametro['data']['parametros']== null){
            $this->log->error("No se encontro el parametro con nombre {$nombre_parametro}");
        }
        return $parametro['data']['parametros'];
    }

    public function obtenerParametro($id) {
        $parametro = $this->getRepository(
            'ControlEscolarCalendarioBundle:Parametro',
            array('parametro_id' => $id),
            null,
            200
        );

        if (!$parametro) {
            $this->respuesta["data"]["parametro"] = array();

            $this->buildRespuesta('get', array('parametro' => null),false,404);


            return $this->respuesta;
        }

        $this->buildRespuesta('get', array('parametro' => $parametro),true,200);

        return $this->respuesta;
    }

    /**
 * Actualización de datos de un parametro
 * @param int $obj['nombre'] nombre del parametro
 * @param int $obj['etiqueta'] etiqueta del parametro
 * @param int $obj['tipo'] identificador del tipo del parametro
 * @param int $obj['valor'] valor del parametro
 * @param int $obj['puede_publicar'] nuevo estatus puede publicar del parametro
 * @param int $obj['modifica_centro'] nuevo estatus si se puede modificar el parametro en el centro
 * @param int $obj['activo'] nuevo estatus de activo del parametro
 * @return mixed objeto respuesta
 */

    public function actualizar($obj) {
        $parametro = $this->getRepository(
                "ControlEscolarCalendarioBundle:Parametro", array('parametro_id' => $obj['parametro_id'])
        );

        if (!$parametro) {
            return $this->respuesta;
        }
/*
        if (array_key_exists('oferta_educativa_estatus_id', $obj)) {

            $ofertaEducativaEstatus = $this->getRepository(
                    "ControlEscolarCalendarioBundle:OfertaEducativaEstatus", array('oferta_educativa_estatus_id' => $obj['oferta_educativa_estatus_id']), null, null, null, 'find', null
            );

            if (!$ofertaEducativaEstatus) {
                $this->buildRespuesta('put');
                return $this->respuesta;
            }
            $ofertaEducativa->setOfertaEducativaEstatus($ofertaEducativaEstatus);
        }
*/
        if (array_key_exists('nombre',$obj)) {
            $parametro->setNombre($obj['nombre']);
        }

        if (array_key_exists('etiqueta',$obj)) {
            $parametro->setEtiqueta($obj['etiqueta']);
        }

        if (array_key_exists('tipo',$obj)) {

            $parametro->setTipo($obj['tipo']);
        }

        if (array_key_exists('valor',$obj)) {

            $parametro->setValor($obj['valor']);
        }


        if (array_key_exists('activo',$obj)) {

            $parametro->setActivo($obj['activo']);
        }


        if (array_key_exists('puede_publicar',$obj)) {

            $parametro->setPuedePublicar($obj['puede_publicar']);
        }

        if (array_key_exists('modifica_centro',$obj)) {

            $parametro->setModificaCentro($obj['modifica_centro']);
        }

        if (array_key_exists('usuario_id', $obj)) {
            $usuario = $this->getRepository(
                    "CoreUserBundle:Usuario", array('id' => $obj['usuario_id']), null, null, null, 'find', null
            );

            if (!$usuario) {
                $this->buildRespuesta('put');
                return $this->respuesta;
            }
            $parametro->setUsuarioModifica($usuario);
        }

        $fecha=new \DateTime();
        $parametro->setFechaModificacion($fecha);

        try{

        $this->entityManager->flush();

        } catch (DBALException $e) {
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }

        $this->buildRespuesta('put');
        return $this->respuesta;
    }


   /**
 * Creación de datos de un parametro
 * @param int $obj['nombre'] nombre del parametro
 * @param int $obj['etiqueta'] etiqueta del parametro
 * @param int $obj['tipo'] identificador del tipo del parametro
 * @param int $obj['valor'] valor del parametro
 * @param int $obj['puede_publicar'] nuevo estatus puede publicar del parametro
 * @param int $obj['modifica_centro'] nuevo estatus si se puede modificar el parametro en el centro
 * @param int $obj['activo'] nuevo estatus de activo del parametro
 * @return mixed objeto respuesta
 */

    public function crear($obj){
        $usuario = $this->getRepository(
                "CoreUserBundle:Usuario", array('id' => $obj['usuario_id']), null, null, null, 'find',null
        );

        if (!$usuario) {
            $this->respuesta["data"]["parametros"] = null;
            return $this->respuesta;
        }


        $fecha=new \DateTime();
        $parametro=new EParametro();

        $parametro->setFechaAlta($fecha);
        $parametro->setFechaModificacion($fecha);

        if (array_key_exists('nombre',$obj)) {
            $parametro->setNombre($obj['nombre']);
        }else{

            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro porque no se especifico un nombre',
                array('parametro' => null),
                404,
                null,
                'error'
            );

        }

        if (array_key_exists('etiqueta',$obj)) {
            $parametro->setEtiqueta($obj['etiqueta']);
        }else{

            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro porque no se especifico una etiqueta',
                array('parametro' => null),
                404,
                null,
                'error'
            );

        }

        $parametro->setTipo(1);

        if (array_key_exists('valor',$obj)) {

            $parametro->setValor($obj['valor']);
        }else{

            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro porque no se especifico un valor',
                array('parametro' => null),
                404,
                null,
                'error'
            );

        }


        if (array_key_exists('activo',$obj)) {

            $parametro->setActivo($obj['activo']);

        }else{

            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro porque no se especifico si esta activo',
                array('parametro' => null),
                404,
                null,
                'error'
            );

        }

        if (array_key_exists('puede_publicar',$obj)) {

            $parametro->setPuedePublicar($obj['puede_publicar']);

        }else{

            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro porque no se especifico si puede publicar',
                array('parametro' => null),
                404,
                null,
                'error'
            );

        }

        if (array_key_exists('modifica_centro',$obj)) {

            $parametro->setModificaCentro($obj['modifica_centro']);

        }else{

            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro porque no se especifico si se puede modificar en centro',
                array('parametro' => null),
                404,
                null,
                'error'
            );

        }

        if (array_key_exists('usuario_id', $obj)) {
            $usuario = $this->getRepository(
                    "CoreUserBundle:Usuario", array('id' => $obj['usuario_id']), null, null, null, 'find', null
            );

            if (!$usuario) {
                $this->buildRespuesta('put');
                return $this->respuesta;
            }
            $parametro->setUsuarioModifica($usuario);
        }
        try {
            $this->entityManager->persist($parametro);
            $this->entityManager->flush();

            $this->buildRespuesta('post', array('parametro' => $parametro));

        } catch (DBALException $e) {
            $this->buildRespuesta(
                'No se almacenó en la base de datos el parametro',
                array('parametro' => $parametro),
                404,
                null,
                'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }

     /**
     * Eliminación de un Parametro
     * @param int $parametro_id identificador del Parametro a eliminar
     * @return mixed objeto respuesta con un mensaje de exito o error
     */
    public function eliminar($parametro_id) {
        $parametro = $this->getRepository(
            "ControlEscolarCalendarioBundle:Parametro", array(
                    'parametro_id' => $parametro_id,
                ),
                null,
                200,
                null,
                'find',
                null);

        if (!$parametro) {
            return $this->respuesta;
        }

        try {
                $parametro->setActivo(false);
                $this->entityManager->flush();
            $this->buildRespuesta('delete');
        } catch (DBALException $e) {
            $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );

            return $this->respuesta;
        }

        return $this->respuesta;
    }
    /**
     * Sincronización de parametros y parametrosActividad a centros(todos o por destino_id)
     * @param mixed $params Contiene de cajón usuario_id, y puede recibir parametros por url que representa destino_id (14,15,16)
     * @return mixed Objeto respuesta de el estado de sincronización: status,code,message,data
     */
    public function sincronizar($params){
        try{
            if(!array_key_exists('usuario_id', $params)){
                return $this->buildRespuesta('No se encontro una sesión activa',false,false,404);
            }

            $parametros = $this->listar(null);
            //return $parametros;
            if(count($parametros['data']['parametros']) < 1){
               return $this->buildRespuesta('No se encontron Parametros para sincronizar',false,false,404);
            }
            //Obtenemos las entidades ParametroActividad
            $genericBuussiness = new BGeneric($this->entityManager);
            $parametrosActividad = $genericBuussiness->listarGenerico(
                "ControlEscolarCalendarioBundle:ParametroActividad",
                array(),
                null
                );
            if(!$parametrosActividad['status']){
                return $this->buildRespuesta('No se encontron ParametrosActividad para sincronizar',false,false,404);
            }
            $paquete = array_merge($parametros['data']['parametros'],$parametrosActividad['data']['controlescolarcalendariobundle:parametroactividad']);

            //verificamos si se desea un destino específico, sino sincronizara a todos.
            $destinos = array_key_exists('destinos', $params)? $params['destinos'] : null;

            //Obtenemos del contenedor de servicios al servicio sincronizacion
            $sincronizacionService = $this->container->get('sincronizacion');

            //Envia el paquete a los destinos validos y retorna un array de success y error de acuerdo al estatus de sincronización.
            $destinosEnviados = $sincronizacionService->enviarPaquete($paquete,$params['usuario_id'],$destinos);

            $respuesta['data'] = array('error' => $destinosEnviados['error'],'succes' => $destinosEnviados['success']);

            return $this->buildRespuesta('Sincronización Completada',$respuesta);

        }catch(\Exception $e){
            return $this->buildRespuesta(
                    $e->getMessage(), null, 404, null, 'error'
            );
        }catch(\DBALException $ex){
            return $this->buildRespuesta(
                    $ex->getMessage(), null, 404, null, 'error'
            );
        }
    }


}