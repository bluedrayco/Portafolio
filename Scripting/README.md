## Scripting
<br/><br/>
Contains some scripts in Bash and PHP to automate tasks, such as:<br/><br/>

**installBiometrico:** This script installs the "Biocheck" project, changes directory owners, permissions and downloads the composer dependencies.<br/>
**tecno.php:** This script is a general purpose script that can create a symlink in to the public directory of the "Gym" project, minimizes the javascript code and html for increase the velocity to download of the webpage, installs the project using composer install, creates the database and datafixtures, it can receive arguments.<br/>
**scripts/createDatabase.php:** Creates a specific database for the "Gym" project using RedBeanPHP as its Table Gateway.<br/>
**scripts/createDatafixtures.php:** Insert data in to the specific database when you install the system.<br/>
**scripts/minified.php:** Minimizes each html and javascript to improve the performance of the application.<br/>
**scripts/symlink.php:** Some use the best practices that consists in not expose the logic layer to web,these files need to copy in public directory changed their owners and the way to access them.<br/>