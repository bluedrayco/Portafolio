<?php

$argumentos = arguments($argv);
procesarComandos($argumentos);
echo "\nejecucion concluida exitosamente...\n";

function arguments($argv){
	$options = array();
	foreach ($argv as $arg){
	    preg_match('/\-\-(\w*)\=?(.+)?/', $arg, $value);
	    if ($value && isset($value[1]) && $value[1])
	        $options[$value[1]] = isset($value[2]) ? $value[2] : null;
	}
	return $options;
}

function procesarComandos($comandos) {
    foreach ($comandos as $comando => $valor) {
        echo "comando: {$comando} {$valor}...\n";
	switch ($comando) {
            case "symlink":
            case "sl":
            case "l":
                echo "\033[01;31m Copiado de Ejecutables a public para su visualización...\033[0m \n";
                exec("scripts/symlink.sh");
                break;
            case "minified":
            case "min":
            case "m":
                echo "\033[01;31m Proceso de minificacion de html, php, css y js...\033[0m \n";
                system("php scripts/minified.php");
                break;
            case "install":
            case "i":
                echo "\033[01;31m Instalación del Proyecto con sym-link y Creacion de Esquemas de Base de Datos...\033[0m \n";
                exec("composer install");
                break;
            case "createdatabase":
            case "cd":
            case "d":
                echo "\033[01;31m Creación del Esquema de la Base de Datos...\033[0m \n";
                exec("php scripts/createDatabase.php");
                break;
            case "createdatafixtures":
            case "df":
                echo "\033[01;31m Creación de Datos iniciales de la base de datos...\033[0m \n";
                exec("php scripts/createDatafixtures.php");
                break;
            default:
                echo "Comando {$comando} no válido...\n";
                break;
        }
    }
}
