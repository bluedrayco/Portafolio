#!/bin/bash
rm -rf public/*
cp -rf app/Frontend/* public/
cp -rf inits/* public/
cp -R inits/.[a-zA-Z0-9]* public/
user=$(who |cut -d' ' -f1 |uniq)
chown -R $user:www-data public/
#echo $user