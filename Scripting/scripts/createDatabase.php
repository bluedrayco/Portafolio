<?php

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->add('App', __DIR__ . '/../app/Backend');
require __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../vendor/socloz/spyc/Spyc.php';
$parameters = \Spyc::YAMLLoad(__DIR__ . '/../Parameters.yml');

use RedBeanPHP\Facade as R;
use App\Business\Core\Database;

R::setup(Database::getStringConnection(), $parameters['Database']['username'], $parameters['Database']['password']);
R::freeze(false);
R::debug(true);

echo "Generación del Esquema de la Base de Datos\n\r";
R::begin();
try {
    R::nuke();
    $permiso = crearPermiso();
    $centro = crearCentro();
    $usuario = crearUsuario($permiso, $centro);
    //modificaciones para agregar referencia circular de usuarios
    $usuario2 = crearUsuario2($permiso, $centro, $usuario);
    //$usuario = modificarUsuario($usuario);
    $centro  = modificarCentro($centro,$usuario);
    $permiso = modificarPermiso($permiso,$usuario);
    $alumno = crearAlumno($usuario, $centro);
    $profesor = crearProfesor($usuario, $centro);
    $credencial = crearCredencial($alumno, $profesor, $usuario);
    $actividad = crearActividad($usuario, $centro);
    $actividadHorario = crearActividadHorario($actividad, $usuario, $centro);
    $actividadProfesor = crearActividadProfesor($profesor, $actividad, $usuario, $centro);
    $claseAlumno = crearClaseAlumno($actividad, $usuario, $alumno, $profesor, $centro);
    $medidasAlumno = crearMedidasAlumno($claseAlumno, $usuario);
    limpiarTablas();
    R::commit();
} catch (Exception $ex) {
    R::rollback();
    echo "hubo un error al tratar de crear el esquema de la base de datos: " . $ex->getMessage() . " " . $ex->getLine() . " " . $ex->getTraceAsString() . "\n";
}
R::close();

function modificarUsuario($usuario){
    $usuario->usuario_alta=$usuario;
    $usuario->usuario_modificacion=$usuario;
    R::store($usuario);
    return $usuario;
}

function modificarCentro($centro,$usuario){
    $centro->usuario_alta=$usuario;
    $centro->usuario_modificacion=$usuario;
    R::store($centro);
    return $centro;
}

function modificarPermiso($permiso,$usuario){
    $permiso->usuario_alta=$usuario;
    $permiso->usuario_modificacion=$usuario;
    R::store($permiso);
    return $permiso;
}

function crearCentro() {
    $centro = R::dispense('centro');
    $centro->nombre = "aerodanza";
    $centro->clave = "aero01";
    $centro->direccion = "direccion centro 1";
    $centro->telefono = "telefono centro 1";
    $centro->telefono2 = "telefono2 centro 1";
    $centro->codigo_postal = "13200";
    $centro->latitud = "-110.5500000000";
    $centro->longitud = "-11.220000000";
    $centro->hora_apertura = new \DateTime("09:00:00");
    $centro->hora_cierre = new \DateTime("21:00:00");
    $centro->activo = true;
    $centro->fecha_alta = new \DateTime();
    $centro->fecha_modificacion = new \DateTime();
    $centro->usuario_alta=null;
    $centro->usuario_modificacion=null;
    R::store($centro);
    return $centro;
}

function crearPermiso() {
    $permiso = R::dispense("permiso");
    $permiso->crear_alumno = true;
    $permiso->listar_alumnos = true;
    $permiso->crear_centro = true;
    $permiso->listar_centros = true;
    $permiso->crear_profesor = true;
    $permiso->listar_profesores = true;
    $permiso->crear_usuario = true;
    $permiso->listar_usuarios = true;
    $permiso->activo = true;
    $permiso->fecha_alta = new \DateTime();
    $permiso->fecha_modificacion = new \DateTime();
    $permiso->usuario_alta=null;
    $permiso->usuario_modificacion=null;
    R::store($permiso);
    return $permiso;
}

function crearUsuario($permiso, $centro) {
    $usuario = R::dispense("usuario");
    $usuario->permiso = $permiso;
    $usuario->centro = $centro;
    $usuario->nombre = "usuario1";
    $usuario->apellido_paterno = "apellido paterno1";
    $usuario->apellido_materno = "apellido_materno1";
    $usuario->multicentro=false;
    $usuario->activo = True;
    $usuario->fecha_alta = new \DateTime();
    $usuario->fecha_modificacion = new \DateTime();
    R::store($usuario);
    return $usuario;
}

function crearUsuario2($permiso, $centro,$usuario1) {
    $usuario = R::dispense("usuario");
    $usuario->permiso = $permiso;
    $usuario->centro = $centro;
    $usuario->nombre = "usuario12";
    $usuario->apellido_paterno = "apellido paterno12";
    $usuario->apellido_materno = "apellido_materno12";
    $usuario->multicentro=false;
    $usuario->activo = True;
    $usuario->fecha_alta = new \DateTime();
    $usuario->fecha_modificacion = new \DateTime();
    $usuario->usuario_alta=$usuario1;
    $usuario->usuario_modificacion=$usuario1;
    R::store($usuario);
    return $usuario;
}

function crearAlumno($usuario, $centro) {
    $alumno = R::dispense("alumno");
    $alumno->nombre = "roberto leroy";
    $alumno->apellido_paterno = "monroy";
    $alumno->apellido_materno = "ruiz";
    $alumno->email = "bluedrayco@gmail.com";
    $alumno->direccion = "don pascuale mz 1 lt 22 miguel hidalgo tlahuac c.p. 13200";
    $alumno->telefono_celular = "55-19-50-53-58";
    $alumno->telefono_particular = "01-55-21-60-57-72";
    $alumno->fecha_nacimiento = new \DateTime();
    $alumno->activo = True;
    $alumno->fecha_alta = new \DateTime();
    $alumno->fecha_modificacion = new \DateTime();
    $alumno->usuario_alta = $usuario;
    $alumno->usuario_modificacion = $usuario;
    $alumno->centro = $centro;
    R::store($alumno);
    return $alumno;
}

function crearProfesor($usuario, $centro) {
    $profesor = R::dispense("profesor");
    $profesor->nombre = "guadalupe";
    $profesor->apellido_paterno = "ruiz";
    $profesor->apellido_materno = "ayala";
    $profesor->curp = "ruag11223344556677889900";
    $profesor->telefono_celular = "55-19-50-53-58";
    $profesor->telefono_particular = "01-55-21-60-57-72";
    $profesor->email = "bluedrayco@gmail.com";
    $profesor->direccion = "gdfgdfgdfgdfgdf";
    $profesor->fecha_nacimiento = new \DateTime();
    $profesor->activo = True;
    $profesor->fecha_alta = new \DateTime();
    $profesor->fecha_modificacion = new \DateTime();
    $profesor->usuario_alta = $usuario;
    $profesor->usuario_modificacion = $usuario;
    $profesor->centro = $centro;
    R::store($profesor);
    return $profesor;
}

function crearCredencial($alumno, $profesor, $usuario) {
    $credencial = R::dispense("credencial");
    $credencial->usuario = $usuario;
    $credencial->profesor = $profesor;
    $credencial->alumno = $alumno;
    $credencial->username = "bluedrayco";
    $credencial->pwd = "123456";
    $credencial->activo = true;
    $credencial->fecha_alta = new \DateTime();
    $credencial->fecha_modificacion = new \DateTime();
    $credencial->access_key = md5("hola mundo...");
    $credencial->fecha_expiracion = new \DateTime();
    R::store($credencial);
    $credencial->usuario = null;
    R::store($credencial);
    $credencial->profesor = null;
    R::store($credencial);
    $credencial->alumno = null;
    R::store($credencial);
    return $credencial;
}

function crearActividad($usuario, $centro) {
    $actividad = R::dispense("actividad");
    $actividad->nombre = "actividad 1";
    $actividad->precio_mensual = 200.00;
    $actividad->precio_unitario = 10.50;
    $actividad->edad_minima = 10;
    $actividad->edad_maxima = 100;
    $actividad->activo = true;
    $actividad->fecha_alta = new \DateTime();
    $actividad->fecha_modificacion = new \DateTime();
    $actividad->usuario_alta = $usuario;
    $actividad->usuario_modificacion = $usuario;
    $actividad->centro = $centro;
    R::store($actividad);
    return $actividad;
}

function crearActividadHorario($actividad, $usuario, $centro) {
    $actividadHorario = R::dispense("actividadhorario");
    $actividadHorario->actividad = $actividad;
    $actividadHorario->dia_semana = 0;
    $actividadHorario->hora_inicio = new \DateTime();
    $actividadHorario->hora_fin = new \DateTime();
    $actividadHorario->activo = true;
    $actividadHorario->fecha_alta = new \DateTime();
    $actividadHorario->fecha_modificacion = new \DateTime();
    $actividadHorario->usuario_alta = $usuario;
    $actividadHorario->usuario_modificacion = $usuario;
    $actividadHorario->centro = $centro;
    R::store($actividadHorario);
    return $actividadHorario;
}

function crearActividadProfesor($profesor, $actividad, $usuario, $centro) {
    $actividadProfesor = R::dispense("actividadprofesor");
    $actividadProfesor->actividad = $actividad;
    $actividadProfesor->profesor = $profesor;
    $actividadProfesor->activo = true;
    $actividadProfesor->fecha_alta = new \DateTime();
    $actividadProfesor->fecha_modificacion = new \DateTime();
    $actividadProfesor->usuario_alta = $usuario;
    $actividadProfesor->usuario_modificacion = $usuario;
    $actividadProfesor->centro = $centro;
    R::store($actividadProfesor);
    return $actividadProfesor;
}

function crearClaseAlumno($actividad, $usuario, $alumno, $profesor, $centro) {
    $claseAlumno = R::dispense("clasealumno");
    $claseAlumno->actividad = $actividad;
    $claseAlumno->alumno = $alumno;
    $claseAlumno->fecha_clase = new \DateTime();
    $claseAlumno->activo = true;
    $claseAlumno->fecha_alta = new \DateTime();
    $claseAlumno->fecha_modificacion = new \DateTime();
    $claseAlumno->usuario_alta = $usuario;
    $claseAlumno->usuario_modificacion = $usuario;
    $claseAlumno->profesor = $profesor;
    $claseAlumno->centro = $centro;
    R::store($claseAlumno);
    return $claseAlumno;
}

function crearMedidasAlumno($claseAlumno, $usuario) {
    $medidasAlumno = R::dispense("medidasalumno");
    $medidasAlumno->busto = 123.02;
    $medidasAlumno->cintura = 123.02;
    $medidasAlumno->cadera = 200.50;
    $medidasAlumno->abdomen = 123.05;
    $medidasAlumno->ancho_espalda = 123.02;
    $medidasAlumno->linea_brasier = 129.02;
    $medidasAlumno->peso = 300.15;
    $medidasAlumno->clasealumno = $claseAlumno;
    $medidasAlumno->activo = true;
    $medidasAlumno->fecha_alta = new \DateTime();
    $medidasAlumno->fecha_modificacion = new \DateTime();
    $medidasAlumno->usuario_alta = $usuario;
    $medidasAlumno->usuario_modificacion = $usuario;
    R::store($medidasAlumno);
    return $medidasAlumno;
}

function limpiarTablas() {
    R::exec("SET FOREIGN_KEY_CHECKS = 0");
    R::exec("Truncate clasealumno");
    R::exec("Truncate medidasalumno");
    R::exec("Truncate actividadhorario");
    R::exec("Truncate actividadprofesor");
    R::exec("Truncate actividad");
    R::exec("Truncate centro");
    R::exec("Truncate credencial");
    R::exec("Truncate permiso");
    R::exec("Truncate profesor");
    R::exec("Truncate alumno");
    R::exec("Truncate usuario");
    R::exec("SET FOREIGN_KEY_CHECKS = 1");
}
