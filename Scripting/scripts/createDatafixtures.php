<?php

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->add('App', __DIR__ . '/../app/Backend');
require __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../vendor/socloz/spyc/Spyc.php';
$parameters = \Spyc::YAMLLoad(__DIR__ . '/../Parameters.yml');

use RedBeanPHP\Facade as R;
use App\Business\Core\Database;

R::setup(Database::getStringConnection(), $parameters['Database']['username'], $parameters['Database']['password']);
R::freeze(true);
R::debug(true);

R::begin();
try {
    $permiso = crearPermiso();
    $centro = crearCentro();
    $usuario = crearUsuario($permiso, $centro);
    $usuario2 = crearUsuario2($permiso, $centro,$usuario);
    $permiso = modificarPermiso($permiso,$usuario);
    $alumno = crearAlumno($usuario, $centro);
    $profesor = crearProfesor($usuario, $centro);
    $credencialUsuarioSistema = crearCredencialUsuarioSistema($usuario);
    $credencialProfesor = crearCrecencialProfesor($profesor);
    $credencialAlumno = crearCredencialAlumno($alumno);
    $actividad = crearActividad($usuario, $centro);
    $actividadHorario = crearActividadHorario($actividad, $usuario, $centro);
    $actividadProfesor = crearActividadProfesor($profesor, $actividad, $usuario, $centro);
    $claseAlumno = crearClaseAlumno($actividad, $usuario, $alumno, $profesor, $centro);
    $medidasAlumno = crearMedidasAlumno($claseAlumno, $usuario);
    R::commit();
} catch (Exception $ex) {
    R::rollback();
    echo "hubo un error al tratar de crear los datafixtures en la base de datos: " . $ex->getMessage() . " " . $ex->getLine() . "\n";
}
R::close();

function crearCentro() {
    $centro = R::dispense('centro');
    $centro->nombre = "Aerodanza Studio";
    $centro->clave = "aero01";
    $centro->direccion = "Don pascuale Mz 1 Lt 22 Miguel Hidalgo Tlahuac";
    $centro->telefono = "21605772";
    $centro->telefono2 = "5519505358";
    $centro->codigo_postal = "13200";
    $centro->latitud = "19.294681";
    $centro->longitud = "-99.0500807";
    $centro->hora_apertura = new \DateTime("09:00:00");
    $centro->hora_cierre = new \DateTime("21:00:00");
    $centro->activo = true;
    $centro->fecha_alta = new \DateTime();
    $centro->fecha_modificacion = new \DateTime();
    $centro->usuario_alta=null;
    $centro->usuario_modificacion=null;
    R::store($centro);
    return $centro;
}

function modificarPermiso($permiso,$usuario){
    $permiso->usuario_alta=$usuario;
    $permiso->usuario_modificacion=$usuario;
    R::store($permiso);
    return $permiso;
}

function crearPermiso() {
    $permiso = R::dispense("permiso");
    $permiso->crear_alumno = true;
    $permiso->listar_alumnos = true;
    $permiso->crear_centro = true;
    $permiso->listar_centros = true;
    $permiso->crear_profesor = true;
    $permiso->listar_profesores = true;
    $permiso->crear_usuario = true;
    $permiso->listar_usuarios = true;
    $permiso->usuario_alta=null;
    $permiso->usuario_modificacion=null;
    $permiso->activo = true;
    $permiso->fecha_alta = new \DateTime();
    $permiso->fecha_modificacion = new \DateTime();
    R::store($permiso);
    return $permiso;
}

function crearUsuario($permiso, $centro) {
    $usuario = R::dispense("usuario");
    $usuario->permiso = $permiso;
    $usuario->centro = $centro;
    $usuario->nombre = "Roberto Leroy";
    $usuario->apellido_paterno = "Monroy";
    $usuario->apellido_materno = "Ruiz";
    $usuario->multicentro=true;
    $usuario->activo = True;
    $usuario->fecha_alta = new \DateTime();
    $usuario->fecha_modificacion = new \DateTime();
    R::store($usuario);
    return $usuario;
}

function crearUsuario2($permiso, $centro,$usuario1) {
    $usuario = R::dispense("usuario");
    $usuario->permiso = $permiso;
    $usuario->centro = $centro;
    $usuario->nombre = "Admin";
    $usuario->apellido_paterno = "1";
    $usuario->apellido_materno = "2";
    $usuario->multicentro=true;
    $usuario->activo = True;
    $usuario->fecha_alta = new \DateTime();
    $usuario->fecha_modificacion = new \DateTime();
    $usuario->usuario_alta=$usuario1;
    $usuario->usuario_modificacion=$usuario1;
    R::store($usuario);
    return $usuario;
}

function crearAlumno($usuario, $centro) {
    $alumno = R::dispense("alumno");
    $alumno->nombre = "Jose Alejandro";
    $alumno->apellido_paterno = "Salvador";
    $alumno->apellido_materno = "Ruiz";
    $alumno->email = "escuelaale5@gmail.com";
    $alumno->direccion = "Don pascuale Mz.1 Lt.22 Miguel Hidalgo Tláhuac";
    $alumno->telefono_celular = "5519505358";
    $alumno->telefono_particular = "";
    $alumno->fecha_nacimiento = new \DateTime("2004-06-05");
    $alumno->activo = True;
    $alumno->fecha_alta = new \DateTime();
    $alumno->fecha_modificacion = new \DateTime();
    $alumno->usuario_alta = $usuario;
    $alumno->usuario_modificacion = $usuario;
    $alumno->centro = $centro;
    R::store($alumno);
    return $alumno;
}

function crearProfesor($usuario, $centro) {
    $profesor = R::dispense("profesor");
    $profesor->nombre = "Guadalupe";
    $profesor->apellido_paterno = "Ruiz";
    $profesor->apellido_materno = "Ayala";
    $profesor->curp = "RUAG700519MDFNZB02";
    $profesor->telefono_celular = "5519507030";
    $profesor->telefono_particular = "21605772";
    $profesor->email = "erodanzastudio@gmail.com";
    $profesor->fecha_nacimiento = new \DateTime();
    $profesor->direccion = "Don pascuale Mz. 1 Lt. 22 miguel hidalgo tlahuac";
    $profesor->activo = True;
    $profesor->fecha_alta = new \DateTime();
    $profesor->fecha_modificacion = new \DateTime();
    $profesor->usuario_alta = $usuario;
    $profesor->usuario_modificacion = $usuario;
    $profesor->centro = $centro;
    R::store($profesor);
    return $profesor;
}

function crearCredencialUsuarioSistema($usuario) {
    $credencial = R::dispense("credencial");
    $credencial->usuario = $usuario;
    $credencial->username = "tecno";
    $credencial->pwd = "123";
    $credencial->activo = true;
    $credencial->fecha_alta = new \DateTime();
    $credencial->fecha_modificacion = new \DateTime();
    $credencial->access_key = md5("hola mundo...");
    $credencial->fecha_expiracion = new \DateTime();
    R::store($credencial);
    return $credencial;
}

function crearCrecencialProfesor($profesor) {
    $credencial = R::dispense("credencial");
    $credencial->profesor = $profesor;
    $credencial->username = "profesor";
    $credencial->pwd = "123";
    $credencial->activo = true;
    $credencial->fecha_alta = new \DateTime();
    $credencial->fecha_modificacion = new \DateTime();
    $credencial->access_key = md5("hola mundo...");
    $credencial->fecha_expiracion = new \DateTime();
    R::store($credencial);
    return $credencial;
}

function crearCredencialAlumno($alumno) {
    $credencial = R::dispense("credencial");
    $credencial->alumno = $alumno;
    $credencial->username = "alumno";
    $credencial->pwd = "123";
    $credencial->activo = true;
    $credencial->fecha_alta = new \DateTime();
    $credencial->fecha_modificacion = new \DateTime();
    $credencial->access_key = md5("hola mundo...");
    $credencial->fecha_expiracion = new \DateTime();
    R::store($credencial);
    return $credencial;
}

function crearActividad($usuario, $centro) {
    $actividad = R::dispense("actividad");
    $actividad->nombre = "Gimnasia Ritmi-Reductiva";
    $actividad->precio_mensual = 300.00;
    $actividad->precio_unitario = 20;
    $actividad->edad_minima = 10;
    $actividad->edad_maxima = 100;
    $actividad->activo = true;
    $actividad->fecha_alta = new \DateTime();
    $actividad->fecha_modificacion = new \DateTime();
    $actividad->usuario_alta = $usuario;
    $actividad->usuario_modificacion = $usuario;
    $actividad->centro = $centro;
    R::store($actividad);
    return $actividad;
}

function crearActividadHorario($actividad, $usuario, $centro) {
    $actividadHorarioArreglo = array();
    $actividadHorario = R::dispense("actividadhorario");
    //dia lunes
    $actividadHorario->actividad = $actividad;
    $actividadHorario->dia_semana = 1;
    $actividadHorario->hora_inicio = "20:00";
    $actividadHorario->hora_fin = "21:00";
    $actividadHorario->activo = true;
    $actividadHorario->fecha_alta = new \DateTime();
    $actividadHorario->fecha_modificacion = new \DateTime();
    $actividadHorario->usuario_alta = $usuario;
    $actividadHorario->usuario_modificacion = $usuario;
    $actividadHorario->centro = $centro;
    R::store($actividadHorario);
    $actividadHorarioArreglo[] = $actividadHorario;
    //dia miercoles
    $actividadHorario->actividad = $actividad;
    $actividadHorario->dia_semana = 3;
    $actividadHorario->hora_inicio = "20:00";
    $actividadHorario->hora_fin = "21:00";
    $actividadHorario->activo = true;
    $actividadHorario->fecha_alta = new \DateTime();
    $actividadHorario->fecha_modificacion = new \DateTime();
    $actividadHorario->usuario_alta = $usuario;
    $actividadHorario->usuario_modificacion = $usuario;
    $actividadHorario->centro = $centro;
    R::store($actividadHorario);
    $actividadHorarioArreglo[] = $actividadHorario;
    //dia viernes
    $actividadHorario->actividad = $actividad;
    $actividadHorario->dia_semana = 5;
    $actividadHorario->hora_inicio = "20:00";
    $actividadHorario->hora_fin = "21:00";
    $actividadHorario->activo = true;
    $actividadHorario->fecha_alta = new \DateTime();
    $actividadHorario->fecha_modificacion = new \DateTime();
    $actividadHorario->usuario_alta = $usuario;
    $actividadHorario->usuario_modificacion = $usuario;
    $actividadHorario->centro = $centro;
    R::store($actividadHorario);
    $actividadHorarioArreglo[] = $actividadHorario;
    return $actividadHorarioArreglo;
}

function crearActividadProfesor($profesor, $actividad, $usuario, $centro) {
    $actividadProfesor = R::dispense("actividadprofesor");
    $actividadProfesor->actividad = $actividad;
    $actividadProfesor->profesor = $profesor;
    $actividadProfesor->activo = true;
    $actividadProfesor->fecha_alta = new \DateTime();
    $actividadProfesor->fecha_modificacion = new \DateTime();
    $actividadProfesor->usuario_alta = $usuario;
    $actividadProfesor->usuario_modificacion = $usuario;
    $actividadProfesor->centro = $centro;
    R::store($actividadProfesor);
    return $actividadProfesor;
}

function crearClaseAlumno($actividad, $usuario, $alumno, $profesor, $centro) {
    $claseAlumno = R::dispense("clasealumno");
    $claseAlumno->actividad = $actividad;
    $claseAlumno->alumno = $alumno;
    $claseAlumno->fecha_clase = new \DateTime();
    $claseAlumno->activo = true;
    $claseAlumno->fecha_alta = new \DateTime();
    $claseAlumno->fecha_modificacion = new \DateTime();
    $claseAlumno->usuario_alta = $usuario;
    $claseAlumno->usuario_modificacion = $usuario;
    $claseAlumno->profesor = $profesor;
    $claseAlumno->centro = $centro;
    R::store($claseAlumno);
    return $claseAlumno;
}

function crearMedidasAlumno($claseAlumno, $usuario) {
    $medidasAlumno = R::dispense("medidasalumno");
    $medidasAlumno->busto = 123.02;
    $medidasAlumno->cintura = 123.02;
    $medidasAlumno->cadera = 200.50;
    $medidasAlumno->abdomen = 123.05;
    $medidasAlumno->ancho_espalda = 123.02;
    $medidasAlumno->linea_brasier = 129.02;
    $medidasAlumno->peso = 300.15;
    $medidasAlumno->clasealumno = $claseAlumno;
    $medidasAlumno->activo = true;
    $medidasAlumno->fecha_alta = new \DateTime();
    $medidasAlumno->fecha_modificacion = new \DateTime();
    $medidasAlumno->usuario_alta = $usuario;
    $medidasAlumno->usuario_modificacion = $usuario;
    R::store($medidasAlumno);
    return $medidasAlumno;
}
