## Welcome to the Roberto Leroy's portfolio
<br/><br/>
It contains some fragments of my diferents projects, which are grouped by:<br/><br/>

**AngularJS:** Projects using AngularJS in the frontend, the way to implement this frontend framework.<br/>
**C:** Some projects where the control in low level hardware devices is needed for example Finger print.<br/>
**Java:** Contains an Agent-PCWatcher, a daemon implementation java-based that check the status of each computer in the centers.<br/>
**NodeJS:** Contains the project hugs, an application like uber for people relations, in which I used NodeJS, Mongoose, ExpressJS and Javascript.<br/>
**PHP:** Contains two projects, the first is a demo that shows the way to program using Symfony2 named Control Escolar, it's a symfony bundle, contains, ORM mapping, Business layer, Rest controllers and Repositories to access to the database, the second is a full Microframework Project that creates templates for printing tickets in a thermal printer, it was make using php7 and SlimPHP 3.5 and I used a Table Gateway named RedBeanPHP (an ORM that doesn't need configuration), this Framework supports query params in the Get rest services.<br/>
**Python:** The "Control Escolar" project needs a way to synchronize the data between all the centers, that's why we used RabbitMQ (Message Queue) and Celery (library to connect python to RabbitMQ), for that I created a RabbitMQ task in python.<br/>
**Scripting:** Sometimes I need to find a way to an automate repetitive task for example to create the database, create datafixtures for a new database, minified html and js to conserve low brandwidth.They were created in PHP and Bash.<br/>