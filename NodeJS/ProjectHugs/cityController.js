var express = require("express");
var router = express.Router();
var cityService = new (require ("../services/cityService"))();
var cityModel = require('../models/cityModel');
//direcciones parciales
router.get('/',getCities);
router.get('/:id',getCityById);
router.post('/',postCity);
router.put('/:id',putCity);
module.exports = router;
function getCities(req, res){
    var commonHelper = new (require("../helpers/commonHelper"))(res);
    console.log('listado de ciudades...');
    cityService.getCities(function(cities){
        commonHelper.buildRespuesta('get',cities,commonHelper.CODES.OK);
    },function(error){
        commonHelper.buldRespuesta('get',error,commonHelper.CODES.BAD_REQUEST);
    });
    
}
function getCityById(req, res){
    var commonHelper = new (require("../helpers/commonHelper"))(res);
    console.log('obtener ciudad por id...');
    cityService.getCityById(req.params.id,function(city){
        commonHelper.buildRespuesta('get',city,commonHelper.CODES.OK);
    },function(error){
        commonHelper.buldRespuesta('get',error,commonHelper.CODES.BAD_REQUEST);
    });
}
function postCity(req, res){
    var commonHelper = new (require("../helpers/commonHelper"))(res);
    console.log('creacion de varias ciudades...');
    console.log(req.body);
    cityService.postCity(req.body,function(cities){
        commonHelper.buildRespuesta('post',cities,commonHelper.CODES.OK);
    },function(error){
        commonHelper.buldRespuesta('post',error,commonHelper.CODES.BAD_REQUEST);
    });
}
function putCity(req, res){
    var commonHelper = new (require("../helpers/commonHelper"))(res);
    console.log('modificación de una ciudad...');
    console.log(req.body);
    console.log(req.params);
    cityService.putCity(req.params.id,req.body,function(city){
        commonHelper.buildRespuesta('put',city,commonHelper.CODES.OK);
    },function(error){
        commonHelper.buldRespuesta('put',error,commonHelper.CODES.BAD_REQUEST);
    });    
}
