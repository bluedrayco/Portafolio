var commonHelper = function(response){
    this.response = response;
    this.CODES = {
        OK                            : 200,
        CREATED                       : 201,
        ACCEPTED                      : 202,
        PARTIAL_INFORMATION           : 203,
        NO_RESPONSE                   : 204,
        BAD_REQUEST                   : 400,
        UNAUTHORIZED                  : 401,
        PAYMENT_REQUIRED              : 402,
        FORBIDDEN                     : 403,
        NOT_FOUND                     : 404,
        INTERNAL_ERROR                : 500,
        NOT_IMPLEMENTED               : 501,
        SERVICE_TEMPORARLY_OVERLOADED : 502,
        
    };
    this.MESSAGES ={
        get     : ['El recurso fue listado exitosamente','Hubo un problema al listar el recurso'],
        post    : ['El recurso fue creado exitosamente','Hubo un problema al listar el recurso'],
        put     : ['El recurso fue actualizado exitosamente','Hubo un problema al actualizar el recurso'],
        delete  : ['El recurso fue eliminado exitosamente','Hubo un problema al eliminar el recurso']
    }
}
commonHelper.prototype.buildRespuesta = function(message,data){
    
    var cod = arguments.length == 3?arguments[2]:404;
    var i= cod>=200 && cod<400 ? 0:1;
    var msg = '';
    switch(message){
        case 'get':
            msg = this.MESSAGES.get[i];
        break;
        case 'post':
            msg = this.MESSAGES.post[i];
        break;
        case 'put':
            msg = this.MESSAGES.put[i];
        break;
        case 'delete':
            msg = this.MESSAGES.delete[i];
        break;
        
        default:
            msg = message;
    } 
    var structure = {message : msg, data: data};
    this.response.status(cod);
    this.response.json(structure);
}
commonHelper.prototype.verifyParams = function(object,params){
    
}
commonHelper.prototype.getBodyParams = function(){
    
    
}
module.exports = commonHelper;
