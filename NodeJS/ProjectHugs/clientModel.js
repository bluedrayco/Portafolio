var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var cityModel = require('./cityModel');
var creditCardModel = require('./creditCardModel');
var clientSchema = new Schema({
    name         : String,
    city         : cityModel,
    age          : Number,
    credit_cards : [creditCardModel],
    genred       : String,
    photo        : String,
    skills       : [String],
    hobbies      : [String],
    active       : Boolean
});
module.exports = mongoose.model('Client',clientSchema);
