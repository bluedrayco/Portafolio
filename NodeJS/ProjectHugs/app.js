/**
 * Module dependencies.
 */
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1/hugs');
var app = express();
//se exponen rutas
//module.exports.routes = app;
//se exponen todos los recursos que tiene el módulo
module.exports= {
    routes            : app,
    services          : {
        
        cityService   : require('./services/cityService'),
        userService   : require('./services/userService')
        
    },
    models       : {
        
        cityModel     : require('./models/cityModel')
        
    },
    middlewares  : {
        
    },
    helpers      : {
        
        commonHelper  : require('./helpers/commonHelper')
        
    }
};
//configuracion de la salida/entrada de nuestra api rest
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//direcciones recursos
var userController    = require('./routes/userController');
var commentController = require('./routes/commentController');
var cityController    = require('./routes/cityController'); 
// Middleware a nivel módulo
app.use(function(req, res, next){
    console.log('middleware a nivel componente '+req.method+' url: '+req.url);
    next();
});
app.use('/user',userController);
app.use('/comment',commentController);
app.use('/city',cityController);
