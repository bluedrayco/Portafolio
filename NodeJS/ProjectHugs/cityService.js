var extend = require('extend');
var cityModel = require('../models/cityModel');
var cityService = function () {
}
cityService.prototype.getCities = function (good, bad) {
    cityModel.find({}, function (err, cities) {
        if (err) {
            bad(err);
        }
        good(cities);
    });
}
cityService.prototype.getCityById = function (id,good,bad) {
    cityModel.find({_id:id}, function (err, city) {
        if (err) {
            bad(err);
        }
        good(city);
    });
}
cityService.prototype.putCity = function(id,cityNew,good,bad){
    console.log("entramos al put....");
    
    cityModel.findOneAndUpdate({_id:id},cityNew,function(err,city){
        if(err){
            bad(err);
        }else{
            cityService.prototype.getCityById(id,good,bad);
        }
    });
}
cityService.prototype.postCity = function (cities, good, bad) {
    console.log("entramos al rest....");
    console.log(cities);
    var cityArray = new Array();
    if (Object.prototype.toString.call( cities ) === '[object Array]') {
        cities.forEach(function (item) {
            var city = new cityModel();
            city.name = item.name;
            city.state = item.state;
            city.country = item.country;
            cityArray.push(city);
        });
    } else {
        console.log("entre al else...");
        var city = new cityModel();
        city.name = cities.name;
        city.state = cities.state;
        city.country = cities.country;
        cityArray.push(city);
    }
    var total = cityArray.length, result = [];
    console.log("total: " + total);
    function saveAll() {
        console.log('entre a save all');
        var doc = cityArray.pop();
        doc.save(function (err, saved) {
            if (err)
                bad(err);//handle error
            result.push(saved);
            if (--total)
                saveAll();
            else // all saved here
                good(result);
        })
    }
    saveAll();
}
module.exports = cityService;
