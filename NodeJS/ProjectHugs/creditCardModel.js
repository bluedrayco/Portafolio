var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var creditCardSchema = new Schema({
    bank           : String,
    account_number : String,
    user           : String,
    password       : String
});
module.exports = mongoose.model('CreditCard',creditCardSchema);
