## Proyect "Hugs"

This project is about to implement the same solution like Uber application for people whom like to advice people and like to listen them.<br/>

The project was implemented in NodeJS using ExpressJS MicroFramework and Mongoose for the MongoDB Database.<br/><br/>

Some files are:<br/><br/>

**app.js:** This is the main file, export all the routes, services, middlewares and helpers for being used in others components, this file ups a server and gets dependencies using SINOPIA (a private npm repository server).<br/>
**cityModel.js, creditCardModel.js and clientModel.js:** These are entities that are mapped to mongo db registers, the model city is a catalog for all the cities in the around the world, the credit card model save the client credit cards and the client model save the basic information about the client such as his name, city, age, credit cards, genred, skills and hobbies, these models were exported to the app.js structure.<br/>
**cityController.js:** It contains the routing of each REST service and the logic layer (without access to the database) such as getCities, getCity by an id, create a city resource, update a city resource, etc.<br/>
**commonHelper.js:** This file is a prototype of a simple common library for build a correct response, verify params and others.<br/>
**cityService.js:** Contains the access to the Mongo database, create, update, get, etc.<br/>